import copy

from greg.models import Identity
from ..iga import IgaPerson
from ..uib import UibRiScim

UIB_SEARCH = {
    "schemas": ["urn:ietf:params:scim:api:messages:2.0:ListResponse"],
    "totalResults": 1,
    "startIndex": 1,
    "itemsPerPage": 1,
    "Resources": [
        {
            "schemas": [
                "urn:ietf:params:scim:schemas:core:2.0:User",
                "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
                "no:edu:scim:user",
            ],
            "id": "",
            "externalId": "",
            "userName": "",
            "displayName": "Ruth Pedersen",
            "profileUrl": "",
            "title": "",
            "userType": "Employee",
            "preferredLanguage": None,
            "active": True,
            "emails": [{"value": "", "type": "work"}],
            "phoneNumbers": [],
            "roles": [{"value": "iam:employee"}, {"value": "iam:operation"}],
            "no:edu:scim:user": {
                "accountType": "primary",
                "employeeNumber": "102160",
                "eduPersonPrincipalName": "ruped001@uib.no",
                "userPrincipalName": "Ruth.Pedersen@uibtest.no",
            },
        }
    ],
}


def test_uib_search(requests_mock):
    """Regular search works as expected"""
    uib_search_wname = copy.deepcopy(UIB_SEARCH)
    uib_search_wname["Resources"][0]["name"] = {
        "formatted": "Ruth Pedersen",
        "givenName": "Ruth",
        "familyName": "Pedersen",
    }

    requests_mock.get(
        "https://example.com/scim/Users?norEduPersonNIN=1",
        json=uib_search_wname,
    )

    client = UibRiScim({"url": "https://example.com/scim", "headers": {"bar": "baz"}})
    assert not client.extid_search(Identity.IdentityType.PASSPORT_NUMBER, "1")
    assert client.extid_search(
        Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, "1"
    ) == IgaPerson(first="Ruth", last="Pedersen")


def test_uib_search_nameless(requests_mock):
    """Nameless people can be also be found"""
    requests_mock.get(
        "https://example.com/scim/Users?norEduPersonNIN=1",
        json=UIB_SEARCH,
    )

    client = UibRiScim({"url": "https://example.com/scim", "headers": {"bar": "baz"}})
    assert not client.extid_search(Identity.IdentityType.PASSPORT_NUMBER, "1")
    assert client.extid_search(
        Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, "1"
    ) == IgaPerson(first=None, last=None)
