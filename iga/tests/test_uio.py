from greg.models import Identity
from ..iga import IgaPerson
from ..uio import UioCerebrum


def test_uio_search_hit(requests_mock):
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?id_type=norwegianNationalId&external_id=123",
        json={
            "external_ids": [
                {
                    "person_id": 1,
                    "source_system": "dfo_sap",
                    "external_id": "123",
                    "id_type": "NO_BIRTHNO",
                }
            ]
        },
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?id_type=passportNumber&external_id=12345",
        json={
            "external_ids": [
                {
                    "person_id": 1,
                    "source_system": "dfo_sap",
                    "external_id": "12345",
                    "id_type": "PASSNR",
                }
            ]
        },
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1",
        json={
            "contexts": ["string"],
            "created_at": "2022-02-17T10:17:31.305Z",
            "href": "http://example.com/cerebrum/v1/search/persons/1",
            "names": [
                {"source_system": "dfo_sap", "variant": "FIRST", "name": "Ola"},
                {"source_system": "dfo_sap", "variant": "LAST", "name": "Nordmann"},
            ],
            "birth_date": "2022-02-17T10:17:31.305Z",
            "id": 1,
        },
    )

    client = UioCerebrum(
        {"url": "http://example.com/cerebrum/", "headers": {"bar": "baz"}}
    )
    resp = client.extid_search(Identity.IdentityType.PASSPORT_NUMBER, "12345")
    assert resp == IgaPerson(first="Ola", last="Nordmann")

    resp = client.extid_search(
        Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, "123"
    )
    assert resp == IgaPerson(first="Ola", last="Nordmann")


def test_uio_search_miss(requests_mock):
    """Verify no matches returns empty list"""
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?id_type=norwegianNationalId&external_id=123",
        json={"external_ids": []},
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?id_type=passportNumber&external_id=12345",
        json={"external_ids": []},
    )

    client = UioCerebrum(
        {"url": "http://example.com/cerebrum/", "headers": {"bar": "baz"}}
    )
    resp = client.extid_search(Identity.IdentityType.PASSPORT_NUMBER, "12345")
    assert not resp

    resp = client.extid_search(
        Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, "123"
    )
    assert not resp
