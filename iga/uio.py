from typing import Optional
import structlog

from cerebrum_client import CerebrumClient

from greg.models import Identity
from .iga import IgaException, IgaImplementation, IgaPerson

logger = structlog.getLogger(__name__)


class UioCerebrum(IgaImplementation):
    def __init__(self, config) -> None:
        self.client = CerebrumClient(**config)
        self.idtype2cerebrum = {
            Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER: "norwegianNationalId",
            Identity.IdentityType.PASSPORT_NUMBER: "passportNumber",
        }

    def extid_search(
        self, id_type: Identity.IdentityType, extid: str
    ) -> Optional[IgaPerson]:
        idtype = self.idtype2cerebrum.get(id_type, None)
        search = self.client.search_person_external_ids(
            id_type=idtype, external_id=extid
        )
        try:
            persons = [self.client.get_person(i.person_id) for i in search]
        except AttributeError as ae:
            logger.exception("bad_cerebrum_response", external_id=extid)
            raise IgaException("Failed to fetch IGA info") from ae

        if persons:
            for pers in persons:
                first = None
                last = None
                for pname in pers.names:
                    if pname.variant == "FIRST":
                        first = pname.name
                    elif pname.variant == "LAST":
                        last = pname.name
                    if first and last:
                        return IgaPerson(first=first, last=last)
        return None
