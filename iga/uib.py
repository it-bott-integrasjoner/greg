from typing import Optional

import structlog
from abstract_iga_client.iga_scim_ri_uib import IgaClient, IgaAbstract, UserListResponse
from scim2_client import user_query_request

from greg.models import Identity
from .iga import IgaImplementation, IgaPerson

logger = structlog.getLogger(__name__)


class UibRiScim(IgaImplementation):
    def __init__(self, config) -> None:
        self.client: IgaClient = IgaAbstract.get_iga_client(
            "scim-ri@uib",
            endpoints={"base_url": config["url"]},
            headers=config["headers"],
        )
        self.idtype_methodmap = {
            Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER: lambda nor_edu_person_nin: self.client.client(
                user_query_request(
                    extra_parameters={"norEduPersonNIN": nor_edu_person_nin}
                )
            ).payload,
            Identity.IdentityType.PASSPORT_NUMBER: None,  # not supported by uib scim
        }

    def extid_search(
        self, id_type: Identity.IdentityType, extid: str
    ) -> Optional[IgaPerson]:
        search = self.idtype_methodmap.get(id_type)

        if not search:
            return None
        user: UserListResponse = search(extid)
        if len(user.resources) == 0:
            return None

        if len(user.resources) > 1:
            # Not expected that this should happen
            logger.warning(
                "Multiple hits found when searching in RI SCIM for {}".format(extid)
            )

        # Return the first user found
        return IgaPerson(
            first=user.resources[0].name.given_name
            if user.resources[0].name and user.resources[0].name.given_name
            else None,
            last=user.resources[0].name.family_name
            if user.resources[0].name and user.resources[0].name.family_name
            else None,
        )
