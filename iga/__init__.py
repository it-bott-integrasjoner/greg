from .uib import UibRiScim
from .uio import UioCerebrum


def get_iga_client(instance, config):
    if instance == "uib":
        return UibRiScim(config)
    return UioCerebrum(config)
