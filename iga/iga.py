from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional

from greg.models import Identity


@dataclass
class IgaPerson:
    """
    Simple Person representation

    Names are not required in sebra, thus we cannot assume everyone has them.
    """

    first: Optional[str]
    last: Optional[str]

    def dict(self):
        return {
            "first": self.first,
            "last": self.last,
        }


class IgaImplementation(ABC):
    @abstractmethod
    def extid_search(
        self, id_type: Identity.IdentityType, extid: str
    ) -> Optional[IgaPerson]:
        pass


class IgaException(Exception):
    pass
