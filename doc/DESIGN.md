# Designdokument for gjesteregistreringsløsning


## Innledning
Gjesteregistreringsløsningen (GREG, Guest REGistation) tilbyr et grensesnitt der institusjoner kan legge inn data
om personer som trenger gjestekontoer for å få tilgang til IT-tjenester. Tjenesten kan integrere med institusjonens IAM (Identity and access management)
løsning for overføring av data til denne.

Hver institusjon som ønsker å bruke GREG vil ha sin egen instans som bare inneholder 
data om brukere ved den institusjonen.

GREG kan på lang sikt komme til å bli erstattet av gjestehåndtering i et personalsystem.

### Brukerroller
Det er to brukerkategorier i tjenesten:
- Sponsorer: Dette er brukere ved institusjonene som kan invitere gjestebrukere
- Gjester: Dette er personer lagt inn av sponsorer og som ønsker tilgang til IT-systemene ved institusjonen

### Kontakt
- Systemeier: *TODO*

## Redegjørelse
Dagens løsning i DFØ-SAP for å registrere gjestebrukere dekker ikke behovene til UiO og UiB, og det ble avgjort å utvikle
en alternativ løsning for å få et system mer i tråd med behovene på plass innen kort tid.

Systemet består av to deler, en kjerne og en selvbetjeningsløsning, og kommunikasjon mellom disse skjer gjennom et 
REST API kjernen gjør tilgjengelig.

Ved å splitte funksjonaliteten på denne måten er teknologivalg gjort i selvbetjeningsløsningen, som er frontenden 
sluttbrukerne ser, uavhengig av kjernen. Så utviklerne kan på hver sine side, frontend og backend, velge teknologier de er komfortable med.


### Designvalg

#### Kjerne
Kjernen er utviklet med Django, et populært webrammeverk i Python som har mange tilleggsmoduler 
som gjør utviklingen enklere. USIT har kompetanse på Django, og det er allerede brukt i Kada og Mreg.

#### Selvbetjeningsløsning
*TODO*

## Detaljbeskrivelse

### Teknisk formål
Kjernen har som hovedoppgave å lagre data om gjestene, sende notifikasjoner om relevante endringer
via en meldingskø og tilby et REST API for å lagre og hente ut data.

Endringer som det publiseres meldinger for inkluderer opprettelse av nye gjester i kjernen, endring gjort på 
eksisterende gjester og sletting av gjester.

IAM-systemet ved en institusjon kan lytte på meldingskøen kjernen publiserer til for å bli gjort oppmerksom 
på endringer for gjester. Informasjonen i meldingene kan brukes til å gjøre oppslag i API'et.

Kjernen vil også periodisk slette persondata for gjester som ikke lenger er aktive.

Adgangskontroll til API'et skjer gjennom Gravitee.

Selvbetjeningsløsningen lar sponsorer opprette gjester og lar gjestene legge inn identitetsinformasjon. Det er 
selvtjeningsløsningen som har ansvaret for flyten gjennom registreringsforløpet, dvs. den sender for eksempel ut e-poster 
til gjestene om at de må legge inn informasjon og til sponsorene om at de må godkjenne gjester som har lagt inn informasjon.

Selvbetjeningsløsningen bruker kjernen hovedsaklig for å lagre informasjon.

### Høynivådesign
![Overview](overview_components.png)

### Lavnivådesign

#### Kjerne
For å tilby et REST API brukes "Django REST framework". 
En [OpenAPI](https://swagger.io/specification/) spesifikasjon av API'et er tilgjengelig 
(generert ved hjelp av [drf-spectacular](https://github.com/tfranzel/drf-spectacular)) 

Sending av meldinger til RabbitMQ skjer ved hjelp av [pika-context-manager](https://git.app.uib.no/it-bott-integrasjoner/pika-context-manager) biblioteket.

Tilgangskontroll til API'et skjer via Gravitee. Brukerne av API'et er selvbetjeningsløsningen og 
institusjonens IAM-system.

### Selvbetjeningsløsning
*TODO*

## Avhengigheter
- RabbitMQ: Brukes for å sende meldinger til andre systemer
- Harbor: Lagring av container images
- Openshift: Applikasjonsplattform for å kjøre containere
- Gravitee: Tilgang til API styres gjennom Gravitee
- Institusjonens IAM system.

## Relaterte systemer
- Cerebrum: Brukeradministrasjonssystem ved UiO. Henter data fra GREG og oppretter brukerkontoer. Sender data tilbake til Greg om hvilke gjester som har fått brukerkontoer.
- RI: Brukeradministrasjonssystem ved UiB. Opererer etter samme mønster integrasjonen mellom Cerebrum og GREG.

## Lenker og referanser
Koden til GREG finnes på UiB sin Gitlab-instans [her](https://git.app.uib.no/it-bott-integrasjoner/greg).