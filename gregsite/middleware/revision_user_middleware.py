from functools import wraps

# from reversion.views import _request_creates_revision, create_revision
from reversion.revisions import (
    create_revision as create_revision_base,
    set_user,
    get_user,
    set_comment,
)


class RevisionUserMiddleware:

    """Wraps the entire request in a revision."""

    manage_manually = False

    using = None

    atomic = True

    def __init__(self, get_response):
        self.get_response = create_revision(
            manage_manually=self.manage_manually,
            using=self.using,
            atomic=self.atomic,
            request_creates_revision=self.request_creates_revision,
        )(get_response)

    def request_creates_revision(self, request):
        return _request_creates_revision(request)

    def __call__(self, request):
        return self.get_response(request)


def _request_creates_revision(request):
    return request.method not in ("OPTIONS", "GET", "HEAD")


def _set_user_from_request(request):
    if (
        getattr(request, "user", None)
        and request.user.is_authenticated
        and get_user() is None
    ):
        set_user(request.user)


def _set_comment_to_external_user_from_request(request):
    if request.headers.get("External-User") is not None:
        set_comment("User: " + request.headers["External-User"])


def create_revision(
    manage_manually=False, using=None, atomic=True, request_creates_revision=None
):
    """
    View decorator that wraps the request in a revision.

    The revision will have it's user set from the request automatically.
    """
    request_creates_revision = request_creates_revision or _request_creates_revision

    def decorator(func):
        @wraps(func)
        def do_revision_view(request, *args, **kwargs):
            if request_creates_revision(request):
                with create_revision_base(
                    manage_manually=manage_manually, using=using, atomic=atomic
                ):
                    response = func(request, *args, **kwargs)
                    # Otherwise, we're good.
                    _set_user_from_request(request)
                    _set_comment_to_external_user_from_request(request)
                    return response
            return func(request, *args, **kwargs)

        return do_revision_view

    return decorator
