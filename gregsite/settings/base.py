"""
Django settings for greg project.

Generated by 'django-admin startproject' using Django 3.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from pathlib import Path
from typing import List

import structlog

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-3zzk=de$-7=)rcnvwfi=05x)84d#s572e8fdq9ewt4y1cg2jy-"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS: List[str] = []


# Application definition

INSTALLED_APPS = [
    "whitenoise.runserver_nostatic",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "rest_framework.authtoken",
    "reversion",
    "drf_spectacular",
    "django_extensions",
    "django_filters",
    "mozilla_django_oidc",
    "django_q",
    "greg",
    "gregui",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django_structlog.middlewares.RequestMiddleware",
    "gregsite.middleware.revision_user_middleware.RevisionUserMiddleware",
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",  # default
    "gregui.authentication.auth_backends.GregOIDCBackend",
]

SESSION_COOKIE_AGE = 24 * 3600  # 24 hours

CSRF_COOKIE_SAMESITE = "Lax"
SESSION_COOKIE_SAMESITE = "Lax"
CSRF_COOKIE_HTTPONLY = False
SESSION_COOKIE_HTTPONLY = False

REST_FRAMEWORK = {
    "DEFAULT_VERSIONING_CLASS": "rest_framework.versioning.NamespaceVersioning",
    "DEFAULT_VERSION": "v1",
    "ALLOWED_VERSIONS": ("v1", "gregui-v1"),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.TokenAuthentication",
        "rest_framework.authentication.SessionAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAdminUser",),
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    # Rate limit settings of invite endpoint
    "DEFAULT_THROTTLE_CLASSES": [
        "rest_framework.throttling.AnonRateThrottle",
    ],
    "DEFAULT_THROTTLE_RATES": {
        "anon": "10/minute",
    },
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Greg",
    "DESCRIPTION": "Guest REGistration",
    # 'VERSION': '1.0.0',
    "PREPROCESSING_HOOKS": ["greg.api.api_doc_preprocessing.custom_preprocessing_hook"],
}

ROOT_URLCONF = "gregsite.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "gregsite.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Override these in dev.py
OIDC_RP_CLIENT_ID = ""
OIDC_RP_CLIENT_SECRET = ""

OIDC_RP_SIGN_ALGO = "RS256"
OIDC_RP_SCOPES = "email openid userid userid-feide userid-nin profile iss "
OIDC_OP_JWKS_ENDPOINT = "https://auth.dataporten.no/openid/jwks"
OIDC_OP_AUTHORIZATION_ENDPOINT = "https://auth.dataporten.no/oauth/authorization"
OIDC_OP_TOKEN_ENDPOINT = "https://auth.dataporten.no/oauth/token"
OIDC_OP_USER_ENDPOINT = "https://auth.dataporten.no/openid/userinfo"
OIDC_OP_FEIDE_EXTENDED_USER_ENDPOINT = "https://api.dataporten.no/userinfo/v1/userinfo"
OIDC_STORE_ID_TOKEN = True

ALLOW_LOGOUT_GET_METHOD = True
OIDC_END_SESSION_ENDPOINT = "https://auth.dataporten.no/openid/endsession"
OIDC_OP_LOGOUT_URL_METHOD = "gregui.authentication.auth_backends.provider_logout"

# Change these later
LOGIN_REDIRECT_URL = "http://localhost:3000/"
LOGOUT_REDIRECT_URL = "http://localhost:3000/"

# Extra config needed since mozilla-django-oidc does not comply with point 2. and 3.
# https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
OIDC_OP_ISSUER = "https://auth.dataporten.no"
OIDC_TRUSTED_AUDIENCES = []

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True
DATETIME_FORMAT = "c"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = "/staticfiles/"
STATIC_ROOT = BASE_DIR / "static"

# STATICFILES_DIRS = [BASE_DIR / ".." / "frontend" / "build"]

# STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


# Logging

# Log level for all loggers except Django
LOG_LEVEL = "INFO"
if DEBUG:
    LOG_LEVEL = "DEBUG"

# Override this to DEBUG locally for super verbose logging
DJANGO_LOG_LEVEL = "INFO"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "json_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.JSONRenderer(),
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "json_formatter",
        },
    },
    "loggers": {
        "django_structlog": {
            "handlers": ["console"],
            "level": DJANGO_LOG_LEVEL,
        },
        "gregui": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
        },
        "greg": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
        },
        "pika": {"handlers": ["console"], "level": "ERROR"},
    },
}

structlog.configure(
    processors=[
        structlog.stdlib.filter_by_level,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    context_class=structlog.threadlocal.wrap_dict(dict),
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)


# By default, the routing key format is "object_type.operation"
# Setting the prefix to "foo." will result in "foo.object_type.operation"
NOTIFICATION_ROUTING_KEY_PREFIX = ""
NOTIFICATION_PUBLISHER = {
    "mq": {
        "connection": {
            "hostname": "localhost",
            "port": 5672,
            "virtual_host": "/",
            "username": "guest",
            "password": "guest",
            "tls_on": False,
            "server_name_indication": True,
        },
        "exchange": "test",
    },
    "poll_interval": 5,
}

INSTANCE_NAME = "local"
# i.e. 'prod', 'test', ...
ENVIRONMENT = "unknown"
INTERNAL_RK_PREFIX = "no.{instance}.greg".format(instance=INSTANCE_NAME)

# No trailing slash
BASE_URL = "https://example.org"

FEIDE_SOURCE = "feide"
CEREBRUM_SOURCE = "cerebrum"

# The default duration for a new invitation link, in days
INVITATION_DURATION = 30

# Used by the OU import from orgreg to distinguish the OuIdentifiers from others
ORGREG_SOURCE = "orgreg"
ORGREG_NAME = "orgreg_id"

# Extra ids to be imported from orgreg. This also is used for fields
# wanted to show when listing units. List of dict with source/type or fieldname/subfield.
# Order is important where the first two fields will be shown in that order
# [{"fieldname": "shortname", "subfield": "nob"},{"source": "sapuio", "type": "legacy_stedkode"}]
ORGREG_EXTRA_IDS = []

# Acronyms to be imported as identifiers from orgreg. List of acronym values, supported ones are: nob, nno, eng
# Example list: ["nob"]
ORGREG_ACRONYMS = []

# Controls how often the orgreg ou import task should be scheduled if the task is
# scheduled by running the management command
ORGREG_SCHEDULE_TYPE = "D"

# Controls how often the sponsor import task should be scheduled if the task is
# scheduled by running the management command
SPONSOR_IMPORT_SCHEDULE_TYPE = "W"

# Controls how often the usernames import should be scheduled
USERNAME_SCHEDULE_ALL = "M"
USERNAME_SCHEDULE_WITHOUT_USERNAME = "H"
USERNAME_SCHEDULE_WITH_USERNAME = "D"

# List of stedkode for units where Sponsors are handled manually
CEREBRUM_MANUAL_SPONSOR_UNITS = []

# Settings related to Django-q used for scheduling tasks in the future.
# We use it to queue creation of Notification objects related to roles when start/end dates are in the future
Q_CLUSTER = {
    "name": "greg",
    "workers": 4,
    "timeout": 500,
    "retry": 600,
    "queue_limit": 50,
    "bulk": 10,
    "orm": "default",
    "max_attempts": 2,
}

# Number of days into the future to notify about ending roles. Used by the task that
# notifies sponsors about ending roles.
SPONSOR_NOTIFIER_LIMIT = 7
# Default schedule for the task notifying about ending roles. Used by the management
# command that triggers the task.
SPONSOR_NOTIFIER_SCHEDULE_TYPE = "W"

# Number of days into the future to notify about ending roles. Used by the task that
# notifies sponsors about ending roles.
GUEST_NOTIFIER_FIRST = 14
GUEST_NOTIFIER_SECOND = 4
# Default schedule for the task notifying about ending roles. Used by the management
# command that triggers the task.
GUEST_NOTIFIER_SCHEDULE_TYPE = "D"

# Source used when creating identities that do not have a clear source like
# feide/id-porten
DEFAULT_IDENTITY_SOURCE = "greg"

# Toggle to prevent verification of NIN from the sponsor frontend.
# Introduced to prevent using greg for takeover of existing accounts in IGA.
DISABLE_NIN_VERIFY = False

# Toggle if national id card number can be used as identification to verify guests.
ENABLE_NATIONAL_ID_CARD_NUMBER = False

# Iga client config used by identity checking view when checking iga
# Take care to also set the check parameter to True if you use this
IGA_CHECK = False
IGA_CLIENT = {
    "url": "http://example.com/cerebrum/",
    "headers": {"X-Gravitee-Api-Key": "<KEY>"},
}

ALLOW_SO_NUMBERS = False

# Which id-types need to be verified for a guest to appear under "Confirmed guests"
ALLOWED_VERIFIED_ID_TYPES = [
    "norwegian_national_id_number",
    "norwegian_national_id_card_number",
    "passport_number",
]

# Which units are excluded when sending notification emails on expiring roles (primary key)
EXCLUDED_UNITS = []

# Number of days after a role's end-date it will be deleted
ROLE_EXPIRY_DAYS = 1826  # 5 years = 1826.21 days

# Default schedule for the task deleting expired roles.
ROLE_REMOVAL_SCHEDULE_TYPE = "W"

# Default schedule for the task removing OUs from unaffiliated sponsors.
SPONSOR_OU_REMOVAL_SCHEDULE_TYPE = "W"

# Ansatt and student affiliation codes in cerebrum
OTHER_AFFILIATIONS = ["131", "129", "ANSATT", "STUDENT"]

# Lenght of other active guest role to not notify of expiring role
CUT_OFF = 60  # Two months

# Sources used to get greg person from cerebrum
GREG_SOURCE = "GREG"
CEREBRUM_GREG_ID_TYPE = "gregPersonId"
