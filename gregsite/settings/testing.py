from .dev import *

ENVIRONMENT = "unittest"

AUTHENTICATION_BACKENDS = [
    "gregui.authentication.auth_backends.DevBackend",  # Fake dev backend
    "django.contrib.auth.backends.ModelBackend",  # default
    "gregui.authentication.auth_backends.GregOIDCBackend",
]

OIDC_RP_CLIENT_ID = "lalalalala"
OIDC_RP_CLIENT_SECRET = "lalalalala"

CEREBRUM_CLIENT = {
    "url": "https://example.com/cerebrum/",
    "headers": {"X-Gravitee-Api-Key": "bar"},
}
CEREBRUM_HIERARCHICAL_ACCESS = True


ORGREG_CLIENT = {
    "endpoints": {"base_url": "https://example.com/orgreg/"},
    "headers": {"X-Gravitee-Api-Key": "foo"},
}
ORGREG_EXTRA_IDS = [
    {"fieldname": "short_name", "subfield": "nob"},
    {"type": "legacy_stedkode", "source": "sapuio"},
]
ORGREG_ACRONYMS = []
