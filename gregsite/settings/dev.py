from .base import *

# This is the default configuration file when running manage.py

ENVIRONMENT = "dev"

ALLOWED_HOSTS += ["localhost", "127.0.0.1", "0.0.0.0"]
CSRF_TRUSTED_ORIGINS = [
    "http://localhost:3000",
    "http://0.0.0.0:3000",
    "http://localhost:8000",
    "http://127.0.0.1:3000",
    "http://127.0.0.1:8000",
]

# EMAIL_HOST = "smtp.uio.no"
# EMAIL_PORT = "468"
# EMAIL_USE_SSL = True
# EMAIL_TIMEOUT = 2
# DEFAULT_FROM_EMAIL = "noreply@uio.no"
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

ORGREG_CLIENT = {
    "endpoints": {"base_url": "https://example.com/fake/"},
    "headers": {"X-Gravitee-Api-Key": "bar"},
}

CEREBRUM_CLIENT = {
    "url": "https://example.com/fake/",
    "headers": {"X-Gravitee-Api-Key": "bar"},
}
CEREBRUM_HIERARCHICAL_ACCESS = True


Q_CLUSTER.update(
    {
        "sync": True,
    }
)

AUTHENTICATION_BACKENDS = [
    "gregui.authentication.auth_backends.DevBackend",  # Fake dev backend
    "django.contrib.auth.backends.ModelBackend",  # default
    "gregui.authentication.auth_backends.GregOIDCBackend",
]

LOGIN_REDIRECT_URL = "http://localhost:3000/"
LOGOUT_REDIRECT_URL = "http://localhost:3000/"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "plain_console": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.dev.ConsoleRenderer(),
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "plain_console",
        },
    },
    "loggers": {
        "django_structlog": {
            "handlers": ["console"],
            "level": DJANGO_LOG_LEVEL,
        },
        "gregui": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
        },
        "greg": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
        },
        "pika": {"handlers": ["console"], "level": "ERROR"},
    },
}

# Disable throttling in development, uncomment CACHES to test
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.dummy.DummyCache",
    }
}


try:
    from .local import *
except ImportError:
    pass
