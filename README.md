# Greg

[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/greg/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/greg/-/commits/master)
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/greg/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/greg/-/commits/master)

Greg (Guest REGistration) provides a core API for keeping track of guests at an institution.

It is built on [Django](https://www.djangoproject.com/) and [REST framework](https://www.django-rest-framework.org/).

## Installation

[Poetry](https://python-poetry.org/docs/) is used for dependency management.

    # Make sure poetry is installed, see docs
    poetry shell        # Start a shell, creating a virtual environment.
    poetry install      # Install dependencies from lock file

## Configuration

Local configuration is read from `gregsite/settings/local.py`. Put secrets and local modifications here.

    # Make sure a local settings file exists
    $ touch gregsite/settings/local.py

`gregsite/settings/base.py` contains common configuration for all environments. Put defaults here.\
`gregsite/settings/dev.py` contains development configuration. It's used when running a development server using `python manage.py runserver`.\
`gregsite/settings/prod.py` contains production configuration. It's used when the application is initialized by an application server via `gregsite.wsgi` or `gregsite.asgi`.

Any settings in `local.py` override definitions in `base.py`, `dev.py` and `prod.py`.

### Login

Sponsors and Guests are expected to log in using Feide/ID-porten through Dataporten.
For development you can make a client at https://dashboard.dataporten.no/.
For production the institution's Feide admin must do it for you.

The following settings must be set:

- Client type: "Confidential"
- Callback url: http://localhost:8000/oidc/callback/ for dev or https://your.domain/oidc/callback for prod
- Scopes: email, openid, userid, userid-feide, userid-nin, profile, iss.
- Require user interaction box: Checked

Auth Providers depends on what you're doing. For development you want at least _Feide guest users_ or _Feide test users_ checked. For production you want _IDporten_ and whichever institution you're configuring.

Take care to also set the OIDC_RP_CLIENT_ID and OIDC_RP_CLIENT_SECRET values in the local.py settings file (if you're using the regular deployment this is done in a separate repository).

Note also that there are a series of other settings variables related to this in the base.py settings file, that will need other values in case you choose to use a different login provider than Dataporten.

## Development

    python manage.py migrate
    # python manage.py loaddata testdata
    # python manage.py createsuperuser
    python manage.py runserver

## API documentation

An OpenAPI schema is available at `/api/schema/`. Point your browser to `/api/schema/swagger-ui/` for an interactive Swagger UI.

Refer to [drf-spectacular](https://github.com/tfranzel/drf-spectacular/) for details about the schema generation.

## Sending notifications

    python manage.py notification_publisher

## Testing

Use pytest with the pytest-django library to run unit tests.

    pytest

There are two scripts for adding data to the database:

- greg/tests/populate_fixtures.py
- greg/tests/populate_database.py

where the former uses randomized data, and the latter uses specific data useful in combination with the frontend. See the respective files for how to use them.

## Themes

The application has three different themes: uio, uib and default. The default uses the palette found in Material UI,
while uio and uib sets a color scheme that matches the institution.

To set the theme used update REACT_APP_THEME in frontend/.env.

## Static type analysis

Use [mypy](http://mypy-lang.org/) to run static type checks using type hints.

    mypy .

## Container images

A `Containerfile` is provided for application deployment purposes. It is not designed to be used for development.

`update-harbor-image.sh` is a utility script for building and uploading a Docker image to our private image repository `harbor.uio.no`.

It expects you to be logged in:

    docker login harbor.uio.no

## Deployment

Deployment is done using the helm chart located at https://git.app.uib.no/it-bott-integrasjoner/greg-chart/.

After deployment the OrgReg import and sponsor notification tasks must be queued. This is done by running

```bash
python manage.py import_from_orgreg
python manage.py role_end_notifier
```

Sponsors can by imported from cerebrum (UiO/UiT/NTNU) by running

```bash
python manage.py import_sponsors_from_cerebrum
```

Otherwise, you will have to create Sponsor objects manually, taking care to link them to their respective units.
