import datetime
import time
from typing import Callable, Dict, Union

from django_structlog.signals import bind_extra_request_metadata
from django.db import models
from django.dispatch import receiver
from django_q.models import Schedule
from greg.models import (
    Person,
    Role,
    RoleType,
    Notification,
    Identity,
    Consent,
    ConsentType,
)
from greg.utils import date_to_datetime_midnight, str2date


@receiver(bind_extra_request_metadata)
def bind_user_invite_id(request, logger, **kwargs):
    if "invite_id" in request.session:
        logger.bind(invite_id=request.session.get("invite_id"))


SUPPORTED_MODELS = (
    Person,
    Role,
    RoleType,
    Identity,
    Consent,
)


@receiver(models.signals.pre_migrate)
def disconnect_notification_signals(*args, **kwargs):
    """
    Disconnect signals before migration.
    This ensures that we can migrate,
    as the signals are called when table is saved.
    """
    models.signals.pre_save.disconnect(dispatch_uid="add_changed_fields_callback")
    models.signals.post_save.disconnect(dispatch_uid="save_notification_callback")
    models.signals.post_delete.disconnect(dispatch_uid="delete_notification_callback")
    models.signals.m2m_changed.connect(
        receiver=m2m_changed_notification_callback,
        dispatch_uid="m2m_changed_notification_callback",
    )


@receiver(models.signals.post_migrate)
def connect_notification_signals(*args, **kwargs):
    """
    Add back signals after migration.
    """
    models.signals.pre_save.connect(
        receiver=add_changed_fields_callback,
        dispatch_uid="add_changed_fields_callback",
    )
    models.signals.post_save.connect(
        receiver=save_notification_callback,
        dispatch_uid="save_notification_callback",
    )
    models.signals.post_delete.connect(
        receiver=delete_notification_callback,
        dispatch_uid="delete_notification_callback",
    )


def _store_notification(identifier, object_type, operation, **kwargs):
    # logger.debug(
    #     '_store_notification identifier=%r, object_type=%r, operation=%r, kwargs=%r',
    #     identifier, object_type, operation, kwargs)
    return Notification.objects.create(
        identifier=identifier,
        object_type=object_type,
        operation=operation,
        issued_at=int(time.time()),
        meta=kwargs,
    )


def _queue_role_notification(
    role_id: int,
    created: str,
    should_notify: Callable[[Role], bool],
):
    """Create a notification if provided function says so"""
    try:
        instance = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        # Role was deleted, ignore it
        return
    if not should_notify(instance):
        return
    meta = _create_metadata(instance)
    operation = "add" if created else "update"
    _store_notification(
        identifier=instance.id,
        object_type=instance._meta.object_name,
        operation=operation,
        **meta,
    )


def _queue_role_start_notification(role_id: int, created: str):
    """Create a notification for the role if start_date is today"""

    def should_notify(instance):
        return instance.start_date == datetime.date.today()

    _queue_role_notification(role_id, created, should_notify)


def _queue_role_end_notification(role_id: int, created: str):
    """Create a notification for the role if end_date was yesterday"""

    def should_notify(instance: Role):
        return instance.end_date == datetime.date.today() - datetime.timedelta(days=1)

    _queue_role_notification(role_id, created, should_notify)


@receiver(models.signals.pre_save, dispatch_uid="add_changed_fields_callback")
def add_changed_fields_callback(sender, instance, raw, *args, **kwargs):
    """
    Makes note of any dirty (changed) fields before they are saved,
    stuffing them in the instance for use in any post-save callbacks.
    """
    if not isinstance(instance, SUPPORTED_MODELS):
        return
    changed = instance.is_dirty()
    if not changed:
        return
    instance._changed_fields = list(  # pylint: disable=protected-access
        instance.get_dirty_fields().keys()
    )


def _handle_role_save_signal(instance: Role):
    """
    Side effects of saving a Role

    Schedules the production of a Notification on the start date and on
    the day following the end date of the Role if their dates are in
    the future
    """
    today = datetime.date.today()
    if (
        "start_date" in instance._changed_fields  # pylint: disable=protected-access
        and instance.start_date
        and str2date(instance.start_date) > today
    ):
        Schedule.objects.create(
            func="greg.signals._queue_role_start_notification",
            args=f"{instance.id},True",
            next_run=date_to_datetime_midnight(str2date(instance.start_date)),
            schedule_type=Schedule.ONCE,
        )
    if (
        "end_date" in instance._changed_fields  # pylint: disable=protected-access
        and str2date(instance.end_date) > today
    ):
        Schedule.objects.create(
            func="greg.signals._queue_role_end_notification",
            args=f"{instance.id},True",
            next_run=date_to_datetime_midnight(
                str2date(instance.end_date) + datetime.timedelta(days=1)
            ),
            schedule_type=Schedule.ONCE,
        )


@receiver(models.signals.post_save, dispatch_uid="save_notification_callback")
def save_notification_callback(sender, instance, created, *args, **kwargs):
    """Do things when supported models are saved"""
    if not isinstance(instance, SUPPORTED_MODELS):
        return
    # Queue future notifications on start and end date for roles
    if isinstance(instance, Role) and hasattr(instance, "_changed_fields"):
        _handle_role_save_signal(instance)

    meta = _create_metadata(instance)
    operation = "add" if created else "update"
    _store_notification(
        identifier=instance.id,
        object_type=instance._meta.object_name,
        operation=operation,
        **meta,
    )


@receiver(models.signals.post_delete, dispatch_uid="delete_notification_callback")
def delete_notification_callback(sender, instance, *args, **kwargs):
    if not isinstance(instance, SUPPORTED_MODELS):
        return
    meta = _create_metadata(instance)
    _store_notification(
        identifier=instance.id,
        object_type=instance._meta.object_name,
        operation="delete",
        **meta,
    )


@receiver(models.signals.m2m_changed, dispatch_uid="m2m_changed_notification_callback")
def m2m_changed_notification_callback(
    sender, instance, action, *args, model=None, pk_set=None, **kwargs
):
    if action not in ("post_add", "post_remove"):
        return
    if sender not in (Consent, Role, Identity):
        return

    operation = "add" if action == "post_add" else "delete"
    instance_type = type(instance)

    if sender is Consent:
        person_consents = []
        if instance_type is Person and model is ConsentType:
            person_consents = Consent.objects.filter(
                person_id=instance.id, consent_id__in=pk_set
            )
        elif instance_type is ConsentType and model is Person:
            person_consents = Consent.objects.filter(
                consent_id=instance.id, person_id__in=pk_set
            )

        for pc in person_consents:
            meta = _create_metadata(pc)
            _store_notification(
                identifier=pc.id,
                object_type=Consent._meta.object_name,
                operation=operation,
                **meta,
            )
    elif sender is Role:
        roles = []
        if instance_type is Person and model is RoleType:
            roles = Role.objects.filter(person_id=instance.id, role_id__in=pk_set)
        elif instance_type is RoleType and model is Person:
            roles = Role.objects.filter(role_id=instance.id, person_id__in=pk_set)

        for pr in roles:
            meta = _create_metadata(pr)
            _store_notification(
                identifier=pr.id,
                object_type=Role._meta.object_name,
                operation=operation,
                **meta,
            )


def _create_metadata(instance) -> Dict[str, Union[int, str]]:
    meta: Dict[str, Union[int, str]] = {}
    if isinstance(instance, Person):
        meta["person_id"] = instance.id
    elif isinstance(instance, Role):
        meta["person_id"] = instance.person.id
        meta["role_id"] = instance.id
        meta["role_type"] = instance.type.identifier
        meta["role_type_id"] = instance.type.id
    elif isinstance(instance, RoleType):
        meta["role_type_id"] = instance.id
    elif isinstance(instance, Identity):
        meta["person_id"] = instance.person.id
        meta["identity_id"] = instance.id
        meta["identity_type"] = instance.type
    elif isinstance(instance, Consent):
        meta["person_id"] = instance.person.id
        meta["consent_id"] = instance.id
        meta["consent_type"] = instance.type.identifier
        meta["consent_type_id"] = instance.type.id
    return meta
