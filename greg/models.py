import uuid

from datetime import date
from typing import Optional

from dirtyfields import DirtyFieldsMixin
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone

from greg.managers import PersonManager
from gregsite.settings.base import ORGREG_NAME


class BaseModel(DirtyFieldsMixin, models.Model):
    """Common fields for all models."""

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Person(BaseModel):
    """A person is someone who has requested guest access."""

    class GenderType(models.TextChoices):
        MALE = "male"
        FEMALE = "female"

    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    date_of_birth = models.DateField(null=True)
    registration_completed_date = models.DateField(null=True)
    gender = models.CharField(
        null=True, max_length=6, choices=GenderType.choices, blank=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name="user",
        null=True,
        blank=True,
    )
    meta = models.JSONField(null=True, blank=True)

    objects = PersonManager()

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.pk})"

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"first_name={repr(self.first_name)}, "
            f"last_name={repr(self.last_name)})"
        )

    @property
    def private_email(self) -> Optional["Identity"]:
        """The user provided private email address."""
        return self.identities.filter(type=Identity.IdentityType.PRIVATE_EMAIL).first()

    @property
    def private_mobile(self) -> Optional["Identity"]:
        """The user provided private mobile number."""
        return self.identities.filter(
            type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER
        ).first()

    @property
    def fnr(self) -> Optional["Identity"]:
        """The person's fnr if they have one registered."""
        return self.identities.filter(
            type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER
        ).first()

    @property
    def national_id_card_number(self) -> Optional["Identity"]:
        """The person's national id card number if they have one registered."""
        return self.identities.filter(
            type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_CARD_NUMBER
        ).first()

    @property
    def passport(self) -> Optional["Identity"]:
        """
        The person's passport if they have one registered.
        This property was introduced to make updating of
        passport easier when the guest registers himself.
        """
        return self.identities.filter(
            type=Identity.IdentityType.PASSPORT_NUMBER
        ).first()

    @property
    def feide_id(self) -> Optional["Identity"]:
        """The person's Feide ID if they have one registered."""
        return self.identities.filter(type=Identity.IdentityType.FEIDE_ID).first()

    @property
    def is_registered(self) -> bool:
        """
        A registered guest is a person who has completed
        the registration process.

        The registration process requires that the guest has verified
        their email address via a token link, filled in the required
        personal information along with providing at least one
        verification method, and accepted the institution's mandatory
        consent forms.

        The registered guest may or may not already be verified,
        depending on the verification method.  However, before a
        guest is cleared for account creation at the institution's
        IGA, the guest must be both registered (``is_registered``)
        and verified (``is_verified``).
        """
        # registration_completed_date is set only after accepting consents
        return (
            self.registration_completed_date is not None
            and self.registration_completed_date <= date.today()
        )

    @staticmethod
    def get_verified_identities_query():
        return Q(
            identities__type__in=settings.ALLOWED_VERIFIED_ID_TYPES,
            identities__verified_at__isnull=False,
            identities__verified_at__lte=timezone.now(),
        )

    @property
    def is_verified(self) -> bool:
        """
        A verified guest is a person whom has had their personal
        identity verified.

        Due to the diversity of guests at a university institution,
        there are many ways for guests to identify themselves.
        These include Feide ID, passport number, driver's license
         and Norwegian national ID number card.

        Some of these methods are implicitly trusted (Feide ID) because
        the guest is likely a visitor from another academic institution
        who has already been pre-verified.  Others are manual, such
        as the sponsor vouching for having checked the guest's
        personal details against his or her passport.

        The verified guest may or may not have completed
        the registration process which implies that it is only
        the combination of being registered (``is_registered``)
        and being verified (``is_verified``) that qualifies for being cleared
        for account creation in the IGA.

        Note that we do not distinguish between the quality,
        authenticity, or trust level of the guest's associated identities.
        """
        return (
            self.identities.filter(
                type__in=settings.ALLOWED_VERIFIED_ID_TYPES,
                verified_at__isnull=False,
                verified_at__lte=timezone.now(),
            ).count()
            >= 1
        )

    @property
    def has_mandatory_consents(self) -> bool:
        """
        A person that has mandatory consents is a person whom has given consent
        to all mandatory consent types known to greg.
        """
        person_consents = sorted(
            i.type.name_en for i in self.consents.filter(consent_given_at__isnull=False)
        )
        mandatory_consents = sorted(
            i.name_en for i in ConsentType.objects.filter(mandatory=True)
        )
        return person_consents == mandatory_consents


class RoleType(BaseModel):
    """A role variant."""

    identifier = models.SlugField(max_length=64, unique=True)
    name_nb = models.CharField(max_length=256)
    name_en = models.CharField(max_length=256)
    description_nb = models.TextField()
    description_en = models.TextField()
    default_duration_days = models.IntegerField(null=True)
    # Max days into the future this role can be assigned
    max_days = models.IntegerField(default=365)
    visible = models.BooleanField(default=False)

    def __str__(self):
        return f"{str(self.name_en or self.name_nb)} ({self.identifier})"

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(pk={repr(self.pk)}, "
            f"identifier={repr(self.identifier)}, "
            f"name_nb={repr(self.name_nb)}, "
            f"name_en={repr(self.name_en)})"
        )


class Role(BaseModel):
    """The relationship between a person and a role."""

    person = models.ForeignKey("Person", on_delete=models.CASCADE, related_name="roles")
    type = models.ForeignKey(
        "RoleType", on_delete=models.PROTECT, related_name="persons"
    )
    orgunit = models.ForeignKey(
        "OrganizationalUnit", on_delete=models.PROTECT, related_name="unit_person_role"
    )
    # The start date can be null for people that are already
    # attached to the institution but does not have a guest account
    start_date = models.DateField(null=True)
    end_date = models.DateField()
    contact_person_unit = models.TextField(blank=True)
    comments = models.TextField(blank=True)
    available_in_search = models.BooleanField(default=False)
    sponsor = models.ForeignKey(
        "Sponsor", on_delete=models.PROTECT, related_name="sponsor_role"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["person_id", "type_id", "orgunit_id", "start_date", "end_date"],
                name="person_role_type_unique",
            )
        ]

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"person={repr(self.person)}, "
            f"type={repr(self.type)})"
        )

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, "
            f"role={self.person}/{self.type}@{self.orgunit.name_nb})"
        )


class Notification(BaseModel):
    """A change notification that should be delivered to a message queue."""

    identifier = models.IntegerField(null=True, blank=True)
    object_type = models.TextField()
    operation = models.TextField()
    issued_at = models.IntegerField()
    meta = models.JSONField(null=True, blank=True)

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"identifier={repr(self.identifier)}, "
            f"object_type={repr(self.object_type)}, "
            f"operation={repr(self.operation)}, "
            f"issued_at={repr(self.issued_at)}, "
            f"meta={repr(self.meta)})"
        )


class Identity(BaseModel):
    """
    Model used for storing identifiers of a person

    A person must have at least one verified PASSPORT_NUMBER or
    NORWEGIAN_NATIONAL_ID_NUMBER to be active.

    If for some reason we find that a value is invalid, even if it has
    been verified, it can be marked as invalid by setting the invalid
    field to True.

    MIGRATION_ID is used to identify persons imported from another
    source system.
    PRIVATE_EMAIL is used when sending invitation emails.
    """

    class IdentityType(models.TextChoices):
        # Feide ID, i.e. eduPersonPrincipalName
        FEIDE_ID = "feide_id"
        FEIDE_EMAIL = "feide_email"
        # Passport number
        # Format: # Format: {ALPHA-2-COUNTRY-CODE}-{DOCUMENT-NUMBER}
        PASSPORT_NUMBER = "passport_number"
        # Norwegian national ID - "fødselsnummer" or D-number (or SO-number)
        # Format: 11 digits, with leading zero if applicable
        NORWEGIAN_NATIONAL_ID_NUMBER = "norwegian_national_id_number"
        # Norwegian national ID card document number
        NORWEGIAN_NATIONAL_ID_CARD_NUMBER = "norwegian_national_id_card_number"
        # Catch-all for saving generic national ID card numbers
        # Format: {ALPHA-2-COUNTRY-CODE}-{DOCUMENT-NUMBER}
        NATIONAL_ID_CARD_NUMBER = "national_id_card_number"
        PRIVATE_EMAIL = "private_email"
        # Mobile phone number
        # Format: E.164
        PRIVATE_MOBILE_NUMBER = "private_mobile"
        # Used to identify the ID of this person in some other system
        MIGRATION_ID = "migration_id"

    class Verified(models.TextChoices):
        AUTOMATIC = "automatic"
        MANUAL = "manual"

    person = models.ForeignKey(
        "Person", on_delete=models.CASCADE, related_name="identities"
    )
    type = models.CharField(max_length=64, choices=IdentityType.choices)
    source = models.CharField(max_length=256)
    value = models.CharField(max_length=256)
    verified = models.CharField(max_length=9, choices=Verified.choices, blank=True)
    verified_by = models.ForeignKey(
        "Sponsor",
        on_delete=models.PROTECT,
        related_name="sponsor",
        null=True,
        blank=True,
    )
    verified_at = models.DateTimeField(null=True, blank=True)
    invalid = models.BooleanField(null=True)

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"type={repr(self.type)}, "
            f"value={repr(self.value)})"
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"person_id={repr(self.person_id)}, "
            f"type={repr(self.type)}, "
            f"source={repr(self.source)}, "
            f"value={repr(self.value)}, "
            f"verified_by={repr(self.verified_by)}, "
            f"verified_at={repr(self.verified_at)})"
        )

    class Meta:
        verbose_name_plural = "identities"
        constraints = (
            models.UniqueConstraint(
                fields=["type", "value", "source"],
                name="identity_type_value_unique",
            ),
        )


class ConsentType(BaseModel):
    """
    Describes some consent, like acknowledging the IT department guidelines, a guest can give.
    """

    identifier = models.SlugField(max_length=64, unique=True)
    name_en = models.CharField(max_length=256)
    name_nb = models.CharField(max_length=256)
    name_nn = models.CharField(max_length=256)
    description_en = models.TextField()
    description_nb = models.TextField()
    description_nn = models.TextField()
    valid_from = models.DateField(default=date.today)
    user_allowed_to_change = models.BooleanField()
    mandatory = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{str(self.name_en or self.name_nb)} ({self.identifier})"

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"identifier={repr(self.identifier)}, "
            f"name_en={repr(self.name_en)}, "
            f"valid_from={repr(self.valid_from)}, "
            f"user_allowed_to_change={repr(self.user_allowed_to_change)})"
        )


class ConsentChoice(BaseModel):
    """
    Describes an option associated with a consent type.
    """

    consent_type = models.ForeignKey(
        "ConsentType", on_delete=models.PROTECT, related_name="choices"
    )
    value = models.CharField(max_length=128)
    text_en = models.CharField(max_length=512)
    text_nb = models.CharField(max_length=512)
    text_nn = models.CharField(max_length=512)

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=["consent_type", "value"],
                name="consent_choice_type_value_unique",
            ),
        )

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, value={self.value}, "
            f"type={self.consent_type}))"
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"consent_type={repr(self.consent_type)} "
            f"value={repr(self.value)}, "
            f"text_en={repr(self.text_en)}, "
            f"text_nb={repr(self.text_nb)}, "
            f"text_nn={repr(self.text_nn)})"
        )


class Consent(BaseModel):
    """
    Links a person and a consent he has given.
    """

    person = models.ForeignKey(
        "Person", on_delete=models.CASCADE, related_name="consents"
    )
    type = models.ForeignKey(
        "ConsentType", on_delete=models.PROTECT, related_name="persons"
    )
    choice = models.ForeignKey(
        "ConsentChoice",
        on_delete=models.PROTECT,
        related_name="consents",
        null=True,
    )
    consent_given_at = models.DateField(null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["person", "type"], name="person_consent_type_unique"
            )
        ]

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, "
            f"person={self.person}, type={self.type}, "
            f"consent_given_at={self.consent_given_at})"
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"person={repr(self.person)}, "
            f"type={repr(self.type)}, "
            f"consent_given_at={repr(self.consent_given_at)})"
        )


class OuIdentifier(BaseModel):
    """Generic identifier"""

    name = models.CharField(max_length=256)
    source = models.CharField(max_length=256, null=True)
    value = models.CharField(max_length=256)
    orgunit = models.ForeignKey(
        "OrganizationalUnit", on_delete=models.CASCADE, related_name="identifiers"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="unique_identifier", fields=["name", "orgunit"]
            )
        ]

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, "
            f"name={self.name}, value={self.value})"
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"name={repr(self.name)}, "
            f"value={repr(self.value)})"
        )


class OrganizationalUnit(BaseModel):
    """
    An organizational unit. Units can be organized in a hierarchical manner.
    """

    name_nb = models.CharField(max_length=256)
    name_en = models.CharField(max_length=256)
    parent = models.ForeignKey(
        "self",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        related_name="children",
    )
    active = models.BooleanField(default=True)
    deleted = models.BooleanField(default=False)

    @property
    def orgreg_id(self) -> Optional[str]:
        """The orgreg ID if it exists"""
        return (
            self.identifiers.filter(name=ORGREG_NAME)
            .values_list("value", flat=True)
            .first()
        )

    @property
    def acronym_nob(self) -> Optional[str]:
        """Gets the Norwegian bokmaal acronym if it exists"""
        return (
            self.identifiers.filter(name="acronym_nob")
            .values_list("value", flat=True)
            .first()
        )

    @property
    def acronym_nno(self) -> Optional[str]:
        """Gets the Norwegian nynorsk acronym if it exists"""
        return (
            self.identifiers.filter(name="acronym_nno")
            .values_list("value", flat=True)
            .first()
        )

    @property
    def acronym_eng(self) -> Optional[str]:
        """Gets the English acronym if it exists"""
        return (
            self.identifiers.filter(name="acronym_eng")
            .values_list("value", flat=True)
            .first()
        )

    @property
    def identifier_1(self) -> Optional[str]:
        """Gets the first extra field to show when listing units"""
        if len(settings.ORGREG_EXTRA_IDS) > 0:
            if "type" in settings.ORGREG_EXTRA_IDS[0].keys():
                identifier = settings.ORGREG_EXTRA_IDS[0]["type"]
            elif "fieldname" in settings.ORGREG_EXTRA_IDS[0].keys():
                identifier = settings.ORGREG_EXTRA_IDS[0]["fieldname"]
            else:
                return None
        else:
            return None
        return (
            self.identifiers.filter(name=identifier)
            .values_list("value", flat=True)
            .first()
        )

    @property
    def identifier_2(self) -> Optional[str]:
        """Gets the first extra field to show when listing units"""
        if len(settings.ORGREG_EXTRA_IDS) > 1:
            if "type" in settings.ORGREG_EXTRA_IDS[1].keys():
                identifier = settings.ORGREG_EXTRA_IDS[1]["type"]
            elif "fieldname" in settings.ORGREG_EXTRA_IDS[1].keys():
                identifier = settings.ORGREG_EXTRA_IDS[1]["fieldname"]
            else:
                return None
        else:
            return None
        return (
            self.identifiers.filter(name=identifier)
            .values_list("value", flat=True)
            .first()
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"name_en={repr(self.name_en)}, "
            f"parent={repr(self.parent)})"
        )

    def __str__(self) -> str:
        return f"{self.name_nb} ({self.name_en}) ({self.identifier_1}) ({self.identifier_2})"

    def fetch_tree(self):
        """
        Convenience method for fetching a set of the entire tree below
        this unit including itself.
        """
        units = set()
        self.get_children(units)
        return units

    def get_children(self, units: set["OrganizationalUnit"]):
        """
        Fetch the entire tree of units with this unit at the top.

        Expects an empty set for bookkeeping to prevent an infinite loop.
        """
        # shortcut to prevent infinite loop in case of a loop in the tree
        if self.id in [i.id for i in units]:
            return
        # Recursion loop for fetching children of children of ...
        units.add(self)
        for child in self.children.all():
            child.get_children(units)


class Sponsor(BaseModel):
    """
    A sponsor is someone who is allowed, with some restrictions,
    to send out invitations to guests and to verify their identity.
    """

    feide_id = models.CharField(max_length=256)
    work_email = models.CharField(max_length=256, null=True)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)

    units = models.ManyToManyField(
        "OrganizationalUnit",
        through="SponsorOrganizationalUnit",
        related_name="sponsor_unit",
    )  # type: ignore[var-annotated]

    def __str__(self) -> str:
        return f"{self.feide_id} ({self.first_name} {self.last_name})"

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"feide_id={repr(self.feide_id)}, "
            f"first_name={repr(self.first_name)}, "
            f"last_name={repr(self.last_name)}, "
            f"work_email={repr(self.work_email)})"
        )

    class Meta:
        constraints = [
            models.UniqueConstraint(name="unique_feide_id", fields=["feide_id"])
        ]

    def get_allowed_units(self) -> list[OrganizationalUnit]:
        """
        Fetch every unit the sponsor has access to.

        This includes both through direct access and hierarchical.
        """
        connections = self.link_sponsor.all()
        ha_units = [i.organizational_unit for i in connections if i.hierarchical_access]

        units: set[OrganizationalUnit] = set()  # Collector set
        # Add units accessible through hierarchical access
        for unit in ha_units:
            units.update(unit.fetch_tree())

        # Add units accessible through direct access
        units = units.union({i.organizational_unit for i in connections})

        # Remove units that are deleted
        units_filtered = [unit for unit in units if not unit.deleted]
        return units_filtered


class SponsorOrganizationalUnit(BaseModel):
    """
    A link between a sponsor and an organizational unit.
    """

    sponsor = models.ForeignKey(
        "Sponsor", on_delete=models.PROTECT, related_name="link_sponsor"
    )
    organizational_unit = models.ForeignKey(
        "OrganizationalUnit", on_delete=models.PROTECT, related_name="link_unit"
    )
    hierarchical_access = models.BooleanField()
    automatic = models.BooleanField(default=False)
    source = models.CharField(max_length=256, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["sponsor", "organizational_unit"],
                name="sponsor_organizational_unit_unique",
            )
        ]

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, sponsor={self.sponsor}, "
            f"org_unit={self.organizational_unit})"
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(id={repr(self.pk)}, "
            f"sponsor={repr(self.sponsor)}, "
            f"organizational_unit={repr(self.organizational_unit)}, "
            f"hierarchical_access={repr(self.hierarchical_access)})"
        )


class InvitationLink(BaseModel):
    """
    Link to an invitation.

    Having the uuid of an InvitationLink should grant access to the view for posting.
    If the Invitation itself is deleted, all InvitationLinks are also removed.
    """

    uuid = models.UUIDField(null=False, default=uuid.uuid4, blank=False)
    invitation = models.ForeignKey(
        "Invitation",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="invitation_links",
    )
    expire = models.DateTimeField(blank=False, null=False)

    def __str__(self) -> str:
        return (
            f"{self.__class__.__name__}(id={self.pk}, invitation={self.invitation}, "
            f"uuid={self.uuid}, expire={self.expire})"
        )


class Invitation(BaseModel):
    """
    Stores information about an invitation.

    Deleting the InvitedPerson deletes the Invitation.
    """

    role = models.ForeignKey(
        "Role",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="invitations",
    )

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(id={self.pk}, role={self.role})"
