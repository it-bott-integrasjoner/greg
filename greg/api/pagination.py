from rest_framework.pagination import CursorPagination


class PrimaryKeyCursorPagination(CursorPagination):
    ordering = "pk"
    page_size = 100
    page_size_query_param = "page_size"
