from .notification import NotificationSerializer
from .person import PersonSerializer
from .role_type import RoleTypeSerializer


def get_serializer(instance):
    return {
        "notification": NotificationSerializer,
        "person": PersonSerializer,
        "role type": RoleTypeSerializer,
    }.get(instance._meta.verbose_name)
