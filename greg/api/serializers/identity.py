from django.core.exceptions import ValidationError
from rest_framework import serializers

from greg.models import Identity


class IdentitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Identity
        fields = "__all__"

    def is_duplicate(self, identity_type: str, value: str) -> bool:
        return Identity.objects.filter(type=identity_type).filter(value=value).exists()

    def validate(self, attrs):
        if self.is_duplicate(attrs["type"], attrs["value"]):
            raise ValidationError("Identity already exists")
        return attrs
