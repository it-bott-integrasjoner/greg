from rest_framework import serializers

from greg.api.serializers.consent_type import ConsentTypeSerializerBrief
from greg.models import Consent


class ConsentSerializerBrief(serializers.ModelSerializer):
    type = ConsentTypeSerializerBrief(read_only=True)
    choice = serializers.CharField(read_only=True, source="choice.value")

    class Meta:
        model = Consent
        queryset = Consent.objects.all().select_related("choice")
        fields = ["id", "type", "consent_given_at", "choice"]
