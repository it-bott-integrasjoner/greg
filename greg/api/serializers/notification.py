from rest_framework import fields, serializers

from greg.models import Notification
from greg.utils import camel_to_snake


class NotificationSerializer(serializers.ModelSerializer):
    meta = fields.JSONField()

    class Meta:
        model = Notification
        fields = ["identifier", "object_type", "operation", "issued_at", "meta"]

    def to_representation(self, instance):
        r = super().to_representation(instance)
        object_type = camel_to_snake(instance.object_type)
        r.update(
            {
                "object_type": object_type,
                "iat": r["issued_at"],
            }
        )
        del r["issued_at"]
        if "meta" in r and not r.get("meta"):
            del r["meta"]
        return r
