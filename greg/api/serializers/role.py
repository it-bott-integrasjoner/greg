from rest_framework import serializers
from rest_framework.fields import IntegerField

from greg.api.serializers.organizational_unit import OrganizationalUnitSerializer
from greg.models import Role, RoleType
from greg.utils import role_invitation_date_validator


class RoleSerializer(serializers.ModelSerializer):
    """
    Serializer for use with GET.

    When doing GET we want the complete orgunit object, not just the id.
    """

    type = serializers.SlugRelatedField(
        queryset=RoleType.objects.all(), slug_field="identifier"
    )
    orgunit = OrganizationalUnitSerializer()

    class Meta:
        model = Role
        fields = [
            "id",
            "start_date",
            "end_date",
            "sponsor",
            "orgunit",
            "created",
            "updated",
            "type",
            "comments",
            "available_in_search",
            "contact_person_unit",
        ]


class RoleWriteSerializer(RoleSerializer):
    """
    Serializer for use when writing.

    When writing we only want the id of the orgunit object.
    """

    orgunit = IntegerField(source="orgunit_id")  # type: ignore

    class Meta:
        model = Role
        fields = [
            "id",
            "start_date",
            "end_date",
            "sponsor",
            "orgunit",
            "created",
            "updated",
            "type",
            "comments",
            "available_in_search",
            "contact_person_unit",
        ]


class RoleInvitationSerializer(RoleWriteSerializer):
    """Serializer used when creating an invitation"""

    def validate(self, attrs):
        """Validate dates"""

        start_date = attrs.get("start_date")
        end_date = attrs.get("end_date")
        max_days = attrs["type"].max_days
        role_invitation_date_validator(
            start_date=start_date, end_date=end_date, max_days=max_days
        )

        return attrs

    class Meta:
        model = Role
        fields = [
            "id",
            "start_date",
            "end_date",
            "orgunit",
            "type",
            "comments",
            "contact_person_unit",
        ]
