from rest_framework import serializers

from greg.api.serializers.organizational_unit import OrganizationalUnitSerializer
from greg.models import Sponsor


class SponsorSerializer(serializers.ModelSerializer):
    orgunits = OrganizationalUnitSerializer(many=True, read_only=True, source="units")

    class Meta:
        model = Sponsor
        fields = [
            "id",
            "feide_id",
            "first_name",
            "last_name",
            "work_email",
            "orgunits",
            "created",
            "updated",
        ]
        read_only_fields = ["created", "updated"]
