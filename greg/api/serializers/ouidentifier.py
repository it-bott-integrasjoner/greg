from rest_framework.serializers import ModelSerializer

from greg.models import OuIdentifier


class OuIdentifierSerializer(ModelSerializer):
    class Meta:
        model = OuIdentifier
        fields = ["id", "source", "name", "value"]
