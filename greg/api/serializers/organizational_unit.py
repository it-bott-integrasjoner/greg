from rest_framework.serializers import ModelSerializer

from greg.api.serializers.ouidentifier import OuIdentifierSerializer

from greg.models import OrganizationalUnit, SponsorOrganizationalUnit


class OrganizationalUnitSerializer(ModelSerializer):
    identifiers = OuIdentifierSerializer(many=True)

    class Meta:
        model = OrganizationalUnit
        fields = [
            "id",
            "created",
            "updated",
            "name_nb",
            "name_en",
            "active",
            "deleted",
            "parent",
            "identifiers",
        ]


class SponsorOrgUnitsSerializer(ModelSerializer):
    class Meta:
        model = SponsorOrganizationalUnit
        fields = [
            "id",
            "sponsor",
            "organizational_unit",
            "hierarchical_access",
            "automatic",
            "source",
            "created",
            "updated",
        ]
        read_only_fields = ["sponsor", "organizational_unit", "created", "updated"]
