from rest_framework import serializers

from greg.api.serializers.role import RoleInvitationSerializer
from greg.models import Identity, Person, Sponsor
from greg.utils import create_objects_for_invitation, is_identity_duplicate
from gregui.api.serializers.identity import IdentityDuplicateError


class InvitationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    role = RoleInvitationSerializer(required=True)
    sponsor = serializers.CharField(max_length=256)

    def create(self, validated_data):
        email = validated_data.pop("email")
        role_data = validated_data.pop("role")
        sponsor = validated_data.pop("sponsor")

        # Find sponsor based on feide_id
        # If not found it will be created
        try:
            sponsor = Sponsor.objects.get(feide_id=sponsor)
        except Sponsor.DoesNotExist:
            pass

        person = create_objects_for_invitation(
            person_data=validated_data,
            person_email=email,
            role_data=role_data,
            sponsor=sponsor,
        )

        return person

    def validate_email(self, email):
        # The e-mail in the invite is the private e-mail
        if is_identity_duplicate(Identity.IdentityType.PRIVATE_EMAIL, email):
            raise IdentityDuplicateError(
                Identity.IdentityType.PRIVATE_EMAIL, "duplicate_private_email"
            )

        # Return the validated data
        return email

    class Meta:
        model = Person
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "role",
            "sponsor",
        )
