from rest_framework import serializers

from greg.api.serializers.consent import ConsentSerializerBrief
from greg.api.serializers.identity import IdentitySerializer
from greg.api.serializers.role import RoleSerializer
from greg.models import Person


class PersonSerializer(serializers.ModelSerializer):
    identities = IdentitySerializer(many=True, read_only=True)
    roles = RoleSerializer(many=True, read_only=True)
    consents = ConsentSerializerBrief(many=True, read_only=True)

    class Meta:
        model = Person
        fields = [
            "id",
            "first_name",
            "last_name",
            "gender",
            "date_of_birth",
            "registration_completed_date",
            "identities",
            "roles",
            "consents",
            "meta",
            "created",
            "updated",
        ]

        read_only_fields = ["created", "updated"]


class PersonSearchSerializer(serializers.ModelSerializer):
    person_id = serializers.CharField(source="id")
    first = serializers.CharField(source="first_name")
    last = serializers.CharField(source="last_name")

    class Meta:
        model = Person
        fields = [
            "person_id",
            "first",
            "last",
            "date_of_birth",
        ]
