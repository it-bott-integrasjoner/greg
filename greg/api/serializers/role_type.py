from rest_framework.serializers import ModelSerializer

from greg.models import RoleType


class RoleTypeSerializer(ModelSerializer):
    class Meta:
        model = RoleType
        fields = "__all__"
