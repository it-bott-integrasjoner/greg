from rest_framework.serializers import ModelSerializer

from greg.models import ConsentChoice, ConsentType


class ConsentChoiceSerializer(ModelSerializer):
    class Meta:
        model = ConsentChoice
        fields = ["value", "text_en", "text_nb", "text_nn"]


class ConsentTypeSerializer(ModelSerializer):
    choices = ConsentChoiceSerializer(many=True)

    class Meta:
        model = ConsentType
        fields = (
            "id",
            "identifier",
            "name_en",
            "name_nb",
            "name_nn",
            "description_en",
            "description_nb",
            "description_nn",
            "mandatory",
            "user_allowed_to_change",
            "valid_from",
            "choices",
            "created",
            "updated",
        )
        read_only_fields = ["created", "updated"]


class ConsentTypeSerializerBrief(ModelSerializer):
    class Meta:
        model = ConsentType
        fields = ["identifier", "mandatory"]
