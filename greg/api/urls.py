from django.urls import (
    re_path,
)
from rest_framework.routers import DefaultRouter

from greg.api.views.consent_type import ConsentTypeViewSet
from greg.api.views.invitation import InvitationViewSet
from greg.api.views.identity import IdentityViewSet
from greg.api.views.organizational_unit import OrganizationalUnitViewSet
from greg.api.views.person import (
    RoleViewSet,
    PersonViewSet,
    PersonIdentityViewSet,
    ConsentViewSet,
)
from greg.api.views.role_type import RoleTypeViewSet
from greg.api.views.sponsor import (
    SponsorViewSet,
    SponsorGuestsViewSet,
    SponsorOrgunitLinkView,
)
from greg.api.views.person import PersonSearchSet

router = DefaultRouter(trailing_slash=False)
router.register(r"persons", PersonViewSet, basename="person")
router.register(r"roletypes", RoleTypeViewSet, basename="roletype")
router.register(r"consenttypes", ConsentTypeViewSet, basename="consenttype")
router.register(r"sponsors", SponsorViewSet, basename="sponsor")
router.register(r"orgunits", OrganizationalUnitViewSet, basename="orgunit")
router.register(r"person-invitations", InvitationViewSet, basename="person-invitation")

urlpatterns = router.urls

urlpatterns += [
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/roles$",
        RoleViewSet.as_view({"get": "list", "post": "create"}),
        name="person_role-list",
    ),
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/roles/(?P<id>[0-9]+)$",
        RoleViewSet.as_view(
            {"get": "retrieve", "patch": "partial_update", "delete": "destroy"}
        ),
        name="person_role-detail",
    ),
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/identities$",
        PersonIdentityViewSet.as_view({"get": "list", "post": "create"}),
        name="person_identity-list",
    ),
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/identities/(?P<id>[0-9]+)$",
        PersonIdentityViewSet.as_view(
            {"get": "retrieve", "delete": "destroy", "patch": "partial_update"}
        ),
        name="person_identity-detail",
    ),
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/consents$",
        ConsentViewSet.as_view({"get": "list", "post": "create"}),
        name="person_consent-list",
    ),
    re_path(
        r"^persons/(?P<person_id>[0-9]+)/consents/(?P<id>[0-9]+)$",
        ConsentViewSet.as_view(
            {"get": "retrieve", "delete": "destroy", "patch": "partial_update"}
        ),
        name="person_consent-detail",
    ),
    re_path(
        r"^sponsors/(?P<sponsor_id>[0-9]+)/guests$",
        SponsorGuestsViewSet.as_view({"get": "list"}),
        name="sponsor_guests-list",
    ),
    re_path(
        r"^sponsors/(?P<sponsor_id>[0-9]+)/orgunits$",
        SponsorOrgunitLinkView.as_view({"get": "list"}),
        name="sponsor_orgunit-list",
    ),
    re_path(
        r"^sponsors/(?P<sponsor_id>[0-9]+)/orgunit/(?P<orgunit_id>[0-9]+)$",
        SponsorOrgunitLinkView.as_view(
            {"post": "create", "delete": "destroy", "get": "retrieve"}
        ),
        name="sponsor_orgunit-detail",
    ),
    re_path(
        "person-search/",
        PersonSearchSet.as_view({"get": "list"}),
        name="person_search-list",
    ),
    re_path(
        "identities/",
        IdentityViewSet.as_view({"get": "list"}),
        name="identities-list",
    ),
]
