from rest_framework import viewsets, permissions

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.role_type import RoleTypeSerializer
from greg.models import RoleType


class RoleTypeViewSet(viewsets.ModelViewSet):
    """Role type API"""

    queryset = RoleType.objects.all().order_by("id")
    serializer_class = RoleTypeSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"
