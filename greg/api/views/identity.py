from django_filters import rest_framework as filters
from rest_framework import viewsets, permissions

from greg.api.filters import IdentityFilter
from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.identity import IdentitySerializer
from greg.models import Identity


class IdentityViewSet(viewsets.ModelViewSet):
    """
    Identity API
    """

    queryset = Identity.objects.all().order_by("id")
    serializer_class = IdentitySerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = IdentityFilter
