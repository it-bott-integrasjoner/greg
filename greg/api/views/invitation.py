from rest_framework import permissions, status, viewsets
from rest_framework.response import Response

from greg.api.serializers.invitation import InvitationSerializer
from greg.models import Sponsor
from greg.utils import queue_mail_to_invitee


class InvitationViewSet(viewsets.ModelViewSet):
    """Invitation API"""

    serializer_class = InvitationSerializer
    permission_classes = (permissions.IsAdminUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        person = serializer.save()

        sponsor_feide_id = serializer.validated_data["sponsor"]
        sponsor = Sponsor.objects.get(feide_id=sponsor_feide_id)

        queue_mail_to_invitee(person, sponsor)

        headers = self.get_success_headers(serializer.validated_data)
        return Response(status=status.HTTP_201_CREATED, headers=headers)
