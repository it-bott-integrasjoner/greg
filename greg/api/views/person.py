from django.core.exceptions import ValidationError
from django_filters import rest_framework as filters
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import serializers
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response

from greg.api.filters import (
    PersonFilter,
    RoleFilter,
    IdentityFilter,
    PersonConsentFilter,
)
from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.consent import ConsentSerializerBrief
from greg.api.serializers.person import (
    PersonSerializer,
    PersonSearchSerializer,
    IdentitySerializer,
)
from greg.api.serializers.role import RoleSerializer, RoleWriteSerializer
from greg.models import Person, Role, Identity, Consent, ConsentChoice, ConsentType
from search_tools.person_search import person_by_string_query


class PersonViewSet(viewsets.ModelViewSet):
    """Person API"""

    queryset = Person.objects.all().order_by("id")
    serializer_class = PersonSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PersonFilter

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="verified",
                description="Only include verified or only not verified. When nothing is set, "
                "both verified and not verified are returned",
                required=False,
                type=bool,
            ),
            OpenApiParameter(
                name="active",
                description="Only persons with an active role is listed if true",
                required=False,
                type=bool,
            ),
            OpenApiParameter(
                name="role_type",
                required=False,
                type=str,
            ),
        ]
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)


class PersonSearchSet(viewsets.ModelViewSet):
    """Search for persons using name, email, phone number and birth date"""

    permission_classes = (permissions.IsAdminUser,)
    serializer_class = PersonSearchSerializer

    def list(self, request, *args, **kwargs):
        if "q" not in self.request.query_params:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "no_search_term",
                    "message": "No search query parameter given",
                },
            )

        if len(self.request.query_params["q"]) > 50:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "search_term_too_large",
                    "message": "Search term is too large",
                },
            )

        hits = self.get_hits()
        return Response(hits)

    def get_hits(self):
        return person_by_string_query(self.request)


class RoleViewSet(viewsets.ModelViewSet):
    """Person role API"""

    queryset = Role.objects.all().order_by("id")
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = RoleFilter
    lookup_field = "id"

    def get_serializer_class(self):
        """
        Fetch different serializer depending on http method.

        To return the complete orgunit object for GET we use a different serializer.
        """
        if self.request.method in ("POST", "PATCH"):
            return RoleWriteSerializer
        return RoleSerializer

    def get_queryset(self):
        qs = self.queryset
        if not self.kwargs:
            return qs.none()
        person_id = self.kwargs["person_id"]
        person_role_id = self.kwargs.get("id")
        qs = qs.filter(person_id=person_id)
        if person_role_id:
            qs = qs.filter(id=person_role_id)
        return qs

    def perform_create(self, serializer):
        person_id = self.kwargs["person_id"]

        if person_id is None:
            # Should not happen, the person ID is part of the API path
            raise ValidationError("No person id")

        serializer.save(person_id=person_id)


class PersonIdentityViewSet(viewsets.ModelViewSet):
    """
    Person identity API
    """

    queryset = Identity.objects.all().order_by("id")
    serializer_class = IdentitySerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = IdentityFilter
    # This is set so that the id parameter in the path of the URL is used for looking up objects
    lookup_url_kwarg = "id"

    def get_queryset(self):
        qs = self.queryset
        if not self.kwargs:
            return qs.none()
        person_id = self.kwargs["person_id"]
        qs = qs.filter(person_id=person_id)
        return qs

    def create(self, request, *args, **kwargs):
        # Want to get the person id which is part of the API path and then
        # include this with the data used to create the identity for the person
        person_id = self.kwargs["person_id"]

        if person_id is None:
            # Should not happen, the person ID is part of the API path
            raise ValidationError("No person id")

        input_data = request.data.copy()
        input_data["person"] = person_id

        serializer = self.get_serializer(data=input_data)
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class ConsentViewSet(viewsets.ModelViewSet):
    """
    Person consent API
    """

    queryset = Consent.objects.all().order_by("id")
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PersonConsentFilter
    lookup_field = "id"

    def get_serializer_class(self):
        """
        Fetch different serializer depending on http method.

        """
        if self.request.method in ("POST", "PATCH"):
            return PersonConsentSerializer

        # Use the same format as in the person endpoint when using a get
        return ConsentSerializerBrief

    def get_queryset(self):
        qs = self.queryset
        if not self.kwargs:
            return qs.none()

        # The ID of the person consent mapping is actually unique globally,
        # so the person ID is not really necessary, but we have the convention in
        # the API that the person ID should be specified as well
        person_id = self.kwargs["person_id"]

        consent_id = self.kwargs.get("id")
        qs = qs.filter(person_id=person_id)
        if consent_id:
            qs = qs.filter(id=consent_id)
        return qs

    def create(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=self._transform_input(request))
        except (ValidationError, ConsentChoice.DoesNotExist):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        try:
            transformed_input = self._transform_input(request)
        except (ValidationError, ConsentChoice.DoesNotExist):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=transformed_input, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def _transform_input(self, request):
        # Transform and augment in the request so that the serializer can handle it

        # Want to get the person id which is part of the API path and then
        # include this with the data used to create the identity for the person
        person_id = self.kwargs["person_id"]

        if person_id is None:
            # Should not happen, the person ID is part of the API path
            raise ValidationError("No person id")

        input_data = request.data.copy()
        input_data["person"] = int(person_id)

        consent_type = ConsentType.objects.get(identifier=input_data["type"])
        consent_choice = input_data["choice"]

        choice = ConsentChoice.objects.get(
            consent_type=consent_type, value=consent_choice
        )

        input_data["type"] = consent_type.id
        input_data["choice"] = choice.id

        return input_data


class PersonConsentSerializer(serializers.ModelSerializer):
    # Not much logic defined here, the validation is done implicitly in the view
    class Meta:
        model = Consent
        read_only_fields = ["id"]
        fields = ["id", "person", "type", "choice", "consent_given_at"]
