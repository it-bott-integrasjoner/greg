from rest_framework import viewsets, permissions

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.consent_type import ConsentTypeSerializer
from greg.models import ConsentType


class ConsentTypeViewSet(viewsets.ModelViewSet):
    """Consent API"""

    queryset = ConsentType.objects.all().order_by("id")
    serializer_class = ConsentTypeSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"
