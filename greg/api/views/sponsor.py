import logging

from django.db.models import ProtectedError
from django.core.exceptions import ValidationError
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import mixins, status, permissions
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers import PersonSerializer
from greg.api.serializers.organizational_unit import SponsorOrgUnitsSerializer
from greg.api.serializers.sponsor import SponsorSerializer
from greg.models import Sponsor, Person, OrganizationalUnit, SponsorOrganizationalUnit

logger = logging.getLogger(__name__)


class SponsorViewSet(ModelViewSet):
    """Sponsor API"""

    queryset = Sponsor.objects.all().order_by("id")
    serializer_class = SponsorSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"

    def destroy(self, request, *args, **kwargs):
        """Overridden method to handle exception"""
        instance = self.get_object()
        try:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ProtectedError as error:
            # Probably the sponsor is being used somewhere as a foreign key and cannot be deleted.
            # Log the error and return bad request instead an internal server error
            logger.error(error)
            return Response(status=status.HTTP_400_BAD_REQUEST)


@extend_schema(
    parameters=[
        OpenApiParameter(
            name="sponsor_id",
            description="Sponsor ID",
            location=OpenApiParameter.PATH,
            required=True,
            type=int,
        )
    ]
)
class SponsorGuestsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Person.objects.all().order_by("id")
    serializer_class = PersonSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"

    def get_queryset(self):
        qs = self.queryset
        if not self.kwargs:
            return qs.none()
        sponsor_id = self.kwargs["sponsor_id"]
        qs = qs.filter(roles__sponsor_id=sponsor_id).order_by("id")
        return qs


class SponsorOrgunitLinkView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """
    Endpoint that allows manipulation of links
    between a sponsor and the units he is attached to
    """

    queryset = SponsorOrganizationalUnit.objects.all().order_by("id")
    serializer_class = SponsorOrgUnitsSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    # This is set so that the orgunit_id parameter in
    # the path of the URL is used for looking up objects
    lookup_url_kwarg = "orgunit_id"

    def create(self, request, *args, **kwargs):
        (sponsor_id, orgunit_id) = self._extract_sponsor_and_orgunit(kwargs)
        sponsor_orgunit = self.queryset.filter(
            sponsor__id=sponsor_id, organizational_unit__id=orgunit_id
        )
        # Set default values if fields are not given
        hierarchical_access = request.data.get("hierarchical_access", "False")
        source = request.data.get("source", "")
        automatic = request.data.get("automatic", "False")

        if sponsor_orgunit:
            sponsor_orgunit.update(
                hierarchical_access=hierarchical_access,
                automatic=automatic,
                source=source,
            )
        else:
            SponsorOrganizationalUnit.objects.create(
                sponsor=Sponsor.objects.get(id=kwargs["sponsor_id"]),
                organizational_unit=OrganizationalUnit.objects.get(
                    id=kwargs["orgunit_id"]
                ),
                hierarchical_access=hierarchical_access,
                automatic=automatic,
                source=source,
            )
        try:
            sponsor_orgunit = self.queryset.get(
                sponsor__id=sponsor_id, organizational_unit__id=orgunit_id
            )
        except SponsorOrganizationalUnit.DoesNotExist:
            return Response({})

        serializer = self.serializer_class(sponsor_orgunit)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        """
        Overridden because a delete at this endpoint
        should not attempt to delete the organizational unit,
        but the link between the sponsor and the unit
        """
        (sponsor_id, orgunit_id) = self._extract_sponsor_and_orgunit(kwargs)
        sponsor = Sponsor.objects.filter(id=sponsor_id).get()
        sponsor.units.remove(orgunit_id)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request, *args, **kwargs):
        """Lists all SponsorOrganizationalUnit objects connected to the sponsor"""
        sponsor_id = kwargs["sponsor_id"]
        all_units = self.queryset.filter(sponsor__id=sponsor_id).all()
        page = self.paginate_queryset(all_units)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.serializer_class(all_units, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """Returns a given SponsorOrganizationalUnit object"""
        (sponsor_id, orgunit_id) = self._extract_sponsor_and_orgunit(kwargs)
        try:
            unit = self.queryset.get(
                sponsor__id=sponsor_id, organizational_unit__id=orgunit_id
            )
        except SponsorOrganizationalUnit.DoesNotExist:
            return Response({})
        serializer = self.serializer_class(unit)
        return Response(serializer.data)

    def _extract_sponsor_and_orgunit(self, request_data):
        sponsor_id = request_data["sponsor_id"]
        orgunit_id = request_data["orgunit_id"]

        if sponsor_id is None:
            raise ValidationError("Missing sponsor ID")

        if orgunit_id is None:
            raise ValidationError("Orgunit ID")

        return (sponsor_id, orgunit_id)
