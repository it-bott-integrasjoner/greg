from rest_framework import viewsets, permissions

from greg.api.pagination import PrimaryKeyCursorPagination
from greg.api.serializers.organizational_unit import OrganizationalUnitSerializer
from greg.models import OrganizationalUnit


class OrganizationalUnitViewSet(viewsets.ModelViewSet):
    """OrganizationalUnit API"""

    queryset = OrganizationalUnit.objects.all().order_by("id")
    serializer_class = OrganizationalUnitSerializer
    pagination_class = PrimaryKeyCursorPagination
    permission_classes = (permissions.IsAdminUser,)
    lookup_field = "id"
