from datetime import datetime

from django.db.models import Q, When, Case, BooleanField
from django_filters.rest_framework import (
    BaseInFilter,
    BooleanFilter,
    CharFilter,
    FilterSet,
)

from greg.models import (
    Person,
    Role,
    Identity,
    Consent,
)


class RoleFilter(FilterSet):
    type = BaseInFilter(field_name="role__type", lookup_expr="in")

    class Meta:
        model = Role
        fields = ["type"]


class PersonFilter(FilterSet):
    verified = BooleanFilter(method="verified_filter")
    active = BooleanFilter(method="active_role_filter")
    role_type = CharFilter(method="role_type_filter")

    class Meta:
        model = Person
        fields = ["first_name", "last_name", "verified", "active", "role_type"]

    def verified_filter(self, queryset, name, value):
        if value is True:
            return queryset.with_verified_identities()
        if value is False:
            return queryset.without_verified_identities()
        return queryset

    def active_role_filter(self, queryset, name, value):
        if value:
            datetime_now = datetime.now()
            return queryset.annotate(
                active=Case(
                    When(
                        Q(roles__start_date__isnull=True)
                        | Q(roles__start_date__lte=datetime_now),
                        roles__end_date__gte=datetime_now,
                        then=True,
                    ),
                    default=False,
                    output_field=BooleanField(),
                )
            ).filter(active=value)

        return queryset

    def role_type_filter(self, queryset, name, value):
        return queryset.filter(roles__type__identifier=value)


class IdentityFilter(FilterSet):
    class Meta:
        model = Identity
        fields = {
            "type": ["exact"],
            "verified_by_id": ["exact"],
            "value": ["exact", "startswith"],
        }


class PersonConsentFilter(FilterSet):
    class Meta:
        model = Consent
        fields = ["id"]
