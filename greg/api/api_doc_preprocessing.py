def custom_preprocessing_hook(endpoints):
    # Do not include the routes used in the frontend in the API-specification
    filtered_endpoints = filter(lambda endpoint: "/ui/" not in endpoint[0], endpoints)
    return filtered_endpoints
