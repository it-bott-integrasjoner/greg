from django.db.models import Count, Manager, QuerySet


class PersonQuerySet(QuerySet):
    def count_verified_identities(self):
        # the requirement is minimum one verified passport or Norwegian national ID number
        # TODO: should we make this configurable or simply outsource the logic
        #       to the systems consuming this data?
        return Count(
            "identities__id",
            filter=self.model.get_verified_identities_query(),
            distinct=True,
        )

    def with_verified_identities(self):
        """Persons with at least one verified identity."""
        return self.annotate(
            verified_identities_count=self.count_verified_identities()
        ).filter(verified_identities_count__gte=1)

    def without_verified_identities(self):
        """Persons without a verified identity."""
        return self.annotate(
            verified_identities_count=self.count_verified_identities()
        ).filter(verified_identities_count=0)


class PersonManager(Manager):
    def get_queryset(self):
        return PersonQuerySet(self.model, using=self._db)

    def with_verified_identities(self):
        return self.get_queryset().with_verified_identities()

    def without_verified_identities(self):
        return self.get_queryset().without_verified_identities()
