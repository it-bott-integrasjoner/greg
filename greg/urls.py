"""greg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from typing import List
from django.urls import path, include
from django.urls.resolvers import URLResolver
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.versioning import NamespaceVersioning

from greg.api import urls as api_urls
from greg.api.views.health import Health

urlpatterns: List[URLResolver] = [
    path(
        "schema/",
        SpectacularAPIView.as_view(versioning_class=NamespaceVersioning),
        name="schema",
    ),  # type: ignore
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(
            url_name="schema", versioning_class=NamespaceVersioning
        ),  # type: ignore
        name="swagger-ui",
    ),
    path("api/health/", Health.as_view()),  # type: ignore
    path("api/v1/", include((api_urls.urlpatterns, "greg"), namespace="v1")),
]
