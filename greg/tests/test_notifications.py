import pytest

from greg.models import (
    Consent,
    ConsentType,
    Notification,
    OrganizationalUnit,
    Person,
    Identity,
    Role,
    RoleType,
    Sponsor,
)

# pylint: disable=redefined-outer-name


@pytest.fixture
def org_unit_bar() -> OrganizationalUnit:
    org = OrganizationalUnit.objects.create()
    return OrganizationalUnit.objects.get(pk=org.id)


@pytest.fixture
def sponsor() -> Sponsor:
    sp = Sponsor.objects.create(feide_id="sponsor_id")
    return Sponsor.objects.get(pk=sp.id)


@pytest.fixture
def identity(person: Person) -> Identity:
    ident = Identity.objects.create(
        person=person,
        type=Identity.IdentityType.PASSPORT_NUMBER,
        source="Test",
        value="12345678901",
    )
    return Identity.objects.get(pk=ident.id)


@pytest.mark.django_db
def test_person_add_notification(person: Person):
    notifications = Notification.objects.filter(object_type="Person")
    assert len(notifications) == 1
    assert notifications[0].operation == "add"
    assert notifications[0].identifier == person.id


@pytest.mark.django_db
def test_person_update_notification(person: Person):
    person.first_name = "New first name"
    person.save()
    notifications = Notification.objects.filter(object_type="Person")
    assert len(notifications) == 2
    assert notifications[1].operation == "update"
    assert notifications[1].identifier == person.id


@pytest.mark.django_db
def test_person_delete_notification(person: Person):
    person_id = person.id
    person.delete()
    notifications = Notification.objects.filter(object_type="Person")
    assert len(notifications) == 2
    assert notifications[1].operation == "delete"
    assert notifications[1].identifier == person_id


@pytest.mark.django_db
def test_role_add_notification(
    person: Person,
    role_type_foo: RoleType,
    org_unit_bar: OrganizationalUnit,
    sponsor: Sponsor,
):
    role = Role.objects.create(
        person=person,
        type=role_type_foo,
        start_date="2021-05-06",
        end_date="2021-10-20",
        orgunit=org_unit_bar,
        sponsor=sponsor,
    )
    notifications = Notification.objects.filter(object_type="Role")
    assert len(notifications) == 1
    assert notifications[0].operation == "add"
    meta = notifications[0].meta
    assert meta["person_id"] == person.id
    assert meta["role_id"] == role.id
    assert meta["role_type"] == role_type_foo.identifier


@pytest.mark.django_db
def test_role_update_notification(
    person: Person,
    role_type_foo: RoleType,
    org_unit_bar: OrganizationalUnit,
    sponsor: Sponsor,
):
    role = Role.objects.create(
        person=person,
        type=role_type_foo,
        start_date="2021-05-06",
        end_date="2021-10-20",
        orgunit=org_unit_bar,
        sponsor=sponsor,
    )
    assert len(person.roles.all()) == 1
    person_role = person.roles.all()[0]
    person_role.end_date = "2021-10-21"
    person_role.save()
    notifications = Notification.objects.filter(object_type="Role")
    assert len(notifications) == 2
    assert notifications[1].operation == "update"
    meta = notifications[1].meta
    assert meta["person_id"] == person.id
    assert meta["role_id"] == role.id
    assert meta["role_type"] == role_type_foo.identifier


@pytest.mark.django_db
def test_role_delete_notification(
    person: Person,
    role_type_foo: RoleType,
    org_unit_bar: OrganizationalUnit,
    sponsor: Sponsor,
):
    role = Role.objects.create(
        person=person,
        type=role_type_foo,
        start_date="2021-05-06",
        end_date="2021-10-20",
        orgunit=org_unit_bar,
        sponsor=sponsor,
    )
    assert len(person.roles.all()) == 1
    person_role = person.roles.all()[0]
    person_role.delete()
    notifications = Notification.objects.filter(object_type="Role")
    assert len(notifications) == 2
    assert notifications[1].operation == "delete"
    meta = notifications[1].meta
    assert meta["person_id"] == person.id
    assert meta["role_id"] == role.id
    assert meta["role_type"] == role_type_foo.identifier
    assert meta["role_type_id"] == role_type_foo.id


@pytest.mark.django_db
def test_consent_add_notification(person: Person, consent_type_foo: ConsentType):
    consent = Consent.objects.create(
        person=person, type=consent_type_foo, consent_given_at="2021-06-20"
    )
    notifications = Notification.objects.filter(object_type="Consent")
    assert len(notifications) == 1
    assert notifications[0].identifier == person.id
    meta = notifications[0].meta
    assert meta["person_id"] == person.id
    assert meta["consent_id"] == consent.id
    assert meta["consent_type"] == consent.type.identifier
    assert meta["consent_type_id"] == consent.type.id


@pytest.mark.django_db
def test_consent_update_notification(person: Person, consent_type_foo: ConsentType):
    consent = Consent.objects.create(
        person=person, type=consent_type_foo, consent_given_at="2021-06-20"
    )
    consents = Consent.objects.filter(person=person, type=consent_type_foo)
    consents[0].consent_given_at = "2021-06-21"
    consents[0].save()

    notifications = Notification.objects.filter(object_type="Consent")
    assert len(notifications) == 2
    assert notifications[0].identifier == person.id
    meta = notifications[0].meta
    assert meta["person_id"] == person.id
    assert meta["consent_id"] == consent.id
    assert meta["consent_type"] == consent.type.identifier
    assert meta["consent_type_id"] == consent.type.id


@pytest.mark.django_db
def test_consent_delete_notification(person: Person, consent_type_foo: ConsentType):
    consent = Consent.objects.create(
        person=person, type=consent_type_foo, consent_given_at="2021-06-20"
    )
    consents = Consent.objects.filter(person=person, type=consent_type_foo)
    consents[0].delete()
    notifications = Notification.objects.filter(object_type="Consent")

    assert len(notifications) == 2
    assert notifications[1].identifier == person.id
    assert notifications[1].operation == "delete"
    meta = notifications[0].meta
    assert meta["person_id"] == person.id
    assert meta["consent_id"] == consent.id
    assert meta["consent_type"] == consent_type_foo.identifier
    assert meta["consent_type_id"] == consent_type_foo.id


@pytest.mark.django_db
def test_identity_add_notification(
    person: Person, identity: Identity, sponsor: Sponsor
):
    notifications = Notification.objects.filter(object_type="Identity")
    assert len(notifications) == 1
    assert notifications[0].identifier == person.id
    assert notifications[0].operation == "add"

    meta = notifications[0].meta
    assert meta["person_id"] == person.id
    assert meta["identity_id"] == identity.id
    assert meta["identity_type"] == identity.type


@pytest.mark.django_db
def test_identity_update_notification(
    person: Person, identity: Identity, sponsor: Sponsor
):
    identity.verified = Identity.Verified.MANUAL
    identity.verified_by = sponsor
    identity.verified_at = "2021-08-02T12:34:56Z"
    identity.save()

    notifications = Notification.objects.filter(object_type="Identity")
    # One notification for adding person identity and one for updating it
    assert len(notifications) == 2
    assert notifications[1].operation == "update"

    meta = notifications[1].meta
    assert meta["person_id"] == person.id
    assert meta["identity_id"] == identity.id
    assert meta["identity_type"] == identity.type
