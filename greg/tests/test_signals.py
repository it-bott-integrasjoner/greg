import datetime
import pytest

from django_q.models import Schedule

from greg.models import Notification, Role
from greg.signals import _queue_role_start_notification, _queue_role_end_notification

# pylint: disable=redefined-outer-name


@pytest.fixture
def role_today(person, role_type_test_guest, sponsor_guy, unit_foo):
    """A test role with end and start date today."""
    role = Role.objects.create(
        person=person,
        type=role_type_test_guest,
        sponsor=sponsor_guy,
        orgunit=unit_foo,
        start_date=datetime.date.today(),
        end_date=datetime.date.today(),
    )
    return Role.objects.get(pk=role.id)


@pytest.mark.django_db
def test_delete_signal_person(person):
    """Check that a Notification is created when a Person object is deleted."""
    assert Notification.objects.count() == 1
    person.delete()
    assert Notification.objects.count() == 2


@pytest.mark.django_db
def test_delete_signal_ou(unit_foo):
    """Check that a Notification is not created when an OrganizationalUnit is deleted."""
    assert Notification.objects.count() == 0
    unit_foo.delete()
    assert Notification.objects.count() == 0


@pytest.mark.django_db
def test_queue_role_start_notification(role_today):
    """Check that a notification is produced if the role starts today"""
    assert Notification.objects.all().count() == 3
    _queue_role_start_notification(role_today.id, True)
    assert Notification.objects.all().count() == 4


@pytest.mark.django_db
def test_queue_role_end_notification(role_today):
    """Check that a notification is produced if the role ends yesterday"""
    role_today.end_date = datetime.date.today() - datetime.timedelta(days=1)
    role_today.save()
    assert Notification.objects.all().count() == 4
    _queue_role_end_notification(role_today.id, True)
    assert Notification.objects.all().count() == 5


@pytest.mark.django_db
def test_role_save_schedules_notifications(role_today):
    """Verify that future end or start dates schedule future notifications"""
    oneday = datetime.timedelta(days=1)
    today = datetime.date.today()
    tomorrow = today + oneday
    yesterday = today - oneday
    assert Notification.objects.all().count() == 3
    assert Schedule.objects.all().count() == 0

    # Future end date schedules
    role_today.end_date = tomorrow
    role_today.save()
    assert Schedule.objects.all().count() == 1
    # Future start date schedules
    role_today.start_date = tomorrow
    role_today.save()
    assert Schedule.objects.all().count() == 2
    # Past end date does not schedule (should be impossible in the application but test anyway)
    role_today.end_date = yesterday
    role_today.save()
    assert Schedule.objects.all().count() == 2  # Past start date schedules
    role_today.start_date = yesterday
    role_today.save()


@pytest.mark.django_db
def test_queue_role_end_notification_wrong_date(role_today):
    """Check that a notification is not produced if the role does not end yesterday"""
    assert Notification.objects.all().count() == 3
    _queue_role_end_notification(role_today.id, True)
    assert Notification.objects.all().count() == 3


@pytest.mark.django_db
def test_queue_role_end_notification_role_deleted():
    """Check that a notification is not produced if the role was deleted"""
    assert Notification.objects.all().count() == 0
    _queue_role_end_notification(10, True)
    assert Notification.objects.all().count() == 0
