import pytest
from django.conf import settings

from greg.utils import (
    is_valid_id_number,
    is_valid_so_number,
    string_contains_illegal_chars,
)


def test_so_number():
    so_number = "25529312345"
    settings.ALLOW_SO_NUMBERS = True
    assert is_valid_id_number(so_number)
    assert is_valid_so_number(so_number)


def test_not_allow_so_number():
    so_number = "25529312345"
    settings.ALLOW_SO_NUMBERS = False
    assert not is_valid_id_number(so_number)
    assert not is_valid_so_number(so_number)


def test_not_valid_so_number():
    so_number = "25529322345"
    settings.ALLOW_SO_NUMBERS = True
    assert not is_valid_id_number(so_number)
    assert not is_valid_so_number(so_number)


@pytest.mark.parametrize(
    "string, expected_output",
    [
        ("AZ az ÀÖ ØÞßö øÿ Āſ", False),
        ("Kari-Mette", False),
        ("Kari M.", False),
        ("aaƂåå", True),
        ("!", True),
        ("÷", True),
        ("汉字", True),
    ],
)
def test_string_contains_illegal_chars(string, expected_output):
    assert string_contains_illegal_chars(string) == expected_output
