import datetime
import logging

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from django.contrib.auth import get_user_model
from django.db import transaction

import pytest

from greg.models import (
    Consent,
    ConsentChoice,
    Notification,
    OuIdentifier,
    Person,
    Sponsor,
    SponsorOrganizationalUnit,
    Identity,
    Role,
    RoleType,
    OrganizationalUnit,
    ConsentType,
)

# pylint: disable=redefined-outer-name

# faker spams the logs with localisation warnings
# see https://github.com/joke2k/faker/issues/753
logging.getLogger("faker").setLevel(logging.ERROR)


@pytest.fixture
def client() -> APIClient:
    user, _ = get_user_model().objects.get_or_create(username="test", is_staff=True)
    token, _ = Token.objects.get_or_create(user=user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION=f"Token {token.key}")
    return client


@pytest.fixture
def non_admin_client() -> APIClient:
    user, _ = get_user_model().objects.get_or_create(username="test")
    token, _ = Token.objects.get_or_create(user=user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION=f"Token {token.key}")
    return client


@pytest.fixture
def unauthenticated_client() -> APIClient:
    client = APIClient()
    return client


@pytest.fixture
def pcm_mock():
    class PCMMock:
        def publish(self, *args, **kwargs):
            return True

    return PCMMock()


@pytest.fixture
def identity_norwegian_national_id_number(person) -> Identity:
    pi = Identity.objects.create(
        person=person,
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        source="Test",
        value="12345",
    )
    return Identity.objects.get(id=pi.id)


@pytest.fixture
def identity_us_passport_1(person) -> Identity:
    pi = Identity.objects.create(
        person=person,
        type=Identity.IdentityType.PASSPORT_NUMBER,
        source="Test",
        value="US-12345",
    )
    return Identity.objects.get(id=pi.id)


@pytest.fixture
def identity_us_passport_2(person) -> Identity:
    pi = Identity.objects.create(
        person=person,
        type=Identity.IdentityType.PASSPORT_NUMBER,
        source="Test",
        value="US-67890",
    )
    return Identity.objects.get(id=pi.id)


@pytest.fixture
def person() -> Person:
    pe = Person.objects.create(
        first_name="Test",
        last_name="Tester",
        date_of_birth="2000-01-27",
    )
    return Person.objects.get(id=pe.id)


@pytest.fixture
def person_foo() -> Person:
    with transaction.atomic():
        person = Person.objects.create(
            first_name="Foo",
            last_name="Foo",
            date_of_birth="2001-01-27",
            registration_completed_date=datetime.date.today(),
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            value="foo@example.org",
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
            value="+4712345678",
        )
    return Person.objects.get(id=person.id)


@pytest.fixture
def person_bar() -> Person:
    with transaction.atomic():
        person = Person.objects.create(
            first_name="Bar",
            last_name="Bar",
            date_of_birth="2000-07-01",
            registration_completed_date=datetime.date.today(),
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            value="bar@example.org",
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
            value="+4712345679",
        )
    return Person.objects.get(id=person.id)


@pytest.fixture
def person_foobar() -> Person:
    with transaction.atomic():
        person = Person.objects.create(
            first_name="Foo", last_name="Bar", date_of_birth="1999-02-05"
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            value="foobar@example.org",
        )
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
            value="+4712345689",
        )
    return Person.objects.get(id=person.id)


@pytest.fixture
def sponsor_guy() -> Sponsor:
    sp = Sponsor.objects.create(
        feide_id="guy@example.org",
        first_name="Sponsor",
        last_name="Guy",
        work_email="sponsor_guy@example.com",
    )
    return Sponsor.objects.get(id=sp.id)


@pytest.fixture
def sponsor_foo() -> Sponsor:
    sp = Sponsor.objects.create(
        feide_id="foo@example.org",
        first_name="Sponsor",
        last_name="Foo",
        work_email="sponsor_foo@example.com",
    )
    return Sponsor.objects.get(id=sp.id)


@pytest.fixture
def person_foo_verified(person_foo, sponsor_guy) -> Identity:
    pi = Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.PASSPORT_NUMBER,
        source="Test",
        value="12345",
        verified=Identity.Verified.MANUAL,
        verified_by=sponsor_guy,
        verified_at="2021-06-15T12:34:56Z",
    )
    return Identity.objects.get(id=pi.id)


@pytest.fixture
def person_foo_not_verified(person_foo) -> Identity:
    pi = Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        source="Test",
        value="12345",
    )
    return Identity.objects.get(id=pi.id)


@pytest.fixture()
def role_type_test_guest() -> RoleType:
    rt = RoleType.objects.create(identifier="Test Guest", name_en="Test Guest")
    return RoleType.objects.get(id=rt.id)


@pytest.fixture
def unit_foo() -> OrganizationalUnit:
    ou = OrganizationalUnit.objects.create(name_en="foo_unit")
    OuIdentifier.objects.create(
        source="orgreg", name="legacy_stedkode", value="123456", orgunit=ou
    )
    return OrganizationalUnit.objects.get(id=ou.id)


@pytest.fixture
def unit_foo2() -> OrganizationalUnit:
    ou = OrganizationalUnit.objects.create(name_en="foo_unit2")
    OuIdentifier.objects.create(
        source="orgreg", name="legacy_stedkode", value="234567", orgunit=ou
    )
    return OrganizationalUnit.objects.get(id=ou.id)


@pytest.fixture
def unit_foo3() -> OrganizationalUnit:
    ou = OrganizationalUnit.objects.create(name_en="foo_unit3")
    OuIdentifier.objects.create(
        source="orgreg", name="legacy_stedkode", value="345678", orgunit=ou
    )
    return OrganizationalUnit.objects.get(id=ou.id)


@pytest.fixture
def role_person_foo(
    person_foo: Person,
    role_type_test_guest: RoleType,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
) -> Role:
    role = Role.objects.create(
        person=person_foo,
        type=role_type_test_guest,
        start_date="2021-08-02",
        end_date="2021-08-06",
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    return Role.objects.get(id=role.id)


@pytest.fixture
def role_person_foo2(
    person_foo: Person,
    role_type_foo: RoleType,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
) -> Role:
    role = Role.objects.create(
        person=person_foo,
        type=role_type_foo,
        start_date=datetime.date.today(),
        end_date=datetime.date.today() + datetime.timedelta(days=4),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    return Role.objects.get(id=role.id)


@pytest.fixture
def role_person_bar(
    person_bar: Person,
    role_type_bar: RoleType,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
) -> Role:
    role = Role.objects.create(
        person=person_bar,
        type=role_type_bar,
        start_date=datetime.date.today(),
        end_date=datetime.date.today() + datetime.timedelta(days=5),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    return Role.objects.get(id=role.id)


@pytest.fixture
def role_person_bar2(
    person_bar: Person,
    role_type_test_guest: RoleType,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
) -> Role:
    role = Role.objects.create(
        person=person_bar,
        type=role_type_test_guest,
        start_date=datetime.date.today(),
        end_date=datetime.date.today() + datetime.timedelta(days=10),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    return Role.objects.get(id=role.id)


@pytest.fixture
def role_type_foo() -> RoleType:
    rt = RoleType.objects.create(identifier="role_foo", name_en="Role Foo")
    return RoleType.objects.get(id=rt.id)


@pytest.fixture
def role_type_bar() -> RoleType:
    rt = RoleType.objects.create(identifier="role_bar", name_en="Role Bar")
    return RoleType.objects.get(id=rt.id)


@pytest.fixture
def consent_fixture(person, consent_type_foo):
    consent_given_date = "2021-06-20"
    Consent.objects.create(
        person=person, type=consent_type_foo, consent_given_at=consent_given_date
    )
    return Consent.objects.get(id=1)


@pytest.fixture
def consent_fixture_choice_yes(person, consent_type_foo):
    consent_given_date = "2021-06-20"
    Consent.objects.create(
        person=person,
        type=consent_type_foo,
        consent_given_at=consent_given_date,
        choice=consent_type_foo.choices.get(value="yes"),
    )
    return Consent.objects.get(id=1)


@pytest.fixture
def consent_type_foo() -> ConsentType:
    consent_foo = ConsentType.objects.create(
        identifier="foo",
        name_en="Foo",
        name_nb="Fu",
        name_nn="F",
        description_en="Description",
        description_nb="Beskrivelse",
        description_nn="Beskriving",
        valid_from="2018-01-20",
        user_allowed_to_change=True,
        mandatory=False,
    )
    ConsentChoice.objects.create(
        consent_type=consent_foo,
        value="yes",
        text_en="Yes",
        text_nb="Ja",
        text_nn="Ja",
    )
    ConsentChoice.objects.create(
        consent_type=consent_foo,
        value="no",
        text_en="No",
        text_nb="Nei",
        text_nn="Nei",
    )
    return ConsentType.objects.get(id=consent_foo.id)


@pytest.fixture
def sponsor_org_unit(sponsor_guy, unit_foo):
    SponsorOrganizationalUnit.objects.create(
        sponsor=sponsor_guy,
        organizational_unit=unit_foo,
        hierarchical_access=False,
        automatic=False,
    )
    return SponsorOrganizationalUnit.objects.get(id=1)


@pytest.fixture
def sponsor_org_unit_guy(sponsor_guy, unit_foo2):
    SponsorOrganizationalUnit.objects.create(
        sponsor=sponsor_guy,
        organizational_unit=unit_foo2,
        hierarchical_access=False,
        automatic=False,
    )
    return SponsorOrganizationalUnit.objects.get(id=2)


@pytest.fixture
def sponsor_org_unit_automatic(sponsor_guy, unit_foo3):
    SponsorOrganizationalUnit.objects.create(
        sponsor=sponsor_guy,
        organizational_unit=unit_foo3,
        hierarchical_access=False,
        automatic=True,
    )
    return SponsorOrganizationalUnit.objects.get(id=3)


@pytest.fixture
def notification():
    Notification.objects.create(
        identifier=5,
        object_type="foo",
        operation="bar",
        issued_at=1234,
        meta="foometa",
    )
    return Notification.objects.get(id=1)


@pytest.fixture
def looped_units():
    # the loop
    a = OrganizationalUnit.objects.create()
    b = OrganizationalUnit.objects.create(parent=a)
    c = OrganizationalUnit.objects.create(parent=b)
    d = OrganizationalUnit.objects.create(parent=c)
    e = OrganizationalUnit.objects.create(parent=d)
    a.parent = e
    a.save()
    return a, b, c, d, e


@pytest.fixture
def loop_sponsor(looped_units, unit_foo) -> Sponsor:
    """A sponsor that is connected to a loop, and a single unit."""
    loop_unit = looped_units[2]
    sp = Sponsor.objects.create()
    # Connection to the loop
    SponsorOrganizationalUnit.objects.create(
        sponsor=sp, organizational_unit=loop_unit, hierarchical_access=True
    )
    # Stand alone connection
    SponsorOrganizationalUnit.objects.create(
        sponsor=sp, organizational_unit=unit_foo, hierarchical_access=False
    )
    return sp


@pytest.fixture
def search_response():
    return {
        "external_ids": [
            {
                "person_id": 1,
                "source_system": "GREG",
                "external_id": 1,
                "id_type": "gregPersonId",
            }
        ]
    }


@pytest.fixture
def search_response2():
    return {
        "external_ids": [
            {
                "person_id": 2,
                "source_system": "GREG",
                "external_id": 2,
                "id_type": "gregPersonId",
            }
        ]
    }


@pytest.fixture
def search_response3():
    return {
        "external_ids": [
            {
                "person_id": 3,
                "source_system": "GREG",
                "external_id": 3,
                "id_type": "gregPersonId",
            }
        ]
    }


@pytest.fixture
def person_account_response_foo():
    return {
        "accounts": [
            {
                "href": "",
                "primary": True,
                "id": 1,
                "name": "foo",
            }
        ]
    }


@pytest.fixture
def person_account_response_bar():
    return {
        "accounts": [
            {
                "href": "",
                "primary": True,
                "id": 2,
                "name": "bar",
            }
        ]
    }


@pytest.fixture
def person_account_response_foobar():
    return {
        "accounts": [
            {
                "href": "",
                "primary": True,
                "id": 3,
                "name": "foobar",
            }
        ]
    }
