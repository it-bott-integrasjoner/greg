import random
from typing import List, TypeVar

from django.db import IntegrityError, connection
from faker import Faker

from greg.models import (
    Consent,
    ConsentType,
    OrganizationalUnit,
    Person,
    Identity,
    Role,
    RoleType,
    Sponsor,
)

R = TypeVar("R")


class DatabasePopulation:
    """
    Helper class for populating database with random data.
    It can be useful to see how things look in the interface for example.

    Run the file in the Django shell: exec(open('greg/tests/populate_database.py').read())
    """

    faker: Faker
    persons: List[Person] = []
    emails: List[Identity] = []
    phones: List[Identity] = []
    units: List[OrganizationalUnit] = []
    sponsors: List[Sponsor] = []
    role_types: List[RoleType] = []
    consent_types: List[ConsentType] = []
    random: random.Random

    def __init__(self, set_seed_to_zero=True):
        self.faker = Faker()

        if set_seed_to_zero:
            # Set seeds so that the generated data is always the same
            self.random = random.Random(0)
            # Note that it is possible the logic the Faker uses
            # to compute the random sequences can change when
            # the version changes
            self.faker.seed_instance(0)
        else:
            self.random = random.Random()

    def populate_database(self):
        for _ in range(10):
            first_name = self.faker.first_name()
            last_name = self.faker.last_name()

            person = Person.objects.create(
                first_name=first_name,
                last_name=last_name,
                date_of_birth=self.faker.date_of_birth(maximum_age=50),
            )
            self.persons.append(person)
            self.emails.append(
                Identity.objects.create(
                    person=person,
                    type=Identity.IdentityType.PRIVATE_EMAIL,
                    value=f"{first_name}.{last_name}@example.org",
                    source=self.faker.company(),
                )
            )
            self.phones.append(
                Identity.objects.create(
                    person=person,
                    type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
                    value=self.faker.phone_number(),
                    source=self.faker.company(),
                )
            )

        for role_type in ("Visiting Professor", "Professor Emeritus", "Consultant"):
            self.role_types.append(
                RoleType.objects.create(identifier=role_type, name_en=role_type)
            )

        for _ in range(10):
            self.units.append(
                OrganizationalUnit.objects.create(name_en=self.faker.company())
            )

        for _ in range(5):
            self.sponsors.append(
                Sponsor.objects.create(
                    feide_id=self.faker.bothify(text="???####@uio.no")
                )
            )

        for _ in range(10):
            self.consent_types.append(
                ConsentType.objects.create(
                    identifier=self.faker.slug(),
                    name_en=self.faker.sentence(nb_words=6),
                    name_nb=self.faker.sentence(nb_words=6),
                    name_nn=self.faker.sentence(nb_words=6),
                    description_en=self.faker.paragraph(nb_sentences=5),
                    description_nb=self.faker.paragraph(nb_sentences=5),
                    description_nn=self.faker.paragraph(nb_sentences=5),
                    user_allowed_to_change=self.random.random() > 0.5,
                )
            )

        self.__add_random_person_role_connections()
        self.__add_random_person_consent_connections()
        self.__add_random_sponsor_unit_connections()
        self.__add_random_person_identification_connections()

    def __add_random_person_identification_connections(self, connections_to_create=5):
        identifier_count = 0
        while identifier_count < connections_to_create:
            person = random.choice(self.persons)
            identity_type = random.choice(Identity.IdentityType.choices)[0]

            if self.random.random() > 0.5:
                sponsor = random.choice(self.sponsors)
                verified_at = self.faker.date_time_this_year()
                identity_type = random.choice(Identity.IdentityType.choices)[0]
                verified = self.faker.text(max_nb_chars=50)
            else:
                sponsor = None
                verified_at = None
                verified = ""

            Identity.objects.create(
                person=person,
                type=identity_type,
                source=self.faker.text(max_nb_chars=50),
                value=self.faker.numerify("##################"),
                verified_by=sponsor,
                verified=verified,
                verified_at=verified_at,
            )

            identifier_count += 1

    def __add_random_sponsor_unit_connections(self, connections_to_create=5):
        sponsor_unit_count = 0
        while sponsor_unit_count < connections_to_create:
            sponsor = random.choice(self.sponsors)
            unit = random.choice(self.units)

            sponsor.units.add(
                unit,
                through_defaults={"hierarchical_access": self.random.random() > 0.5},
            )
            sponsor_unit_count += 1

    def __add_random_person_role_connections(self, connections_to_create=5):
        person_role_count = 0
        while person_role_count < connections_to_create:
            try:
                Role.objects.create(
                    person=random.choice(self.persons),
                    type=random.choice(self.role_types),
                    orgunit=random.choice(self.units),
                    start_date=self.faker.date_this_decade(),
                    end_date=self.faker.date_this_decade(
                        before_today=False, after_today=True
                    ),
                    contact_person_unit=self.faker.name(),
                    available_in_search=self.random.random() > 0.5,
                    sponsor=random.choice(self.sponsors),
                )
                person_role_count += 1
            except IntegrityError:
                # This is probably caused by the same person
                # added with the same role to the same unit.
                # Try again and see if the randomly selected
                # values will make it pass on the next attempt.
                pass

    def __add_random_person_consent_connections(self, number_of_connections_to_make=5):
        person_consent_count = 0
        while person_consent_count < number_of_connections_to_make:
            person = random.choice(self.persons)
            consent_type = random.choice(self.consent_types)
            Consent.objects.create(
                person=person,
                type=consent_type,
                consent_given_at=self.faker.date_this_decade(),
            )
            person_consent_count += 1

    def truncate_tables(self):
        with connection.cursor() as cursor:
            for table in (
                "greg_consent",
                "greg_consenttype",
                "greg_notification",
                "greg_identity",
                "greg_person",
                "greg_sponsororganizationalunit",
                "greg_sponsor",
                "greg_organizationalunit",
                "greg_role",
                "greg_roletype",
            ):
                cursor.execute(f"DELETE FROM {table}")


if __name__ == "__main__":
    database_population = DatabasePopulation()
    database_population.truncate_tables()
    database_population.populate_database()
