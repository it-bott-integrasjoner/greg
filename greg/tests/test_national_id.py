import pytest

from greg.utils import is_valid_norwegian_national_id_number

test_data = [
    # Mostly random numbers from http://www.fnrinfo.no/Verktoy/FinnLovlige_Tilfeldig.aspx
    ("13063626672", False, True),
    ("09095114412", False, True),
    ("02048538757", False, True),
    ("12042335418", False, True),
    ("16061739592", False, True),
    ("05087648003", False, True),
    ("22051602882", False, True),
    ("11120618212", False, True),
    ("19045838326", False, True),
    ("17041579641", False, True),
    ("20064632362", False, True),
    ("12118913939", False, True),
    ("04062141242", False, True),
    ("10075049345", False, True),
    ("17093749006", False, True),
    ("07049226653", False, True),
    ("25110899915", False, True),
    ("01014000719", False, True),
    ("19035205023", False, True),
    ("12345678912", False, False),
    ("410185", True, False),
    # Test cases from https://github.com/navikt/fnrvalidator/blob/master/tests/fnr.spec.js
    ("13097248022", False, True),
    ("29029648784", False, True),
    ("29020075838", False, True),
    ("29020112345", False, False),
    ("15021951940", False, True),
    ("1234567890", False, False),
    ("123456789101", False, False),
    ("1234567891A", False, False),
    ("13097248032", False, False),
    ("13097248023", False, False),
    ("32127248022", False, False),
    ("13137248022", False, False),
    ("53097248016", True, True),
]


@pytest.mark.parametrize("test_number, is_dnumber, valid", test_data)
def test_register_guest(test_number, is_dnumber, valid):
    assert is_valid_norwegian_national_id_number(test_number, is_dnumber) == valid
