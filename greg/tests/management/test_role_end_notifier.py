import pytest

from django.core.management import call_command
from django.core.management.base import CommandError
from django_q.tasks import Schedule


@pytest.mark.django_db
def test_schedule_tasks():
    assert Schedule.objects.all().count() == 0
    call_command("role_end_notifier", "--guests", "--schedule")
    assert Schedule.objects.all().count() == 1
    sc = Schedule.objects.first()
    assert sc.func == "greg.tasks.notify_role_end.notify_guests_roles_ending"


@pytest.mark.django_db
def test_only_one_notify_argument_allowed():
    with pytest.raises(CommandError):
        call_command("role_end_notifier", "--guests", "--schedule", "--sponsors")
