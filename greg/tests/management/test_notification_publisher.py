import json
import pytest
from django.test import override_settings

from greg.management.commands.notification_publisher import (
    handle_one_notification,
    create_cloud_event_payload,
    generate_routing_key,
)
from greg.models import Notification

# pylint: disable=redefined-outer-name


@pytest.fixture
def role_type_notification():
    Notification.objects.create(
        identifier=5,
        object_type="RoleType",
        operation="update",
        issued_at=1234,
        meta={
            "integer": 123,
            "string": "foo",
        },
    )
    return Notification.objects.get(id=1)


@pytest.mark.django_db
def test_handle_one_notification(role_type_notification, pcm_mock):
    """Check that Notifications are deleted when published to message queue"""
    assert Notification.objects.count() == 1
    handle_one_notification(role_type_notification, pcm_mock, exchange="bar")
    assert Notification.objects.count() == 0


@pytest.mark.django_db
def test_create_cloud_event_payload(role_type_notification):
    payload = create_cloud_event_payload(role_type_notification)
    assert json.loads(payload) == {
        "id": "1",
        "source": "greg:local:unittest",
        "specversion": "1.0",
        "type": "role_type.update",
        "data": {"integer": 123, "string": "foo"},
    }


@pytest.mark.django_db
def test_generate_routing_key(role_type_notification):
    assert generate_routing_key(role_type_notification) == "role_type.update"


@pytest.mark.django_db
@override_settings(NOTIFICATION_ROUTING_KEY_PREFIX="foo.")
def test_override_generate_routing_key(role_type_notification):
    assert generate_routing_key(role_type_notification) == "foo.role_type.update"
