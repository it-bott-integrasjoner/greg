import pytest

from django.core.management import call_command

from greg.models import Person, Role


@pytest.mark.django_db
def test_import_guests(sponsor_org_unit, role_type_foo, role_type_bar):
    """Test import of two persons from two files"""

    assert Person.objects.count() == 0
    assert Role.objects.count() == 0

    # One guest with missing required id, one okay
    call_command("import_guests", "./greg/tests/management/fixtures/guests.json")
    assert Person.objects.count() == 1
    assert Role.objects.count() == 1

    # Two guests with ok ids
    call_command("import_guests", "./greg/tests/management/fixtures/guests2.json")
    assert Person.objects.count() == 2
    assert Role.objects.count() == 2

    # First file again, but a matching id so we import the new role we missed the first
    # time
    call_command("import_guests", "./greg/tests/management/fixtures/guests.json")
    assert Person.objects.count() == 2
    assert Role.objects.count() == 3


@pytest.mark.django_db
def test_import_guests_update_existing(sponsor_org_unit, role_type_foo, role_type_bar):
    """Verify that update existing works correctly"""

    assert Person.objects.count() == 0
    assert Role.objects.count() == 0

    # One guest with missing required id, one okay
    call_command(
        "import_guests",
        "./greg/tests/management/fixtures/guests.json",
        "--update-existing",
    )
    assert Person.objects.count() == 1
    assert Role.objects.count() == 1

    # Two guests with ok ids
    call_command(
        "import_guests",
        "./greg/tests/management/fixtures/guests2.json",
        "--update-existing",
    )
    assert Person.objects.count() == 2
    assert Role.objects.count() == 2
    pe = Person.objects.get(first_name="Foo")
    assert pe.last_name == "Bar"
    pe = Person.objects.get(first_name="Test")
    assert pe.roles.first().comments == "Updated comment"
    # First file again, but a matching id so we import the new role
    call_command(
        "import_guests",
        "./greg/tests/management/fixtures/guests.json",
        "--update-existing",
    )
    assert Person.objects.count() == 2
    assert Role.objects.count() == 3
    pe = Person.objects.get(first_name="Foo")
    assert pe.last_name == "New name to be ignored unless specified"
    pe = Person.objects.get(first_name="Test")
    assert pe.roles.first().comments == "First comment"
