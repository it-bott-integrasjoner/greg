import datetime
import pytest
from django.conf import settings
from django.core.management import call_command
from django_q.tasks import Schedule

from greg.models import Sponsor, SponsorOrganizationalUnit

# pylint: disable=redefined-outer-name


class MockResponse:
    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response


@pytest.fixture
def group_response():
    created_at = (datetime.datetime.now() - datetime.timedelta(days=1)).isoformat()
    return MockResponse(
        f"""{{
            "href": "",
            "id": 1,
            "name": "",
            "description": "",
            "created_at": "{created_at}",
            "contexts": ["das"],
            "visibility": "",
            "moderators": ""
        }}"""
    )


@pytest.fixture
def group_members_response():
    return MockResponse("""{"members":[{"href": "asd", "type": "person", "id": 1}]}""")


@pytest.fixture
def group_members_response2():
    return MockResponse("""{"members":[{"href": "asd", "type": "person", "id": 2}]}""")


@pytest.fixture
def person_response():
    return MockResponse(
        """{
        "contexts": [""],
        "href": "",
        "names": [
            {"source_system":"DFO_SAP", "variant": "FIRST", "name": "Ola"},
            {"source_system":"DFO_SAP", "variant": "LAST", "name": "Nordmann"}
        ],
        "id": 1}"""
    )


@pytest.fixture
def person_response2():
    return MockResponse(
        """{
        "contexts": [""],
        "href": "",
        "names": [
            {"source_system":"DFO_SAP", "variant": "FIRST", "name": "Kari"},
            {"source_system":"DFO_SAP", "variant": "LAST", "name": "Nordmann"}
        ],
        "id": 2}"""
    )


@pytest.fixture
def account_response():
    return MockResponse(
        """{"accounts": [{
        "href": "",
        "primary": true,
        "id": 1,
        "name": "olanord"}]}"""
    )


@pytest.fixture
def account_response2():
    return MockResponse(
        """{"accounts": [{
        "href": "",
        "primary": true,
        "id": 2,
        "name": "karinor"}]}"""
    )


@pytest.mark.django_db
def test_import_sponsors_from_cerebrum(
    requests_mock,
    group_response,
    group_members_response,
    group_members_response2,
    person_response,
    person_response2,
    account_response,
    account_response2,
    sponsor_org_unit,
    unit_foo2,
):
    """Check that only non manual sponsors are imported and linked"""
    settings.CEREBRUM_CLIENT = {
        "url": "http://example.com/cerebrum/",
        "headers": {"X-Gravitee-Api-Key": "fake-key"},
    }
    settings.CEREBRUM_MANUAL_SPONSOR_UNITS = ["123456"]
    requests_mock.get(
        "http://example.com/cerebrum/v1/groups/adm-leder-123456",
        text=group_response.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/groups/adm-leder-123456/members/",
        text=group_members_response.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1",
        text=person_response.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1/accounts",
        text=account_response.json(),
    )

    requests_mock.get(
        "http://example.com/cerebrum/v1/groups/adm-leder-234567",
        text=group_response.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/groups/adm-leder-234567/members/",
        text=group_members_response2.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/2",
        text=person_response2.json(),
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/2/accounts",
        text=account_response2.json(),
    )
    assert Sponsor.objects.all().count() == 1
    assert SponsorOrganizationalUnit.objects.count() == 1
    call_command("import_sponsors_from_cerebrum")
    # One from unit with one sponsor
    # One excluded from unit with stedkode in manual list
    assert Sponsor.objects.all().count() == 2
    assert SponsorOrganizationalUnit.objects.count() == 2

    # when a SponsorOrganizationalUnit with source != 'cerebrum'
    # and automatic = False exists, replace it with an automatic one
    sunit_lookup = {
        "organizational_unit__identifiers__name": "legacy_stedkode",
        "organizational_unit__identifiers__value": "234567",
    }
    sunit = SponsorOrganizationalUnit.objects.filter(**sunit_lookup).first()
    assert isinstance(sunit, SponsorOrganizationalUnit)
    sunit.source = "manual"
    sunit.automatic = False
    sunit.save()
    call_command("import_sponsors_from_cerebrum")
    sunit = SponsorOrganizationalUnit.objects.filter(**sunit_lookup).first()
    assert isinstance(sunit, SponsorOrganizationalUnit)
    assert sunit.source == "cerebrum"
    assert sunit.automatic


@pytest.mark.django_db
def test_schedule():
    assert Schedule.objects.all().count() == 0
    call_command("import_sponsors_from_cerebrum", "--schedule")
    assert Schedule.objects.all().count() == 1
    schedule = Schedule.objects.first()
    assert schedule is not None
    assert schedule.func == "greg.tasks.sponsors.import_sponsors_from_cerebrum"
    call_command("import_sponsors_from_cerebrum", "--schedule")
    assert Schedule.objects.all().count() == 2
    call_command("import_sponsors_from_cerebrum", "--schedule")
    assert Schedule.objects.all().count() == 3


@pytest.mark.django_db
def test_schedule_args():
    assert Schedule.objects.all().count() == 0
    call_command("import_sponsors_from_cerebrum", "--schedule", "--root", "1")
    schedule = Schedule.objects.first()
    assert schedule is not None
    assert schedule.kwargs == "{'root': '1'}"
