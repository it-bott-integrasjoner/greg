import datetime

import pytest
from django.core.management import call_command
from django.test import override_settings
from django_q.models import Schedule
from orgreg_client.models import OrgUnit, ExternalKey, LocalisedValue

from greg.tasks.orgreg import OrgregImporter, OrganizationalUnit
from greg.models import OuIdentifier

# pylint: disable=redefined-outer-name


@pytest.fixture
def org_unit():
    org_unit = OrgUnit(
        ou_id=1,
        valid_from=datetime.date(year=2020, month=2, day=6),
        external_keys=[
            ExternalKey(type="legacy_stedkode", source_system="sapuio", value="1")
        ],
        short_name=LocalisedValue(nob="FOO", sme=None, nno=None, eng=None),
        start_date=None,
        category=None,
        valid_to=None,
        end_date=None,
        email=None,
        note=None,
        postal_address=None,
        visit_address=None,
        building=None,
        phone=None,
        fax=None,
        parent=None,
    )
    return org_unit


@pytest.mark.django_db
def test_command_with_schedule_flag_schedules_task():
    assert Schedule.objects.all().count() == 0
    call_command("import_from_orgreg", "--schedule")
    assert Schedule.objects.all().count() == 1
    sc = Schedule.objects.first()
    assert sc.func == "greg.tasks.orgreg.import_from_orgreg"


@pytest.mark.django_db
def test_upsert_extra_identities(org_unit):
    organizational_unit = OrganizationalUnit.objects.create(
        name_nb="foo_nb", name_en="foo_en"
    )

    org_importer = OrgregImporter()
    org_importer.processed[org_unit.ou_id] = organizational_unit
    org_importer.upsert_extra_identities(org_unit)
    assert OuIdentifier.objects.all().count() == 2


@pytest.mark.django_db
@override_settings(ORGREG_EXTRA_IDS=[{"fieldname": "foo", "subfield": "nob"}])
def test_upsert_wrong_extra_identities(org_unit):
    organizational_unit = OrganizationalUnit.objects.create(
        name_nb="foo_nb", name_en="foo_en"
    )
    org_importer = OrgregImporter()
    org_importer.processed[org_unit.ou_id] = organizational_unit
    org_importer.upsert_extra_identities(org_unit)
    assert OuIdentifier.objects.all().count() == 0


@pytest.mark.django_db
def test_upsert_extra_identities_update(org_unit):
    """
    Verify that already imported values are updated
    """
    ou = OrganizationalUnit.objects.create(name_nb="foo_nb", name_en="foo_en")
    OuIdentifier.objects.create(name="short_name", value="OLD", orgunit=ou)
    OuIdentifier.objects.create(
        name="legacy_stedkode", value="OLD", orgunit=ou, source="sapuio"
    )
    assert OuIdentifier.objects.all().count() == 2

    org_importer = OrgregImporter()
    org_importer.processed[org_unit.ou_id] = ou
    org_importer.upsert_extra_identities(org_unit)
    assert OuIdentifier.objects.all().count() == 2
    assert ou.identifiers.filter(name="short_name").first().value == "FOO"
    assert ou.identifiers.filter(name="legacy_stedkode").first().value == "1"
