import pytest
from django.core.management import call_command
from django_q.models import Schedule


@pytest.mark.django_db
def test_schedule_command():
    assert Schedule.objects.all().count() == 0
    call_command("delete_expired_roles", "--schedule")
    assert Schedule.objects.all().count() == 1
    sc = Schedule.objects.first()
    assert sc.func == "greg.tasks.delete_expired_persons.delete_expired"
