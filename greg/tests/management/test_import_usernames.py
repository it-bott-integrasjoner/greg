import datetime
import pytest

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import override_settings
from django_q.tasks import Schedule

from greg.models import Person, Identity

# pylint: disable=redefined-outer-name


@pytest.fixture
def person_one_second_account():
    person = Person.objects.create(
        first_name="Foo",
        last_name="Foo",
        registration_completed_date=datetime.date.today(),
    )
    Identity.objects.create(
        person=person,
        type=Identity.IdentityType.FEIDE_ID,
        value="foo@inst.no",
        source="cerebrum",
    )
    return Person.objects.get(id=person.id)


@pytest.fixture
def person_one(person_foo):
    person_foo.registration_completed_date = datetime.date.today()
    person_foo.save()
    Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.PASSPORT_NUMBER,
        value="HHH111111",
    )
    return Person.objects.get(id=person_foo.id)


@pytest.fixture
def person_two(person_bar):
    person_bar.registration_completed_date = datetime.date.today()
    person_bar.save()
    Identity.objects.create(
        person=person_bar,
        type=Identity.IdentityType.FEIDE_ID,
        source="cerebrum",
        value="not_bar@inst.no",
    )
    return Person.objects.get(id=person_bar.id)


@pytest.fixture
def person_three(person_foobar):
    person_foobar.registration_completed_date = datetime.date.today()
    person_foobar.save()
    Identity.objects.create(
        person=person_foobar,
        type=Identity.IdentityType.FEIDE_ID,
        source="feide",
        value="foobar@test.no",
    )
    return Person.objects.get(id=person_foobar.id)


@pytest.fixture
def person_not_registered():
    person = Person.objects.create(first_name="Not", last_name="Registered")
    return Person.objects.get(id=person.id)


@pytest.mark.django_db
def test_schedule(person_one, person_two, person_three):
    assert Schedule.objects.all().count() == 0
    call_command("import_usernames_from_cerebrum", "--all-persons", "--schedule")
    assert Schedule.objects.all().count() == 1
    sc = Schedule.objects.first()
    assert sc.func == "greg.tasks.usernames.import_usernames"
    call_command("import_usernames_from_cerebrum", "--without-usernames", "--schedule")
    assert Schedule.objects.all().count() == 2
    call_command("import_usernames_from_cerebrum", "--with-username", "--schedule")
    assert Schedule.objects.all().count() == 3


@pytest.mark.django_db
def test_only_one_import_argument_allowed():
    with pytest.raises(CommandError):
        call_command(
            "import_usernames_from_cerebrum",
            "--without-usernames",
            "--with-username",
            "--schedule",
        )


@override_settings(INSTANCE_NAME="inst")
@pytest.mark.django_db
def test_import_wo_feide_id(
    requests_mock,
    search_response,
    person_account_response_foo,
    person_one,
):
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=1",
        json=search_response,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1/accounts",
        json=person_account_response_foo,
    )
    usr_id_type = Identity.IdentityType.FEIDE_ID
    assert not person_one.identities.filter(type=usr_id_type)

    call_command("import_usernames_from_cerebrum", "--without-usernames", "--commit")
    person_identities = person_one.identities.filter(type=usr_id_type)
    assert len(person_identities) == 1
    assert person_identities[0].value == "foo@inst.no"
    assert person_identities[0].verified_at is not None


@override_settings(INSTANCE_NAME="inst")
@pytest.mark.django_db
def test_import_w_feide_id(
    requests_mock,
    search_response2,
    person_account_response_bar,
    person_two,
):
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=1",
        json=search_response2,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/2/accounts",
        json=person_account_response_bar,
    )
    usr_id_type = Identity.IdentityType.FEIDE_ID
    person_identities = person_two.identities.filter(type=usr_id_type)
    assert len(person_identities) == 1
    assert person_identities[0].value == "not_bar@inst.no"
    call_command("import_usernames_from_cerebrum", "--with-usernames", "--commit")
    person_identities = person_two.identities.filter(type=usr_id_type)
    assert len(person_identities) == 1
    assert person_identities[0].value == "bar@inst.no"


@override_settings(INSTANCE_NAME="inst")
@pytest.mark.django_db
def test_different_feide_id_already_exists(
    requests_mock,
    search_response3,
    person_account_response_foobar,
    person_three,
):
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=1",
        json=search_response3,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/3/accounts",
        json=person_account_response_foobar,
    )
    usr_id_type = Identity.IdentityType.FEIDE_ID
    person_identities = person_three.identities.filter(type=usr_id_type)
    assert len(person_identities) == 1
    assert person_identities[0].value == "foobar@test.no"
    call_command("import_usernames_from_cerebrum", "--without-usernames", "--commit")
    person_identities = person_three.identities.filter(type=usr_id_type)
    assert len(person_identities) == 2
    assert person_identities[0].value == "foobar@test.no"
    assert person_identities[1].value == "foobar@inst.no"


@override_settings(INSTANCE_NAME="inst")
@pytest.mark.django_db
def test_import_usernames(
    requests_mock,
    search_response,
    search_response2,
    search_response3,
    person_account_response_foo,
    person_account_response_bar,
    person_account_response_foobar,
    person_one,
    person_two,
    person_three,
):
    """
    This test is mostly just all the other three tests in
    one to check that all-persons does all the same stuff
    """
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=1",
        json=search_response,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=2",
        json=search_response2,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=3",
        json=search_response3,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1/accounts",
        json=person_account_response_foo,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/2/accounts",
        json=person_account_response_bar,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/3/accounts",
        json=person_account_response_foobar,
    )
    usr_id_type = Identity.IdentityType.FEIDE_ID
    person_identities_two = person_two.identities.filter(type=usr_id_type)
    person_identities_three = person_three.identities.filter(type=usr_id_type)
    assert not person_one.identities.filter(type=usr_id_type)
    assert len(person_identities_two) == 1
    assert person_identities_two[0].value == "not_bar@inst.no"
    assert len(person_identities_three) == 1
    assert person_identities_three[0].value == "foobar@test.no"

    call_command("import_usernames_from_cerebrum", "--all-persons", "--commit")
    person_identities_one = person_one.identities.filter(type=usr_id_type)
    person_identities_two = person_two.identities.filter(type=usr_id_type)
    person_identities_three = person_three.identities.filter(type=usr_id_type)

    assert len(person_identities_one) == 1
    assert person_identities_one[0].value == "foo@inst.no"
    assert len(person_identities_two) == 1
    assert person_identities_two[0].value == "bar@inst.no"
    assert len(person_identities_three) == 2
    assert person_identities_three[0].value == "foobar@test.no"
    assert person_identities_three[1].value == "foobar@inst.no"


@override_settings(INSTANCE_NAME="inst")
@pytest.mark.django_db
def test_guest_registered_twice(
    requests_mock,
    search_response,
    person_account_response_foo,
    person_one,
    person_one_second_account,
    caplog,
):
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "source_system=GREG&id_type=gregPersonId&external_id=1",
        json=search_response,
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1/accounts",
        json=person_account_response_foo,
    )
    call_command("import_usernames_from_cerebrum", "--without-usernames", "--commit")
    assert (
        "UNIQUE constraint failed: greg_identity.type, greg_identity.value, "
        "greg_identity.source. There might be two person objects for person_id=1"
        in caplog.text
    )


@pytest.mark.django_db
def test_not_registered_guest(person_not_registered, caplog):
    call_command("import_usernames_from_cerebrum", "--all", "--commit")
    assert "Looking up username for person_id=1" not in caplog.text
