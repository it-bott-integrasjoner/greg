import pytest

from django.core.management import call_command
from django_q.tasks import Schedule


@pytest.mark.django_db
def test_schedule_tasks():
    assert Schedule.objects.all().count() == 0
    call_command("remove_ou_unaffiliated_sponsors", "--schedule")
    assert Schedule.objects.all().count() == 1
    sc = Schedule.objects.first()
    assert (
        sc.func
        == "greg.tasks.remove_ou_unaffiliated_sponsors.remove_ou_unaffiliated_sponsors"
    )
