import pytest


@pytest.mark.django_db
def test_sponsor_org_repr(sponsor_org_unit):
    assert (
        repr(sponsor_org_unit)
        == "SponsorOrganizationalUnit(id=1, sponsor=Sponsor(id=1, feide_id='guy@example.org', "
        "first_name='Sponsor', last_name='Guy', work_email='sponsor_guy@example.com'), "
        "organizational_unit=OrganizationalUnit(id=1, name_en='foo_unit', parent=None), "
        "hierarchical_access=False)"
    )
