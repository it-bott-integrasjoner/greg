import pytest


@pytest.mark.django_db
def test_notification_repr(notification):
    assert (
        repr(notification) == "Notification(id=1, identifier=5, object_type='foo', "
        "operation='bar', issued_at=1234, meta='foometa')"
    )
