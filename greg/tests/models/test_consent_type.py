import pytest


@pytest.mark.django_db
def test_consent_type_repr(consent_type_foo):
    assert (
        repr(consent_type_foo) == "ConsentType(id=1, identifier='foo', name_en='Foo', "
        "valid_from=datetime.date(2018, 1, 20), user_allowed_to_change=True)"
    )


@pytest.mark.django_db
def test_consent_type_str(consent_type_foo):
    assert str(consent_type_foo) == "Foo (foo)"
