from datetime import timedelta
from functools import partial

import pytest
from django.utils import timezone
from django.test import override_settings

from greg.models import (
    Consent,
    ConsentChoice,
    ConsentType,
    OrganizationalUnit,
    Person,
    Identity,
    Role,
    RoleType,
    Sponsor,
)

# pylint: disable=redefined-outer-name

role_with = partial(
    Role.objects.create,
    start_date="2020-03-05",
    end_date="2020-06-10",
    contact_person_unit="Contact Person",
    available_in_search=True,
)

_a_year_ago = timezone.now() - timedelta(days=365)
_a_year_into_future = timezone.now() + timedelta(days=365)


@pytest.fixture
def person_registered_past() -> Person:
    Person.objects.create(
        first_name="Test",
        last_name="Tester",
        date_of_birth="2000-01-27",
        registration_completed_date=_a_year_ago,
    )
    return Person.objects.get(id=1)


@pytest.fixture
def person_registered_future() -> Person:
    Person.objects.create(
        first_name="Test",
        last_name="Tester",
        date_of_birth="2000-01-27",
        registration_completed_date=_a_year_into_future,
    )
    return Person.objects.get(id=1)


@pytest.fixture
def feide_id() -> Identity:
    return Identity(
        type=Identity.IdentityType.FEIDE_ID,
        verified_at=_a_year_ago,
    )


@pytest.fixture
def verified_national_id_number() -> Identity:
    return Identity(
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        verified_at=_a_year_ago,
        value="12118913939",
    )


@pytest.fixture
def passport() -> Identity:
    return Identity(
        type=Identity.IdentityType.PASSPORT_NUMBER,
        verified_at=_a_year_ago,
    )


@pytest.fixture
def unverified_passport() -> Identity:
    return Identity(type=Identity.IdentityType.PASSPORT_NUMBER)


@pytest.fixture
def migration_id() -> Identity:
    return Identity(
        type=Identity.IdentityType.MIGRATION_ID,
        verified_at=_a_year_ago,
    )


@pytest.fixture
def national_id_card() -> Identity:
    return Identity(
        type=Identity.IdentityType.NATIONAL_ID_CARD_NUMBER,
        verified_at=_a_year_ago,
    )


@pytest.fixture
def future_identity() -> Identity:
    return Identity(
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        verified_at=_a_year_into_future,
    )


@pytest.fixture
def feide_verified(person: Person, feide_id: Identity) -> Person:
    person.identities.add(feide_id, bulk=False)
    return person


@pytest.mark.django_db
def test_add_multiple_roles_to_person(
    person: Person, role_type_foo: RoleType, role_type_bar: RoleType
):
    ou = OrganizationalUnit.objects.create(name_en="Test unit")
    role_with(
        person=person,
        type=role_type_foo,
        orgunit=ou,
        sponsor=Sponsor.objects.create(feide_id="foosponsor@uio.no"),
    )
    role_with(
        person=person,
        type=role_type_bar,
        orgunit=ou,
        sponsor=Sponsor.objects.create(feide_id="barsponsor@uio.no"),
    )
    assert person.roles.count() == 2


@pytest.mark.django_db
def test_identities(person: Person, feide_id: Identity, passport: Identity):
    person.identities.add(feide_id, bulk=False)
    assert list(person.identities.all()) == [feide_id]
    person.identities.add(passport, bulk=False)
    assert list(person.identities.all()) == [feide_id, passport]


@pytest.mark.django_db
def test_is_registered_incomplete(person):
    assert person.registration_completed_date is None
    assert not person.is_registered


@pytest.mark.django_db
def test_is_registered_completed_in_past(person_registered_past):
    assert person_registered_past.is_registered


@pytest.mark.django_db
def test_is_registered_completed_in_future(person_registered_future):
    assert not person_registered_future.is_registered


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("identity_type", "is_verified"),
    (
        (Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, True),
        (Identity.IdentityType.PASSPORT_NUMBER, True),
        (Identity.IdentityType.PRIVATE_EMAIL, False),
        (Identity.IdentityType.PRIVATE_MOBILE_NUMBER, False),
        (Identity.IdentityType.FEIDE_ID, False),
    ),
)
def test_is_verified(identity_type, is_verified, person):
    identity = Identity(type=identity_type, verified_at=_a_year_ago)
    person.identities.add(identity, bulk=False)
    assert person.is_verified == is_verified


@pytest.mark.django_db
def test_test_is_verified_multiple_identities(person, feide_id, passport):
    person.identities.add(feide_id, passport, bulk=False)
    assert person.is_verified


@pytest.mark.django_db
def test_is_verified_at_identity_is_unverified(person, unverified_passport):
    person.identities.add(unverified_passport, bulk=False)
    assert not person.is_verified


@pytest.mark.django_db
def test_is_verified_mixed_verified_and_unverified_identities(
    person,
    verified_national_id_number,
    unverified_passport,
    future_identity,
):
    person.identities.add(
        verified_national_id_number, unverified_passport, future_identity, bulk=False
    )
    assert person.is_verified


@pytest.mark.django_db
def test_is_verified_extra_id_types(
    person,
    person_foo,
    person_bar,
    national_id_card,
    verified_national_id_number,
    migration_id,
):
    person.identities.add(migration_id, bulk=False)
    person_foo.identities.add(national_id_card, bulk=False)
    person_bar.identities.add(verified_national_id_number, bulk=False)
    with override_settings(
        ALLOWED_VERIFIED_ID_TYPES=[
            "migration_id",
            "national_id_card_number",
        ]
    ):
        assert person.is_verified
        assert person_foo.is_verified
        assert not person_bar.is_verified
    assert not person.is_verified
    assert not person_foo.is_verified
    assert person_bar.is_verified


@pytest.mark.django_db
def test_is_verified_in_future(person, future_identity):
    person.identities.add(future_identity, bulk=False)
    assert not person.is_verified


@pytest.mark.django_db
def test_person_str(person: Person):
    assert str(person) == "Test Tester (1)"


@pytest.mark.django_db
def test_person_repr(person: Person):
    assert repr(person) == "Person(id=1, first_name='Test', last_name='Tester')"


@pytest.mark.django_db
def test_person_has_mandatory_consents(person):
    """
    No consents exists.

    Should return True
    """
    assert person.has_mandatory_consents is True


@pytest.mark.django_db
def test_person_has_mandatory_consents_only_optional(person):
    """
    An optional consent exists, and the person has not given consent.

    Should return True
    """
    ConsentType.objects.create(user_allowed_to_change=True)
    assert person.has_mandatory_consents is True


@pytest.mark.django_db
def test_person_has_mandatory_consents_mandatory_exists(person):
    """
    A mandatory consent exists, and the person has not given it.

    Should return False
    """
    ConsentType.objects.create(user_allowed_to_change=True, mandatory=True)
    assert person.has_mandatory_consents is False


@pytest.mark.django_db
def test_person_has_mandatory_consents_mandatory_given(person):
    """
    A mandatory consent exists, and the person has given it.

    Should return True
    """
    ct = ConsentType.objects.create(user_allowed_to_change=True, mandatory=True)
    cc = ConsentChoice.objects.create(consent_type=ct)
    Consent.objects.create(
        person=person, type=ct, choice=cc, consent_given_at=timezone.now()
    )
    assert person.has_mandatory_consents is True
