import pytest


@pytest.mark.django_db
def test_roletype_str(role_type_foo):
    assert str(role_type_foo) == "Role Foo (role_foo)"


@pytest.mark.django_db
def test_role_type_repr(role_type_foo):
    assert (
        repr(role_type_foo)
        == "RoleType(pk=1, identifier='role_foo', name_nb='', name_en='Role Foo')"
    )
