import datetime

import pytest

from greg.models import (
    Person,
    ConsentType,
    Consent,
)


@pytest.mark.django_db
def test_add_consent_to_person(person: Person, consent_type_foo: ConsentType):
    consent_given_date = "2021-06-20"
    Consent.objects.create(
        person=person, type=consent_type_foo, consent_given_at=consent_given_date
    )
    consents = Consent.objects.filter(person_id=person.id)

    assert len(consents) == 1
    assert consents[0].person_id == person.id
    assert consents[0].type_id == consent_type_foo.id
    assert consents[0].consent_given_at == datetime.date(2021, 6, 20)


@pytest.mark.django_db
def test_add_not_acknowledged_consent_to_person(
    person: Person, consent_type_foo: ConsentType
):
    Consent.objects.create(person=person, type=consent_type_foo)
    consents = Consent.objects.filter(person_id=person.id)
    assert len(consents) == 1
    assert consents[0].person_id == person.id
    assert consents[0].type_id == consent_type_foo.id
    assert consents[0].consent_given_at is None


@pytest.mark.django_db
def test_consent_repr(consent_fixture):
    assert (
        repr(consent_fixture)
        == "Consent(id=1, person=Person(id=1, first_name='Test', last_name='Tester'), "
        "type=ConsentType(id=1, identifier='foo', name_en='Foo', "
        "valid_from=datetime.date(2018, 1, 20), user_allowed_to_change=True), "
        "consent_given_at=datetime.date(2021, 6, 20))"
    )
