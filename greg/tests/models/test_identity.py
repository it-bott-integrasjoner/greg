import pytest


@pytest.mark.django_db
def test_identity_repr(person_foo_verified):
    assert (
        repr(person_foo_verified)
        == "Identity(id=3, person_id=1, type='passport_number', source='Test', value='12345', "
        "verified_by=Sponsor(id=1, feide_id='guy@example.org', first_name='Sponsor', "
        "last_name='Guy', work_email='sponsor_guy@example.com'), "
        "verified_at=datetime.datetime(2021, 6, 15, 12, 34, 56, tzinfo=datetime.timezone.utc))"
    )
