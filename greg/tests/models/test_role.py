import pytest


@pytest.mark.django_db
def test_role_repr(role_person_foo):
    assert (
        repr(role_person_foo)
        == "Role(id=1, person=Person(id=1, first_name='Foo', last_name='Foo'), "
        "type=RoleType(pk=1, identifier='Test Guest', name_nb='', name_en='Test Guest'))"
    )
