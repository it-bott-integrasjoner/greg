import pytest

from greg.models import OrganizationalUnit


@pytest.mark.django_db
def test_set_parent():
    parent = OrganizationalUnit.objects.create()
    child = OrganizationalUnit.objects.create(parent=parent)
    assert list(OrganizationalUnit.objects.filter(parent__id=parent.id)) == [child]


@pytest.mark.django_db
def test_org_repr(unit_foo):
    assert repr(unit_foo) == "OrganizationalUnit(id=1, name_en='foo_unit', parent=None)"


@pytest.mark.django_db
def test_org_str(unit_foo):
    assert str(unit_foo) == " (foo_unit) (None) (123456)"


@pytest.mark.django_db
def test_fetch_tree_loop(looped_units):
    a = looped_units[0]
    units = a.fetch_tree()
    assert [x.id for x in units] == [i.id for i in looped_units]
