import pytest


@pytest.mark.django_db
def test_health_says_ok(client):
    response = client.get("/api/health/")
    assert response.content == b"OK"
    assert response.status_code == 200
