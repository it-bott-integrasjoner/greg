import pytest
from rest_framework.reverse import reverse

from greg.models import Identity


@pytest.mark.django_db
def test_get_identity(client, identity_norwegian_national_id_number, person):
    response = client.get(
        reverse(
            "v1:identities-list",
        ),
        data={
            "type": identity_norwegian_national_id_number.type,
            "value": identity_norwegian_national_id_number.value,
        },
    )

    data = response.json()["results"][0]
    assert data["type"] == identity_norwegian_national_id_number.type
    assert data["value"] == identity_norwegian_national_id_number.value
    assert data["person"] == person.id


@pytest.mark.django_db
def test_get_identity_value_startswith(
    client, identity_us_passport_1, identity_us_passport_2
):
    response = client.get(
        reverse(
            "v1:identities-list",
        ),
        data={"type": Identity.IdentityType.PASSPORT_NUMBER, "value_startswith": "US-"},
    )

    data = response.json()["results"]
    assert len(data) == 2
