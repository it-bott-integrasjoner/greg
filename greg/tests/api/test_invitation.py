from typing import Dict
from unittest.mock import patch

import pytest
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from greg.models import OrganizationalUnit, Person, RoleType, Sponsor
from gregui.api.views.invitation import InvitationLink
from gregui.mailutils.invite_guest import InviteGuest

# pylint: disable=redefined-outer-name


@pytest.fixture
def data(sponsor_guy: Sponsor, unit_foo: OrganizationalUnit) -> Dict:
    role = {
        "type": "Test",
        "start_date": "2021-06-10",
        "end_date": "2021-08-10",
        "orgunit": unit_foo.id,
    }
    data = {
        "first_name": "Test",
        "last_name": "Tester",
        "email": "test.tester@example.com",
        "role": role,
        "sponsor": sponsor_guy.feide_id,
    }
    return data


@pytest.mark.django_db
def test_create_invitation_with_existing_sponsor(client: APIClient, data: Dict):
    RoleType.objects.create(identifier="Test")

    url = reverse("v1:person-invitation-list")
    with patch.object(InviteGuest, "queue_mail", return_value=None) as queue_mail_mock:
        response = client.post(url, data=data, format="json")
        queue_mail_mock.assert_called()

    assert response.status_code == status.HTTP_201_CREATED
    assert Person.objects.get(id=1)
    assert InvitationLink.objects.get(id=1)


@pytest.mark.django_db
def test_create_invitation_with_new_sponsor(client: APIClient, data: Dict):
    RoleType.objects.create(identifier="Test")

    data["sponsor"] = "sponsor.guy@example.org"

    url = reverse("v1:person-invitation-list")
    with patch.object(InviteGuest, "queue_mail", return_value=None) as queue_mail_mock:
        response = client.post(url, data=data, format="json")
        queue_mail_mock.assert_called()

    assert response.status_code == status.HTTP_201_CREATED
    assert Person.objects.get(id=1)
    assert InvitationLink.objects.get(id=1)
    assert Sponsor.objects.get(id=2)
