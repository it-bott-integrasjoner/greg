import datetime
from typing import Dict

import pytest
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK
from rest_framework.test import APIClient

from django.utils import timezone

from greg.models import (
    Consent,
    Identity,
    Sponsor,
    RoleType,
    OrganizationalUnit,
    Person,
    Role,
)

# pylint: disable=redefined-outer-name


def find(items, key, value):
    """Return the first item in `items` where `key` equals `value`."""
    return next(x for x in items if x.get(key) == value)


@pytest.fixture
def role_type_visiting_professor() -> RoleType:
    return RoleType.objects.create(
        identifier="visiting_professor",
        name_nb="Gjesteprofessor",
        name_en="Visiting professor",
        description_nb="Gjesteprofessor",
        description_en="Visiting professor",
        default_duration_days=180,
    )


@pytest.fixture
def unit_human_resources() -> OrganizationalUnit:
    return OrganizationalUnit.objects.create(
        name_nb="Personal", name_en="Human Resources"
    )


@pytest.fixture()
def sponsor_bar() -> Sponsor:
    return Sponsor.objects.create(feide_id="bar")


@pytest.fixture
def role_data_guest(
    role_type_test_guest: RoleType, sponsor_bar: Sponsor, unit_foo: OrganizationalUnit
) -> Dict:
    return {
        "type": "Test Guest",
        "start_date": "2021-06-10",
        "end_date": "2021-08-10",
        "sponsor": sponsor_bar.id,
        "comments": "Test Comment",
        "orgunit": unit_foo.id,
    }


@pytest.mark.django_db
def test_get_person(client, person_foo, consent_type_foo):
    Consent.objects.create(
        person=person_foo,
        type=consent_type_foo,
        choice=consent_type_foo.choices.get(value="yes"),
    )
    resp = client.get(reverse("v1:person-detail", kwargs={"id": person_foo.id}))
    assert resp.status_code == HTTP_200_OK
    data = resp.json()
    assert data.get("id") == person_foo.id
    assert data.get("first_name") == person_foo.first_name
    assert data.get("last_name") == person_foo.last_name
    assert data.get("consents") == [
        {
            "id": 1,
            "consent_given_at": None,
            "type": {"identifier": "foo", "mandatory": False},
            "choice": "yes",
        }
    ]


@pytest.mark.django_db
def test_persons(client, person_foo, person_bar):
    resp = client.get(reverse("v1:person-list"))
    assert resp.status_code == HTTP_200_OK
    data = resp.json()
    assert len(data["results"]) == 2


@pytest.mark.django_db
def test_persons_verified_filter_include(
    client, person_bar, person_foo, person_foo_verified
):
    url = reverse("v1:person-list")
    response = client.get(url, {"verified": "true"})
    results = response.json()["results"]
    assert len(results) == 1
    assert results[0]["first_name"] == "Foo"
    assert results[0]["last_name"] == "Foo"


@pytest.mark.django_db
def test_persons_verified_filter_exclude(
    client, person_bar, person_foo, person_foo_verified
):
    url = reverse("v1:person-list")
    response = client.get(url, {"verified": "false"})
    results = response.json()["results"]
    assert len(results) == 1
    assert results[0]["first_name"] == "Bar"
    assert results[0]["last_name"] == "Bar"


@pytest.mark.django_db
def test_person_create(client):
    url = reverse("v1:person-list")
    response = client.post(url, {"first_name": "foo木👍أ", "last_name": "غbar水"})
    results = response.json()
    assert results.get("first_name") == "foo木👍أ"
    assert results.get("last_name") == "غbar水"
    assert results.get("created") is not None
    assert results.get("updated") is not None


@pytest.mark.django_db
def test_add_role(
    client, person, role_type_visiting_professor, sponsor_guy, unit_human_resources
):
    url = reverse("v1:person_role-list", kwargs={"person_id": person.id})
    roles_for_person = client.get(url).json()["results"]

    # Check that there are no roles for the person, and then add one
    assert len(roles_for_person) == 0

    role_data = {
        "type": "visiting_professor",
        "start_date": "2021-06-10",
        "end_date": "2021-08-10",
        "sponsor": "1",
        "orgunit": "1",
    }
    response = client.post(url, role_data)

    assert response.status_code == status.HTTP_201_CREATED

    response_data = response.json()
    roles_for_person = client.get(url).json()["results"]

    # Check that the role shows up when listing roles for the person now
    assert len(roles_for_person) == 1
    assert roles_for_person[0]["id"] == response_data["id"]


@pytest.mark.django_db
def test_get_role(client, person_foo, role_data_guest):
    # Check role list
    url = reverse("v1:person_role-list", kwargs={"person_id": person_foo.id})
    client.post(url, role_data_guest)
    response = client.get(url).json()["results"]
    assert response[0]["type"] == role_data_guest["type"]

    # Check for single role
    url_detail = reverse(
        "v1:person_role-detail", kwargs={"person_id": person_foo.id, "id": 1}
    )
    response_detail = client.get(url_detail).json()
    assert response_detail["type"] == role_data_guest["type"]


@pytest.mark.django_db
def test_update_role(client, person_foo, role_data_guest):
    url = reverse("v1:person_role-list", kwargs={"person_id": person_foo.id})
    response = client.post(url, role_data_guest)
    response_data = response.json()

    assert response_data["start_date"] == "2021-06-10"

    # Update the date and check that the change is registered
    role_id = response.json()["id"]
    updated_role = role_data_guest.copy()
    updated_role["start_date"] = "2021-06-15"

    url_detail = reverse(
        "v1:person_role-detail", kwargs={"person_id": person_foo.id, "id": role_id}
    )
    client.patch(url_detail, updated_role)

    updated_role_data = client.get(url)
    updated_data = updated_role_data.json()["results"][0]

    assert updated_data["id"] == role_id
    assert updated_data["start_date"] == "2021-06-15"


@pytest.mark.django_db
def test_delete_role(client, person_foo, role_data_guest):
    url = reverse("v1:person_role-list", kwargs={"person_id": person_foo.id})
    role_id = client.post(url, role_data_guest).json()["id"]
    roles_for_person = client.get(url).json()["results"]

    assert len(roles_for_person) == 1

    url_detail = reverse(
        "v1:person_role-detail", kwargs={"person_id": person_foo.id, "id": role_id}
    )
    client.delete(url_detail)

    assert len(client.get(url).json()["results"]) == 0


@pytest.mark.django_db
def test_identity_list(
    client, person_foo, person_foo_verified, person_foo_not_verified
):
    response = client.get(
        reverse("v1:person-list"),
        data={"first_name": person_foo.first_name, "last_name": person_foo.last_name},
    )
    person_id = response.json()["results"][0]["id"]
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person_id})
    )
    data = response.json()["results"]
    assert len(data) == 4

    passport = find(data, "type", "passport_number")
    assert passport["person"] == 1
    assert passport["type"] == "passport_number"
    assert passport["source"] == "Test"
    assert passport["value"] == "12345"
    assert passport["verified"] == "manual"
    assert passport["verified_by"] == 1
    # assert first["verified_at"] is a datetime string
    # assert first["created"] is a datetime string


@pytest.mark.django_db
def test_identity_add(client, person):
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 0

    data = {
        "type": Identity.IdentityType.FEIDE_ID,
        "source": "Test source",
        "value": "12345",
    }
    client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 1


@pytest.mark.django_db
def test_identity_add_duplicate_fails(client, person_foo, person_bar):
    data = {
        "type": Identity.IdentityType.FEIDE_ID,
        "source": "Test source",
        "value": "12345",
    }
    client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person_bar.id}),
        data=data,
    )

    response = client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person_foo.id}),
        data=data,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"non_field_errors": ["Identity already exists"]}


@pytest.mark.django_db
def test_person_search_list(client, person_foo):
    response = client.get(reverse("v1:person_search-list") + "?q=Foo")
    data = response.json()
    assert len(data) == 1

    person = response.json()[0]
    birth_date = person_foo.date_of_birth
    assert person["first"] == person_foo.first_name
    assert person["last"] == person_foo.last_name
    assert person["date_of_birth"] == birth_date.strftime("%Y-%m-%d")


@pytest.mark.django_db
def test_add_invalid_type(client, person):
    data = {
        "type": "OTHER",
        "source": "Test source",
        "value": "12345",
    }

    assert len(person.identities.all()) == 0

    response = client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    person.refresh_from_db()
    assert len(person.identities.all()) == 0


@pytest.mark.django_db
def test_identity_delete(client, person):
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 0

    data = {
        "type": Identity.IdentityType.FEIDE_ID,
        "source": "Test source",
        "value": "12345",
    }
    post_response = client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )
    identity_id = post_response.json()["id"]

    # Create two identities for the user
    data = {
        "type": Identity.IdentityType.PASSPORT_NUMBER,
        "source": "Test",
        "value": "1234413241235",
    }
    post_response2 = client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )
    identity_id2 = post_response2.json()["id"]

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 2

    # Delete the first identity created
    client.delete(
        reverse(
            "v1:person_identity-detail",
            kwargs={"person_id": person.id, "id": identity_id},
        )
    )

    # Check that the other identity is still there
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]

    assert len(results) == 1
    assert results[0]["id"] == identity_id2


@pytest.mark.django_db
def test_identity_update(client, person):
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 0

    data = {
        "type": Identity.IdentityType.FEIDE_ID,
        "source": "Test source",
        "value": "12345",
    }
    client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 1

    identity_id = results[0]["id"]

    assert results[0]["type"] == data["type"]
    assert results[0]["source"] == data["source"]
    assert results[0]["value"] == data["value"]

    data_updated = {
        "type": Identity.IdentityType.PASSPORT_NUMBER,
        "source": "Test",
        "value": "10000",
    }
    patch_response = client.patch(
        reverse(
            "v1:person_identity-detail",
            kwargs={"person_id": person.id, "id": identity_id},
        ),
        data=data_updated,
    )
    assert patch_response.status_code == status.HTTP_200_OK

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 1

    assert results[0]["type"] == data_updated["type"]
    assert results[0]["source"] == data_updated["source"]
    assert results[0]["value"] == data_updated["value"]


@pytest.mark.django_db
def test_remove_person(
    client, person_foo, role_data_guest, person_foo_verified, person_foo_not_verified
):
    url = reverse("v1:person_role-list", kwargs={"person_id": person_foo.id})
    client.post(url, role_data_guest)

    roles_for_person = client.get(url).json()["results"]
    assert len(roles_for_person) == 1

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person_foo.id})
    )
    assert len(response.json()["results"]) == 4

    # Delete the person and check that the data has been removed
    client.delete(reverse("v1:person-detail", kwargs={"id": person_foo.id}))

    updated_role_data = client.get(url)
    assert len(updated_role_data.json()["results"]) == 0

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person_foo.id})
    )
    assert len(response.json()["results"]) == 0


@pytest.mark.skip("fails with IntegrityError")
@pytest.mark.django_db
def test_add_duplicate_role_fails(client, person_foo: Person, role_person_foo):
    url = reverse("v1:person_role-list", kwargs={"person_id": person_foo.id})
    roles_for_person = client.get(url).json()["results"]

    assert len(roles_for_person) == 1

    role_data = {
        "type": role_person_foo.type.identifier,
        "start_date": role_person_foo.start_date,
        "end_date": role_person_foo.end_date,
        "sponsor": role_person_foo.sponsor.id,
        "orgunit": role_person_foo.unit_id,
    }
    response = client.post(url, role_data)
    # If the role cannot be create the return code is 400
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    # TODO(jbr): assert response.json() explains integrity error
    assert False

    # Check that there is still only one role attached to the person
    roles_for_person = client.get(url).json()["results"]
    assert len(roles_for_person) == 1


@pytest.mark.django_db
def test_filter_active_includes_person_with_active_role(
    client,
    person_foo: Person,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
    role_type_test_guest: Role,
):
    date_today = datetime.date.today()

    Role.objects.create(
        person=person_foo,
        type=role_type_test_guest,
        end_date=date_today + datetime.timedelta(days=1),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )

    url = reverse("v1:person-list")
    response = client.get(url, {"active": "true"})
    results = response.json()["results"]
    assert len(results) == 1
    assert results[0]["first_name"] == "Foo"
    assert results[0]["last_name"] == "Foo"


@pytest.mark.django_db
def test_filter_active_excludes_person_with_no_active_role(
    client, person_foo: Person, role_person_foo: Role
):
    url = reverse("v1:person-list")
    response = client.get(url, {"active": "true"})
    results = response.json()["results"]
    assert len(results) == 0


@pytest.mark.django_db
def test_filter_active_value_false(
    client: APIClient,
    person_foo: Person,
    role_type_test_guest: Role,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
):
    """Check that filter is not used when specified to False"""
    date_today = datetime.date.today()

    # Inactive role
    Role.objects.create(
        person=person_foo,
        type=role_type_test_guest,
        end_date=date_today - datetime.timedelta(days=1),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )

    url = reverse("v1:person-list")
    response = client.get(url, {"active": False})
    results = response.json()["results"]
    assert len(results) == 1


@pytest.mark.django_db
def test_filter_role_type(
    client: APIClient,
    person_foo: Person,
    person: Person,
    person_bar: Person,
    role_type_test_guest: Role,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
):
    date_today = datetime.date.today()

    best_test_guest = RoleType.objects.create(
        identifier="Best Test Guest",
    )

    Role.objects.create(
        person=person_foo,
        type=best_test_guest,
        end_date=date_today + datetime.timedelta(days=1),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    Role.objects.create(
        person=person,
        type=role_type_test_guest,
        end_date=date_today + datetime.timedelta(days=1),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )
    Role.objects.create(
        person=person_bar,
        type=role_type_test_guest,
        end_date=date_today + datetime.timedelta(days=1),
        sponsor=sponsor_guy,
        orgunit=unit_foo,
    )

    url = reverse("v1:person-list")
    response = client.get(url, {"role_type": "Best Test Guest"})
    results = response.json()["results"]

    assert len(results) == 1


@pytest.mark.django_db
def test_person_consents_get(
    client: APIClient, person: Person, consent_fixture_choice_yes: Consent
):
    url = reverse("v1:person_consent-list", kwargs={"person_id": person.id})
    consents_for_person = client.get(url).json()["results"]

    assert consents_for_person[0]["type"]["identifier"] == "foo"
    assert not consents_for_person[0]["type"]["mandatory"]
    assert consents_for_person[0]["choice"] == "yes"


@pytest.mark.django_db
def test_person_consent_add(
    client: APIClient, person_foo: Person, consent_fixture: Consent
):
    url = reverse("v1:person_consent-list", kwargs={"person_id": person_foo.id})
    consents_for_person = client.get(url).json()["results"]

    assert len(consents_for_person) == 0

    consent_data = {"type": "foo", "choice": "yes"}
    response = client.post(url, consent_data)

    assert response.status_code == status.HTTP_201_CREATED

    consents_for_person = client.get(url).json()["results"]

    assert consents_for_person[0]["type"]["identifier"] == "foo"
    assert not consents_for_person[0]["type"]["mandatory"]
    assert consents_for_person[0]["choice"] == "yes"


@pytest.mark.django_db
def test_person_consent_patch(
    client: APIClient, person: Person, consent_fixture_choice_yes: Consent
):
    consent_id = person.consents.get().id
    url = reverse(
        "v1:person_consent-detail", kwargs={"person_id": person.id, "id": consent_id}
    )

    consent_data = {"type": "foo", "choice": "no"}
    response = client.patch(url, consent_data)

    assert response.status_code == status.HTTP_200_OK

    get_url = reverse("v1:person_consent-list", kwargs={"person_id": person.id})
    consents_for_person = client.get(get_url).json()["results"]

    assert consents_for_person[0]["type"]["identifier"] == "foo"
    assert not consents_for_person[0]["type"]["mandatory"]
    assert consents_for_person[0]["choice"] == "no"


@pytest.mark.django_db
def test_person_consent_delete(
    client: APIClient, person: Person, consent_fixture_choice_yes: Consent
):
    consent_id = person.consents.get().id
    get_url = reverse("v1:person_consent-list", kwargs={"person_id": person.id})

    consents_for_person = client.get(get_url).json()["results"]
    assert len(consents_for_person) == 1

    delete_url = reverse(
        "v1:person_consent-detail", kwargs={"person_id": person.id, "id": consent_id}
    )
    response = client.delete(delete_url)

    assert response.status_code == status.HTTP_204_NO_CONTENT

    consents_for_person = client.get(get_url).json()["results"]
    assert len(consents_for_person) == 0


@pytest.mark.django_db
def test_person_consent_add_invalid_choice_fails(
    client: APIClient, person_foo: Person, consent_fixture: Consent
):
    url = reverse("v1:person_consent-list", kwargs={"person_id": person_foo.id})
    consents_for_person = client.get(url).json()["results"]

    assert len(consents_for_person) == 0

    # Try with blue as a choice
    consent_data = {"type": "foo", "choice": "blue"}
    response = client.post(url, consent_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    # No consent should have been added to the person
    consents_for_person = client.get(url).json()["results"]
    assert len(consents_for_person) == 0


@pytest.mark.django_db
def test_identity_post_fails_if_duplicate(client, person, person_foo):
    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    assert len(results) == 0

    Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        source="Test source",
        value="12345678901",
        verified_at=timezone.now() - datetime.timedelta(days=205),
    )

    data = {
        "type": Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        "source": "Test source",
        "value": person_foo.fnr.value,
    }
    response = client.post(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id}),
        data=data,
    )

    # The request should fail
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    response = client.get(
        reverse("v1:person_identity-list", kwargs={"person_id": person.id})
    )
    results = response.json()["results"]
    # No Norwegian national ID should have been added
    assert len(results) == 0


@pytest.mark.django_db
def test_meta_field_person(client):
    url = reverse("v1:person-list")
    data_bar = {"first_name": "Bar", "last_name": "Bar", "meta": {"likes": "math"}}
    resp_post_bar = client.post(url, data=data_bar, format="json").json()
    assert resp_post_bar.get("meta").get("likes") == data_bar["meta"]["likes"]

    resp_get = client.get(url).json()["results"]
    assert len(resp_get) == 1

    data_foo = {"first_name": "Foo", "last_name": "Foo", "meta": {"likes": "fishing"}}
    resp_post_foo = client.post(url, data=data_foo, format="json").json()
    assert resp_post_foo.get("meta") == data_foo["meta"]

    resp_all = client.get(url).json()["results"]
    assert len(resp_all) == 2

    resp_bar = resp_all[0]
    resp_foo = resp_all[1]
    assert resp_bar.get("meta") == data_bar["meta"]
    assert resp_foo.get("meta") == data_foo["meta"]


@pytest.mark.django_db
def test_meta_field_person_id(client, person_foo):
    url = reverse("v1:person-detail", kwargs={"id": person_foo.id})
    resp = client.get(url).json()
    assert resp.get("meta") is None

    put_data = {"first_name": "Foo", "last_name": "Foo", "meta": {"likes": "math"}}
    resp_put = client.put(url, data=put_data, format="json").json()
    assert resp_put.get("meta").get("likes") == put_data["meta"]["likes"]

    resp = client.get(url).json()
    assert resp.get("meta").get("likes") == put_data["meta"]["likes"]

    patch_data = {
        "meta": {"likes": "math", "hobby": "eating"},
    }
    resp_patch = client.patch(url, data=patch_data, format="json").json()
    assert resp_patch.get("meta") == patch_data["meta"]

    resp = client.get(url).json()
    assert resp.get("meta") == patch_data["meta"]
