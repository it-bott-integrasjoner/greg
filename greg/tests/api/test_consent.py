import datetime

import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from greg.models import ConsentType


@pytest.mark.django_db
def test_get_consent_for_person_through_person_endpoint(
    client, person, consent_type_foo, consent_fixture_choice_yes
):
    response = client.get(reverse("v1:person-detail", kwargs={"id": person.id}))

    assert response.status_code == status.HTTP_200_OK
    response_body = response.json()
    consents = response_body["consents"]

    assert len(consents) == 1
    # Check that the ID shows up in the consent list from the person endpoint as well
    assert consents == [
        {
            "id": 1,
            "type": {"identifier": "foo", "mandatory": False},
            "consent_given_at": "2021-06-20",
            "choice": "yes",
        }
    ]


@pytest.mark.django_db
def test_get_consent_for_person(
    client, person, consent_type_foo, consent_fixture_choice_yes
):
    response = client.get(
        reverse("v1:person_consent-list", kwargs={"person_id": person.id})
    )

    assert response.status_code == status.HTTP_200_OK
    response_body = response.json()["results"]

    assert response_body == [
        {
            "id": 1,
            "type": {"identifier": "foo", "mandatory": False},
            "consent_given_at": "2021-06-20",
            "choice": "yes",
        }
    ]


@pytest.mark.django_db
def test_add_consent_to_person(client, person, consent_type_foo: ConsentType):
    data = {
        "type": consent_type_foo.identifier,
        "choice": "yes",
        "consent_given_at": "2021-02-25",
    }

    assert person.consents.count() == 0
    response = client.post(
        reverse("v1:person_consent-list", kwargs={"person_id": person.id}), data=data
    )
    assert response.status_code == status.HTTP_201_CREATED

    consent = person.consents.get()
    assert consent.type == consent_type_foo
    assert consent.choice == consent_type_foo.choices.get(value="yes")
    assert consent.consent_given_at == datetime.date(2021, 2, 25)


@pytest.mark.django_db
def test_patch_consent(client, person, consent_type_foo, consent_fixture_choice_yes):
    data = {
        "type": consent_type_foo.identifier,
        "choice": "no",
        "consent_given_at": "2021-02-26",
    }

    consent = person.consents.get()
    response = client.patch(
        reverse(
            "v1:person_consent-detail",
            kwargs={"person_id": person.id, "id": consent.id},
        ),
        data=data,
    )

    assert response.status_code == status.HTTP_200_OK

    # Reload data from database and check that it has been updated
    consent.refresh_from_db()
    assert consent.type == consent_type_foo
    assert consent.choice == consent_type_foo.choices.get(value="no")
    assert consent.consent_given_at == datetime.date(2021, 2, 26)
