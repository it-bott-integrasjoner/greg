import pytest

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN


ROUTES: tuple[tuple[str, dict], ...] = (
    ("v1:consenttype-list", {}),
    ("v1:consenttype-detail", {"id": 1}),
    ("v1:orgunit-list", {}),
    ("v1:orgunit-detail", {"id": 1}),
    ("v1:person-list", {}),
    ("v1:person-detail", {"id": 1}),
    ("v1:person_identity-list", {"person_id": 1}),
    ("v1:person_identity-detail", {"person_id": 1, "id": 1}),
    ("v1:person_role-list", {"person_id": 1}),
    ("v1:person_role-detail", {"person_id": 1, "id": 1}),
    ("v1:roletype-list", {}),
    ("v1:roletype-detail", {"id": 1}),
    ("v1:sponsor-list", {}),
    ("v1:sponsor-detail", {"id": 1}),
)


def assert_returns_status(endpoint, query_kwargs, client, status):
    url = reverse(endpoint, kwargs=query_kwargs)
    get_response = client.get(url)
    assert get_response.status_code == status
    post_response = client.post(url, data={"foo": "bar"})
    assert post_response.status_code == status
    patch_response = client.post(url, data={"foo": "bar"})
    assert patch_response.status_code == status
    delete_response = client.get(url)
    assert delete_response.status_code == status


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("endpoint", "query_kwargs"),
    ROUTES,
)
def test_forbids_non_admin_users(endpoint, query_kwargs, non_admin_client):
    # client authenticates with a token, but is not an administrator
    assert_returns_status(
        endpoint=endpoint,
        query_kwargs=query_kwargs,
        client=non_admin_client,
        status=HTTP_403_FORBIDDEN,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("endpoint", "query_kwargs"),
    ROUTES,
)
def test_requires_authentication(endpoint, query_kwargs, unauthenticated_client):
    # client does not authenticate
    assert_returns_status(
        endpoint=endpoint,
        query_kwargs=query_kwargs,
        client=unauthenticated_client,
        status=HTTP_401_UNAUTHORIZED,
    )
