import pytest
from rest_framework import status

from rest_framework.reverse import reverse

from greg.models import (
    OrganizationalUnit,
    Sponsor,
    Person,
    Identity,
    SponsorOrganizationalUnit,
)


@pytest.mark.django_db
def test_add_sponsor(client):
    data = {
        "feide_id": "sponsor@example.org",
        "first_name": "Test",
        "last_name": "Sponsor",
        "work_email": "sponsor123@example.org",
    }

    post_response = client.post(reverse("v1:sponsor-list"), data=data)

    assert post_response.status_code == status.HTTP_201_CREATED

    response_data = post_response.json()
    list_response = client.get(
        reverse("v1:sponsor-detail", kwargs={"id": response_data["id"]})
    )
    list_response_data = list_response.json()

    assert list_response_data["feide_id"] == data["feide_id"]
    assert list_response_data["first_name"] == data["first_name"]
    assert list_response_data["last_name"] == data["last_name"]
    assert list_response_data["work_email"] == data["work_email"]


@pytest.mark.django_db
def test_sponsor_guest_list(client, sponsor_guy, role_person_foo):
    url = reverse("v1:sponsor_guests-list", kwargs={"sponsor_id": sponsor_guy.id})
    guests_for_sponsor = client.get(url).json()["results"]

    assert len(guests_for_sponsor) == 1
    assert guests_for_sponsor[0]["id"] == role_person_foo.person_id


@pytest.mark.django_db
def test_sponsor_empty_guest_list(client, sponsor_guy):
    url = reverse("v1:sponsor_guests-list", kwargs={"sponsor_id": sponsor_guy.id})
    guests_for_sponsor = client.get(url).json()["results"]

    assert len(guests_for_sponsor) == 0


@pytest.mark.django_db
def test_add_sponsor_with_unit(client, unit_foo: OrganizationalUnit):
    sponsor_url = reverse("v1:sponsor-list")
    data = {
        "feide_id": "sponsor@example.org",
        "first_name": "Test",
        "last_name": "Sponsor",
    }

    response = client.post(sponsor_url, data=data)
    sponsor_id = response.json()["id"]

    assert response.status_code == status.HTTP_201_CREATED

    sponsor_lookup_response = client.get(sponsor_url, kwargs={"id": sponsor_id})
    sponsor_lookup_response_body = sponsor_lookup_response.json()

    assert len(sponsor_lookup_response_body["results"]) == 1
    assert len(sponsor_lookup_response_body["results"][0]["orgunits"]) == 0

    data = {"hierarchical_access": "True"}

    create_sponsor_link_url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_id, "orgunit_id": unit_foo.id},
    )
    response = client.post(create_sponsor_link_url, data=data)
    assert response.status_code == status.HTTP_200_OK

    sponsor_lookup_response = client.get(sponsor_url, kwargs={"id": sponsor_id})
    sponsor_lookup_response_body = sponsor_lookup_response.json()
    assert len(sponsor_lookup_response_body["results"][0]["orgunits"]) == 1

    attached_unit = sponsor_lookup_response_body["results"][0]["orgunits"][0]
    assert attached_unit["id"] == unit_foo.id


@pytest.mark.django_db
def test_remove_sponsor_orgunit_link(
    client, sponsor_guy: Sponsor, unit_foo: OrganizationalUnit
):
    sponsor_detail_url = reverse("v1:sponsor-detail", kwargs={"id": sponsor_guy.id})
    response_get = client.get(sponsor_detail_url).json()
    assert len(response_get["orgunits"]) == 0

    data = {"hierarchical_access": "True"}

    sponsor_orgunit_url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_guy.id, "orgunit_id": unit_foo.id},
    )
    response = client.post(sponsor_orgunit_url, data=data)
    assert response.status_code == status.HTTP_200_OK

    response_get = client.get(sponsor_detail_url).json()

    assert len(response_get["orgunits"]) == 1
    assert response_get["orgunits"][0]["id"] == unit_foo.id

    response_delete = client.delete(sponsor_orgunit_url)
    assert response_delete.status_code == status.HTTP_204_NO_CONTENT

    response_get = client.get(sponsor_detail_url).json()
    assert len(response_get["orgunits"]) == 0


@pytest.mark.django_db
def test_delete_sponsor_connected_to_identity_not_allowed(
    client,
    sponsor_guy: Sponsor,
    person_foo: Person,
    person_foo_verified: Identity,
    unit_foo: OrganizationalUnit,
):
    sponsor_detail_url = reverse("v1:sponsor-detail", kwargs={"id": sponsor_guy.id})
    response_get = client.get(sponsor_detail_url).json()
    assert len(response_get["orgunits"]) == 0

    response_delete = client.delete(sponsor_detail_url)

    # The delete should fail
    assert response_delete.status_code == status.HTTP_400_BAD_REQUEST
    response_get = client.get(sponsor_detail_url).json()
    assert response_get["id"] == sponsor_guy.id

    # Remove the identity and try to delete the sponsor again
    url_delete = reverse(
        "v1:person_identity-detail",
        kwargs={"person_id": person_foo.id, "id": person_foo_verified.id},
    )
    client.delete(url_delete)

    response_delete = client.delete(sponsor_detail_url)
    assert response_delete.status_code == status.HTTP_204_NO_CONTENT

    # Check that the sponsor has been deleted
    response_get = client.get(sponsor_detail_url)
    assert response_get.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_add_sponsor_unit_link_with_no_access_parameter(
    client, unit_foo: OrganizationalUnit
):
    sponsor_url = reverse("v1:sponsor-list")
    data = {
        "feide_id": "sponsor@example.org",
        "first_name": "Test",
        "last_name": "Sponsor",
    }

    response = client.post(sponsor_url, data=data)
    sponsor_id = response.json()["id"]

    # Do a post with no data
    create_sponsor_link_url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_id, "orgunit_id": unit_foo.id},
    )
    response = client.post(create_sponsor_link_url)
    assert response.status_code == status.HTTP_200_OK
    response_body = response.json()
    assert response_body["sponsor"] == sponsor_id
    assert response_body["organizational_unit"] == unit_foo.id

    # Check that the unit is attached to the sponsor
    sponsor_lookup_response = client.get(sponsor_url, kwargs={"id": sponsor_id})
    sponsor_lookup_response_body = sponsor_lookup_response.json()
    assert len(sponsor_lookup_response_body["results"][0]["orgunits"]) == 1

    attached_unit = sponsor_lookup_response_body["results"][0]["orgunits"][0]
    assert attached_unit["id"] == unit_foo.id

    # Check that hierarchical_access is set to False for the link between the sponsor and unit
    sponsor_organization_unit = SponsorOrganizationalUnit.objects.filter(
        sponsor_id=sponsor_id, organizational_unit_id=unit_foo.id
    ).get()
    assert not sponsor_organization_unit.hierarchical_access


@pytest.mark.django_db
def test_retrieve_sponsor_orgunit(
    client,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
    sponsor_org_unit: SponsorOrganizationalUnit,
):
    url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_guy.id, "orgunit_id": unit_foo.id},
    )
    response_body = client.get(url).json()

    assert response_body["sponsor"] == sponsor_guy.id
    assert response_body["organizational_unit"] == unit_foo.id


@pytest.mark.django_db
def test_no_sponsor_orgunit_exists(
    client,
    sponsor_foo: Sponsor,
    unit_foo: OrganizationalUnit,
):
    url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_foo.id, "orgunit_id": unit_foo.id},
    )
    response_body = client.get(url).json()

    assert not response_body


@pytest.mark.django_db
def test_list_all_sponsor_orgunits(
    client,
    sponsor_guy: Sponsor,
    sponsor_org_unit: SponsorOrganizationalUnit,
    sponsor_org_unit_guy: SponsorOrganizationalUnit,
):
    url = reverse("v1:sponsor_orgunit-list", kwargs={"sponsor_id": sponsor_guy.id})

    sponsor_units = client.get(url).json()["results"]

    assert len(sponsor_units) == 2


@pytest.mark.django_db
def test_update_sponsor_orgunit(
    client,
    sponsor_guy: Sponsor,
    unit_foo: OrganizationalUnit,
    sponsor_org_unit: SponsorOrganizationalUnit,
):
    data = {
        "hierarchical_access": True,
        "automatic": False,
        "source": "test",
    }
    url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_guy.id, "orgunit_id": unit_foo.id},
    )
    assert not sponsor_org_unit.hierarchical_access
    response_data = client.post(url, data=data).json()

    assert response_data["hierarchical_access"] == data["hierarchical_access"]
    assert response_data["automatic"] == data["automatic"]
    assert response_data["source"] == data["source"]


@pytest.mark.django_db
def test_create_new_sponsor_orgunit(
    client,
    sponsor_foo: Sponsor,
    unit_foo: OrganizationalUnit,
):
    data = {
        "hierarchical_access": True,
        "automatic": False,
        "source": "test",
    }
    get_url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_foo.id, "orgunit_id": unit_foo.id},
    )
    sponsor_unit = client.get(get_url).json()
    assert not sponsor_unit

    post_url = reverse(
        "v1:sponsor_orgunit-detail",
        kwargs={"sponsor_id": sponsor_foo.id, "orgunit_id": unit_foo.id},
    )

    response_data = client.post(post_url, data=data).json()
    assert response_data["sponsor"] == sponsor_foo.id
    assert response_data["organizational_unit"] == unit_foo.id
    assert response_data["hierarchical_access"] == data["hierarchical_access"]
    assert response_data["automatic"] == data["automatic"]
    assert response_data["source"] == data["source"]
