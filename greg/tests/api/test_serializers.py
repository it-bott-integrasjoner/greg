from collections import OrderedDict

import pytest

from greg.api.serializers import (
    NotificationSerializer,
    get_serializer,
    PersonSerializer,
    RoleTypeSerializer,
)
from greg.models import Notification, RoleType, Person


@pytest.mark.django_db
def test_get_serializer():
    n = Notification.objects.create(
        identifier=5, object_type="foo", operation="bar", issued_at=9, meta=""
    )
    p = Person.objects.create()
    rt = RoleType.objects.create()
    assert get_serializer(n) == NotificationSerializer
    assert get_serializer(p) == PersonSerializer
    assert get_serializer(rt) == RoleTypeSerializer


@pytest.mark.django_db
def test_notificationserializer_falsy_meta():
    ns = NotificationSerializer()
    n = Notification.objects.create(
        identifier=5, object_type="foo", operation="bar", issued_at=9, meta=""
    )
    expected = OrderedDict(
        {
            "identifier": 5,
            "object_type": "foo",
            "operation": "bar",
            "iat": 9,
        }
    )
    assert ns.to_representation(n) == expected


@pytest.mark.django_db
def test_notificationserializer_no_meta():
    ns = NotificationSerializer()
    n = Notification.objects.create(
        identifier=5,
        object_type="foo",
        operation="bar",
        issued_at=9,
    )
    expected = OrderedDict(
        {
            "identifier": 5,
            "object_type": "foo",
            "operation": "bar",
            "iat": 9,
        }
    )
    assert ns.to_representation(n) == expected


@pytest.mark.django_db
def test_notificationserializer_meta():
    ns = NotificationSerializer()
    n = Notification.objects.create(
        identifier=5, object_type="foo", operation="bar", issued_at=9, meta="baz"
    )
    expected = OrderedDict(
        {
            "identifier": 5,
            "object_type": "foo",
            "operation": "bar",
            "meta": "baz",
            "iat": 9,
        }
    )
    assert ns.to_representation(n) == expected
