import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from greg.models import ConsentType


@pytest.mark.django_db
def test_get_consent_type(client, consent_type_foo: ConsentType):
    resp = client.get(
        reverse("v1:consenttype-detail", kwargs={"id": consent_type_foo.id})
    )
    assert resp.status_code == status.HTTP_200_OK
    data = resp.json()
    assert data == {
        "id": consent_type_foo.id,
        "identifier": consent_type_foo.identifier,
        "name_en": consent_type_foo.name_en,
        "name_nb": consent_type_foo.name_nb,
        "name_nn": consent_type_foo.name_nn,
        "description_en": consent_type_foo.description_en,
        "description_nb": consent_type_foo.description_nb,
        "description_nn": consent_type_foo.description_nn,
        "mandatory": False,
        "user_allowed_to_change": True,
        "valid_from": "2018-01-20",
        "choices": [
            {
                "value": "yes",
                "text_en": "Yes",
                "text_nb": "Ja",
                "text_nn": "Ja",
            },
            {
                "value": "no",
                "text_en": "No",
                "text_nb": "Nei",
                "text_nn": "Nei",
            },
        ],
        "created": consent_type_foo.created.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "updated": consent_type_foo.updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
    }
