"""
Scheduled task for deleting older users.
"""
import logging
from datetime import date, timedelta

from django.conf import settings

from greg.models import Person, Role

logger = logging.getLogger(__name__)


class DeletePersons:
    def find_expired_people(self):
        """
        Find people where all roles have expired
        """

        # First find expired roles
        date_for_deletion = date.today() - timedelta(days=settings.ROLE_EXPIRY_DAYS)
        expired_roles = Role.objects.filter(
            end_date__lte=date_for_deletion
        ).select_related("person")
        logger.debug("Found %d expired roles", len(expired_roles))
        persons = set()
        expired_persons = []

        # Find all persons with expired roles
        for expired_role in expired_roles:
            persons.add(expired_role.person)

        logger.debug("Found %s people with expired roles", len(persons))

        # Check if all roles of person are expired
        for person in persons:
            person_roles = person.roles.all()
            no_active_roles = True
            if len(person_roles) > 1:
                for role in person_roles:
                    if role.end_date > date_for_deletion:
                        no_active_roles = False
                        logger.info(
                            "Person %s has an active role as %s. Will not delete",
                            person.id,
                            role.type,
                        )
                        break
            if no_active_roles:
                logger.info("Person %s has no active roles", person.id)
                expired_persons.append(person)
        return expired_persons

    def delete_roles_and_persons(self):
        """
        Deletes expired roles, and people with only expired roles
        """
        logger.info(
            "Looking for roles that expired over %d days ago", settings.ROLE_EXPIRY_DAYS
        )

        expired_people = self.find_expired_people()
        if not expired_people:
            logger.info("No expired people found")
            return

        for person in expired_people:
            logger.info("Deleting person %s", person.id)
            Person.objects.get(id=person.id).delete()


def delete_expired():
    importer = DeletePersons()
    importer.delete_roles_and_persons()
