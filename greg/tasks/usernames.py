import logging

from cerebrum_client import CerebrumClient
from django.conf import settings
from django.db.utils import IntegrityError
from django.utils import timezone

from greg.models import Identity, Person
from greg.utils import get_cerebrum_person_by_greg_id

logger = logging.getLogger(__name__)


class UsernameImporter:
    def __init__(self):
        self.client = CerebrumClient(**settings.CEREBRUM_CLIENT)

        self.source = settings.GREG_SOURCE
        self.cerebrum_source = settings.CEREBRUM_SOURCE
        self.cerebrum_id_type = settings.CEREBRUM_GREG_ID_TYPE
        self.id_type = Identity.IdentityType.FEIDE_ID

    def get_username(self, person):
        person_id = person.person_id
        username = self.client.get_person_primary_account_name(person_id)
        return username

    def update_or_create_person_feide_id(self, person, username):
        existing_feide_id = person.identities.filter(
            type=self.id_type, source=self.cerebrum_source
        )
        feide_id = f"{username}@{settings.INSTANCE_NAME}.no"
        if not existing_feide_id:
            logger.info("Creating Feide-ID for person_id=%s", person.pk)
            try:
                Identity.objects.create(
                    person=person,
                    type=self.id_type,
                    source=self.cerebrum_source,
                    value=feide_id,
                    verified=Identity.Verified.AUTOMATIC,
                    verified_at=timezone.now(),
                )
                return
            except IntegrityError as e:
                logger.warning(
                    "%s. There might be two person objects for person_id=%s",
                    e,
                    person.pk,
                )
                return
        existing_feide_id_obj = existing_feide_id.first()
        existing_feide_id_value = existing_feide_id_obj.value
        old_username, _ = existing_feide_id_value.split("@")
        if old_username != username:
            logger.info(
                "Updating Feide-ID for person_id=%s from %s to %s",
                person.pk,
                existing_feide_id_value,
                feide_id,
            )
            existing_feide_id.update(value=feide_id, verified_at=timezone.now())

    def import_usernames(self, version):
        if version == "with_usernames":
            all_persons = Person.objects.filter(
                identities__type=Identity.IdentityType.FEIDE_ID,
                identities__source=self.cerebrum_source,
            )
        elif version == "without_usernames":
            all_persons = Person.objects.exclude(
                identities__type=Identity.IdentityType.FEIDE_ID,
                identities__source=self.cerebrum_source,
            ).filter(registration_completed_date__isnull=False)
        else:
            all_persons = Person.objects.filter(
                registration_completed_date__isnull=False
            )

        for person in all_persons:
            logger.info(
                "Looking up username for person_id=%s from cerebrum",
                person.pk,
            )
            cerebrum_person = get_cerebrum_person_by_greg_id(
                person=person,
                client=self.client,
                id_type=self.cerebrum_id_type,
                source=self.source,
            )
            try:
                username = self.get_username(cerebrum_person)
            except ConnectionError as e:
                logger.exception(
                    "Failed to get username for person_id=%s. Got error=%s. Skipping person",
                    person.pk,
                    e,
                )
                continue
            if not username:
                logger.info(
                    "Found no username in cerebrum for person_id=%s. Skipping person",
                    person.pk,
                )
                continue

            logger.info(
                "Found username=%s for person_id=%s in cerebrum.",
                username,
                person.pk,
            )
            self.update_or_create_person_feide_id(person, username)


def import_usernames(version):
    importer = UsernameImporter()
    importer.import_usernames(version)
