import datetime
import logging
from collections import defaultdict

from cerebrum_client import CerebrumClient
from django.conf import settings

from greg.models import Person, Role, Sponsor
from greg.utils import get_cerebrum_person_by_greg_id
from gregui.mailutils.role_ending import RolesEnding

logger = logging.getLogger(__name__)


def person_still_has_affiliation(person: Person) -> bool:
    # Checks if person has other guest roles
    cut_off = datetime.date.today() + datetime.timedelta(days=settings.CUT_OFF)
    person_has_guest_roles = person.roles.filter(end_date__gt=cut_off)
    if person_has_guest_roles:
        return True

    client = CerebrumClient(**settings.CEREBRUM_CLIENT)  # type: ignore[arg-type]
    source = settings.GREG_SOURCE
    id_type = settings.CEREBRUM_GREG_ID_TYPE
    cerebrum_person = get_cerebrum_person_by_greg_id(
        person, client=client, source=source, id_type=id_type
    )
    if cerebrum_person is None:
        return False
    try:
        affiliation = client.get_person_affiliations(cerebrum_person.person_id)
    except ConnectionError as e:
        logger.exception(
            "Failed to get affiliation for person_id=%s. Got error=%s",
            person.pk,
            e,
        )
        return False
    # Check if person has ansatt or student aff
    if affiliation is None:
        return False
    for aff in affiliation:
        if aff.affiliation in settings.OTHER_AFFILIATIONS:
            return True
    return False


def notify_sponsors_roles_ending() -> list[str]:  # pylint: disable=too-many-locals
    """
    This task notifies sponsors of roles (that are accessible to them)
    that are about to expire.

    It is meant to be run at set intervals. Typically every week or
    two. The script will notify of all roles ending between today and
    the number of days set in the NOTIFIER_LIMIT setting.

    While it is sensible to set the run interval to the same value as
    the NOTIFIER_LIMIT, it is not mandatory, as there is a use case for
    setting the run interval to e.g 1 week and the limit to 2. This
    would ensure that the sponsor gets 2 notifications about the same
    role ending. (Be careful not to swap them though, as that would
    ensure some roles ending without a notification being sent to the
    sponsor).
    """
    # Map ending roles to units
    today = datetime.date.today()
    cutoff = today + datetime.timedelta(days=settings.SPONSOR_NOTIFIER_LIMIT)
    logger.info(
        "Looking for roles with end dates between %s and %s",
        today,
        cutoff,
    )
    ending_roles = Role.objects.filter(
        end_date__gte=today,
        end_date__lte=cutoff,
    )
    logger.info("Found %d roles that are ending soon", ending_roles.count())
    unit_to_roles = defaultdict(list)
    for role in ending_roles:
        unit_to_roles[role.orgunit.id].append(role)

    # Map sponsors with email to units
    logger.info("Mapping sponsors to their organizational units...")
    sponsors = Sponsor.objects.filter(work_email__isnull=False)
    sponsor_to_unit = {s.id: s.get_allowed_units() for s in sponsors}

    # Map sponsors to ending roles
    # Make sure only the sponsor(s) closest to the role is notified
    logger.info("Mapping roles to sponsors...")
    sponsor_to_roles: dict = defaultdict(list)
    for unit_id, roles in unit_to_roles.items():
        for role in roles:
            role_notified = False
            for sp, units in reversed(sponsor_to_unit.items()):
                for unit in units:
                    if (
                        unit.id
                        # Implemented to save the principal from notification spam
                        not in settings.EXCLUDED_UNITS
                        and unit_id == unit.id
                        and (not role_notified or unit.parent not in units)
                    ):
                        sponsor_to_roles[sp].append(role)
                        role_notified = True

    # Send emails to sponsors
    remindermailer = RolesEnding()
    task_ids = []
    for sponsor_id, roles in sponsor_to_roles.items():
        sponsor = sponsors.get(id=sponsor_id)
        logger.info(
            "Queueing role end notification for %s for roles %s",
            sponsor,
            [role.id for role in roles],
        )
        task_ids.append(
            remindermailer.queue_sponsor_mail(
                mail_to=sponsor.work_email,  # type: ignore
                num_roles=len(roles),
            )
        )
    logger.info("Queued %d tasks", len(task_ids))
    return task_ids


def notify_guests_roles_ending():
    """
    This task notifies guests of roles that are about to expire if the role
    is the last affiliation the guest has to UiO.

    Will notify the guest twice before the role ends.
    """
    # Map ending roles to units
    today = datetime.date.today()
    first_warning = today + datetime.timedelta(days=settings.GUEST_NOTIFIER_FIRST)
    second_warning = today + datetime.timedelta(days=settings.GUEST_NOTIFIER_SECOND)
    logger.info("Looking for roles about to expire")
    roles_ending = Role.objects.filter(end_date=first_warning)
    roles_ending_soon = Role.objects.filter(end_date=second_warning)
    logger.info(
        "Found %d roles that are ending in %s days",
        roles_ending.count(),
        settings.GUEST_NOTIFIER_FIRST,
    )
    logger.info(
        "Found %d roles that are ending in %s days",
        roles_ending_soon.count(),
        settings.GUEST_NOTIFIER_SECOND,
    )
    all_roles = roles_ending.union(roles_ending_soon)

    # Send emails to guests
    remindermailer = RolesEnding()
    nr_of_tasks = 0
    for role in all_roles:
        person = role.person
        # Skip those who are not fully registered
        if person.registration_completed_date is None:
            continue
        role_end_notified = False
        if person_still_has_affiliation(person):
            continue
        logger.info(
            "Queueing role end notification for %s",
            person,
        )
        # Notify guest on both private email and UiO email
        if person.feide_id:
            remindermailer.queue_guest_mail(
                mail_to=person.feide_id.value,
                end_date=role.end_date,
                name_nb=role.type.name_nb,
                name_en=role.type.name_en,
            )
            nr_of_tasks += 1
            role_end_notified = True
        if person.private_email:
            remindermailer.queue_guest_mail(
                mail_to=person.private_email.value,
                end_date=role.end_date,
                name_nb=role.type.name_nb,
                name_en=role.type.name_en,
            )
            nr_of_tasks += 1
            role_end_notified = True
        # Believe everyone should have a private email or feide_id, but in case
        if not role_end_notified:
            logger.info(
                "Found no email to contact %s about role %s ending", person, role
            )
    logger.info("Queued %d tasks", nr_of_tasks)
