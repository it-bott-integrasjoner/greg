"""
Scheduled task for removing OUs from sponsors who are no longer affiliated
with their institution. By removing the OU from the sponsor, the unaffiliated
sponsors will no longer receive emails about roles ending.
"""

import logging

from cerebrum_client import CerebrumClient
from django.conf import settings

from greg.models import Sponsor, SponsorOrganizationalUnit
from greg.utils import get_cerebrum_person_by_username

logger = logging.getLogger(__name__)


class RemoveSponsorsOU:
    def sponsor_still_has_affiliation(self, sponsor: Sponsor):
        """
        Checks if sponsor still has any affiliations
        """
        sponsor_username = sponsor.feide_id.split("@")[0]
        client = CerebrumClient(**settings.CEREBRUM_CLIENT)  # type: ignore[arg-type]
        cerebrum_person = get_cerebrum_person_by_username(
            username=sponsor_username, client=client
        )
        if cerebrum_person is None:
            return False
        try:
            affiliation = client.get_person_affiliations(cerebrum_person.id)
        except ConnectionError as e:
            logger.exception(
                "Failed to get affiliation for person_id=%s. Got error=%s",
                cerebrum_person.id,
                e,
            )
            raise
        if affiliation is None:
            return False
        for aff in affiliation:
            if aff.affiliation != "":
                return True
        return False

    def remove_ou_from_sponsors(self):
        """
        Removes OUs that were manually added from sponsors without any affiliations.
        """
        logger.info("Looking for manually added sponsor organizational units...")
        sponsor_OUs = SponsorOrganizationalUnit.objects.filter(automatic=False)
        logger.info(
            "Found %s manually added sponsor organizational units", len(sponsor_OUs)
        )

        delete_count = 0
        for sponsor_OU in sponsor_OUs:
            sponsor = sponsor_OU.sponsor
            if self.sponsor_still_has_affiliation(sponsor=sponsor):
                continue
            OU_name = sponsor_OU.organizational_unit.name_en
            logger.info(
                "Sponsor %s no longer has any affiliations, removing OU %s from sponsor...",
                sponsor.id,
                OU_name,
            )
            sponsor_OU.delete()
            delete_count += 1
            logger.info("Removed OU %s from sponsor %s", OU_name, sponsor.id)
        logger.info("Deleted %s sponsor organizational untis", delete_count)


def remove_ou_unaffiliated_sponsors():
    importer = RemoveSponsorsOU()
    importer.remove_ou_from_sponsors()
