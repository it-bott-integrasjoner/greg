"""
Fetch sponsors for all units in GREG.

Uses the members of the adm-leder-<legacy_stedkode> groups to
populate Sponsors and SponsorOrganizationalUnits.

This script does only remove the SponsorOrganizationalUnit.
The Sponsor objects are kept, even with no units

Any unit with an OuIdentifier with name legacy_stedkode and value
present in the settings variable CEREBRUM_MANUAL_SPONSOR_UNITS will be
skipped and is expected to have their Sponsors created manually.
"""

from typing import Optional, Tuple

import cerebrum_client
import structlog
from cerebrum_client import CerebrumClient
from django.conf import settings
from greg.models import OrganizationalUnit, Sponsor, SponsorOrganizationalUnit

logger = structlog.getLogger(__name__)


class SponsorImporter:
    CEREBRUM_SOURCE = "cerebrum"
    CEREBRUM_FEIDE_INST = "uio.no"
    CEREBRUM_NAME_SOURCE_PRIORITY = ["Cached", "Override", "DFO_SAP", "FS", "Manual"]
    CEREBRUM_STEDKODE_IDENTIFIER_NAME = "legacy_stedkode"

    def __init__(self):
        self.client = CerebrumClient(**settings.CEREBRUM_CLIENT)

    def _upsert_sponsor_unit_link(self, sponsor: Sponsor, unit: OrganizationalUnit):
        """Ensure a link between sponsor and unit."""
        try:
            sunit = SponsorOrganizationalUnit.objects.get(
                sponsor=sponsor,
                organizational_unit=unit,
            )
            created = False
        except SponsorOrganizationalUnit.DoesNotExist:
            sunit = SponsorOrganizationalUnit.objects.create(
                sponsor=sponsor,
                organizational_unit=unit,
                source=self.CEREBRUM_SOURCE,
                automatic=True,
                hierarchical_access=settings.CEREBRUM_HIERARCHICAL_ACCESS,
            )
            created = True
        if created:
            logger.info("sponsor_ou_link_create", sponsor=sponsor.id, sunit=sunit.id)
        else:
            logger.info("sponsor_ou_link_found", sponsor=sponsor.id, sunit=sunit.id)
            sunit.source = self.CEREBRUM_SOURCE
            sunit.automatic = True
            sunit.hierarchical_access = settings.CEREBRUM_HIERARCHICAL_ACCESS
            changes = sunit.get_dirty_fields(check_relationship=True, verbose=True)
            if changes:
                logger.info("sponsor_ou_link_changed", changes=changes)
            sunit.save()

        return SponsorOrganizationalUnit.objects.get(id=sunit.id)

    def _remove_sponsor_unit_link(self, sunit: SponsorOrganizationalUnit):
        logger.info("sponsor_ou_deleted", sunit=sunit.id)
        sunit.delete()

    def _get_person_name(
        self, person: cerebrum_client.models.Person
    ) -> Tuple[Optional[str], Optional[str]]:
        """Get a persons chosen name."""
        first_names = {x.source_system: x for x in person.names if x.variant == "FIRST"}
        last_names = {x.source_system: x for x in person.names if x.variant == "LAST"}

        for source_system in self.CEREBRUM_NAME_SOURCE_PRIORITY:
            if source_system in first_names and source_system in last_names:
                return first_names[source_system].name, last_names[source_system].name

        return None, None

    def _get_feide_id(self, person_id: str) -> Optional[str]:
        """Infer the feide id from the primary user."""
        primary_uname = self.client.get_person_primary_account_name(person_id)

        if primary_uname:
            return f"{primary_uname}@{self.CEREBRUM_FEIDE_INST}"
        return None

    def _upsert_sponsor_from_cerebrum(
        self, person_id: str, unit: OrganizationalUnit
    ) -> Optional[Sponsor]:
        """Insert or update a sponsor from Cerebum data."""
        person = self.client.get_person(person_id)
        if not person:
            logger.warning("cerebrum_person_missing", cerebrum_person_id=person_id)
            return None

        feide_id = self._get_feide_id(person_id)
        if not feide_id:
            logger.warning("cerebrum_no_primary_account", cerebrum_person_id=person_id)
            return None

        first_name, last_name = self._get_person_name(person)
        if not first_name or not last_name:
            logger.warning("cerebrum_no_valid_name", cerebrum_person_id=person_id)
            return None

        try:
            sponsor = Sponsor.objects.get(feide_id=feide_id)
            sponsor.first_name = first_name
            sponsor.last_name = last_name
            sponsor.save()
            logger.info(
                "sponsor_updated", sponsor=sponsor.id, cerebrum_person_id=person_id
            )

        except Sponsor.DoesNotExist:
            sponsor = Sponsor.objects.create(
                first_name=first_name,
                last_name=last_name,
                feide_id=feide_id,
            )
            logger.info(
                "sponsor_created", sponsor=sponsor.id, cerebrum_person_id=person_id
            )
        return Sponsor.objects.get(id=sponsor.id)

    # pylint: disable=R0912,R0914
    def handle(self, root: Optional[str] = None):
        """Import of Sponsors from Cerebrum."""
        excluded_parent_units = OrganizationalUnit.objects.filter(
            active=True,
            deleted=False,
            identifiers__name=self.CEREBRUM_STEDKODE_IDENTIFIER_NAME,
            identifiers__value__in=settings.CEREBRUM_MANUAL_SPONSOR_UNITS,
        )
        excluded_units = set()
        for excluded_parent in excluded_parent_units:
            children = excluded_parent.fetch_tree()
            logger.info(
                "excluding_unit", parent=excluded_parent, num_children=len(children)
            )
            excluded_units.update(children)

        all_active_units = OrganizationalUnit.objects.filter(
            active=True,
            deleted=False,
        )

        if root is not None:
            parent = OrganizationalUnit.objects.get(id=root)
            tree = parent.fetch_tree()
            target_units_before_exclusion = OrganizationalUnit.objects.filter(
                id__in=[x.id for x in tree]
            )
        else:
            target_units_before_exclusion = all_active_units.exclude(
                id__in=[x.id for x in excluded_units]
            )

        target_units = target_units_before_exclusion.exclude(
            id__in=[x.id for x in excluded_units]
        )

        logger.info(
            "import_start",
            num_active_units=len(all_active_units),
            num_excluded_units=len(excluded_units),
            num_target_units=len(target_units),
        )
        for unit in target_units:
            logger.bind(unit=unit.id)
            sko = unit.identifiers.filter(
                name=self.CEREBRUM_STEDKODE_IDENTIFIER_NAME
            ).first()
            if not sko:
                logger.warning("orgreg_unit_missing_legacy_stedkode")
                continue
            logger.bind(legacy_stedkode=sko.value)

            current_sponsors = unit.link_unit.filter(
                automatic=True, source=self.CEREBRUM_SOURCE
            ).all()

            group_name = f"adm-leder-{sko.value}"
            group = self.client.get_group(group_name)
            if not group:
                # No group in cererbum, remove sponsors.
                logger.info(
                    "cerebrum_group_not_found",
                    unit_id=unit.id,
                    cerebrum_group=group_name,
                )
                for sponsor in current_sponsors:
                    self._remove_sponsor_unit_link(sponsor)
                continue

            if group.expire_date:
                # Group is expired, remove sponsors
                logger.info(
                    "cerebrum_group_expired",
                    unit_id=unit.id,
                    cerebrum_group=group_name,
                )
                for sponsor in current_sponsors:
                    self._remove_sponsor_unit_link(sponsor)
                continue

            group_members = list(self.client.list_group_members(group_name))
            if not group_members:
                # No members in group, remove sponsors
                logger.info(
                    "cerebrum_group_empty", unit_id=unit.id, cerebrum_group=group_name
                )
                for sponsor in current_sponsors:
                    self._remove_sponsor_unit_link(sponsor)
                continue

            cerebrum_sponsors = set()
            for member in group_members:
                if member.type == "person":
                    sponsor_ = self._upsert_sponsor_from_cerebrum(member.id, unit)
                    if sponsor_:
                        sponsor_link = self._upsert_sponsor_unit_link(
                            sponsor=sponsor_, unit=unit
                        )
                        cerebrum_sponsors.add(sponsor_link)

            for sponsor in set(current_sponsors) - cerebrum_sponsors:
                self._remove_sponsor_unit_link(sponsor)

        logger.info("import_end")


def import_sponsors_from_cerebrum(root: Optional[str] = None):
    importer = SponsorImporter()
    importer.handle(root=root)
