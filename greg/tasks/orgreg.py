"""
Fetch all OUs from OrgReg and add the complete tree to Greg.

Ignores OrganizationalUnits without identifiers with source and name matching global
variables ORGREG_SOURCE and ORGREG_NAME

Assumes that the header used for authentication is of the type
'X-Gravitee-Api-Key': 'token'.

If the path to the endpoint of the OUs is oregreg/v3/ou/ you want to give
orgreg/v3/ as the url argument (note the trailing slash).
"""
import datetime
import logging
from typing import Mapping, Optional, Union, Any

from django.conf import settings
import orgreg_client
from orgreg_client.models import ExternalKey, OrgUnit

from greg.models import OrganizationalUnit, OuIdentifier

logger = logging.getLogger(__name__)


class OrgregImporter:
    processed: dict[int, OrganizationalUnit] = {}

    def upsert_extra_identities(self, ou: OrgUnit):
        """Upsert any configured extra IDs from orgreg."""
        for extra_id in settings.ORGREG_EXTRA_IDS:
            if "type" in extra_id.keys():
                identity_in_orgreg = self._get_external_key_from_ou(extra_id, ou)
                if identity_in_orgreg is not None:
                    self._upsert_identifier(
                        extra_id["source"],
                        extra_id["type"],
                        identity_in_orgreg.value,
                        ou.ou_id,
                    )
            elif "fieldname" in extra_id.keys():
                if hasattr(ou, extra_id["fieldname"]):
                    field = getattr(ou, extra_id["fieldname"])
                    # Subfields are typically the language, but can be other things
                    subfield = extra_id.get("subfield", None)
                    if subfield and hasattr(field, subfield):
                        value = getattr(field, subfield)
                    # Give nob if nno/eng don't exist
                    elif (
                        subfield
                        and subfield in ["nno", "eng"]
                        and hasattr(field, "nob")
                    ):
                        value = getattr(field, "nob")
                    else:
                        value = field
                    if not isinstance(value, str):
                        logger.warning(
                            "Cannot find value: %s. Make sure 'fieldname' "
                            "and 'subfield' are correctly given.",
                            value,
                        )
                        continue
                    self._upsert_identifier(
                        settings.ORGREG_SOURCE,
                        extra_id["fieldname"],
                        value,
                        ou.ou_id,
                    )

            else:
                logger.warning("Unsupported key value %s", extra_id.keys()[0])

        # Acronyms can also be used as identifiers
        for acronym in settings.ORGREG_ACRONYMS:
            self.handle_acronym_identifier(acronym, ou)

    def handle_acronym_identifier(self, acronym, ou):
        if acronym == "nob" and ou.acronym.nob is not None:
            self._upsert_identifier(
                settings.ORGREG_SOURCE,
                "acronym_" + acronym,
                ou.acronym.nob,
                ou.ou_id,
            )
        if acronym == "eng" and ou.acronym.eng is not None:
            self._upsert_identifier(
                settings.ORGREG_SOURCE,
                "acronym_" + acronym,
                ou.acronym.eng,
                ou.ou_id,
            )
        if acronym == "nno" and ou.acronym.nno is not None:
            self._upsert_identifier(
                settings.ORGREG_SOURCE,
                "acronym_" + acronym,
                ou.acronym.nno,
                ou.ou_id,
            )

    @staticmethod
    def _get_external_key_from_ou(
        extra_id: dict[str, str], ou: OrgUnit
    ) -> Optional[ExternalKey]:
        matching_ids = [
            x
            for x in ou.external_keys
            if x.source_system == extra_id["source"] and x.type == extra_id["type"]
        ]

        if not matching_ids:
            logger.warning(
                "No %s id from %s found in OrgReg for ou %s",
                extra_id["type"],
                extra_id["source"],
                ou.ou_id,
            )
            return None

        if len(matching_ids) > 1:
            # External_ids
            logger.warning(
                "Found multiple ids matching type: %s source: %s in OrgReg. "
                "Using the first one: %s",
                extra_id["type"],
                extra_id["source"],
                matching_ids[0].value,
            )

        return matching_ids[0]

    def _upsert_identifier(
        self, source: str, identifier_type: str, new_value: str, ou_id: int
    ):
        # strip whitespace, in case of \r or \n
        new_value = new_value.strip()

        # Check if the id exists
        saved_identifier = (
            self.processed[ou_id]
            .identifiers.filter(
                name=identifier_type,
            )
            .first()
        )

        if saved_identifier:
            if saved_identifier.value != new_value:
                logger.info(
                    "Updating identifier for ou_id=%r type=%r source=%r old_value=%r new_value=%r",
                    ou_id,
                    identifier_type,
                    source,
                    saved_identifier.value,
                    new_value,
                )
                saved_identifier.value = new_value
                saved_identifier.save()
        else:
            OuIdentifier.objects.create(
                name=identifier_type,
                source=source,
                value=new_value,
                orgunit=self.processed[ou_id],
            )
            logger.info(
                "Added new identifier to ou_id=%r type=%r source=%r value=%r",
                ou_id,
                identifier_type,
                source,
                new_value,
            )

    def _get_or_create_and_set_values(
        self,
        ou: OrgUnit,
        values: Mapping[str, Union[str, int, bool, OrganizationalUnit]],
    ):
        """Upsert ou with latest values and store in processed dict."""
        try:
            identifier = OuIdentifier.objects.get(
                name=settings.ORGREG_NAME, source=settings.ORGREG_SOURCE, value=ou.ou_id
            )
        except OuIdentifier.DoesNotExist:
            self.processed[ou.ou_id] = OrganizationalUnit.objects.create()
            identifier = OuIdentifier.objects.create(
                name=settings.ORGREG_NAME,
                source=settings.ORGREG_SOURCE,
                value=ou.ou_id,
                orgunit=self.processed[ou.ou_id],
            )
            created = True
        else:
            self.processed[ou.ou_id] = identifier.orgunit
            created = False
        for k, v in values.items():
            setattr(self.processed[ou.ou_id], k, v)
        self.upsert_extra_identities(ou)
        changes = None
        if self.processed[ou.ou_id].is_dirty(check_relationship=True):
            changes = self.processed[ou.ou_id].get_dirty_fields(
                check_relationship=True, verbose=True
            )
        self.processed[ou.ou_id].save()
        if changes:
            logger.info(
                "%s %s with %s",
                "Created" if created else "Updated",
                self.processed[ou.ou_id],
                changes,
            )
        else:
            logger.info("No changes for %s", self.processed[ou.ou_id])

    def _upsert_ou(self, ou: OrgUnit, ous: Mapping[int, OrgUnit]):
        """
        Update or create a OU with current values.

        There are three cases:
        1. OU has no parent -> Create it.
        2. OU has parent and parent exists -> Create it.
        3. OU has parent but parent has not been created -> Create parent, then child.

        Case 3 is solved by calling this method recursively so that the parent and its
        parent are created before the child.
        """

        # skip if already processed (happens if a previous ou needed this one as a
        # parent)
        if ou.ou_id in self.processed:
            return
        values: dict[str, Any] = {"deleted": False}
        # add names if present
        # we strip whitespace as \r and \n sometimes trickle through
        if ou.name:
            if ou.name.nob:
                values["name_nb"] = ou.name.nob.strip()
            if ou.name.eng:
                values["name_en"] = ou.name.eng.strip()

        # Set inactive if ou is no longer valid
        is_inactive = ou.valid_to and ou.valid_to < datetime.date.today()
        values["active"] = not is_inactive

        # Create OUs without parent (this should only happen for the root node)
        if not ou.parent:
            self._get_or_create_and_set_values(ou, values)
            return

        # If OU has parent and it is not already created, we create it and point the
        # child to it.
        if ou.parent not in self.processed:
            self._upsert_ou(ous[ou.parent], ous)
        values["parent"] = self.processed[ou.parent]

        # Finally create the OU we're currently looking at
        self._get_or_create_and_set_values(ou, values)

    def handle(self, *args, **options):
        """
        Handle import of OUs from OrgReg.

        - Updates already present OUs with new information
        - Creates not present OUs
        - Set not present OUs as inactive
        """
        # Empty processed in case of class reuse by tests
        self.processed = {}
        client = orgreg_client.get_client(**settings.ORGREG_CLIENT)

        # Fetch already present OUs and those in OrgReg
        current_ous = {
            int(i.value): i.orgunit
            for i in OuIdentifier.objects.filter(
                source=settings.ORGREG_SOURCE, name=settings.ORGREG_NAME
            )
        }

        logger.info("Fetching OUs from Orgreg...")
        orgreg_ous = {i.ou_id: i for i in client.get_ou()}

        # Set deleted if an OU from Greg no longer exists in OrgReg
        logger.info("Setting deleted tag on removed OUs...")
        for ou_id, ou in current_ous.items():
            if ou_id not in orgreg_ous:
                logger.info("%s marked as deleted", ou)
                ou.deleted = True
                ou.save()
                continue

        # Create new OUs recursively to ensure parents exist before children
        logger.info("Updating OU values...")
        for ou_id, ou in orgreg_ous.items():
            self._upsert_ou(ou, orgreg_ous)


def import_from_orgreg():
    importer = OrgregImporter()
    importer.handle()
