from rest_framework.permissions import BasePermission

from gregui.models import GregUserProfile


class IsSponsor(BasePermission):
    def has_permission(self, request, view):
        try:
            user = GregUserProfile.objects.get(user=request.user)
            return bool(user.sponsor)
        except GregUserProfile.DoesNotExist:
            return False
