from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GregAppConfig(AppConfig):
    name = "greg"
    verbose_name = _("Greg")

    def ready(self):
        import greg.signals  # pylint: disable=unused-import, import-outside-toplevel
