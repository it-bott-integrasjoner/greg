"""
Adds a few more specific models for testing purposes. Alternative to the similiar
script for adding random data to various fields.

WARNING: This command removes all entries in most tables. Do not execute it unless you
are absolutely certain you know what you are doing.

There are 4 guests:
  - One has been invited and not done anything yet
  - One has followed the invite link and provided a passport number, and is waiting for
    the sponsor to act
  - One has had their passport number confirmed by the sponsor and the role is active
  - One like the previous but the role has ended

There is an OU tree with 4 OUs on three levels, with respective OUIdentifiers.
There is an invitation object related to each guest with InvitationLinks that are
expired for all but they invited guest that has not responded.

There are Consent Types, and the active guests have consented to the mandatory one, but
one of them has denied the other one.

"""

import datetime

from django.db import connection
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from greg.models import (
    Consent,
    ConsentChoice,
    ConsentType,
    Identity,
    Invitation,
    InvitationLink,
    OrganizationalUnit,
    OuIdentifier,
    Person,
    Role,
    RoleType,
    Sponsor,
    SponsorOrganizationalUnit,
)


ROLE_TYPE_GUEST_RESEARCHER = "guest-researcher"
ROLE_TYPE_EMERITUS = "emeritus"
ROLE_TYPE_EXTERNAL_CONSULTANT = "external-consultant"
ROLE_TYPE_EXTERNAL_PARTNER = "external-partner"
TESTDATA_SOURCE = "testsource"
SPONSOR_FEIDEID = "sponsor@feide.no"
OU_EUROPE_NAME_EN = "Europe"
CONSENT_IDENT_MANDATORY = "mandatory"
CONSENT_IDENT_OPTIONAL = "optional"


class DatabasePopulation:
    """
    Helper class for populating database with specific data

    Run the file in the Django shell: exec(open('greg/tests/populate_fixtures.py').read())
    """

    def truncate_tables(self):
        print("truncating tables...")
        with connection.cursor() as cursor:
            for table in (
                "greg_consent",
                "greg_consentchoice",
                "greg_consenttype",
                "greg_notification",
                "greg_identity",
                "greg_invitationlink",
                "greg_invitation",
                "greg_role",
                "greg_sponsororganizationalunit",
                "greg_roletype",
                "greg_ouidentifier",
                "greg_organizationalunit",
                "gregui_greguserprofile",
                "greg_person",
                "greg_sponsor",
            ):
                print("purging table", table)
                cursor.execute(f"DELETE FROM {table}")
        print("...tables purged")

    def _add_consent_types_and_choices(self):
        mandatory = ConsentType.objects.create(
            identifier=CONSENT_IDENT_MANDATORY,
            name_en="Mandatory consent",
            name_nb="Obligatorisk samtykke",
            name_nn="Obligatorisk samtykke",
            description_en="Description for mandatory consent",
            description_nb="Beskrivelse for obligatorisk samtykke",
            description_nn="Forklaring for obligatorisk samtykke",
            user_allowed_to_change=False,
            mandatory=True,
        )
        ConsentChoice.objects.create(
            consent_type=mandatory,
            value="yes",
            text_en="Yes",
            text_nb="Ja",
            text_nn="Ja",
        )
        optional = ConsentType.objects.create(
            identifier=CONSENT_IDENT_OPTIONAL,
            name_en="Optional consent",
            name_nb="Valgfritt samtykke",
            name_nn="Valfritt samtykke",
            user_allowed_to_change=False,
        )
        ConsentChoice.objects.create(
            consent_type=optional,
            value="yes",
            text_en="Yes",
            text_nb="Ja",
            text_nn="Ja",
        )
        ConsentChoice.objects.create(
            consent_type=optional,
            value="no",
            text_en="No",
            text_nb="Nei",
            text_nn="Nei",
        )

    def _add_ous_with_identifiers(self):
        """
        Create a simple tree

        earth - america
              - europe - norway
        """
        earth = OrganizationalUnit.objects.create(
            name_nb="Universitetet i Jorden", name_en="University of Earth"
        )
        OuIdentifier.objects.create(
            name=settings.ORGREG_NAME,
            source=settings.ORGREG_SOURCE,
            value="2",
            orgunit=earth,
        )
        OuIdentifier.objects.create(
            name="shortname",
            source=settings.ORGREG_SOURCE,
            value="UiJ",
            orgunit=earth,
        )
        OuIdentifier.objects.create(
            name="legacy_stedkode",
            source="sapuio",
            value="1000",
            orgunit=earth,
        )
        europe = OrganizationalUnit.objects.create(
            name_nb="Europa", name_en=OU_EUROPE_NAME_EN, parent=earth
        )
        OuIdentifier.objects.create(
            name=settings.ORGREG_NAME,
            source=settings.ORGREG_SOURCE,
            value="3",
            orgunit=europe,
        )
        OuIdentifier.objects.create(
            name="shortname",
            source=settings.ORGREG_SOURCE,
            value="EU",
            orgunit=europe,
        )
        OuIdentifier.objects.create(
            name="legacy_stedkode",
            source="sapuio",
            value="1100",
            orgunit=europe,
        )
        america = OrganizationalUnit.objects.create(
            name_nb="Amerika", name_en="America", parent=earth
        )
        OuIdentifier.objects.create(
            name="shortname",
            source=settings.ORGREG_SOURCE,
            value="AM",
            orgunit=america,
        )
        OuIdentifier.objects.create(
            name="legacy_stedkode",
            source="sapuio",
            value="1200",
            orgunit=america,
        )
        OuIdentifier.objects.create(
            name=settings.ORGREG_NAME,
            source=settings.ORGREG_SOURCE,
            value="4",
            orgunit=america,
        )
        norway = OrganizationalUnit.objects.create(
            name_nb="Norge", name_en="Norway", parent=europe
        )
        OuIdentifier.objects.create(
            name=settings.ORGREG_NAME,
            source=settings.ORGREG_SOURCE,
            value="5",
            orgunit=norway,
        )
        OuIdentifier.objects.create(
            name="shortname",
            source=settings.ORGREG_SOURCE,
            value="EU-NOR",
            orgunit=norway,
        )
        OuIdentifier.objects.create(
            name="legacy_stedkode",
            source="sapuio",
            value="1110",
            orgunit=norway,
        )

    def _add_roletypes(self):
        RoleType.objects.create(
            identifier=ROLE_TYPE_GUEST_RESEARCHER,
            name_nb="Gjesteforsker",
            name_en="Guest researcher",
            description_nb="Forsker som ikke er lønnet av UiO",
            description_en="Researchers who are not paid by UiO",
        )
        RoleType.objects.create(
            identifier=ROLE_TYPE_EMERITUS,
            name_nb="Emeritus",
            name_en="Emeritus",
            description_nb="Ansatt som har gått av med pensjon",
            description_en="A retired employee",
            max_days=700,
        )
        RoleType.objects.create(
            identifier=ROLE_TYPE_EXTERNAL_CONSULTANT,
            name_nb="Ekstern konsulent",
            name_en="External consultant",
            description_nb="Person som UiO ikke lønner direkte",
            description_en="A person that UiO does not pay directly",
        )
        RoleType.objects.create(
            identifier=ROLE_TYPE_EXTERNAL_PARTNER,
            name_nb="Ekstern partner",
            name_en="External partner",
            description_nb="Person som ikke er lønnet av UiO",
            description_en="A person that is not paid by UiO",
        )

    def _add_sponsors(self):
        """Add a sponsor connected to the Europe unit"""
        sam = Sponsor.objects.create(
            feide_id=SPONSOR_FEIDEID, first_name="Sam", last_name="Sponsorson"
        )
        SponsorOrganizationalUnit.objects.create(
            sponsor=sam,
            organizational_unit=OrganizationalUnit.objects.get(
                name_en=OU_EUROPE_NAME_EN
            ),
            hierarchical_access=False,
        )

    def _add_invited_person(self):
        """Person that has been invited and has not followed their invite"""
        iggy = Person.objects.create(first_name="Iggy", last_name="Invited")
        role = Role.objects.create(
            person=iggy,
            type=RoleType.objects.get(identifier=ROLE_TYPE_GUEST_RESEARCHER),
            orgunit=OrganizationalUnit.objects.get(name_en=OU_EUROPE_NAME_EN),
            start_date=datetime.date.today() + datetime.timedelta(days=2),
            end_date=datetime.date.today() + datetime.timedelta(days=100),
            sponsor=Sponsor.objects.get(feide_id=SPONSOR_FEIDEID),
        )
        invitation = Invitation.objects.create(
            role=role,
        )
        Identity.objects.create(
            person=iggy,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            source=TESTDATA_SOURCE,
            value="iggy@example.com",
        )
        InvitationLink.objects.create(
            invitation=invitation,
            expire=timezone.now() + datetime.timedelta(days=30),
        )

    def _add_waiting_person(self):
        """
        A person with an active role but missing a verified identity of type national
        id or passport.
        """
        walter = Person.objects.create(
            first_name="Walter",
            last_name="Waiting",
            registration_completed_date=datetime.date.today()
            - datetime.timedelta(days=10),
        )
        role = Role.objects.create(
            person=walter,
            type=RoleType.objects.get(identifier=ROLE_TYPE_GUEST_RESEARCHER),
            orgunit=OrganizationalUnit.objects.get(name_en=OU_EUROPE_NAME_EN),
            start_date=datetime.date.today() - datetime.timedelta(days=30),
            end_date=datetime.date.today() + datetime.timedelta(days=100),
            sponsor=Sponsor.objects.get(feide_id=SPONSOR_FEIDEID),
        )
        Identity.objects.create(
            person=walter,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            source=TESTDATA_SOURCE,
            value="walter@example.com",
        )
        Identity.objects.create(
            person=walter,
            type=Identity.IdentityType.PASSPORT_NUMBER,
            source=TESTDATA_SOURCE,
            value="SE-123456789",
        )
        invitation = Invitation.objects.create(
            role=role,
        )
        InvitationLink.objects.create(
            invitation=invitation,
            expire=timezone.now() - datetime.timedelta(days=35),
        )

    def _add_active_person(self):
        """
        A person with an active role and a verified identity of type Norwegian national ID number or
        passport.
        """
        adam = Person.objects.create(
            first_name="Adam",
            last_name="Active",
            registration_completed_date=datetime.date.today()
            - datetime.timedelta(days=10),
        )
        role = Role.objects.create(
            person=adam,
            type=RoleType.objects.get(identifier=ROLE_TYPE_GUEST_RESEARCHER),
            orgunit=OrganizationalUnit.objects.get(name_en=OU_EUROPE_NAME_EN),
            start_date=datetime.date.today() - datetime.timedelta(days=30),
            end_date=datetime.date.today() + datetime.timedelta(days=100),
            sponsor=Sponsor.objects.get(feide_id=SPONSOR_FEIDEID),
        )
        Identity.objects.create(
            person=adam,
            type=Identity.IdentityType.PASSPORT_NUMBER,
            source=TESTDATA_SOURCE,
            value="NO-123456789",
            verified_at=timezone.now() - datetime.timedelta(days=31),
        )
        Identity.objects.create(
            person=adam,
            type=Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
            source=TESTDATA_SOURCE,
            value="+4792492412",
            verified_at=timezone.now() - datetime.timedelta(days=205),
        )
        Identity.objects.create(
            person=adam,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            source=TESTDATA_SOURCE,
            value="adam@example.com",
        )
        invitation = Invitation.objects.create(
            role=role,
        )
        InvitationLink.objects.create(
            invitation=invitation,
            expire=timezone.now() - datetime.timedelta(days=32),
        )
        mandatory_consent_type = ConsentType.objects.get(
            identifier=CONSENT_IDENT_MANDATORY
        )
        Consent.objects.create(
            person=adam,
            type=mandatory_consent_type,
            consent_given_at=datetime.date.today() - datetime.timedelta(days=10),
            choice=ConsentChoice.objects.get(
                consent_type=mandatory_consent_type, value="yes"
            ),
        )
        optional_consent_type = ConsentType.objects.get(
            identifier=CONSENT_IDENT_OPTIONAL
        )
        Consent.objects.create(
            person=adam,
            type=optional_consent_type,
            consent_given_at=datetime.date.today() - datetime.timedelta(days=10),
            choice=ConsentChoice.objects.get(
                consent_type=optional_consent_type, value="no"
            ),
        )

    def _add_expired_person(self):
        """
        A person with an inactive role, and a verified identity of type
        Norwegian national ID number or passport.
        """
        esther = Person.objects.create(
            first_name="Esther",
            last_name="Expired",
            registration_completed_date=timezone.now() - datetime.timedelta(days=206),
        )
        role = Role.objects.create(
            person=esther,
            type=RoleType.objects.get(identifier=ROLE_TYPE_GUEST_RESEARCHER),
            orgunit=OrganizationalUnit.objects.get(name_en=OU_EUROPE_NAME_EN),
            start_date=datetime.date.today() - datetime.timedelta(days=200),
            end_date=datetime.date.today() - datetime.timedelta(days=100),
            sponsor=Sponsor.objects.get(feide_id=SPONSOR_FEIDEID),
        )
        Identity.objects.create(
            person=esther,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            source=TESTDATA_SOURCE,
            value="esther@example.com",
        )
        Identity.objects.create(
            person=esther,
            type=Identity.IdentityType.PASSPORT_NUMBER,
            source=TESTDATA_SOURCE,
            value="DK-123456789",
            verified_at=timezone.now() - datetime.timedelta(days=205),
        )
        Identity.objects.create(
            person=esther,
            type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
            source=TESTDATA_SOURCE,
            value="12345678901",
            verified_at=timezone.now() - datetime.timedelta(days=205),
        )
        invitation = Invitation.objects.create(
            role=role,
        )
        InvitationLink.objects.create(
            invitation=invitation,
            expire=timezone.now() - datetime.timedelta(days=204),
        )
        mandatory_consent_type = ConsentType.objects.get(
            identifier=CONSENT_IDENT_MANDATORY
        )
        Consent.objects.create(
            person=esther,
            type=mandatory_consent_type,
            consent_given_at=datetime.date.today() - datetime.timedelta(days=10),
            choice=ConsentChoice.objects.get(
                consent_type=mandatory_consent_type, value="yes"
            ),
        )

    def populate_database(self):
        print("populating db...")
        # Add the types, sponsors and ous
        self._add_consent_types_and_choices()
        self._add_ous_with_identifiers()
        self._add_roletypes()
        self._add_sponsors()
        # Add the four guests
        self._add_active_person()
        self._add_waiting_person()
        self._add_invited_person()
        self._add_expired_person()
        print("...done populating db")


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser):
        parser.add_argument(
            "--destructive",
            type=str,
            required=True,
            help="Verify database name. THIS COMMAND IS DESTRUCTIVE.",
        )

    def handle(self, *args, **options):
        """
        Handle test data population
        """
        db_name = str(settings.DATABASES["default"]["NAME"])
        if options.get("destructive") != db_name:
            raise CommandError(
                f"Must pass {repr(db_name)} to --destructive, as its tables will be truncated"
            )

        database_population = DatabasePopulation()
        database_population.truncate_tables()
        database_population.populate_database()
