"""
Command for running or scheduling the django-q task for
importing sponsors from Cerebrum.
"""


import structlog

from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django_q.tasks import schedule

from greg.tasks.sponsors import SponsorImporter

logger = structlog.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__ or ""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--root",
            type=str,
            help="Use the specified OU ID (internal pk) as the "
            "root and only import itself and its children",
        )
        parser.add_argument(
            "--schedule",
            default=False,
            action="store_true",
            help="Add a scheduled task for running the import",
        )

    def handle(self, *args, **options):
        if options["schedule"]:
            self.schedule(*args, **options)
            return

        importer = SponsorImporter()
        importer.handle(root=options.get("root"))

    def schedule(self, *args, **kwargs):
        logger.info("Scheduling sponsor import task...")
        schedule(
            func="greg.tasks.sponsors.import_sponsors_from_cerebrum",
            root=kwargs.get("root"),
            schedule_type=settings.SPONSOR_IMPORT_SCHEDULE_TYPE,
        )
        logger.info("Sponsor import task scheduled.")
