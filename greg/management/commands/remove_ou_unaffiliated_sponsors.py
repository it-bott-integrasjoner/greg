"""
Command for running or scheduling removal of manually added OUs from unaffiliated sponsors.
"""

import logging

from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django_q.tasks import schedule

from greg.tasks.remove_ou_unaffiliated_sponsors import RemoveSponsorsOU

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--schedule",
            default=False,
            action="store_true",
            help="Add a scheduled task to remove manually added OUs from all unaffiliated sponsors",
        )
        parser.add_argument(
            "--run-once",
            default=False,
            action="store_true",
            help="Remove manually added OUs from all unaffiliated sponsors once",
        )
        parser.add_argument(
            "--commit",
            default=False,
            action="store_true",
            help="Commit the changes done by running the task once",
        )

    def handle(self, *args, **options):
        if options["schedule"]:
            self.schedule()
            return

        commit = options["commit"]

        if options["run_once"]:
            with transaction.atomic():
                remove = RemoveSponsorsOU()
                remove.remove_ou_from_sponsors()
                if not commit:
                    logger.info("Rolling back changes...")
                    transaction.set_rollback(True)
                else:
                    logger.info("Changes committed")

    def schedule(self):
        logger.info(
            "Scheduling removal of manually added OUs from unaffiliated sponsors..."
        )
        schedule(
            func="greg.tasks.remove_ou_unaffiliated_sponsors.remove_ou_unaffiliated_sponsors",
            schedule_type=settings.SPONSOR_OU_REMOVAL_SCHEDULE_TYPE,
        )
        logger.info("Sponsor OU removal task scheduled")
