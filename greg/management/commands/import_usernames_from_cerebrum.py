"""
Command for running or scheduling the django-q task for importing
usernames from Cerebrum
"""
import logging

from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django.db import transaction
from django_q.tasks import schedule

from greg.tasks.usernames import UsernameImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser: CommandParser) -> None:
        group = parser.add_mutually_exclusive_group()
        group.add_argument(
            "--all-persons",
            action="store_true",
            default=False,
            help="Import username for all persons in GREG",
        )
        group.add_argument(
            "--without-usernames",
            action="store_true",
            default=False,
            help="Only import those without usernames in GREG",
        )
        group.add_argument(
            "--with-usernames",
            action="store_true",
            default=False,
            help="Only import those with usernames in GREG",
        )
        parser.add_argument(
            "--schedule",
            action="store_true",
            default=False,
            help="Add a scheduled task for running the import",
        )
        parser.add_argument(
            "--commit",
            action="store_true",
            default=False,
            help="Commit the changes done by running the import once",
        )

    def schedule_task(self, version, schedule_type, logger_msg):
        logger.info(logger_msg)
        schedule(
            func="greg.tasks.usernames.import_usernames",
            version=version,
            schedule_type=schedule_type,
        )
        logger.info("Username import scheduled")

    def handle(self, *args, **options):
        """Schedule or run import of usernames"""

        if options["with_usernames"]:
            version = "with_usernames"
            schedule_type = settings.USERNAME_SCHEDULE_WITH_USERNAME
            logger_msg = "Scheduling import of usernames for persons with username..."
        elif options["without_usernames"]:
            version = "without_usernames"
            schedule_type = settings.USERNAME_SCHEDULE_WITHOUT_USERNAME
            logger_msg = (
                "Scheduling import of usernames for persons without username..."
            )
        else:
            version = "all"
            schedule_type = settings.USERNAME_SCHEDULE_ALL
            logger_msg = "Scheduling import of usernames for all persons..."

        if options["schedule"]:
            self.schedule_task(
                version=version,
                schedule_type=schedule_type,
                logger_msg=logger_msg,
            )
            return

        # Will assume to run once if schedule is not given
        with transaction.atomic():
            logger.info("Importing usernames...")
            importer = UsernameImporter()
            importer.import_usernames(version)
            if not options["commit"]:
                logger.info("Rolling back changes...")
                transaction.set_rollback(True)
            else:
                logger.info("Changes committed")
