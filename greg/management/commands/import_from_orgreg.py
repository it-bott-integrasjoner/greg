"""
Command for running or scheduling the django-q task for
importing OUs from OrgReg.
"""
import logging

from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django_q.tasks import schedule

from greg.tasks.orgreg import OrgregImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__ or ""

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--schedule",
            default=False,
            action="store_true",
            help="Add a scheduled task for running the import",
        )
        parser.add_argument(
            "--run-once",
            default=False,
            action="store_true",
            help="Run the import once",
        )
        parser.add_argument(
            "--commit",
            default=False,
            action="store_true",
            help="Commit the changes done by running the import once",
        )

    def handle(self, *args, **options):
        if options["schedule"]:
            self.schedule()
            return

        commit = options["commit"]

        if options["run_once"]:
            with transaction.atomic():
                importer = OrgregImporter()
                importer.handle()
                if not commit:
                    logger.info("Rolling back changes...")
                    transaction.set_rollback(True)
                else:
                    logger.info("Changes committed")

    def schedule(self):
        logger.info("Scheduling orgreg import task...")
        schedule(
            func="greg.tasks.orgreg.import_from_orgreg",
            schedule_type=settings.ORGREG_SCHEDULE_TYPE,
        )
        logger.info("Orgreg import task scheduled.")
