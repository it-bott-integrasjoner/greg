"""
Command for scheduling or running the task for notifying sponsors of
expiring roles.
"""

import logging
import logging.config

from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django_q.tasks import schedule

from greg.tasks.notify_role_end import (
    notify_sponsors_roles_ending,
    notify_guests_roles_ending,
)

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser: CommandParser) -> None:
        guest_sponsor = parser.add_mutually_exclusive_group()
        parser.add_argument(
            "--schedule",
            default=False,
            action="store_true",
            help="Add a scheduled task for running the role end notifier",
        )
        parser.add_argument(
            "--run-once",
            default=False,
            action="store_true",
            help="Run the import once",
        )
        guest_sponsor.add_argument(
            "--guests",
            default=False,
            action="store_true",
            help="Notify guests about roles ending",
        )
        guest_sponsor.add_argument(
            "--sponsors",
            default=False,
            action="store_true",
            help="Notify sponsors about roles ending",
        )

    def handle(self, *args, **options):
        if options["sponsors"] and options["schedule"]:
            self.schedule_sponsors()
            return

        if options["sponsors"] and options["run_once"]:
            notify_sponsors_roles_ending()
            return

        if options["guests"] and options["schedule"]:
            self.schedule_guests()
            return

        if options["guests"] and options["run_once"]:
            notify_guests_roles_ending()
            return

        logger.info("Nothing done")

    def schedule_sponsors(self, *args, **options):
        logger.info("Scheduling role end notifier task for sponsors...")
        schedule(
            func="greg.tasks.notify_role_end.notify_sponsors_roles_ending",
            schedule_type=settings.SPONSOR_NOTIFIER_SCHEDULE_TYPE,
        )
        logger.info("Role end notifier task for sponsors scheduled.")

    def schedule_guests(self, *args, **options):
        logger.info("Scheduling role end notifier task for guests...")
        schedule(
            func="greg.tasks.notify_role_end.notify_guests_roles_ending",
            schedule_type=settings.GUEST_NOTIFIER_SCHEDULE_TYPE,
        )
        logger.info("Role end notifier task for guests scheduled.")
