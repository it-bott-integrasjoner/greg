"""
Command for moving roles between OUs.
"""
import re
import logging

from argparse import ArgumentTypeError

from django.db import transaction
from django.core.management.base import BaseCommand, CommandParser

from greg.models import OrganizationalUnit, Role

logger = logging.getLogger(__name__)

RE_SOURCE_TARGET = r"(\d+):(\d+)"


def get_ou_by_stedkode(stedkode):
    try:
        return OrganizationalUnit.objects.get(
            identifiers__name="legacy_stedkode", identifiers__value=stedkode
        )
    except OrganizationalUnit.DoesNotExist:
        return None


def source_target_stedkode(s):
    match = re.search(RE_SOURCE_TARGET, s)
    if not match:
        raise ArgumentTypeError(
            "should be on the form source:target, i.e. 100000:200000"
        )
    source_sko, target_sko = match.groups()
    source = get_ou_by_stedkode(source_sko)
    if not source:
        raise ArgumentTypeError(f"no OU identified by legacy_stedkode {source_sko!r}")
    target = get_ou_by_stedkode(target_sko)
    if not target:
        raise ArgumentTypeError(f"no OU identified by legacy_stedkode {target_sko!r}")
    return source, target


def move_roles_for_ou(source: OrganizationalUnit, target: OrganizationalUnit):
    roles = Role.objects.filter(orgunit=source)
    logger.info("Moving roles from %s to %s", source, target)
    logger.info("Found %d roles at %s", roles.count(), source)
    for role in roles:
        role.orgunit = target
        changes = role.get_dirty_fields(check_relationship=True, verbose=True)
        logger.info(
            "Changing OU for role_id=%d from %s to %s %r",
            role.pk,
            source,
            target,
            changes,
        )
        role.save()


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--move-stedkode",
            type=source_target_stedkode,
            action="append",
            help="Source and target OU, identified by legacy_stedkode, formatted as source:target",
        )
        parser.add_argument(
            "--commit",
            default=False,
            action="store_true",
            help="Commit the changes done by running the import once",
        )

    def handle(self, *args, **options):
        commit = options["commit"]

        move_stedkode = options["move_stedkode"]
        candidates = move_stedkode

        with transaction.atomic():
            for source, target in candidates:
                move_roles_for_ou(source, target)
            if not commit:
                logger.info("Rolling back changes...")
                transaction.set_rollback(True)
            else:
                logger.info("Changes committed")
