"""
Command for running or scheduling deletion of inactive roles and users.
"""
import logging

from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django_q.tasks import schedule

from greg.tasks.delete_expired_persons import DeletePersons

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--schedule",
            default=False,
            action="store_true",
            help="Add a scheduled task for deleting all expired roles and users",
        )
        parser.add_argument(
            "--run-once",
            default=False,
            action="store_true",
            help="Delete all expired roles and users once",
        )
        parser.add_argument(
            "--commit",
            default=False,
            action="store_true",
            help="Commit the changes done by running the task once",
        )

    def handle(self, *args, **options):
        if options["schedule"]:
            self.schedule()
            return

        commit = options["commit"]

        if options["run_once"]:
            with transaction.atomic():
                delete = DeletePersons()
                delete.delete_roles_and_persons()
                if not commit:
                    logger.info("Rolling back changes...")
                    transaction.set_rollback(True)
                else:
                    logger.info("Changes committed")

    def schedule(self):
        logger.info("Scheduling deletion of roles and persons...")
        schedule(
            func="greg.tasks.delete_expired_persons.delete_expired",
            schedule_type=settings.ROLE_REMOVAL_SCHEDULE_TYPE,
        )
        logger.info("Delete task scheduled")
