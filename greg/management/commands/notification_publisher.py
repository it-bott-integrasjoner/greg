import json
import logging
import logging.config
from typing import Union

from django.conf import settings
from django.core.management.base import BaseCommand
from pika_context_manager import PCM

from greg.models import Notification
from greg.utils import camel_to_snake

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)


def get_notifications(limit):
    return Notification.objects.all()[:limit]


def generate_event_type(n: Notification) -> str:
    """
    Generate CloudEvent compliant type field.
    """
    object_type = camel_to_snake(n.object_type)
    return f"{object_type}.{n.operation}"


def generate_routing_key(n: Notification) -> str:
    """
    Generate an AMQP routing key.

    By default this equals the payload type field, but is prefixed with the
    value in settings.NOTIFICATION_ROUTING_KEY_PREFIX
    """
    event_type = generate_event_type(n)
    prefix = settings.NOTIFICATION_ROUTING_KEY_PREFIX
    return f"{prefix}{event_type}"


def create_cloud_event_payload(n: Notification) -> str:
    """
    Produce a notification payload from a Notification object that matches CloudEvents
    spec.

    This ensures that the combination of source and id is unique for all messages
    published from this instance of greg. If you mix messages from multiple instances of
    greg this will fail.

    Using the object_type and the identifier gives us which api endpoint to use and
    which id to look up.

    specversion specifies version of CloudEvents spec, in our case 1.0. Note we're using
    only major and minor versions. This function was written at the time of specversion
    1.0.1.

    The type describes which sort of event has happened to the object identifier by id
    and source, i.e no.local.greg.person.add
    """

    content: dict[str, Union[str, dict[str, str]]] = {
        "id": str(n.id),
        "source": f"greg:{settings.INSTANCE_NAME}:{settings.ENVIRONMENT}",
        "specversion": "1.0",
        "type": generate_event_type(n),
    }
    if n.meta:
        content["data"] = n.meta
    return json.dumps(content)


def handle_one_notification(notification: Notification, pcm: PCM, exchange: str):
    """
    Publish a message to an exchange with the information contained in a Notification
    and delete the Notification afterwards.
    """
    payload = create_cloud_event_payload(notification)
    routing_key = generate_routing_key(notification)

    if pcm.publish(
        exchange,
        routing_key,
        payload,
        content_type="application/json",
    ):
        logger.info(
            "Published message %s with routing key %s to exchange %s",
            payload,
            routing_key,
            exchange,
        )

        notification.delete()
    else:
        logger.error(
            "Failed published message %s with routing key %s to exchange %s",
            payload,
            routing_key,
            exchange,
        )


class Command(BaseCommand):
    help = "Publish notifications via AMQP"

    def handle(self, *args, **options):
        logger.info("Notification publisher started")
        connection_config = settings.NOTIFICATION_PUBLISHER["mq"]["connection"]
        exchange = settings.NOTIFICATION_PUBLISHER["mq"]["exchange"]
        poll_interval = settings.NOTIFICATION_PUBLISHER["poll_interval"]
        with PCM(**connection_config) as pcm:
            while True:
                for notification in get_notifications(limit=500):
                    handle_one_notification(
                        notification=notification, pcm=pcm, exchange=exchange
                    )
                pcm.sleep(poll_interval)
