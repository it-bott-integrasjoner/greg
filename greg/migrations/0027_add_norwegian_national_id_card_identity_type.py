# Generated by Django 4.1.4 on 2022-12-08 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("greg", "0026_remove_ouidentifier_unique_identifier_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="identity",
            name="type",
            field=models.CharField(
                choices=[
                    ("feide_id", "Feide Id"),
                    ("feide_email", "Feide Email"),
                    ("passport_number", "Passport Number"),
                    ("norwegian_national_id_number", "Norwegian National Id Number"),
                    (
                        "norwegian_national_id_card_number",
                        "Norwegian National Id Card Number",
                    ),
                    ("private_email", "Private Email"),
                    ("private_mobile", "Private Mobile Number"),
                    ("migration_id", "Migration Id"),
                ],
                max_length=64,
            ),
        ),
    ]
