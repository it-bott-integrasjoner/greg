# Generated by Django 4.1.2 on 2022-10-13 13:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("greg", "0023_identity_invalid"),
    ]

    operations = [
        migrations.AlterField(
            model_name="person",
            name="gender",
            field=models.CharField(
                blank=True,
                choices=[("male", "Male"), ("female", "Female")],
                max_length=6,
                null=True,
            ),
        ),
    ]
