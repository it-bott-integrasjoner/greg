# Generated by Django 3.2.8 on 2021-10-26 14:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('greg', '0010_roletype_max_days'),
    ]

    operations = [
        migrations.RenameField(
            model_name='role',
            old_name='orgunit_id',
            new_name='orgunit',
        ),
        migrations.RenameField(
            model_name='role',
            old_name='sponsor_id',
            new_name='sponsor',
        ),
    ]
