from django.contrib import admin
from django.contrib.admin import display
from reversion.admin import VersionAdmin

from greg.models import (
    OuIdentifier,
    Invitation,
    InvitationLink,
    Notification,
    Person,
    Role,
    RoleType,
    Identity,
    Consent,
    ConsentChoice,
    ConsentType,
    OrganizationalUnit,
    Sponsor,
    SponsorOrganizationalUnit,
)

admin.site.site_header = "Guest Registration Admin"


class RoleInline(admin.TabularInline):
    model = Role
    extra = 1


class IdentityInline(admin.TabularInline):
    model = Identity
    extra = 1


class ConsentInline(admin.TabularInline):
    model = Consent
    extra = 1


class ConsentChoiceInline(admin.TabularInline):
    model = ConsentChoice
    extra = 1


class PersonAdmin(VersionAdmin):
    list_display = (
        "first_name",
        "last_name",
        "role_count",
    )
    search_fields = ("first_name", "last_name")  # TODO: "identities__value"?
    readonly_fields = ("id", "created", "updated")
    inlines = (RoleInline, IdentityInline, ConsentInline)

    def role_count(self, person):
        return str(person.roles.count())

    role_count.short_description = "# roles"  # type: ignore


class RoleAdmin(VersionAdmin):
    list_display = (
        "id",
        "person",
        "type",
        "start_date",
        "end_date",
        "orgunit",
        "sponsor",
        "comments",
    )
    search_fields = (
        "person__id",
        "person__first_name",
        "person__last_name",
        "type__id",
        "type__identifier",
    )
    list_filter = ("type",)
    raw_id_fields = ("person", "type")
    readonly_fields = ("id", "created", "updated")


class RoleTypeAdmin(VersionAdmin):
    list_display = ("id", "identifier", "name_nb", "name_en", "visible")
    readonly_fields = ("id", "created", "updated")


class IdentityAdmin(VersionAdmin):
    list_display = ("id", "person", "type", "verified")
    list_filter = ("verified",)
    search_fields = ("value",)
    readonly_fields = ("id", "created", "updated")


class ConsentAdmin(VersionAdmin):
    list_display = ("id", "person", "get_consent_type_name")
    readonly_fields = ("id", "created", "updated")

    @display(description="Consent name")
    def get_consent_type_name(self, obj):
        return obj.type.name_en


class ConsentTypeAdmin(VersionAdmin):
    list_display = (
        "id",
        "name_en",
        "valid_from",
        "user_allowed_to_change",
        "mandatory",
    )
    readonly_fields = ("id", "created", "updated")
    inlines = (ConsentChoiceInline,)


class OuIdentifierInline(admin.TabularInline):
    model = OuIdentifier
    extra = 1


class IdentifierAdmin(VersionAdmin):
    list_display = ("id", "name", "source", "value")
    search_fields = ("id", "value")


class OrganizationalUnitAdmin(VersionAdmin):
    list_display = ("id", "name_en", "name_nb", "parent")
    readonly_fields = ("id", "created", "updated")
    search_fields = ("name_en", "name_nb", "id")
    inlines = (OuIdentifierInline,)
    # This is mainly to make the org unit list be sorted in the drop-down box
    # in the role line under persons
    ordering = ("name_nb",)


class OrganizationalUnitInline(admin.TabularInline):
    model = SponsorOrganizationalUnit
    extra = 1


class SponsorAdmin(VersionAdmin):
    list_display = ("id", "feide_id", "first_name", "last_name")
    inlines = (OrganizationalUnitInline,)
    readonly_fields = ("id", "created", "updated")
    search_fields = ("feide_id", "first_name", "last_name")
    ordering = ["feide_id"]


class SponsorOrganizationalUnitAdmin(VersionAdmin):
    list_display = (
        "id",
        "sponsor",
        "organizational_unit",
        "hierarchical_access",
        "source",
        "automatic",
    )
    readonly_fields = ("id", "created", "updated")


class InvitationAdmin(VersionAdmin):
    list_display = ("id", "role", "get_role_person", "get_role_type", "get_role_ou")

    @display(description="Person")
    def get_role_person(self, obj):
        return obj.role.person

    @display(description="Role type")
    def get_role_type(self, obj):
        return obj.role.type

    @display(description="OU")
    def get_role_ou(self, obj):
        return obj.role.orgunit


class InvitationLinkAdmin(VersionAdmin):
    list_display = ("uuid", "invitation", "created", "expire")
    readonly_fields = ("uuid",)


class NotificationAdmin(VersionAdmin):
    list_display = (
        "id",
        "object_type",
        "identifier",
        "operation",
        "created",
        "updated",
        "issued_at",
    )


admin.site.register(Person, PersonAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(RoleType, RoleTypeAdmin)
admin.site.register(Identity, IdentityAdmin)
admin.site.register(Consent, ConsentAdmin)
admin.site.register(ConsentType, ConsentTypeAdmin)
admin.site.register(OrganizationalUnit, OrganizationalUnitAdmin)
admin.site.register(Sponsor, SponsorAdmin)
admin.site.register(SponsorOrganizationalUnit, SponsorOrganizationalUnitAdmin)
admin.site.register(Invitation, InvitationAdmin)
admin.site.register(InvitationLink, InvitationLinkAdmin)
admin.site.register(OuIdentifier, IdentifierAdmin)
admin.site.register(Notification, NotificationAdmin)
