import logging
import re
import typing
from datetime import date, datetime, timedelta, timezone as dt_timezone

from rest_framework import serializers
from django.db import transaction
from django.conf import settings
from django.utils import timezone
from cerebrum_client import CerebrumClient

from greg.models import (
    Identity,
    Invitation,
    InvitationLink,
    Person,
    Role,
    Sponsor,
)
from gregui.mailutils.invite_guest import InviteGuest

logger = logging.getLogger(__name__)


def camel_to_snake(s: str) -> str:
    """Turns `FooBar` into `foo_bar`."""
    return re.sub("([A-Z])", "_\\1", s).lower().lstrip("_")


def is_valid_id_number(input_digits: str) -> bool:
    """
    Checks whether the input represents a valid national ID number (fødselsnummer), a valid D-number
    or a valid SO-number.
    """
    is_dnumber = int(input_digits[0:1]) >= 4
    return is_valid_norwegian_national_id_number(
        input_digits, is_dnumber
    ) or is_valid_so_number(input_digits)


def is_valid_so_number(input_digits: str) -> bool:
    """
    Checks if SO numbers are allowed and if so, check that it is valid.
    """
    if not settings.ALLOW_SO_NUMBERS:
        return False
    is_so_number = False
    day, month, year, pnr = (
        input_digits[0:2],
        int(input_digits[2:4]),
        input_digits[4:6],
        input_digits[6:],
    )
    if pnr[0] != "1":
        return False
    if month > 50:
        is_so_number = True
        month_str = _check_month(month)
        input_digits = day + month_str + year + pnr
    return is_so_number and _check_birthdate(input_digits, is_dnumber=False)


def is_valid_norwegian_national_id_number(input_digits: str, is_dnumber: bool) -> bool:
    """
    Checks whether input_digits is a valid Norwegian national ID number or D-number.

    It is based on the code found here:
    https://github.com/navikt/fnrvalidator/blob/master/src/validator.js
    """
    return (
        _check_correct_number_of_digits(input_digits)
        and _compute_checksum(input_digits)
        and _check_birthdate(input_digits, is_dnumber)
    )


def _check_correct_number_of_digits(input_digits: str) -> bool:
    return re.search("^\\d{11}$", input_digits) is not None


def _check_birthdate(digits: str, is_dnumber: bool) -> bool:
    if is_dnumber:
        # First number has been increased by 4, look here for an explanation of what the
        # numbers in the D-number mean:
        # https://www.skatteetaten.no/person/utenlandsk/norsk-identitetsnummer/d-nummer/
        digits = str((int(digits[0:1]) - 4)) + digits[1:]

    day = int(digits[0:2])
    month = int(digits[2:4])
    year = int(digits[4:6])

    # Year 0 should mean 2000, not 1900
    if year == 0:
        year = 2000

    try:
        # Try to create a date object, it will fail is the date is not valid
        date(year, month, day)
    except ValueError:
        # Not a valid date
        return False

    return True


def _check_month(month: int) -> str:
    month -= 50
    if month < 10:
        return "0" + str(month)
    return str(month)


def _compute_checksum(input_digits: str) -> bool:
    d = [int(s) for s in input_digits]
    k1 = 11 - (
        (
            3 * d[0]
            + 7 * d[1]
            + 6 * d[2]
            + 1 * d[3]
            + 8 * d[4]
            + 9 * d[5]
            + 4 * d[6]
            + 5 * d[7]
            + 2 * d[8]
        )
        % 11
    )

    k2 = 11 - (
        (
            5 * d[0]
            + 4 * d[1]
            + 3 * d[2]
            + 2 * d[3]
            + 7 * d[4]
            + 6 * d[5]
            + 5 * d[6]
            + 4 * d[7]
            + 3 * d[8]
            + 2 * k1
        )
        % 11
    )

    if k1 == 11:
        k1 = 0

    if k2 == 11:
        k2 = 0

    return k1 < 10 and k2 < 10 and k1 == d[9] and k2 == d[10]


def date_to_datetime_midnight(in_date: date) -> datetime:
    """Convert a date object to a datetime object with timezone utc"""
    return datetime.combine(in_date, datetime.min.time(), tzinfo=dt_timezone.utc)


def str2date(in_date: typing.Union[str, date]):
    return date.fromisoformat(in_date) if isinstance(in_date, str) else in_date


def get_default_invitation_expire_date_from_now():
    return timezone.now() + timedelta(days=settings.INVITATION_DURATION)


def is_identity_duplicate(identity_type: Identity.IdentityType, value: str) -> bool:
    return Identity.objects.filter(type=identity_type).filter(value=value).exists()


def create_objects_for_invitation(
    person_data: dict,
    person_email: str,
    role_data: dict,
    sponsor: typing.Union[Sponsor, str],
) -> Person:
    with transaction.atomic():
        # If only sponsor's feide_id is provided create the sponsor
        if isinstance(sponsor, str):
            sponsor = Sponsor.objects.create(feide_id=sponsor)

        person = Person.objects.create(**person_data)
        Identity.objects.create(
            person=person,
            type=Identity.IdentityType.PRIVATE_EMAIL,
            value=person_email,
            source=settings.DEFAULT_IDENTITY_SOURCE,
        )
        role_data["person"] = person
        role_data["sponsor"] = sponsor
        role = Role.objects.create(**role_data)
        invitation = Invitation.objects.create(role=role)
        InvitationLink.objects.create(
            invitation=invitation,
            expire=timezone.now() + timedelta(days=30),
        )

    return person


def queue_mail_to_invitee(
    person: Person,
    sponsor: Sponsor,
    is_fresh_registration: bool = True,
):
    for invitationlink in InvitationLink.objects.filter(
        invitation__role__person_id=person.id,
        invitation__role__sponsor_id=sponsor.id,
    ):
        invitemailer = InviteGuest()
        invitemailer.queue_mail(invitationlink, is_fresh_registration)


def role_invitation_date_validator(
    start_date: typing.Optional[datetime], end_date: datetime, max_days: int
):
    """Validates dates for a role. max_days comes from a roleType."""

    # Ensure end date is after start date if start date is given
    if start_date and end_date < start_date:
        raise serializers.ValidationError("End date cannot be before start date.")

    # Ensure end date is not further into the future than the role type allows
    today = date.today()
    max_date = today + timedelta(days=max_days)
    if max_date < end_date:
        raise serializers.ValidationError(
            "New end date too far into the future for this type. "
            f"Must be before {max_date.strftime('%Y-%m-%d')}."
        )


def string_contains_illegal_chars(string: str) -> bool:
    # Only allow the following characters:
    # ----- Basic Latin -----
    # U+0020 (Space)
    # U+002D (Hyphen-minus)
    # U+002E (Full stop)
    # U+0041 - U+005A (Latin Alphabet: Uppercase)
    # U+0061 - U+007A (Latin Alphabet: Lowercase)
    # ----- Latin-1 Supplement -----
    # U+00C0 - U+00D6 (Letters: Uppercase)
    # U+00D8 - U+00DE (Letters: Uppercase)
    # U+00DF - U+00F6 (Letters: Lowercase)
    # U+00F8 - U+00FF (Letters: Lowercase)
    # ----- Latin Extended-A -----
    # U+0100 - U+017F (European Latin)
    return bool(
        re.search(
            r"[^\u0020\u002D\u002E\u0041-\u005A\u0061-"
            "\u007A\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u017F]",
            string,
        )
    )


def get_cerebrum_person_by_greg_id(
    person: Person, client: CerebrumClient, source: str, id_type: str
):
    try:
        persons = list(
            client.search_person_external_ids(
                source_system=source,
                id_type=id_type,
                external_id=person.pk,
            )
        )
    except ConnectionError as e:
        logger.exception(
            "Failed to get person with person_id=%s. Got error=%s. Skipping person",
            person.pk,
            e,
        )
        return None
    if not persons:
        logger.info(
            "Found no person in cerebrum matching id_type=%s and person_id=%s from source=%s",
            id_type,
            person.pk,
            source,
        )
        return None
    if len(persons) > 1:
        logger.info(
            "Found %s persons with same id_type=%s and person_id=%s in source_system=%s",
            len(persons),
            id_type,
            person.pk,
            source,
        )
        return None

    return persons[0]


def get_cerebrum_person_by_username(username: str, client: CerebrumClient):
    try:
        account = client.get_account(name=username)
    except ConnectionError as e:
        logger.exception(
            "Failed to get account with username=%s. Got error=%s.",
            username,
            e,
        )
        raise
    if not account:
        logger.info("Found no account with username %s")
        return None
    try:
        person = client.get_person(person_id=account.owner.id)
    except ConnectionError as e:
        logger.exception(
            "Failed to get person for account with username=%s. Got error=%s.",
            username,
            e,
        )
        raise
    if not person:
        logger.info("Found no person for account with username=%s", username)
        return None
    return person
