const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = (app) => {
  app.use(
    [
      '/admin*', // Django admin
      '/staticfiles*', // Django static files
      '/api*', // API
      '/schema*', // API schema
      '/oidc*', // OpenID connect login endpoint
    ],
    createProxyMiddleware({
      target: 'http://localhost:8000',
      changeOrigin: true,
    })
  )
}
