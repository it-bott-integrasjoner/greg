// adds the 'fetchMock' global variable and rewires 'fetch' global to call 'fetchMock' instead of the real implementation
// eslint-disable-next-line import/no-extraneous-dependencies
require('jest-fetch-mock').enableMocks()
// changes default behavior of fetchMock to use the real 'fetch' implementation and not mock responses
// eslint-disable-next-line no-undef
fetchMock.dontMock()
