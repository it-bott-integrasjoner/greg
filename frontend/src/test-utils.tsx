import { ThemeProvider } from '@mui/material/styles'
import { render } from '@testing-library/react'
import React from 'react'
import { I18nextProvider, initReactI18next } from 'react-i18next'
import i18n from 'i18next'

import { defaultTheme } from 'themes'

i18n.use(initReactI18next).init({
  lng: 'en',
  fallbackLng: 'en',

  ns: ['translationsNS'],
  defaultNS: 'translationsNS',

  debug: false,

  interpolation: {
    escapeValue: false,
  },

  resources: { en: { translationsNS: {} } },
})

// Providers used in test rendering
const AllTheProviders = ({ children }: any) => (
  <I18nextProvider i18n={i18n}>
    <ThemeProvider theme={defaultTheme}> {children} </ThemeProvider>
  </I18nextProvider>
)

// Custom testing-library/react renderer using our providers.
const customRender = (ui: React.ReactElement, options?: any) =>
  render(ui, { wrapper: AllTheProviders, ...options })

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }
