import parse from 'date-fns/parse'
import {
  getCookie,
  deleteCookie,
  setCookie,
  isValidEmail,
  isValidFirstName,
  isValidFnr,
  isValidSoNumber,
  isValidLastName,
  isValidMobilePhoneNumber,
  maybeCsrfToken,
  submitJsonOpts,
  isFemaleBasedOnNationalId,
  extractBirthdateFromNationalId,
} from './index'

// Mock i18next module to return a translation that just returns the key
jest.mock('i18next', () => ({
  t: (value: string) => value,
}))

test('Invalid phone number', async () => {
  expect(isValidMobilePhoneNumber('dafasfdsfasdf')).not.toEqual(true)
})

test('Empty mobile phone number', async () => {
  expect(isValidMobilePhoneNumber(undefined)).not.toEqual(true)
})

test('Valid phone number', async () => {
  expect(isValidMobilePhoneNumber('+47 97510000')).toEqual(true)
})

test('Valid e-mail', async () => {
  isValidEmail('test@example.org').then((data) => {
    expect(data).toEqual(true)
  })
  isValidEmail('test.testerson@example.org').then((data) => {
    expect(data).toEqual(true)
  })
})

test('Invalid e-mail', async () => {
  isValidEmail('testexample.org').then((data) => {
    expect(data).toEqual('common:validation.invalidEmail')
  })
  isValidEmail('test').then((data) => {
    expect(data).toEqual('common:validation.invalidEmail')
  })
  // Treat special characters as invalid, some services allow them though
  isValidEmail('Øyvind.Åsen@example.org').then((data) => {
    expect(data).toEqual('common:validation.invalidEmail')
  })
  isValidEmail('Test.Tester@åsen.org').then((data) => {
    expect(data).toEqual('common:validation.invalidEmail')
  })
})

test('Valid first name', async () => {
  expect(isValidFirstName('AZ az ÀÖ ØÞßö øÿ Āſ')).toEqual(true)
})

test('Invalid first name', async () => {
  expect(isValidFirstName('')).toEqual('common:validation.firstNameRequired')
  expect(isValidFirstName('aaƂåå')).toEqual(
    'common:validation.firstNameContainsInvalidChars'
  )
  expect(isValidFirstName('!')).toEqual(
    'common:validation.firstNameContainsInvalidChars'
  )
  expect(isValidFirstName('÷')).toEqual(
    'common:validation.firstNameContainsInvalidChars'
  )
  expect(isValidFirstName('汉字')).toEqual(
    'common:validation.firstNameContainsInvalidChars'
  )
})

test('Valid last name', async () => {
  expect(isValidLastName('AZ az ÀÖ ØÞßö øÿ Āſ')).toEqual(true)
})

test('Invalid last name', async () => {
  expect(isValidLastName('')).toEqual('common:validation.lastNameRequired')
  expect(isValidLastName('aaƂåå')).toEqual(
    'common:validation.lastNameContainsInvalidChars'
  )
  expect(isValidLastName('!')).toEqual(
    'common:validation.lastNameContainsInvalidChars'
  )
  expect(isValidLastName('÷')).toEqual(
    'common:validation.lastNameContainsInvalidChars'
  )
  expect(isValidLastName('汉字')).toEqual(
    'common:validation.lastNameContainsInvalidChars'
  )
})

test('Body has values', async () => {
  const data = { foo: 'bar' }
  expect(submitJsonOpts('POST', data)).toEqual({
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: '{"foo":"bar"}',
    credentials: 'same-origin',
  })
})

test('Method has correct value', async () => {
  const data = { foo: 'bar' }
  expect(submitJsonOpts('PATCH', data)).toEqual({
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: '{"foo":"bar"}',
    credentials: 'same-origin',
  })
})

test('Get cookie when set', async () => {
  document.cookie = 'csrftoken=tokenvalue'
  expect(maybeCsrfToken()).toEqual({ 'X-CSRFToken': 'tokenvalue' })
  document.cookie = 'csrftoken= ; expires = Thu, 01 Jan 1970 00:00:00 GMT'
})

test('Get null if cookie not set', async () => {
  expect(maybeCsrfToken()).toEqual(null)
})

test('Get unknown cookie returns null', async () => {
  document.cookie = 'csrftoken=tokenvalue'
  expect(getCookie('foo')).toEqual(null)
  document.cookie = 'csrftoken= ; expires = Thu, 01 Jan 1970 00:00:00 GMT'
})

test('Get known cookie returns value', async () => {
  document.cookie = 'key=value'
  expect(getCookie('key')).toEqual('value')
  document.cookie = 'key= ; expires = Thu, 01, Jan 1970 00:00:00 GMT'
})

test('Deleting cookie removes it', async () => {
  document.cookie = 'key=value'
  expect(getCookie('key')).toEqual('value')
  deleteCookie('key')
  expect(getCookie('key')).toEqual(null)
})

test('setCookie creates a cookie with correct value', async () => {
  setCookie('key', 'value')
  expect(getCookie('key')).toEqual('value')
  deleteCookie('key')
})

test('setCookie overrides value that was set before', async () => {
  setCookie('key', 'value')
  expect(getCookie('key')).toEqual('value')
  setCookie('key', 'differentvalue')
  expect(getCookie('key')).toEqual('differentvalue')
  deleteCookie('key')
})

test('Valid fnr', async () => {
  expect(isValidFnr('04026514903')).toEqual(true)
})

test('SO number validation', async () => {
  expect(isValidSoNumber('22522218883')).toEqual(true)
  expect(isValidSoNumber('22022218883')).toEqual(false)

  expect(isValidFnr('22522218883')).toEqual("common:validation.invalidIdNumber")
  expect(isValidFnr('22522218883', false, true)).toEqual(true)
})

test('Null fnr', async () => {
  expect(isValidFnr(undefined)).toEqual('common:validation.invalidIdNumber')
})

test('Invalid fnr', async () => {
  expect(isValidFnr('')).toEqual('common:validation.invalidIdNumber')
})

test('Female extracted from fnr', async () => {
  expect(isFemaleBasedOnNationalId('12103626631')).toEqual(true)
  expect(isFemaleBasedOnNationalId('08015214474')).toEqual(true)
  expect(isFemaleBasedOnNationalId('26052088029')).toEqual(true)
  expect(isFemaleBasedOnNationalId('11082335449')).toEqual(true)
  expect(isFemaleBasedOnNationalId('11081670619')).toEqual(true)
})

test('Male extracted from fnr', async () => {
  expect(isFemaleBasedOnNationalId('12103626712')).toEqual(false)
  expect(isFemaleBasedOnNationalId('08015214555')).toEqual(false)
  expect(isFemaleBasedOnNationalId('01088538788')).toEqual(false)
  expect(isFemaleBasedOnNationalId('15101739551')).toEqual(false)
  expect(isFemaleBasedOnNationalId('05127648192')).toEqual(false)
})

test('Date of birth extract from D-number', async () => {
  expect(extractBirthdateFromNationalId('53097248016')).toEqual(
    parse('1972-09-13', 'yyyy-MM-dd', new Date())
  )
})

test('Date of birth extracted from fødselsnummer', async () => {
  expect(extractBirthdateFromNationalId('04062141242')).toEqual(
    parse('1921-06-04', 'yyyy-MM-dd', new Date())
  )
})
