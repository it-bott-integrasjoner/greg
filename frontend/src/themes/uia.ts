import { ThemeOptions } from '@mui/material'

/*
 UiA style guide: https://www.uia.no/om-uia/profilmanual-for-uia
 */
const uiaTheme: ThemeOptions = {
  greg: {
    footerLinkBgColor: '#131E29',
    wizardButtonColor: 'black',
  },
  typography: {
    fontFamily: 'Open Sans',
  },
  palette: {
    primary: {
      main: '#C8102E',
      light: '#F0C6CC',
    },
    secondary: {
      main: '#131E29',
      light: '#192343',
    },
  },
}

export default uiaTheme
