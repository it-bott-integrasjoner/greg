const OLD_ENV = process.env

beforeEach(() => {
  jest.resetModules() // Most important - it clears the cache
  process.env = { ...OLD_ENV } // Make a copy
})
afterAll(() => {
  process.env = OLD_ENV // Restore old environment
})

test('Get theme uio', async () => {
  process.env.REACT_APP_INST = 'uio'
  // eslint-disable-next-line global-require
  const getTheme = require('./index').default
  expect(getTheme().palette.primary.main).toEqual('#000000')
})

test('Get theme uib', async () => {
  process.env.REACT_APP_INST = 'uib'
  // eslint-disable-next-line global-require
  const getTheme = require('./index').default
  expect(getTheme().palette.primary.main).toEqual('#cf3c3a')
})

test('Get theme default when set', async () => {
  process.env.REACT_APP_INST = 'default'
  // eslint-disable-next-line global-require
  const getTheme = require('./index').default
  expect(getTheme().palette.primary.main).toEqual('#1976d2')
})

test('Get theme default when unknown', async () => {
  process.env.REACT_APP_INST = 'unknown'
  // eslint-disable-next-line global-require
  const getTheme = require('./index').default
  expect(getTheme().palette.primary.main).toEqual('#1976d2')
})

export {}
