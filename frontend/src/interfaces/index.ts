export type Guest = {
  pid: string
  first: string
  last: string
  email: string
  mobile: string
  fnr: Identity | null
  passport: Identity | null
  national_id_card_number: Identity | null
  feide_id: string | null
  active: boolean
  registered: boolean
  verified: boolean
  invitation_status: string
  roles: Role[]
}

type VerifiedChoices = 'manual' | 'automatic' | ''

export type Identity = {
  id: string
  type: string
  verified_at: Date | null
  verified_by: string | null
  verified: VerifiedChoices
  source: string | null
  value: string
}

export type FetchedIdentity = {
  id: string
  type: string
  verified_at: string | null
  verified_by: string | null
  verified: VerifiedChoices
  source: string | null
  value: string
}

export interface FetchedGuest {
  pid: string
  first: string
  last: string
  email: string
  mobile: string
  fnr: FetchedIdentity | null
  national_id_card_number: FetchedIdentity | null
  passport: FetchedIdentity | null
  feide_id: string | null
  active: boolean
  registered: boolean
  verified: boolean
  invitation_status: string
  roles: FetchedRole[]
}

export type Role = {
  id: string
  name_nb: string
  name_en: string
  ou_nb: string
  ou_en: string
  start_date?: Date
  end_date: Date
  max_days: number
  contact_person_unit: string | null
  comments: string | null
  sponsor_name: string
  sponsor_feide_id: string
  ou_id: number
}

export type FetchedRole = {
  id: string
  name_nb: string
  name_en: string
  ou_nb: string
  ou_en: string
  start_date: string
  end_date: string
  max_days: number
  contact_person_unit: string | null
  comments: string | null
  sponsor_name: string
  sponsor_feide_id: string
  ou_id: number
}

export type ConsentType = {
  name_en: string
  name_nb: string
  name_nn: string
}

export type ConsentChoice = {
  text_en: string
  text_nb: string
  text_nn: string
}

export type FetchedConsent = {
  id: string
  type: ConsentType
  choice: ConsentChoice
  consent_given_at: string
}
export type Consent = {
  id: string
  type: ConsentType
  choice: ConsentChoice
  consent_given_at: Date | null
}

export interface User {
  auth: boolean
  auth_type: string
  fetched: boolean
  person_id: string
  sponsor_id: string
  first_name: string
  last_name: string
  feide_id: string
  email: string
  mobile_phone: string
  fnr: string
  national_id_card_number: string
  passport: string
  roles: FetchedRole[]
  consents: FetchedConsent[]
  registration_completed_date: Date | null
}
