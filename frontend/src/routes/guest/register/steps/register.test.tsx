import { render, screen, waitFor } from 'test-utils'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { LocalizationProvider } from '@mui/lab'
import { addYears } from 'date-fns/fp'
import { act } from 'react-dom/test-utils'

import { FeatureContext } from 'contexts'
import { fireEvent } from '@testing-library/react'
import GuestRegisterStep from './register'
import { GuestRegisterData } from '../enteredGuestData'
import AuthenticationMethod from '../authenticationMethod'
import { GuestInviteInformation } from '../guestDataForm'

function getEmptyGuestData(): GuestInviteInformation {
  return {
    first_name: '',
    last_name: '',
    ou_name_en: '',
    ou_name_nb: '',
    role_name_en: '',
    role_name_nb: '',
    role_start: '',
    role_end: '',
    email: '',
    mobile_phone: '',
    date_of_birth: null,
    fnr: '',
    passport: '',
    countryForCallingCode: '',
    authentication_method: AuthenticationMethod.Invite,
    gender: '',
  }
}

const allFeaturesOn = {
  displayContactAtUnit: true,
  displayComment: true,
  displayContactAtUnitGuestInput: true,
  displayGenderFieldForGuest: true,
}

test('Guest register page showing passport field on manual registration', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={getEmptyGuestData()}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('passport_number_input')).toBeInTheDocument()
  })
})

test('Name not editable if present and invite is Feide', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const testData = getEmptyGuestData()
  testData.authentication_method = AuthenticationMethod.Feide
  testData.first_name = 'Test'
  testData.last_name = 'Tester'

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={testData}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('first-name-input-text')).toBeDisabled()
    expect(screen.queryByTestId('last-name-input-text')).toBeDisabled()
  })
})

test('Name editable if missing and invite is Feide', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const testData = getEmptyGuestData()
  testData.authentication_method = AuthenticationMethod.Feide

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={testData}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('first-name-input-text')).toBeEnabled()
    expect(screen.queryByTestId('last-name-input-text')).toBeEnabled()
  })
})

test('Identifier fields disabled if invite is Feide and Norwegian national ID number present', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const testData = getEmptyGuestData()
  testData.authentication_method = AuthenticationMethod.Feide
  testData.fnr = '12042335418'

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={testData}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('national-id-number-input')).toBeDisabled()
    expect(screen.queryByTestId('passport_number_input')).toBeDisabled()
    expect(screen.queryByTestId('passport-nationality-input')).toBeDisabled()
  })
})

test('Identifier fields enabled if invite is Feide but Norwegian national ID number missing', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const testData = getEmptyGuestData()
  testData.authentication_method = AuthenticationMethod.Feide
  testData.fnr = ''

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={testData}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('national-id-number-input')).toBeEnabled()
    expect(screen.queryByTestId('passport_number_input')).toBeEnabled()
    expect(screen.queryByTestId('passport-nationality-input')).toBeEnabled()
  })
})

test('Identifier fields disabled and name fields enabled if invite is ID-porten', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const testData = getEmptyGuestData()
  testData.authentication_method = AuthenticationMethod.Feide
  testData.fnr = ''

  render(
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <GuestRegisterStep
        nextHandler={nextHandler}
        initialGuestData={testData}
        registerData={null}
      />
    </LocalizationProvider>
  )

  await waitFor(() => {
    expect(screen.queryByTestId('national-id-number-input')).toBeEnabled()
    expect(screen.queryByTestId('passport_number_input')).toBeEnabled()
    expect(screen.queryByTestId('passport-nationality-input')).toBeEnabled()
  })
})

test('Gender required to be set if gender field is showing', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: 'NO',
    mobilePhone: '97543980',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '12345678',
    passportNationality: 'NO',
    dateOfBirth: addYears(-20)(new Date()),
    gender: null,
  }

  const testData = getEmptyGuestData()

  // Need this to be able to call on the submit-method on the form
  const reference = {
    current: {
      doSubmit: jest.fn(),
    },
  }

  render(
    <FeatureContext.Provider value={allFeaturesOn}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  reference.current?.doSubmit()

  // The validation should fail for gender since to value has been set
  const validationMessage = await waitFor(() =>
    screen.getByText('validation.genderIsRequired')
  )
  expect(validationMessage).toBeInTheDocument()
})

test('Gender not required to be set if gender field is not showing', async () => {
  const nextHandler = (registerData: GuestRegisterData) => {
    console.log(`Entered data: ${registerData}`)
  }

  const displayGenderFieldOff = {
    displayContactAtUnit: true,
    displayComment: true,
    displayContactAtUnitGuestInput: true,
    displayGenderFieldForGuest: false,
  }

  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: 'NO',
    mobilePhone: '97543980',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '12345678',
    passportNationality: 'NO',
    dateOfBirth: addYears(-20)(new Date()),
    gender: null,
  }

  const testData = getEmptyGuestData()
  const reference = {
    current: {
      doSubmit: jest.fn(),
    },
  }

  render(
    <FeatureContext.Provider value={displayGenderFieldOff}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  reference.current?.doSubmit()
  await waitFor(
    () => {
      expect(
        screen.queryByText('validation.genderIsRequired')
      ).not.toBeInTheDocument()
    },
    {
      timeout: 5000,
    }
  )
})

test('Guest not allowed to proceed in wizard if phone number is not valid', async () => {
  const nextHandler = jest.fn()

  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: 'NO',
    mobilePhone: '50',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '12345678',
    passportNationality: 'NO',
    dateOfBirth: addYears(-20)(new Date()),
    gender: 'male',
  }

  const testData = getEmptyGuestData()
  testData.gender = 'male'
  const submitMock = jest.fn()

  // Need this to be able to call on the submit-method on the form
  const reference = {
    current: {
      doSubmit: submitMock,
    },
  }

  render(
    <FeatureContext.Provider value={allFeaturesOn}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  await act(async () => reference.current?.doSubmit())

  expect(nextHandler.mock.calls.length).toBe(0)
})

test('Guest allowed to proceed in wizard if data is valid', async () => {
  const nextHandler = jest.fn()

  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: 'NO',
    mobilePhone: '97543992',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '12345678',
    passportNationality: 'NO',
    dateOfBirth: addYears(-20)(new Date()),
    gender: 'male',
  }

  const testData = getEmptyGuestData()
  testData.gender = 'male'
  const submitMock = jest.fn()

  // Need this to be able to call on the submit-method on the form
  const reference = {
    current: {
      doSubmit: submitMock,
    },
  }

  render(
    <FeatureContext.Provider value={allFeaturesOn}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  await act(async () => reference.current?.doSubmit())

  expect(nextHandler.mock.calls.length).toBe(1)
})

test('Default country code gets set in form values', async () => {
  // This stores the data sent from the form when the user clicks submit
  let guestRegisterData: GuestRegisterData | null = null
  const nextHandler = (registerData: GuestRegisterData) => {
    guestRegisterData = registerData
  }

  // Leave the mobile phone country code blank, it should
  // be populated with a default value
  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: '',
    mobilePhone: '97543992',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '12345678',
    passportNationality: 'NO',
    dateOfBirth: addYears(-20)(new Date()),
    gender: 'male',
  }

  const testData = getEmptyGuestData()
  testData.gender = 'male'
  const submitMock = jest.fn()

  // Need this to be able to call on the submit-method on the form
  const reference = {
    current: {
      doSubmit: submitMock,
    },
  }

  render(
    <FeatureContext.Provider value={allFeaturesOn}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  expect(guestRegisterData).toBeNull()
  await act(async () => reference.current?.doSubmit())
  // Check that the mobile country has gotten the default value
  // @ts-ignore
  expect(guestRegisterData.mobilePhoneCountry).toEqual('NO')
})

test('Foreign country code gets set in form values', async () => {
  // This stores the data sent from the form when the user clicks submit
  let guestRegisterData: GuestRegisterData | null = null
  const nextHandler = (registerData: GuestRegisterData) => {
    guestRegisterData = registerData
  }

  const formData: GuestRegisterData = {
    firstName: 'Test',
    lastName: 'TestTwo',
    mobilePhoneCountry: '',
    mobilePhone: '3892778472',
    nationalIdNumber: '',
    nationalIdCardNumber: '',
    passportNumber: '123456',
    passportNationality: 'IT',
    dateOfBirth: addYears(-20)(new Date()),
    gender: 'male',
  }

  const testData = getEmptyGuestData()
  testData.gender = 'male'
  const submitMock = jest.fn()

  // Need this to be able to call on the submit-method on the form
  const reference = {
    current: {
      doSubmit: submitMock,
    },
  }

  render(
    <FeatureContext.Provider value={allFeaturesOn}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestRegisterStep
          nextHandler={nextHandler}
          initialGuestData={testData}
          registerData={formData}
          ref={reference}
        />
      </LocalizationProvider>
    </FeatureContext.Provider>
  )

  const countryCodeInput = screen.getByTestId('phone-country-code-select-inner')
  fireEvent.change(countryCodeInput, { target: { value: 'IT' } })

  expect(guestRegisterData).toBeNull()
  await act(async () => reference.current?.doSubmit())
  // Check that the mobile phone country code has been set to what was entered
  // @ts-ignore
  expect(guestRegisterData.mobilePhoneCountry).toEqual('IT')
})
