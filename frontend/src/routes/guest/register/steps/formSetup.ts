/**
 * Controls how the register form looks. Depending on the invite type and whether there already exists data
 * the form needs to enable and disable fields.
 */
export type FormSetup = {
  allowFirstNameEditable: boolean
  allowLastNameEditable: boolean
  disableIdentifierFields: boolean
}
