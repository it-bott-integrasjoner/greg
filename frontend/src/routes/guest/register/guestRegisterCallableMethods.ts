/**
 * Contains methods that GuestRegister can call on in GuestRegisterStep
 */
export interface GuestRegisterCallableMethods {
  doSubmit: () => void
}
