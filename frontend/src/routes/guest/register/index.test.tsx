import React from 'react'
import fetchMock, { enableFetchMocks } from 'jest-fetch-mock'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { LocalizationProvider } from '@mui/lab'
import { BrowserRouter } from 'react-router-dom'

import { render, screen, act, waitFor, fireEvent } from 'test-utils'
import { FeatureContext } from 'contexts'
import GuestRegister from './index'

enableFetchMocks()

const testData = {
  person: {
    first_name: 'TestTwenty',
    last_name: 'Tester',
    private_mobile: '+4797543910',
    private_email: 'test@example.org',
    fnr: '08015214555',
    passport: 'DK-123456',
  },
  role: {
    ou_name_en: 'English organizational unit name',
    ou_name_nb: 'Norsk navn organisasjonsenhet',
    role_name_en: 'Guest role',
    role_name_nb: 'Gjesterolle',
    start: '2021-08-10',
    end: '2021-08-16',
    contact_person_unit: 'Test contact person',
  },
  meta: {
    session_type: 'invite',
  },
}

const allFeaturesOn = {
  displayContactAtUnit: true,
  displayComment: true,
  displayContactAtUnitGuestInput: true,
  displayGenderFieldForGuest: true,
}

test('Field showing values correctly', async () => {
  fetchMock.mockIf('/api/ui/v1/invited/', () =>
    Promise.resolve<any>(JSON.stringify(testData))
  )

  render(
    <BrowserRouter>
      <FeatureContext.Provider value={allFeaturesOn}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <GuestRegister />
        </LocalizationProvider>
      </FeatureContext.Provider>
    </BrowserRouter>
  )

  await screen.findByDisplayValue(testData.person.first_name)
  await screen.findByDisplayValue(testData.person.last_name)
  await screen.findByDisplayValue(testData.person.private_email)
  await screen.findByDisplayValue(testData.person.fnr)

  // Check that suggestions for date of birth and gender are showing
  await screen.findByDisplayValue('1952-01-08')
  await screen.findByDisplayValue('male')

  // Passport nationality. The i18n-mock sets up en as the i18n.language property, so look for the English name
  await screen.findByText('DK')
  await screen.findByDisplayValue('123456')

  // Mobile phone country code
  await screen.findByDisplayValue('NO')
  await screen.findByDisplayValue('97543910')

  // Role information
  await screen.findByDisplayValue(testData.role.ou_name_en)
  // TODO Haven't been able to mock getting role types properly yet, so not checking role name
  // await screen.findByDisplayValue(testData.role.name_en)
  await screen.findByDisplayValue(
    `${testData.role.start} - ${testData.role.end}`
  )

  // For the default setup the contact person at unit field should be showing
  await screen.findByDisplayValue(testData.role.contact_person_unit)
})

test('Gender and birth date suggestions not showing if no Norwegian national ID given', async () => {
  // Clear the fnr
  // @ts-ignore
  testData.person.fnr = null
  fetchMock.mockIf('/api/ui/v1/invited/', () =>
    Promise.resolve<any>(JSON.stringify(testData))
  )
  render(
    <BrowserRouter>
      <FeatureContext.Provider value={allFeaturesOn}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <GuestRegister />
        </LocalizationProvider>
      </FeatureContext.Provider>
    </BrowserRouter>
  )

  // Wait a bit so that all the values are showing
  await screen.findByDisplayValue(testData.person.first_name)
  await screen.findByDisplayValue(testData.person.last_name)

  // No national is given in the input data so there should be no
  // suggestion for the birthdate or gender
  const dateOfBirth = screen.queryByDisplayValue('1952-01-08')
  expect(dateOfBirth).toBeNull()

  const gender = screen.queryByDisplayValue('male')
  expect(gender).toBeNull()
})

test('Gender and birth date suggestions not overriding existing values', async () => {
  // Make the date of birth and Norwegian national ID not match
  testData.person.fnr = '08015214555'
  // @ts-ignore
  testData.person.date_of_birth = '1960-01-08'
  // Also set the gender to female to check that it is not overridden by a suggestion
  // @ts-ignore
  testData.person.gender = 'female'

  fetchMock.mockIf('/api/ui/v1/invited/', () =>
    Promise.resolve<any>(JSON.stringify(testData))
  )
  render(
    <BrowserRouter>
      <FeatureContext.Provider value={allFeaturesOn}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <GuestRegister />
        </LocalizationProvider>
      </FeatureContext.Provider>
    </BrowserRouter>
  )

  // In this a date of birth was already set, and it should not have been overridden by a suggestion
  // @ts-ignore
  await screen.findByDisplayValue(testData.person.date_of_birth)

  // Check that the gender has not been overridden
  await screen.findByDisplayValue('female')
})

test('Gender is remembered when going back', async () => {
  testData.person.fnr = ''
  // @ts-ignore
  testData.person.date_of_birth = '2002-08-20'
  testData.person.passport = 'NO-12345678'

  fetchMock.mockIf('/api/ui/v1/invited/', () =>
    Promise.resolve<any>(JSON.stringify(testData))
  )

  render(
    <BrowserRouter>
      <FeatureContext.Provider value={allFeaturesOn}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <GuestRegister />
        </LocalizationProvider>
      </FeatureContext.Provider>
    </BrowserRouter>
  )

  // Wait for data to be visible
  await screen.findByDisplayValue(testData.person.first_name)

  // Set gender to male
  const genderInput = screen.getByTestId('gender-select-inner')
  fireEvent.change(genderInput, { target: { value: 'male' } })
  await waitFor(() => screen.getByText('input.male'))

  // Go forward and then back in wizard
  const nextButton = screen.getByTestId('button-next')
  act(() => nextButton.click())
  await screen.findByTestId('button-back')
  const backButton = screen.getByTestId('button-back')
  act(() => backButton.click())

  // The selection should still be male
  await waitFor(() => screen.getByText('input.male'))
})

test('Gender not required when gender field is not shown', async () => {
  testData.person.fnr = ''
  // @ts-ignore
  testData.person.date_of_birth = '2002-08-20'
  testData.person.passport = 'NO-12345678'

  fetchMock.mockIf('/api/ui/v1/invited/', () =>
    Promise.resolve<any>(JSON.stringify(testData))
  )

  const genderFieldOff = {
    displayContactAtUnit: true,
    displayComment: true,
    displayContactAtUnitGuestInput: true,
    displayGenderFieldForGuest: false,
  }

  render(
    <BrowserRouter>
      <FeatureContext.Provider value={genderFieldOff}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <GuestRegister />
        </LocalizationProvider>
      </FeatureContext.Provider>
    </BrowserRouter>
  )

  // Wait for data to be visible
  await screen.findByDisplayValue(testData.person.first_name)

  // No gender is set, but it should still be possible
  // to go forward and back in wizard
  let nextButton = screen.getByTestId('button-next')
  act(() => nextButton.click())
  await screen.findByTestId('button-back')
  const backButton = screen.getByTestId('button-back')
  act(() => backButton.click())
  nextButton = screen.getByTestId('button-next')
  act(() => nextButton.click())
})
