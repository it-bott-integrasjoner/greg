/**
 * This is data the guest has entered about himself. It is stored
 * separate from ContactInformationBySponsor to make it clear that
 * most of the data there the guest cannot change.
 */
export type GuestRegisterData = {
  firstName: string
  lastName: string
  mobilePhoneCountry: string
  mobilePhone: string
  nationalIdNumber: string
  nationalIdCardNumber: string
  passportNumber: string
  passportNationality: string
  dateOfBirth: Date | null
  gender: string | null
}

export type GuestConsentData = {
  consents?: Array<{
    type: string
    choice: string
  }>
}
