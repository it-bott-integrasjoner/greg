import AuthenticationMethod from './authenticationMethod'

/**
 * This is the basis for the data shown in the guest registration form.
 *
 * It mostly contains data about the guest that was entered during the invite step.
 */
export type GuestInviteInformation = {
  first_name: string
  last_name: string
  ou_name_nb: string
  ou_name_en: string
  role_name_en: string
  role_name_nb: string
  role_start: string
  role_end: string
  contact_person_unit?: string
  gender?: string

  feide_id?: string
  email?: string

  // These fields are in the form, but it is not expected that
  // they are set, when the guest first follows the invite link
  mobile_phone_country_code?: string
  mobile_phone?: string
  fnr?: string
  national_id_card_number?: string
  passport?: string
  passportNationality?: string
  countryForCallingCode?: string
  date_of_birth: Date | null

  authentication_method: AuthenticationMethod
}
