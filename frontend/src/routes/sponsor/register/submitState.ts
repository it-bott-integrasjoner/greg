enum SubmitState {
  NotSubmitted,
  SubmitSuccess,
  SubmitFailure,
}

export default SubmitState
