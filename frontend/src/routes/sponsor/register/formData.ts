export type RegisterFormData = {
  first_name?: string
  last_name?: string
  role_type?: number
  role_start: Date
  role_end: Date | null
  contact_person_unit?: string
  comment?: string
  ou_id?: number
  email?: string
}
