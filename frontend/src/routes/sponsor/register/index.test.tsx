import React from 'react'
import userEvent from '@testing-library/user-event'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { LocalizationProvider } from '@mui/lab'
import { BrowserRouter } from 'react-router-dom'

import { render, waitFor, screen, act } from 'test-utils'
import StepRegistration from './stepRegistration'

jest.mock('hooks/useOus', () => () => ({
  ous: [],
  loading: false,
}))

jest.mock('hooks/useRoleTypes', () => jest.fn(() => []))

test('Validation message showing if last name is missing', async () => {
  render(
    <BrowserRouter>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StepRegistration />
      </LocalizationProvider>
    </BrowserRouter>
  )

  // Try to go to the next step and check that the validation message is showing
  const submitButton = screen.getByTestId('button-next')
  act(() => userEvent.click(submitButton))

  const validationMessage = await waitFor(() =>
    screen.getByText('validation.lastNameRequired')
  )
  expect(validationMessage).toBeInTheDocument()

  screen.queryByText('validation.lastNameRequired')

  const inputValue = 'Test input value'
  // Note that we need to use the test-ID of the input field inside the Material UI TextField-component
  act(() => userEvent.type(screen.getByTestId('lastName-input-field'), inputValue))
  expect(screen.getByDisplayValue(inputValue)).toBeInTheDocument()

  // Type in text, the message should disappear
  screen.queryByText('validation.lastNameRequired')
  act(() => userEvent.click(submitButton))

  await waitFor(
    () => {
      expect(
        screen.queryByText('validation.lastNameRequired')
      ).not.toBeInTheDocument()
    },
    {
      timeout: 5000,
    }
  )
})
