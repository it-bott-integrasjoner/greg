import React from 'react'
import { render, screen } from 'test-utils'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { LocalizationProvider } from '@mui/lab'
// eslint-disable-next-line import/no-extraneous-dependencies
import { Guest } from 'interfaces'
import parse from 'date-fns/parse'
import { MemoryRouter, Route, Routes } from 'react-router-dom'
import { addDays } from 'date-fns/fp'
import GuestRoleInfo from './index'
import { waitFor } from '../../../../test-utils'

const guest: Guest = {
  pid: '100',
  first: 'Test',
  last: 'Tester',
  email: 'test@example.org',
  mobile: '(+47)97543991',
  fnr: null,
  passport: null,
  feide_id: null,
  active: true,
  registered: true,
  verified: true,
  invitation_status: 'active',
  roles: [
    {
      id: '200',
      ou_en: 'English organizational unit name',
      ou_nb: 'Norsk navn organisasjonsenhet',
      name_en: 'Guest role',
      name_nb: 'Gjesterolle',
      start_date: parse('2021-08-10', 'yyyy-MM-dd', new Date()),
      end_date: addDays(10)(new Date()),
      contact_person_unit: 'Test contact person',
      max_days: 100,
      comments: 'Test comment',
      sponsor_name: 'Test',
      ou_id: 1,
    },
    {
      id: '201',
      ou_en: 'English organizational unit name',
      ou_nb: 'Norsk navn organisasjonsenhet',
      name_en: 'Test role',
      name_nb: 'Testrolle',
      start_date: parse('2021-09-06', 'yyyy-MM-dd', new Date()),
      end_date: addDays(10)(new Date()),
      contact_person_unit: 'Test contact person',
      max_days: 100,
      comments: 'Test comment',
      sponsor_name: 'Test',
      ou_id: 2,
    },
  ],
}

// Mock useOus so that it looks like the user is a sponsor for
// the unit with ID 1
jest.mock('hooks/useOus', () => () => ({
  ous: [
    {
      id: 1,
      nb: 'Test',
      en: 'Test2',
    },
  ],
  loading: false,
}))

test('End now enabled for role that is not expired and in unit where user is host', async () => {
  render(
    <MemoryRouter initialEntries={['/sponsor/guest/1/roles/200']}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Routes>
          <Route
            path="sponsor/guest/:pid/roles/:id"
            element={<GuestRoleInfo guest={guest} reloadGuest={() => {}} />}
          />
        </Routes>
      </LocalizationProvider>
    </MemoryRouter>
  )

  await waitFor(
    () => {
      expect(screen.getByText('button.save')).toBeDisabled()
      expect(screen.getByText('sponsor.endNow')).toBeEnabled()
    },
    { timeout: 5000 }
  )
}, 10000)

test('End now disabled for role that is not expired and in unit where user is not host', async () => {
  render(
    <MemoryRouter initialEntries={['/sponsor/guest/1/roles/201']}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Routes>
          <Route
            path="sponsor/guest/:pid/roles/:id"
            element={<GuestRoleInfo guest={guest} reloadGuest={() => {}} />}
          />
        </Routes>
      </LocalizationProvider>
    </MemoryRouter>
  )

  await waitFor(
    () => {
      expect(screen.getByText('button.save')).toBeDisabled()
      expect(screen.getByText('sponsor.endNow')).toBeDisabled()
    },
    { timeout: 5000 }
  )
}, 10000)
