import { LocalizationProvider } from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import React from 'react'
import _ from 'lodash'
import GuestPage from './index'
import { UserContext } from '../../contexts'
import { render, screen } from '../../test-utils'

const userData = {
  user: {
    auth: true,
    auth_type: 'oidc',
    fetched: true,
    first_name: 'Frank Foreleser',
    last_name: 'Føllesen',
    feide_id: 'frank_foreleser@spusers.feide.no',
    person_id: '20622',
    sponsor_id: '',
    email: 'frank@example.org',
    mobile_phone: '',
    fnr: '190663*****',
    passport: '',
    roles: [
      {
        id: '20319',
        ou_nb: 'Det humanistiske fakultet, sekretariatet',
        ou_en: 'The Faculty of Humanities, faculty administration',
        name_nb: 'Konsulent',
        name_en: 'Consultant',
        start_date: '2022-03-24',
        end_date: '2022-03-31',
        max_days: 100,
        contact_person_at_unit: 'None',
        comments: 'Test',
      },
    ],
    consents: [
      {
        id: '8656',
        type: { name_en: 'Search', name_nb: 'Søk', name_nn: 'Søk' },
        choice: { text_en: 'Yes', text_nb: 'Yes', text_nn: 'Yes' },
        consent_given_at: '2022-03-24',
      },
    ],
    registration_completed_date: '2022-03-23T23:00:00.000Z',
  },
  fetchUserInfo: () => {},
  clearUserInfo: () => {},
  getUserInfo: () => {},
}

test('Guest values showing', async () => {
  render(
    // @ts-ignore
    <UserContext.Provider value={userData}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestPage />
      </LocalizationProvider>
    </UserContext.Provider>
  )

  screen.findByDisplayValue(
    `${userData.user.first_name} ${userData.user.last_name}`
  )
  screen.findByDisplayValue(userData.user.feide_id)
  screen.findByDisplayValue(userData.user.email)
  screen.findByDisplayValue(userData.user.mobile_phone)
  screen.findByDisplayValue(userData.user.roles[0].name_en)
  screen.findByDisplayValue(userData.user.roles[0].ou_en)
  screen.findByDisplayValue(
    `${userData.user.roles[0].start_date} - ${userData.user.roles[0].end_date}`
  )

  screen.findByDisplayValue(userData.user.consents[0].type.name_en)
  screen.findByDisplayValue(userData.user.consents[0].choice.text_en)
  screen.findByDisplayValue(userData.user.consents[0].consent_given_at)
})

test('Guest values showing when consent date is not set', async () => {
  const userDataCopy = _.cloneDeep(userData)
  // @ts-ignore
  userDataCopy.user.consents[0].consent_given_at = null

  render(
    // @ts-ignore
    <UserContext.Provider value={userData}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <GuestPage />
      </LocalizationProvider>
    </UserContext.Provider>
  )

  // Just check that the page is showing
  screen.findByDisplayValue(
    `${userData.user.first_name} ${userData.user.last_name}`
  )
})
