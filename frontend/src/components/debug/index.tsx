import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import CheckIcon from '@mui/icons-material/Check'
import ClearIcon from '@mui/icons-material/Clear'

import {
  Box,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Typography,
} from '@mui/material'

import { appInst, appTimezone, appVersion } from 'appConfig'
import { Link } from 'react-router-dom'
import { useUserContext } from 'contexts'

const Yes = () => <CheckIcon color="success" />
const No = () => <ClearIcon color="error" />

export const Debug = () => {
  const [apiHealth, setApiHealth] = useState('not yet')
  const [didContactApi, setDidContactApi] = useState(false)
  const { i18n } = useTranslation(['common'])
  const { user } = useUserContext()

  if (!didContactApi) {
    setDidContactApi(true)
    fetch('/api/health/')
      .then((res) => res.text())
      .then((result) => {
        if (result === 'OK') {
          setApiHealth('yes')
        } else {
          setApiHealth(result)
        }
      })
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .catch((error) => {
        setApiHealth('error')
      })
  }

  const d = [
    ['NODE_ENV', process.env.NODE_ENV],
    ['Version', appVersion],
    ['Timezone', appTimezone],
    ['Language', i18n.language],
    ['Theme', appInst],
    ['Institution', appInst],
    ['API reachable?', apiHealth === 'yes' ? <Yes /> : apiHealth],
    ['Authenticated?', user.auth ? <Yes /> : <No />],
  ]
  return (
    <Box>
      <Typography variant="body1">
        <strong>Routes</strong>
        <ul>
          <li key="root">
            <Link to="/">Front page</Link>
          </li>
          <li key="guest">
            <Link to="/guest">Guest</Link>
          </li>
          <li key="sponsor">
            <Link to="/sponsor">Sponsor</Link>
          </li>
          <li key="register">
            <Link to="/register">Registration</Link>
          </li>
          <li key="guestregister">
            <Link to="/guestregister">Guest Registration</Link>
          </li>
        </ul>
      </Typography>
      <Typography variant="h2">Debug</Typography>
      <Box sx={{ maxWidth: '30rem' }}>
        <Table>
          <TableBody>
            {d.map(([key, value]) => (
              <TableRow>
                <TableCell>{key}</TableCell>
                <TableCell>{value}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Box>
    </Box>
  )
}

export default Debug
