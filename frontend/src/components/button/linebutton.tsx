import React from 'react'
import { Button } from '@mui/material'

interface IHrefButton {
  to: string
  children: React.ReactNode
}

function HrefLineButton({ to, children }: IHrefButton) {
  return (
    <Button href={to} color="secondary">
      {children}
    </Button>
  )
}

// eslint-disable-next-line import/prefer-default-export
export { HrefLineButton }
