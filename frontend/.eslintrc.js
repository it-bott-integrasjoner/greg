module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'airbnb-typescript',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {
    semi: 'off',
    'react/jsx-props-no-spreading': 'off',
    '@typescript-eslint/semi': ['error', 'never'],
    'react/jsx-uses-react': 'off',
    'jsx-quotes': 'error',
    'react/react-in-jsx-scope': 'off',
  },
}
