from django.db.models import Q, Value, CharField
from django.db.models.functions import Concat
from greg.models import Identity, Person


def person_by_string_query(request):
    search = request.query_params["q"]
    number_of_hits = 20
    if "gregui" in request.version:
        id_field_name = "pid"
    else:
        id_field_name = "person_id"

    split_search = search.lower().split()
    search_regex = "(.*)".join(split_search)
    reversed_search_regex = "(.*)".join(list(reversed(split_search)))

    hits = []
    # First look for hits on name and birth date
    persons = Person.objects.annotate(
        full_name=Concat(
            "first_name", Value(" "), "last_name", output_field=CharField()
        )
    ).filter(
        Q(full_name__iregex=search_regex)
        | Q(full_name__iregex=reversed_search_regex)
        | Q(date_of_birth__iregex=search_regex)
    )[
        :number_of_hits
    ]

    included_persons = []
    for person in persons:
        hits.append(
            {
                id_field_name: person.id,
                "first": person.first_name,
                "last": person.last_name,
                "date_of_birth": person.date_of_birth,
            }
        )
        included_persons.append(person.id)

    if len(hits) == number_of_hits:
        # Max number of hits, no need to search more
        return hits

    # Look for hits in e-mail and mobile phone
    identities = Identity.objects.filter(
        value__iregex=search_regex,
        type__in=[
            Identity.IdentityType.PRIVATE_EMAIL,
            Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
        ],
    )[: (number_of_hits - len(hits))]

    for identity in identities:
        if identity.person_id in included_persons:
            continue

        hits.append(
            {
                id_field_name: identity.person_id,
                "first": identity.person.first_name,
                "last": identity.person.last_name,
                "date_of_birth": identity.person.date_of_birth,
                "value": identity.value,
                "type": identity.type,
            }
        )

    return hits
