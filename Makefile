DJANGO_SETTINGS_MODULE ?= gregsite.settings.dev

BLACK ?= black -q
COVERAGE ?= coverage
MYPY ?= mypy
PIP ?= pip -q
POETRY ?= poetry
PYLINT ?= pylint -sn
PYTEST ?= pytest -v -s --no-header
PYTHON ?= python3.10
VENV ?= venv

mypy = $(MYPY) --config-file mypy.ini
pip = python -m $(PIP)
poetry = python -m $(POETRY)
venv = . $(VENV)/bin/activate &&

PACKAGES = greg/ gregsite/ gregui/

all: test

.PHONY: clean
clean:
	$(RM) -r $(VENV)

$(VENV): $(VENV)/touchfile
$(VENV)/touchfile:
	test -d $(VENV) || $(PYTHON) -m venv $(VENV)
	$(venv) $(pip) install -U pip
	$(venv) $(pip) install poetry
	$(venv) $(poetry) install
	touch $@

.PHONY: install
install: $(VENV)
	$(venv) $(poetry) install

.PHONY: test
test: $(VENV)
	$(venv) $(mypy) -p greg
	$(venv) $(COVERAGE) run --source greg,gregsite,gregui -m $(PYTEST)
	$(venv) $(COVERAGE) report
	$(venv) $(COVERAGE) xml

.PHONY: lint
lint: $(VENV)
	$(venv) $(poetry) -q check
	$(venv) $(PYLINT) $(PACKAGES)
	$(venv) $(BLACK) --check --diff $(PACKAGES)

.PHONY: fmt
fmt: $(VENV)
	$(venv) $(BLACK) $(PACKAGES)

.PHONY: deps
deps: poetry.lock

poetry.lock: pyproject.toml | $(VENV)
	$(venv) $(poetry) update
