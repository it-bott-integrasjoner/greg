from django import template
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy

from greg.models import BaseModel, Person, Sponsor


class GregUserProfile(BaseModel):
    """Link the django user to a Person or Sponsor."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="user_profiles",
        blank=True,
        null=True,
    )
    sponsor = models.ForeignKey(
        Sponsor,
        on_delete=models.CASCADE,
        related_name="user_profiles",
        blank=True,
        null=True,
    )
    userid_feide = models.CharField(gettext_lazy("userid-feide"), max_length=150)


class EmailTemplate(BaseModel):
    """
    Stores templates for emails.

    Only one template of each type is allowed. To introduce new ones, simply add a new
    EmailType.

    GUEST_REGISTRATION is for informing a guest that they have been invited
    GUEST_REDO_REGISTRATION is for informing a guest that the
    identification data is incorrect and need to be resubmitted
    SPONSOR_CONFIRMATION is for informing the sponsor they must verify the guest's information
    ROLE_END_REMINDER is used when reminding the sponsor if their ending roles in the near future
    ROLE_END_REMINDER_GUEST is used for reminding guests
    that their role is ending in the near future
    INVALID_EMAIL is used to notify sponsors when email sending fails
    due to a problem with the address of an invited guest
    """

    class EmailType(models.TextChoices):
        """Types of Emails"""

        GUEST_REGISTRATION = "guest_registration"
        GUEST_REDO_REGISTRATION = "guest_redo_registration"
        SPONSOR_CONFIRMATION = "sponsor_confirmation"
        ROLE_END_REMINDER = "role_end_reminder"
        ROLE_END_REMINDER_GUEST = "role_end_reminder_guest"
        INVALID_EMAIL = "invalid_email"

    template_key = models.CharField(
        max_length=64, choices=EmailType.choices, unique=True
    )
    subject = models.CharField(max_length=255, blank=True, null=True)
    from_email = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)

    def get_rendered_template(self, tpl, context):
        return template.Template(tpl).render(context)

    def get_subject(self, context):
        return self.get_rendered_template(self.subject, context)

    def get_body(self, context):
        return self.get_rendered_template(self.body, context)
