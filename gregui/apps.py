from django.apps import AppConfig


class GreguiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "gregui"
