import re
from typing import Optional
import phonenumbers
from rest_framework import serializers

from greg.utils import is_valid_id_number

_VALID_EMAIL_REGEX = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b"


def validate_norwegian_national_id_number(value):
    """
    Both fødselsnummer and D-nummer are accepted
    """
    if not is_valid_id_number(value):
        raise serializers.ValidationError("Not a valid ID number")


def validate_norwegian_national_id_card_number(value):
    if not value:
        raise serializers.ValidationError("Not a valid national ID card number")


def validate_phone_number(value):
    if not value or not phonenumbers.is_valid_number(phonenumbers.parse(value)):
        raise serializers.ValidationError("Invalid phone number")


def validate_email(value: Optional[str]):
    if not value or not re.fullmatch(_VALID_EMAIL_REGEX, value):
        raise serializers.ValidationError("Invalid e-mail")
