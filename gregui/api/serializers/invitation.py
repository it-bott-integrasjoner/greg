from rest_framework import serializers

from greg.models import Person, Identity
from greg.utils import (
    create_objects_for_invitation,
    is_identity_duplicate,
    string_contains_illegal_chars,
)
from gregui.api.serializers.identity import IdentityDuplicateError
from gregui.api.serializers.role import InviteRoleSerializerUi
from gregui.models import GregUserProfile


class InviteGuestSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    role = InviteRoleSerializerUi(required=True)
    uuid = serializers.UUIDField(read_only=True)

    def create(self, validated_data):
        email = validated_data.pop("email")
        role_data = validated_data.pop("role")

        user_str = self.context["request"].user
        user = GregUserProfile.objects.get(user=user_str)
        sponsor = user.sponsor

        person = create_objects_for_invitation(
            person_data=validated_data,
            person_email=email,
            role_data=role_data,
            sponsor=sponsor,
        )

        return person

    def validate_first_name(self, first_name):
        if string_contains_illegal_chars(first_name):
            raise serializers.ValidationError("First name contains illegal characters")
        return first_name

    def validate_last_name(self, last_name):
        if string_contains_illegal_chars(last_name):
            raise serializers.ValidationError("Last name contains illegal characters")
        return last_name

    def validate_email(self, email):
        # The e-mail in the invite is the private e-mail
        if is_identity_duplicate(Identity.IdentityType.PRIVATE_EMAIL, email):
            raise IdentityDuplicateError(
                Identity.IdentityType.PRIVATE_EMAIL, "duplicate_private_email"
            )

        # Return the validated data
        return email

    class Meta:
        model = Person
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "date_of_birth",
            "role",
            "uuid",
        )
        read_only_field = ("uuid",)
