from rest_framework.serializers import ModelSerializer

from greg.models import RoleType


class RoleTypeSerializerUi(ModelSerializer):
    class Meta:
        model = RoleType
        fields = ("id", "identifier", "name_en", "name_nb", "max_days")
