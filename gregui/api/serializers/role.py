from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueTogetherValidator

from greg.models import Role
from greg.utils import role_invitation_date_validator


class RoleSerializerUi(serializers.ModelSerializer):
    """Serializer for the Role model with validation of various fields"""

    class Meta:
        model = Role
        fields = [
            "orgunit",
            "start_date",
            "type",
            "end_date",
            "contact_person_unit",
            "comments",
            "available_in_search",
            "person",
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=Role.objects.all(),
                fields=["person", "type", "orgunit", "start_date", "end_date"],
            )
        ]

    def validate_orgunit(self, unit):
        """Enforce rules related to orgunit"""
        # A sponsor can only add roles to units they are sponsors at, or children of
        # that unit if they have hierarchical access.
        allowed_units = self.context["sponsor"].get_allowed_units()
        if unit not in allowed_units:
            raise ValidationError("You can only edit roles connected to your units.")
        return unit

    def validate(self, attrs):
        """Validate things that need access to multiple fields"""

        # Validate dates
        start_date = attrs.get("start_date")
        end_date = attrs.get("end_date")
        if self.instance:
            start_date = start_date or self.instance.start_date
            end_date = end_date or self.instance.end_date
            max_days = self.instance.type.max_days
        else:
            max_days = attrs["type"].max_days
        role_invitation_date_validator(
            start_date=start_date, end_date=end_date, max_days=max_days
        )

        # A sponsor can not modify roles connected to units they do not have access to
        allowed_units = self.context["sponsor"].get_allowed_units()
        if self.instance and self.instance.orgunit not in allowed_units:
            raise ValidationError("You can only edit roles connected to your units.")

        return attrs


class InviteRoleSerializerUi(RoleSerializerUi):
    """
    Serializer for the role part of an invite.

    This one exists so that we don't have to specify the person argument on the role.
    Simply reuses all the logic of the parent class except requiring the person field.
    """

    class Meta:
        model = Role
        fields = [
            "orgunit",
            "start_date",
            "type",
            "end_date",
            "contact_person_unit",
            "comments",
            "available_in_search",
        ]


class ExtendedRoleSerializer(serializers.ModelSerializer):
    """
    A role serializer with additional human readable names for the
    role type and associated organizational unit.
    """

    name_nb = SerializerMethodField(source="type")
    name_en = SerializerMethodField(source="type")
    ou_nb = SerializerMethodField(source="orgunit")
    ou_en = SerializerMethodField(source="orgunit")
    max_days = SerializerMethodField(source="type")
    sponsor_name = SerializerMethodField(source="sponsor")
    sponsor_feide_id = SerializerMethodField(source="sponsor")
    ou_id = SerializerMethodField(source="orgunit")

    def get_name_nb(self, obj):
        return obj.type.name_nb

    def get_name_en(self, obj):
        return obj.type.name_en

    def get_ou_nb(self, obj):
        return obj.orgunit.name_nb

    def get_ou_en(self, obj):
        return obj.orgunit.name_en

    def get_max_days(self, obj):
        return obj.type.max_days

    def get_sponsor_name(self, obj):
        return f"{obj.sponsor.first_name} {obj.sponsor.last_name}"

    def get_sponsor_feide_id(self, obj):
        return obj.sponsor.feide_id

    def get_ou_id(self, obj):
        return obj.orgunit.id

    class Meta:
        model = Role
        fields = [
            "id",
            "name_nb",
            "name_en",
            "ou_nb",
            "ou_en",
            "start_date",
            "end_date",
            "max_days",
            "contact_person_unit",
            "comments",
            "sponsor_name",
            "sponsor_feide_id",
            "ou_id",
        ]
        read_only_fields = ["contact_person_unit", "sponsor_name", "sponsor_feide_id"]
