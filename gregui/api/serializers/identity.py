from django.conf import settings
from django.utils import timezone
from rest_framework import serializers
from rest_framework import status
from rest_framework.exceptions import APIException, ValidationError

from greg.models import Identity, Role
from gregui.models import GregUserProfile


class IdentityDuplicateError(APIException):
    """
    Used to signal the there is already an identity registered with the same value
    for the type given as input parameter.
    """

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Validation error"
    _type: Identity.IdentityType

    def __init__(self, identity_type: Identity.IdentityType, code: str):
        super().__init__()
        self._type = identity_type
        self.code = code
        self.detail = {"code": code, "message": "Duplicate identity"}


class IdentitySerializer(serializers.ModelSerializer):
    """Serializer for the Identity model with validation of various fields"""

    class Meta:
        model = Identity
        fields = "__all__"
        read_only_fields = [
            "id",
            "person",
            "type",
            "source",
            "value",
            "verified",
            "verified_by",
        ]

    def _get_sponsor(self):
        """
        Fetch the sponsor doing the request

        Since the ViewSet using this Serializer uses the IsSponsor permission, we know
        that the user is connect to a GregUserProfile with a sponsor object.
        """
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        return GregUserProfile.objects.get(user=user).sponsor

    def validate(self, attrs):
        """
        Set values automatically when updating.

        - All updates are manual from the ui endpoints.
        - The one verifying must be the logged in user which we know is a sponsor since
          the user passed the IsSponsor permission in the view using this serializer.
        - No point leaving setting the verified_at time to anyone other than the
          server itself.

        Note: Get requests do not use this method, making it safe.
        """
        # Prevent nin verification. (This will only trigger if someone is posting the
        # requests themselves. The frontend has its own setting disabling the button
        # used against this endpoint.)
        if settings.DISABLE_NIN_VERIFY and self.instance.type == str(
            Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER
        ):
            raise ValidationError(
                detail="NIN verification currently disabled.",
            )
        attrs["verified"] = Identity.Verified.MANUAL
        attrs["verified_by"] = self._get_sponsor()
        attrs["verified_at"] = timezone.now()
        return attrs


class PartialIdentitySerializer(serializers.ModelSerializer):
    verified_by = serializers.SerializerMethodField()
    value = serializers.SerializerMethodField()

    class Meta:
        model = Identity
        fields = [
            "id",
            "value",
            "type",
            "source",
            "verified",
            "verified_by",
            "verified_at",
        ]

    def _get_sponsor(self):
        """
        Fetch the sponsor doing the request
        """
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        return GregUserProfile.objects.get(user=user).sponsor

    def _can_see_identity(self, obj):
        """
        Check if the sponsor have access to the unit where the person has a role
        """
        sponsor = self._get_sponsor()
        person_id = obj.person.id
        value = Role.objects.filter(person=person_id).filter(
            orgunit__in=sponsor.get_allowed_units()
        )
        if value:
            return True
        return False

    def get_value(self, dictionary):
        if self._can_see_identity(dictionary):
            return dictionary.value

        if dictionary.type == "norwegian_national_id_number":
            dob = dictionary.value[:6]  # Date of birth
            return dob + "*****"
        if dictionary.type == "passport_number":
            return "*" * len(dictionary.value)
        raise ValueError(
            f"{dictionary.type} is not a supported identity type for serializer"
        )

    def get_verified_by(self, obj):
        sponsor = obj.verified_by
        return " ".join((sponsor.first_name, sponsor.last_name)) if sponsor else None
