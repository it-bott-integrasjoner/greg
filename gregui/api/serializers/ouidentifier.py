from rest_framework.serializers import ModelSerializer

from greg.models import OuIdentifier


class IdentifierSerializer(ModelSerializer):
    class Meta:
        model = OuIdentifier
        fields = ("id", "name", "source", "value")
