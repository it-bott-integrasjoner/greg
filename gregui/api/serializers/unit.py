from rest_framework.serializers import ModelSerializer

from greg.models import OrganizationalUnit
from gregui.api.serializers.ouidentifier import IdentifierSerializer


class UnitSerializerId(ModelSerializer):
    identifiers = IdentifierSerializer(many=True, read_only=True)

    class Meta:
        model = OrganizationalUnit
        fields = ("name_nb", "name_en", "identifiers")
