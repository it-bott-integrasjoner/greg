import datetime
from typing import Literal

from django.conf import settings
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import BooleanField, CharField, SerializerMethodField


from greg.models import (
    Consent,
    ConsentChoice,
    ConsentType,
    Identity,
    Person,
    InvitationLink,
)
from greg.utils import is_identity_duplicate, string_contains_illegal_chars
from gregui.api.serializers.identity import (
    PartialIdentitySerializer,
    IdentityDuplicateError,
)
from gregui.api.serializers.role import ExtendedRoleSerializer
from gregui.validation import (
    validate_phone_number,
    validate_norwegian_national_id_card_number,
    validate_norwegian_national_id_number,
)


def create_identity_or_update(
    identity_type: Identity.IdentityType, value: str, person: Person
):
    existing_identity = person.identities.filter(type=identity_type).first()
    if not existing_identity:
        Identity.objects.create(
            person=person,
            type=identity_type,
            source=settings.DEFAULT_IDENTITY_SOURCE,
            value=value,
        )
    else:
        existing_identity.value = value
        existing_identity.invalid = None
        existing_identity.save()


# pylint: disable=W0223
class GuestConsentChoiceSerializer(serializers.Serializer):
    type = serializers.CharField(required=True)
    choice = serializers.CharField(required=True)

    def validate(self, attrs):
        """Check that the combination of consent type and choice exists."""
        choice = ConsentChoice.objects.filter(
            consent_type__identifier=attrs["type"], value=attrs["choice"]
        )
        if not choice.exists():
            raise serializers.ValidationError("invalid consent type or choice")
        return attrs


class GuestRegisterSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=False, min_length=1)
    last_name = serializers.CharField(required=False, min_length=1)
    # E-mail set to not required to avoid raising exception if it is not included in input.
    # It is not given that the guest should be allowed to update it.
    private_email = serializers.CharField(required=False)
    private_mobile = serializers.CharField(
        required=True, validators=[validate_phone_number]
    )
    fnr = serializers.CharField(
        required=False, validators=[validate_norwegian_national_id_number]
    )
    national_id_card_number = serializers.CharField(
        required=False, validators=[validate_norwegian_national_id_card_number]
    )
    passport = serializers.CharField(required=False)
    date_of_birth = serializers.DateField(required=False)
    gender = serializers.CharField(required=False)
    consents = GuestConsentChoiceSerializer(required=False, many=True, write_only=True)

    def update(self, instance, validated_data):
        if "private_email" in validated_data:
            email = validated_data.pop("private_email")
            create_identity_or_update(
                Identity.IdentityType.PRIVATE_EMAIL, email, instance
            )

        if "private_mobile" in validated_data:
            mobile_phone = validated_data.pop("private_mobile")
            create_identity_or_update(
                Identity.IdentityType.PRIVATE_MOBILE_NUMBER, mobile_phone, instance
            )

        if "fnr" in validated_data:
            fnr = validated_data.pop("fnr")
            create_identity_or_update(
                Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, fnr, instance
            )

        if (
            settings.ENABLE_NATIONAL_ID_CARD_NUMBER is True
            and "national_id_card_number" in validated_data
        ):
            national_id_card_number = validated_data.pop("national_id_card_number")
            create_identity_or_update(
                Identity.IdentityType.NORWEGIAN_NATIONAL_ID_CARD_NUMBER,
                national_id_card_number,
                instance,
            )

        if "passport" in validated_data:
            passport = validated_data.pop("passport")
            create_identity_or_update(
                Identity.IdentityType.PASSPORT_NUMBER, passport, instance
            )

        # If the name is not allowed to be updated,
        # the request has already been denied at an earlier stage
        if "first_name" in validated_data:
            instance.first_name = validated_data["first_name"]

        if "last_name" in validated_data:
            instance.last_name = validated_data["last_name"]

        if "date_of_birth" in validated_data:
            instance.date_of_birth = validated_data["date_of_birth"]

        if "gender" in validated_data:
            instance.gender = validated_data["gender"]

        consents = validated_data.get("consents", {})
        self._handle_consents(person=instance, consents=consents)

        return instance

    def _handle_consents(self, person, consents):
        consent_types = [x["type"] for x in consents]
        mandatory_consents = ConsentType.objects.filter(mandatory=True).values_list(
            "identifier", flat=True
        )
        missing_consents = list(set(mandatory_consents) - set(consent_types))
        if missing_consents:
            raise ValidationError(f"missing mandatory consents {missing_consents}")

        for consent in consents:
            consent_type = ConsentType.objects.get(identifier=consent["type"])
            choice = ConsentChoice.objects.get(
                consent_type=consent_type, value=consent["choice"]
            )
            consent_instance, created = Consent.objects.get_or_create(
                type=consent_type,
                person=person,
                choice=choice,
                defaults={"consent_given_at": timezone.now()},
            )
            if not created and consent_instance.choice != choice:
                consent_instance.choice = choice
                consent_instance.save()

    def validate_first_name(self, first_name):
        if string_contains_illegal_chars(first_name):
            raise serializers.ValidationError("First name contains illegal characters")
        return first_name

    def validate_last_name(self, last_name):
        if string_contains_illegal_chars(last_name):
            raise serializers.ValidationError("Last name contains illegal characters")
        return last_name

    def validate_date_of_birth(self, date_of_birth):
        today = datetime.date.today()

        # Check that the date of birth is between the
        # interval starting about 100 years ago and last year
        if (
            not today - datetime.timedelta(weeks=100 * 52)
            < date_of_birth
            < today - datetime.timedelta(weeks=52)
        ):
            raise serializers.ValidationError("Invalid date of birth")

        return date_of_birth

    def validate_gender(self, gender):
        # Looks like the gender choices are enforced by the person model on
        # serialization, so need to check that the gender is valid here
        if gender not in Person.GenderType:
            raise serializers.ValidationError("Unexpected gender value")

        return gender

    def validate_private_email(self, private_email: str):
        if is_identity_duplicate(Identity.IdentityType.PRIVATE_EMAIL, private_email):
            raise IdentityDuplicateError(
                Identity.IdentityType.PRIVATE_EMAIL, "duplicate_identity_info"
            )

        # Return the validated data
        return private_email

    def validate_private_mobile(self, private_mobile: str):
        if is_identity_duplicate(
            Identity.IdentityType.PRIVATE_MOBILE_NUMBER, private_mobile
        ):
            raise IdentityDuplicateError(
                Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
                "duplicate_identity_info",
            )

        # Return the validated data
        return private_mobile

    def validate_fnr(self, fnr: str):
        if is_identity_duplicate(
            Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER, fnr
        ):
            raise IdentityDuplicateError(
                Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
                "duplicate_identity_info",
            )

        # Return the validated data
        return fnr

    def validate_national_id_card_number(self, national_id_card_number: str):
        if is_identity_duplicate(
            Identity.IdentityType.NORWEGIAN_NATIONAL_ID_CARD_NUMBER,
            national_id_card_number,
        ):
            raise IdentityDuplicateError(
                Identity.IdentityType.NORWEGIAN_NATIONAL_ID_CARD_NUMBER,
                "duplicate_private_national_id_card_number",
            )

        # Return the validated data
        return national_id_card_number

    def validate_passport(self, passport: str):
        if is_identity_duplicate(Identity.IdentityType.PASSPORT_NUMBER, passport):
            raise IdentityDuplicateError(
                Identity.IdentityType.PASSPORT_NUMBER,
                "duplicate_identity_info",
            )

        # Return the validated data
        return passport

    class Meta:
        model = Person
        fields = (
            "id",
            "first_name",
            "last_name",
            "gender",
            "private_email",
            "private_mobile",
            "fnr",
            "national_id_card_number",
            "passport",
            "date_of_birth",
            "consents",
        )
        read_only_fields = ("id",)


class GuestSerializer(serializers.ModelSerializer):
    """
    Serializer used for presenting guests to sponsors.
    """

    pid = CharField(source="id", read_only=True)
    first = CharField(source="first_name", read_only=True)
    last = CharField(source="last_name", read_only=True)
    email = SerializerMethodField(source="private_email")
    mobile = SerializerMethodField(source="private_mobile", read_only=True)
    fnr = PartialIdentitySerializer(read_only=True)
    national_id_card_number = PartialIdentitySerializer(read_only=True)
    passport = PartialIdentitySerializer(read_only=True)
    feide_id = SerializerMethodField(source="feide_id", read_only=True)
    active = SerializerMethodField(source="active", read_only=True)
    registered = BooleanField(source="is_registered", read_only=True)
    verified = BooleanField(source="is_verified", read_only=True)
    invitation_status = SerializerMethodField(
        source="get_invitation_status", read_only=True
    )
    roles = ExtendedRoleSerializer(many=True, read_only=True)

    def get_email(self, obj):
        return obj.private_email and obj.private_email.value

    def get_mobile(self, obj):
        return obj.private_mobile and obj.private_mobile.value

    def get_active(self, obj):
        return obj.is_registered and obj.is_verified

    def get_feide_id(self, obj):
        return obj.feide_id and obj.feide_id.value

    def get_invitation_status(self, obj):
        if obj.private_email and obj.private_email.invalid:
            return "invalidEmail"

        invitation_links = InvitationLink.objects.filter(
            invitation__role__person__id=obj.id
        )
        non_expired_links = invitation_links.filter(expire__gt=timezone.now())
        if non_expired_links.count():
            return "active"
        if invitation_links.count():
            return "expired"
        return "none"

    class Meta:
        model = Person
        fields = [
            "pid",
            "first",
            "last",
            "mobile",
            "fnr",
            "national_id_card_number",
            "email",
            "passport",
            "feide_id",
            "active",
            "registered",
            "verified",
            "invitation_status",
            "roles",
        ]


class GuestUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer used for letting a guest change data about themselves.
    This exists since GuestSerializer requires the user to be a sponsor.

    Only updating their mobile phone number is currently supported.
    """

    mobile = SerializerMethodField(source="private_mobile", read_only=True)

    def get_mobile(self, obj):
        return obj.private_mobile and obj.private_mobile.value

    class Meta:
        model = Person
        fields = ["mobile"]


class FrontPageGuestSerializer(serializers.ModelSerializer):
    """
    Serializer used for presenting guests to sponsors.
    """

    pid = CharField(source="id", read_only=True)
    first = CharField(source="first_name", read_only=True)
    last = CharField(source="last_name", read_only=True)
    active = SerializerMethodField(source="active", read_only=True)
    registered = BooleanField(source="is_registered", read_only=True)
    verified = SerializerMethodField(source="verified", read_only=True)
    invitation_status = SerializerMethodField(
        source="get_invitation_status", read_only=True
    )
    roles = ExtendedRoleSerializer(many=True, read_only=True)

    def get_active(self, obj: Person) -> bool:
        return obj.is_registered and self.get_verified(obj)

    def get_verified(self, obj: Person) -> bool:
        return (
            len(
                [
                    x
                    for x in obj.identities.all()
                    if x.type in settings.ALLOWED_VERIFIED_ID_TYPES
                    and x.verified_at
                    and x.verified_at <= timezone.now()
                ]
            )
            >= 1
        )

    def get_invitation_status(
        self, obj: Person
    ) -> Literal["active", "expired", "invalidEmail", "none"]:
        private_email = next(
            iter(
                [
                    x
                    for x in obj.identities.all()
                    if x.type == Identity.IdentityType.PRIVATE_EMAIL
                ]
            ),
            None,
        )

        if private_email and private_email.invalid:
            return "invalidEmail"

        invitations = [
            item
            for sublist in [list(x.invitations.all()) for x in obj.roles.all()]
            for item in sublist
        ]

        # Get all invitation_links from all invitations and flatten them
        invitation_links = [
            item
            for sublist in [list(x.invitation_links.all()) for x in invitations]
            for item in sublist
        ]

        non_expired_links = [x for x in invitation_links if x.expire > timezone.now()]

        if len(non_expired_links) > 0:
            return "active"
        if len(invitation_links) > 0:
            return "expired"
        return "none"

    class Meta:
        model = Person
        fields = [
            "pid",
            "first",
            "last",
            "active",
            "registered",
            "verified",
            "invitation_status",
            "roles",
        ]
