from rest_framework import mixins, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from greg.models import Identity, Person
from greg.permissions import IsSponsor
from greg.utils import is_identity_duplicate
from gregui import validation
from gregui.api.serializers.guest import (
    GuestSerializer,
    GuestUpdateSerializer,
    FrontPageGuestSerializer,
    create_identity_or_update,
)
from gregui.api.serializers.identity import IdentityDuplicateError
from gregui.models import GregUserProfile
from search_tools.person_search import person_by_string_query


class PersonViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """
    Fetch person info for any guest as long as you are a sponsor

    This is required for the functionality where a sponsor wants to add a guest role to
    an already existing person that is not already their guest.

    Returns enough information to fill a profile page in the frontend
    """

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    queryset = Person.objects.all()
    http_methods = ["get", "patch"]
    serializer_class = GuestSerializer

    def update(self, request, *args, **kwargs):
        """
        Overriding the update-method to be able to return an error response
        with a special format if there is a duplicate identity
        """
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        try:
            self.perform_update(serializer)
        except IdentityDuplicateError as error:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": error.code,
                    "message": error.detail["message"],
                },
            )
        except ValueError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "no_data_received",
                    "message": "Received no data",
                },
            )

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}  # pylint: disable=protected-access

        return Response(serializer.data)

    def perform_update(self, serializer):
        """Update email when doing patch"""
        email = self.request.data.get("email")
        mobile = self.request.data.get("mobile")
        person = self.get_object()

        if email is None and mobile is None:
            raise ValueError()

        if email is not None:
            validation.validate_email(email)
            if is_identity_duplicate(Identity.IdentityType.PRIVATE_EMAIL, email):
                raise IdentityDuplicateError(
                    Identity.IdentityType.PRIVATE_EMAIL, "duplicate_private_email"
                )
            create_identity_or_update(
                Identity.IdentityType.PRIVATE_EMAIL, email, person
            )

        if mobile is not None:
            validation.validate_phone_number(mobile)
            if is_identity_duplicate(
                Identity.IdentityType.PRIVATE_MOBILE_NUMBER, mobile
            ):
                raise IdentityDuplicateError(
                    Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
                    "duplicate_private_mobile_number",
                )
            create_identity_or_update(
                Identity.IdentityType.PRIVATE_MOBILE_NUMBER, mobile, person
            )

        return super().perform_update(serializer)


class PersonUpdateViewSet(mixins.UpdateModelMixin, GenericViewSet):
    """
    Endpoint for letting a guest update data about themselves.
    Only updating their mobile phone number is currently supported.
    """

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Person.objects.all()
    http_methods = ["patch"]
    serializer_class = GuestUpdateSerializer

    def update(self, request, *args, **kwargs):
        """
        Overriding the update-method to be able to return an error response
        with a special format if there is a duplicate identity
        """
        partial = kwargs.pop("partial", False)
        user = self.request.user
        greg_user = GregUserProfile.objects.filter(user=user).first()
        if greg_user is None:
            return Response(status=status.HTTP_403_FORBIDDEN)
        person = greg_user.person
        if person is None:
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(person, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        try:
            self.perform_update(serializer)
        except IdentityDuplicateError as error:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": error.code,
                    "message": error.detail["message"],
                },
            )
        except ValueError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "no_data_received",
                    "message": "Received no data",
                },
            )

        if getattr(person, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            person._prefetched_objects_cache = {}  # pylint: disable=protected-access

        return Response(serializer.data)

    def perform_update(self, serializer):
        """Update mobile phone number when doing patch"""
        user = self.request.user
        greg_user = GregUserProfile.objects.filter(user=user).first()
        if greg_user is None:
            return Response(status=status.HTTP_403_FORBIDDEN)
        person = greg_user.person
        if person is None:
            return Response(status=status.HTTP_403_FORBIDDEN)

        mobile = self.request.data.get("mobile")
        if mobile is None:
            raise ValueError()

        validation.validate_phone_number(mobile)
        if is_identity_duplicate(Identity.IdentityType.PRIVATE_MOBILE_NUMBER, mobile):
            raise IdentityDuplicateError(
                Identity.IdentityType.PRIVATE_MOBILE_NUMBER,
                "duplicate_identity_info",
            )
        create_identity_or_update(
            Identity.IdentityType.PRIVATE_MOBILE_NUMBER, mobile, person
        )

        return super().perform_update(serializer)


class PersonSearchViewSet(GenericViewSet):
    """Search for persons using name, email, phone number and birth date"""

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]

    def list(self, request, *args, **kwargs):
        if "q" not in self.request.query_params:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "no_search_term",
                    "message": "No search query parameter given",
                },
            )

        if len(self.request.query_params["q"]) > 50:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "search_term_too_large",
                    "message": "Search term is too large",
                },
            )

        hits = self.get_hits()
        return Response(hits)

    def get_hits(self):
        return person_by_string_query(self.request)


class GuestInfoViewSet(mixins.ListModelMixin, GenericViewSet):
    """
    Fetch all guests at the sponsor's units.

    If the Sponsor's connection to the Unit is marked with hierarchical access, guests
    at child units of the Unit are included.

    Lists all persons connected to the roles the logged in sponsor is connected to.
    """

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    serializer_class = FrontPageGuestSerializer

    def get_queryset(self):
        """
        Fetch Persons connected to the sponsor some way.

        Any person with a role connected to the same unit as the sponsor, or a unit
        that the sponsor is connected to through hierarchical access.
        """

        user = GregUserProfile.objects.get(user=self.request.user)
        assert user.sponsor is not None
        units = user.sponsor.get_allowed_units()
        return (
            Person.objects.filter(roles__orgunit__in=list(units))
            .prefetch_related(
                "identities",
                "roles",
                "roles__sponsor",
                "roles__orgunit",
                "roles__person",
                "roles__type",
                "roles__invitations",
                "roles__invitations__invitation_links",
            )
            .distinct()
            .order_by("id")
        )
