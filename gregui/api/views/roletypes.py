from rest_framework import permissions
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from greg.models import RoleType
from gregui.api.serializers.roletype import RoleTypeSerializerUi


class RoleTypeViewSet(ListAPIView):
    queryset = RoleType.objects.all().order_by("id")
    permission_classes = [permissions.AllowAny]
    serializer_class = RoleTypeSerializerUi

    def get(self, request, *args, **kwargs):
        roletypes = self.queryset.filter(visible=True)
        serializer = self.serializer_class(roletypes, many=True)
        return Response(serializer.data)
