from django.conf import settings
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins, status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import RetrieveAPIView

from greg.models import Identity
from greg.permissions import IsSponsor
from gregui.api.serializers.identity import IdentitySerializer
from iga import get_iga_client


class IdentityViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    """
    Fetch or update identity info for any guest as long as you are a sponsor.

    This is required for when a host(sponsor) needs to verify the identity of a guest
    so that they are considered "active".

    Limited to GET and PATCH so that we can only update or get a single identity at a
    time.
    """

    queryset = Identity.objects.all()
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    serializer_class = IdentitySerializer
    http_method_names = ["get", "patch"]


class IdentityCheckView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]

    def get(self, request, *args, **kwargs):
        if not settings.IGA_CHECK:
            return Response({"match": None})
        client = get_iga_client(settings.INSTANCE_NAME, settings.IGA_CLIENT)
        try:
            ident: Identity = Identity.objects.get(pk=kwargs["id"])
        except Identity.DoesNotExist:
            return Response(
                f"Unknown identifier: {kwargs['id']}",
                status=status.HTTP_404_NOT_FOUND,
            )
        match = client.extid_search(ident.type, ident.value)
        return Response({"match": match.dict() if match else None})


class EmailExistsView(RetrieveAPIView):
    """
    View used for checking if an email is already in use
    """

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    serializer_class = IdentitySerializer
    queryset = Identity.objects.filter(type="private_email")
    lookup_field = "value"
