from rest_framework import permissions
from rest_framework.generics import ListAPIView

from greg.models import ConsentType
from greg.api.serializers.consent_type import ConsentTypeSerializer


class ConsentTypeViewSet(ListAPIView):
    queryset = ConsentType.objects.all().order_by("id")
    permission_classes = [permissions.AllowAny]
    serializer_class = ConsentTypeSerializer
