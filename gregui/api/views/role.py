from django.db import transaction
from rest_framework import status
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from greg.models import Role
from greg.permissions import IsSponsor
from gregui.api.serializers.role import RoleSerializerUi
from gregui.models import GregUserProfile


class RoleInfoViewSet(ModelViewSet):
    queryset = Role.objects.all()
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    serializer_class = RoleSerializerUi

    def partial_update(self, request, *args, **kwargs):
        try:
            role = Role.objects.get(pk=kwargs["pk"])
        except Role.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        sponsor = GregUserProfile.objects.get(user=self.request.user).sponsor
        with transaction.atomic():
            serializer = self.serializer_class(
                instance=role,
                data=request.data,
                partial=True,
                context={"sponsor": sponsor},
            )
            serializer.is_valid(raise_exception=True)
            serializer.update(role, serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        sponsor = GregUserProfile.objects.get(user=self.request.user).sponsor
        with transaction.atomic():
            serializer = self.serializer_class(
                data=request.data,
                context={
                    "sponsor": sponsor,
                },
            )
            serializer.is_valid(raise_exception=True)
            serializer.save(sponsor=sponsor)
        return Response(status=status.HTTP_201_CREATED)
