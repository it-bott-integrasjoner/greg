from rest_framework import mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import CharField, ModelSerializer
from rest_framework.viewsets import GenericViewSet

from greg.models import OrganizationalUnit
from greg.permissions import IsSponsor
from gregui.models import GregUserProfile


class OrgUnitsSerializer(ModelSerializer):
    nb = CharField(source="name_nb")
    en = CharField(source="name_en")

    class Meta:
        model = OrganizationalUnit
        fields = [
            "id",
            "nb",
            "en",
            "orgreg_id",
            "identifier_1",
            "identifier_2",
            "acronym_nob",
            "acronym_nno",
            "acronym_eng",
        ]


class OusViewSet(mixins.ListModelMixin, GenericViewSet):
    """Fetch Ous related to the authenticated sponsor."""

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, IsSponsor]
    serializer_class = OrgUnitsSerializer

    def get_queryset(self):
        sponsor = GregUserProfile.objects.get(user=self.request.user).sponsor
        return sponsor.get_allowed_units()
