import datetime
from enum import Enum
from typing import Optional, List
import structlog

from django.core import exceptions
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.http.response import JsonResponse
from django.utils import timezone
from django.contrib.auth.models import AnonymousUser
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.generics import CreateAPIView, GenericAPIView, DestroyAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.throttling import AnonRateThrottle
from rest_framework.views import APIView

from greg.models import Consent, Identity, Invitation, InvitationLink, Person
from greg.permissions import IsSponsor
from greg.utils import (
    get_default_invitation_expire_date_from_now,
    queue_mail_to_invitee,
)
from gregui.api.serializers.guest import GuestRegisterSerializer
from gregui.api.serializers.invitation import InviteGuestSerializer
from gregui.mailutils.confirm_guest import ConfirmGuest
from gregui.mailutils.invite_guest import InviteGuest
from gregui.models import GregUserProfile

logger = structlog.getLogger(__name__)


class InvitationView(CreateAPIView, DestroyAPIView):
    """
    Endpoint for invitation creation and cancelling


    {
        "first_name": "dfff",
        "last_name": "sss",
        "date_of_birth": null,
        "role": {
            "orgunit": 1,
            "start_date": null,
            "type": 1,
            "end_date": "2021-12-15",
            "contact_person_unit": "",
            "comments": "",
            "available_in_search": false
        }
    }
    """

    authentication_classes = [BasicAuthentication, SessionAuthentication]
    permission_classes = [IsSponsor]
    parser_classes = [JSONParser]
    serializer_class = InviteGuestSerializer

    def post(self, request, *args, **kwargs) -> Response:
        """
        Invitation creation endpoint

        Restricted to Sponsors, and a sponsor can only invite guests to OUs
        they are registered to.

        The next step in the flow is for the guest to use their link, review the
        information, and confirm it.
        """
        sponsor_user = GregUserProfile.objects.get(user=request.user)
        sponsor = sponsor_user.sponsor

        serializer = self.serializer_class(
            data=request.data,
            context={"request": request, "sponsor": sponsor},
        )
        serializer.is_valid(raise_exception=True)
        person = serializer.save()

        queue_mail_to_invitee(person, sponsor)

        # Empty json-body to avoid client complaining about non-json data in response
        return Response(status=status.HTTP_201_CREATED, data={})

    def delete(self, request, *args, **kwargs) -> Response:
        try:
            person_id = int(request.query_params["person_id"])
            person = Person.objects.get(id=person_id)
        except Person.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if person.is_registered or person.is_verified:
            # The guest has already gone through the registration step. The guest should
            # not be verified, but including that check just in case here.
            # If this is the case then there is an unexpected situation, the cancel option
            # should only apply to guests that have not completed the registration
            logger.warning("try_delete_registered_invite", person_id=person_id)
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # Delete the person.
        # The delete will cascade and all roles, identities and invitations will be removed.
        # It is OK to do this here since the person has not gone through the registration,
        # so it is not expected that there is any information that cannot be deleted without
        # problems attached to him
        person.delete()

        return Response(status=status.HTTP_200_OK)


class CheckInvitationView(APIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    throttle_classes = [AnonRateThrottle]

    def post(self, request, *args, **kwargs) -> Response:
        """
        Endpoint for verifying and setting invite_id in session.

        This endpoint is meant to be called in the background by the frontend on the
        page you get to by following the invitation url to the frontend. This way a
        session is created keeping the invite id safe, until the user returns from
        feide login if they choose to use it.

        Uses post to prevent invite id from showing up in various logs.
        """
        invite_id = request.data.get("invite_token")
        if not invite_id:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "missing_invite_token",
                    "message": "An invite token is required",
                },
            )
        try:
            invite_link = InvitationLink.objects.get(uuid=invite_id)
        except (InvitationLink.DoesNotExist, exceptions.ValidationError):
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={
                    "code": "invalid_invite_token",
                    "message": "Invite token is invalid",
                },
            )
        if invite_link.expire <= timezone.now():
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={
                    "code": "expired_invite_token",
                    "message": "Invite token has expired",
                },
            )
        request.session["invite_id"] = invite_id
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs) -> Response:
        if "invite_id" in request.session:
            logger.info(
                "invitation_session_deleted", invite_id=request.session["invite_id"]
            )
            del request.session["invite_id"]
            return Response(status.HTTP_200_OK)

        return Response(status=status.HTTP_403_FORBIDDEN)


class SessionType(Enum):
    INVITE = "invite"
    FEIDE = "feide"
    ID_PORTEN = "idporten"


class InvitedGuestView(GenericAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    # The endpoint is only for invited guests,
    # but the authorization happens in the actual method
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    serializer_class = GuestRegisterSerializer

    # Fields where the user can update values,
    # not that the user is allowed to insert value into
    # these fields on the person if no value exists previously
    fields_allowed_to_update_if_invite = [
        "first_name",
        "last_name",
        "private_email",
        "fnr",
        "national_id_card_number",
        "private_mobile",
        "passport",
        "date_of_birth",
        "consents",
        "gender",
    ]
    fields_allowed_to_update_if_feide_or_idporten = [
        "private_mobile",
        "passport",
        "consents",
        "gender",
    ]

    def get(self, request, *args, **kwargs):
        """
        Endpoint for fetching data related to an invite

        Used by the frontend for fetching data from the backend for review by a guest
        in the frontend, before calling the post endpoint defined below with updated
        info and confirmation.
        """

        # Find the invitation related to the invite
        invite_id = request.session.get("invite_id")
        invite_link = None
        if invite_id:
            # The proper way using an invite link with id set in session
            try:
                invite_link = InvitationLink.objects.get(uuid=invite_id)
            except (InvitationLink.DoesNotExist, exceptions.ValidationError):
                pass
        else:
            # Last ditch effort to find an invite link through the user object if the
            # user has logged in, connecting the invite to a user previously.
            if not isinstance(request.user, AnonymousUser):
                try:
                    greguser = GregUserProfile.objects.get(user=request.user)
                except GregUserProfile.DoesNotExist:
                    greguser = None
                if greguser and greguser.person:
                    invite_link = InvitationLink.objects.filter(
                        invitation__role__person=greguser.person
                    ).first()
                    request.session["invite_id"] = str(invite_link.uuid)
        if not invite_link:
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={"code": "invalid_invite", "message": "Invalid invite"},
            )
        if invite_link.expire <= timezone.now():
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={"code": "invite_expired", "message": "Invite expired"},
            )

        # An invitation was found. Construct response and send it
        role = invite_link.invitation.role
        person = role.person
        sponsor = role.sponsor

        session_type = self._determine_session_type(person, request)

        data = {
            "person": {
                "first_name": person.first_name,
                "last_name": person.last_name,
                "private_email": person.private_email and person.private_email.value,
                "private_mobile": person.private_mobile and person.private_mobile.value,
                "date_of_birth": person.date_of_birth,
                "fnr": person.fnr and person.fnr.value,
                "national_id_card_number": person.national_id_card_number
                and person.national_id_card_number.value,
                "passport": person.passport and person.passport.value,
                "feide_id": person.feide_id and person.feide_id.value,
                "gender": person.gender,
            },
            "sponsor": {
                "first_name": sponsor.first_name,
                "last_name": sponsor.last_name,
            },
            "role": {
                "ou_name_nb": role.orgunit.name_nb,
                "ou_name_en": role.orgunit.name_en,
                "role_name_nb": role.type.name_nb,
                "role_name_en": role.type.name_en,
                "start": role.start_date,
                "end": role.end_date,
                "contact_person_unit": role.contact_person_unit,
            },
            "meta": {"session_type": session_type.value},
        }
        return JsonResponse(data=data, status=status.HTTP_200_OK)

    @staticmethod
    def _determine_session_type(person, request) -> SessionType:
        # We do not store the login used in the session, and it is not necessary either, we will
        # just look at the information registered on the person attached to the user to figure
        # out how he logged in
        if request.user.is_anonymous:
            # If the user is not logged in then tell the client
            # to take him through the manual registration process
            return SessionType.INVITE
        if person.fnr and person.fnr.source == "idporten":
            # If the user has logged in through ID-porten the
            # Norwegian national ID number should have been
            # added to the person at this stage
            return SessionType.ID_PORTEN
        if person.feide_id:
            # User is logged in and has a Feide ID attached to him,
            # assume information about him has come from Feide
            return SessionType.FEIDE

        # Not expected, default to invite
        logger.warning(
            "unexpected_state_when_determining_session_type", person_id=person.id
        )
        return SessionType.INVITE

    def post(self, request, *args, **kwargs):
        """
        Endpoint for confirmation of data updated by guest

        Used by frontend when the confirm button at the end of the review flow is
        clicked by the user. The next part of the flow is for the sponsor to confirm
        the guest.
        """
        invite_id = request.session.get("invite_id")

        # Ensure the invitation link is valid and not expired
        try:
            invite_link = InvitationLink.objects.get(uuid=invite_id)
        except (InvitationLink.DoesNotExist, exceptions.ValidationError):
            return Response(status=status.HTTP_403_FORBIDDEN)
        if invite_link.expire <= timezone.now():
            return Response(status=status.HTTP_403_FORBIDDEN)

        person = invite_link.invitation.role.person
        data = request.data

        session_type = self._determine_session_type(person, request)
        optional_response = self._verify_only_allowed_updates_in_request(
            person, data, session_type
        )
        if optional_response:
            # Some illegal update was in the data
            return optional_response

        with transaction.atomic():
            # Serialize data for the person, this includes identity and consent information
            serializer = self.get_serializer(
                instance=person, data=request.data["person"]
            )
            serializer.is_valid(raise_exception=True)
            person = serializer.save()

            # Mark guest interaction done
            person.registration_completed_date = timezone.now().date()
            person.save()

            # Expire the invite link
            invite_link.expire = timezone.now()
            invite_link.save()

        # Send an email to the sponsor if the guest needs to be verified
        if not person.is_verified:
            confirmmailer = ConfirmGuest()
            confirmmailer.send_confirmation_mail_from_link(invite_link)
        return Response(status=status.HTTP_200_OK)

    def _verify_only_allowed_updates_in_request(
        self, person: Person, data, session_type: SessionType
    ) -> Optional[Response]:
        # This is not a case that is expected to happen, but having the check here as a safeguard
        # since it is an indication of a bug if does
        fnr = data.get("person") and data["person"].get("fnr")
        if self._verified_fnr_already_exists(person) and fnr:
            # The user should not be allowed to change a verified fnr
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "update_national_id_not_allowed",
                    "message": "Not allowed to update verified Norwegian ID",
                },
            )

        # If there is a Feide ID registered with the guest,
        # assume that the name is also coming from there.
        # There will always be a name for a user in Feide
        feide_id = self._get_identity_or_none(person, Identity.IdentityType.FEIDE_ID)

        # It is not certain that all fields required will come from Feide,
        # like the Norwegian national ID number,
        # so allow updates to empty fields to handle the case where the
        # information is missing from Feide and the user has to enter it manually
        illegal_fields = self._illegal_updates(
            data,
            self.fields_allowed_to_update_if_invite
            if feide_id is None
            else self.fields_allowed_to_update_if_feide_or_idporten,
            person,
            session_type,
        )
        if illegal_fields:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "code": "cannot_update_fields",
                    "message": f"cannot_update_fields: {illegal_fields}",
                },
            )
        return None

    @staticmethod
    def _illegal_updates(
        request_data, changeable_fields, person, session_type: SessionType
    ) -> List[str]:
        person_data = request_data.get("person", {})
        changed_fields = person_data.keys()

        illegal_field_updates = []
        for changed_field in changed_fields:
            if changed_field == "consents":
                # Consents can be inserted and updated, no need for further checks on them
                continue

            if (
                changed_field in ("first_name", "last_name")
                and session_type == SessionType.ID_PORTEN
            ):
                # From ID-porten only the Norwegian ID-number is given,
                # so the name must be what the sponsor wrote, and can be changed
                continue

            try:
                attribute = getattr(person, changed_field)
            except AttributeError:
                # The property does not exist on person
                illegal_field_updates.append(changed_field)
                continue

            if changed_field not in changeable_fields:
                # It is still allowed to update fields where there is no existing value even if
                # they are not in the list of changeable fields
                if not attribute:
                    # No existing value for field, so allow change
                    continue
                # Quick fix to be able compare date with string
                if changed_field == "date_of_birth":
                    if attribute.strftime("%Y-%m-%d") != person_data[changed_field]:
                        # There is an existing date value
                        illegal_field_updates.append(changed_field)
                elif attribute != person_data[changed_field]:
                    # There is an existing value
                    illegal_field_updates.append(changed_field)

        return illegal_field_updates

    @staticmethod
    def _verified_fnr_already_exists(person) -> bool:
        return (
            person.identities.filter(
                Q(type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER)
                & ~Q(verified=None)
            ).first()
            is not None
        )

    @staticmethod
    def _get_identity_or_none(
        person: Person, identity_type: Identity.IdentityType
    ) -> Optional[str]:
        try:
            return person.identities.get(type=identity_type).value
        except Identity.DoesNotExist:
            return None


class ResendInvitationView(UpdateModelMixin, APIView):
    authentication_classes = [BasicAuthentication, SessionAuthentication]
    permission_classes = [IsSponsor]

    def patch(self, request, *args, **kwargs) -> Response:
        person_id = kwargs["person_id"]

        invitation_links = InvitationLink.objects.filter(
            invitation__role__person__id=person_id
        )

        if invitation_links.count() == 0:
            # No invitation, not expected that the endpoint should be called in this case
            return Response(status=status.HTTP_400_BAD_REQUEST)

        non_expired_links = invitation_links.filter(expire__gt=timezone.now())
        invitemailer = InviteGuest()
        if non_expired_links.count() > 0:
            if non_expired_links.count() > 1:
                # Do not expect this to happen
                logger.warning("found_multiple_invitation_links", person_id=person_id)

            # Just resend all and do not create a new one
            for link in non_expired_links:
                invitemailer.queue_mail(link)
        else:
            # All the invitation links have expired, create a new one
            invitations_to_resend = {
                invitation_link.invitation for invitation_link in invitation_links
            }

            if len(invitations_to_resend) > 1:
                # Do not expected that a person has several open invitations, it could happen
                # if he has been invited by different sponsor at the same time, but that
                # could be an indication that there has been a mixup
                logger.warning("found_multiple_invitations", person_id=person_id)

            for invitation in invitations_to_resend:
                invitation_link = InvitationLink.objects.create(
                    invitation=invitation,
                    expire=get_default_invitation_expire_date_from_now(),
                )
                invitemailer.queue_mail(invitation_link)

        return Response(status=status.HTTP_200_OK)


class RecreateInvitationView(UpdateModelMixin, APIView):
    """
    Endpoint for rejecting a guest's identity data.

    This endpoint is to be used when a sponsor deems
    a guest's supplied identification data as invalid.
    The guest will be set to unregistered,
    and they will receive an email with a new registration link.
    """

    authentication_classes = [BasicAuthentication, SessionAuthentication]
    permission_classes = [IsSponsor]

    def patch(self, request, *args, **kwargs) -> Response:
        sponsor_user = GregUserProfile.objects.get(user=request.user)
        sponsor = sponsor_user.sponsor
        if sponsor is None:
            return Response(status=status.HTTP_403_FORBIDDEN)

        identity_id = kwargs["identity_id"]
        try:
            ident: Identity = Identity.objects.get(id=identity_id)
        except Identity.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        guest: Person = ident.person

        # This endpoint should only be used on guests that
        # has been registered/verified with invalid data.
        if not guest.is_registered and not guest.is_verified:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        with transaction.atomic():
            # Set the guest as unregistered
            guest.registration_completed_date = None
            guest.save()

            # Delete the guest's private mobile to not get
            # duplicate identity error on reregister
            # Delete identification used for verification
            # so the guest can use a different field
            identities = Identity.objects.filter(person__id=guest.id)
            for identity in identities:
                if identity.type == Identity.IdentityType.PRIVATE_MOBILE_NUMBER:
                    identity.delete()
                if identity.type in settings.ALLOWED_VERIFIED_ID_TYPES:
                    identity.delete()

            # Delete the guest's consents to
            # not violate uniqueness constraint
            guest_consents = Consent.objects.filter(person__id=guest.id)
            for consent in guest_consents:
                consent.delete()

            for invitation in Invitation.objects.filter(
                role__person_id=guest.id, role__sponsor_id=sponsor.id
            ):
                InvitationLink.objects.create(
                    invitation=invitation,
                    expire=timezone.now() + datetime.timedelta(days=30),
                )

        queue_mail_to_invitee(
            person=guest,
            sponsor=sponsor,
            is_fresh_registration=False,
        )

        return Response(status=status.HTTP_200_OK)
