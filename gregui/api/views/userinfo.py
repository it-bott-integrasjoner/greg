from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_403_FORBIDDEN
from rest_framework.views import APIView
from rest_framework.response import Response
from greg.models import InvitationLink

from gregui.models import GregUserProfile


class UserInfoView(APIView):
    """
    User info view.

    Return info about the logged inn user.
    Quick draft, we might want to expand this later.
    """

    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]

    def get(self, request, format=None):  # pylint: disable=redefined-builtin
        """
        Get info about the visiting user.

        Works for users logged in using Feide, and those relying solely on an
        invitation id. Pure django users, and anonymous users are denied access.
        """
        user = request.user

        invite_id = request.session.get("invite_id")

        auth_type = "invite"
        if "oidc_states" in request.session.keys():
            auth_type = "oidc"

        person = None
        sponsor = None
        user_profile = None

        content = {
            "feide_id": None,
            "sponsor_id": None,
            "person_id": None,
            "roles": [],
            "consents": [],
            "auth_type": auth_type,
            "registration_completed_date": None,
        }

        if user.is_authenticated:
            try:
                user_profile = GregUserProfile.objects.select_related(
                    "person", "sponsor"
                ).get(user=user)
            except GregUserProfile.DoesNotExist:
                return Response(
                    status=HTTP_403_FORBIDDEN,
                    data={
                        "code": "no_user_profile",
                        "message": "Authenticated, but missing user profile",
                    },
                )

        # Fetch sponsor and/or person object from profile of authenticated user
        if user_profile:
            sponsor = user_profile.sponsor
            person = user_profile.person
            content["feide_id"] = user_profile.userid_feide
        # Or fetch person info for invited guest
        elif invite_id:
            link = InvitationLink.objects.select_related(
                "invitation__role__person",
            ).get(uuid=invite_id)
            person = link.invitation.role.person
        # Otherwise, deny access
        else:
            return Response(status=HTTP_403_FORBIDDEN)

        if sponsor:
            content["sponsor_id"] = sponsor.id
            content["first_name"] = sponsor.first_name
            content["last_name"] = sponsor.last_name
            content["feide_id"] = sponsor.feide_id

        if person:
            person_roles = [
                {
                    "id": role.id,
                    "ou_nb": role.orgunit.name_nb,
                    "ou_en": role.orgunit.name_en,
                    "name_nb": role.type.name_nb,
                    "name_en": role.type.name_en,
                    "start_date": role.start_date,
                    "end_date": role.end_date,
                    "sponsor": {
                        "first_name": role.sponsor.first_name,
                        "last_name": role.sponsor.last_name,
                    },
                }
                for role in person.roles.all().select_related(
                    "orgunit", "type", "sponsor"
                )
            ]
            person_consents = [
                {
                    "id": cons.id,
                    "type": {
                        "name_en": cons.type.name_en,
                        "name_nb": cons.type.name_nb,
                        "name_nn": cons.type.name_nn,
                    },
                    "choice": {
                        "text_en": cons.choice.text_en,
                        "text_nb": cons.choice.text_nb,
                        "text_nn": cons.choice.text_nn,
                    },
                    "consent_given_at": cons.consent_given_at,
                }
                for cons in person.consents.all()
            ]
            content.update(
                {
                    "person_id": person.id,
                    "first_name": person.first_name,
                    "last_name": person.last_name,
                    "email": person.private_email and person.private_email.value,
                    "mobile_phone": person.private_mobile
                    and person.private_mobile.value,
                    "fnr": person.fnr and "".join((person.fnr.value[:-5], "*****")),
                    "national_id_card_number": person.national_id_card_number
                    and person.national_id_card_number.value,
                    "passport": person.passport and person.passport.value,
                    "roles": person_roles,
                    "consents": person_consents,
                    "registration_completed_date": person.registration_completed_date,
                }
            )
        return Response(content)
