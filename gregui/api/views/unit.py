from rest_framework import permissions
from rest_framework.generics import ListAPIView

from greg.models import OrganizationalUnit
from gregui.api.serializers.unit import UnitSerializerId


class UnitsViewSet(ListAPIView):
    queryset = OrganizationalUnit.objects.all().order_by("id")
    permission_classes = [permissions.AllowAny]
    serializer_class = UnitSerializerId
