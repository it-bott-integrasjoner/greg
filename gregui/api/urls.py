from django.urls import re_path, path
from rest_framework.routers import DefaultRouter
from gregui.api.views.identity import (
    IdentityCheckView,
    IdentityViewSet,
    EmailExistsView,
)

from gregui.api.views.invitation import (
    CheckInvitationView,
    InvitationView,
    InvitedGuestView,
    RecreateInvitationView,
    ResendInvitationView,
)
from gregui.api.views.ou import OusViewSet
from gregui.api.views.person import (
    GuestInfoViewSet,
    PersonSearchViewSet,
    PersonUpdateViewSet,
    PersonViewSet,
)
from gregui.api.views.role import RoleInfoViewSet
from gregui.api.views.consent import ConsentTypeViewSet
from gregui.api.views.roletypes import RoleTypeViewSet
from gregui.api.views.unit import UnitsViewSet
from gregui.api.views.userinfo import UserInfoView

router = DefaultRouter(trailing_slash=False)
router.register(r"role", RoleInfoViewSet, basename="role")
router.register(r"identity", IdentityViewSet, basename="identity")
router.register(r"ous", OusViewSet, basename="ou")
router.register(r"person", PersonViewSet, basename="person")
router.register(r"guests/", GuestInfoViewSet, basename="guests")

urlpatterns = router.urls
urlpatterns += [
    re_path(r"consenttypes/$", ConsentTypeViewSet.as_view(), name="consent-types"),
    re_path(r"roletypes/$", RoleTypeViewSet.as_view(), name="role-types"),
    re_path(r"units/$", UnitsViewSet.as_view(), name="units"),
    path("invited/", InvitedGuestView.as_view(), name="invited-info"),
    path("invitecheck/", CheckInvitationView.as_view(), name="invite-verify"),
    path(
        "invite/<int:person_id>/resend",
        ResendInvitationView.as_view(),
        name="invite-resend",
    ),
    path(
        "invite/<int:identity_id>/recreate",
        RecreateInvitationView.as_view(),
        name="invite-recreate",
    ),
    path("invite/", InvitationView.as_view(), name="invitation"),
    path(
        "person/search/",
        PersonSearchViewSet.as_view({"get": "list"}),
        name="person-search",
    ),
    path(
        "me/profile/update/",
        PersonUpdateViewSet.as_view({"patch": "update"}),
        name="profile-update",
    ),
    path("userinfo/", UserInfoView.as_view(), name="userinfo"),
    path("identitycheck/<int:id>", IdentityCheckView.as_view(), name="identitycheck"),
    path("email/<str:value>", EmailExistsView.as_view(), name="email"),
]
