import logging
import datetime

from django.template.context import Context
from django_q.tasks import async_task

from gregui.mailutils.protocol import prepare_arguments
from gregui.models import EmailTemplate

logger = logging.getLogger(__name__)


class RolesEnding:
    def __init__(self):
        pass

    def queue_sponsor_mail(self, mail_to: str, num_roles: int) -> str:
        logger.info(
            "Queueing a role end notification email to %s about %d roles",
            mail_to,
            num_roles,
        )
        arguments = prepare_arguments(
            template=EmailTemplate.objects.get(
                template_key=EmailTemplate.EmailType.ROLE_END_REMINDER
            ),
            context=Context({"num_roles": num_roles}),
            mail_to=mail_to,
        )
        return async_task(
            "gregui.mailutils.protocol.try_send_mail",
            email_type=EmailTemplate.EmailType.ROLE_END_REMINDER,
            **arguments,
        )

    def queue_guest_mail(
        self, mail_to: str, end_date: datetime.date, name_nb: str, name_en: str
    ) -> str:
        logger.info("Queueing a role end notification email to guest %s", mail_to)
        arguments = prepare_arguments(
            template=EmailTemplate.objects.get(
                template_key=EmailTemplate.EmailType.ROLE_END_REMINDER_GUEST
            ),
            context=Context(
                {"end_date": end_date, "name_nb": name_nb, "name_en": name_en}
            ),
            mail_to=mail_to,
        )
        return async_task(
            "gregui.mailutils.protocol.try_send_mail",
            email_type=EmailTemplate.EmailType.ROLE_END_REMINDER_GUEST,
            **arguments,
        )
