import logging
from typing import Optional, Union

from django.conf import settings
from django.template.context import Context
from django_q.tasks import async_task

from greg.models import InvitationLink
from gregui.mailutils.protocol import prepare_arguments
from gregui.models import EmailTemplate

logger = logging.getLogger(__name__)


class ConfirmGuest:
    def __init__(self):
        pass

    def _make_guest_profile_url(self, person_id: Union[str, int]) -> str:
        return f"{settings.BASE_URL}/sponsor/guest/{person_id}"

    def get_template(self):
        return EmailTemplate.objects.get(
            template_key=EmailTemplate.EmailType.SPONSOR_CONFIRMATION
        )

    def queue_mail(
        self, mail_to: str, guest_name: str, guest_person_id: Union[int, str]
    ) -> str:
        arguments = prepare_arguments(
            template=self.get_template(),
            context=Context(
                {
                    "guest": guest_name,
                    "confirmation_link": self._make_guest_profile_url(guest_person_id),
                }
            ),
            mail_to=mail_to,
        )
        return async_task(
            "gregui.mailutils.protocol.try_send_mail",
            email_type=EmailTemplate.EmailType.SPONSOR_CONFIRMATION,
            **arguments,
        )

    def send_confirmation_mail_from_link(self, link: InvitationLink) -> Optional[str]:
        guest = link.invitation.role.person
        return self.queue_mail(
            mail_to=link.invitation.role.sponsor.work_email,
            guest_name=f"{guest.first_name} {guest.last_name}",
            guest_person_id=guest.id,
        )
