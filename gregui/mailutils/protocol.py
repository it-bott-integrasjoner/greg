from typing import Protocol, Union
from smtplib import SMTPException
import logging

from django.core import mail

from gregui.models import EmailTemplate

logger = logging.getLogger(__name__)


def prepare_arguments(
    template: EmailTemplate, context: dict[str, str], mail_to: str
) -> dict[str, Union[str, list[str]]]:
    """Combine input to a dict ready for use as arguments to django's send_mail"""
    return {
        "subject": template.get_subject(context),
        "message": template.get_body(context),
        "from_email": template.from_email or None,
        "recipient_list": [mail_to],
    }


def log_and_send_mail(**kwargs):
    logger.info("Queueing email: %s", kwargs)
    return mail.send_mail(**kwargs)


def try_send_mail(email_type: str, **kwargs):
    try:
        log_and_send_mail(**kwargs)
    except SMTPException as smtp_err:
        logger.warning(
            "Could not send %s e-mail to %s. Exception: %s: %s",
            email_type,
            kwargs["recipient_list"],
            type(smtp_err).__name__,
            smtp_err,
        )


class EmailSender(Protocol):
    def queue_mail(self, *args):
        pass
