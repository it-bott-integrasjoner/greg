from django.contrib import admin
from reversion.admin import VersionAdmin

from gregui.models import EmailTemplate, GregUserProfile


class GregUserProfileAdmin(VersionAdmin):
    list_display = ["id", "userid_feide", "person", "sponsor"]
    search_fields = ("userid_feide",)


class EmailTemplateAdmin(VersionAdmin):
    list_display = ["id", "template_key", "subject", "from_email"]
    save_as = True


admin.site.register(GregUserProfile, GregUserProfileAdmin)
admin.site.register(EmailTemplate, EmailTemplateAdmin)
