import datetime
import re
import time
import urllib.parse
from typing import Optional

import requests
import structlog

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend, UserModel
from django.core.exceptions import SuspiciousOperation
from django.utils import timezone

from mozilla_django_oidc.auth import OIDCAuthenticationBackend

from greg.models import Identity, Person, Sponsor, InvitationLink
from gregui.models import GregUserProfile
from gregui.utils import name_diff_too_large

logger = structlog.getLogger(__name__)


class DevBackend(BaseBackend):
    """
    Development backend, no password checking.

    TODO:
    - Expand to add person/sponsor for the user
    - How do we do login? A dev login site?

    """

    def __init__(self):
        self.UserModel = get_user_model()

    dev_users = {
        "bruker1": {
            "email": "bruker1@uio.no",
            "first_name": "test",
            "last_name": "testesen",
        },
        "bruker2": {
            "email": "bruker2@uio.no",
            "first_name": "foo",
            "last_name": "foosen",
        },
    }

    def get_userinfo(self, username):
        return self.dev_users.get(username, {})

    def get_or_create_user(self, username):
        if not username:
            return None
        try:
            user = self.UserModel.objects.get(username=username)
        except self.UserModel.DoesNotExist:
            userinfo = self.get_userinfo(username)
            user = self.UserModel(username=username, **userinfo)
            user.save()
        return user

    def authenticate(self, request, **kwargs):
        # This has been made so that developers can authenticate as any user in test
        username = kwargs.get("username")
        logger.debug("Username is %s", username)
        request.session["oidc_id_token_payload"] = {"iat": time.time()}
        return self.get_or_create_user(username)

    def get_user(self, user_id):
        try:
            return self.UserModel.objects.get(pk=user_id)
        except self.UserModel.DoesNotExist:
            return None


# This regex is quite naive since it assumes that the person only has one last_name,
# and everything before that is the first_name.
NAMES_REGEX = re.compile(r"^(?P<first_name>.+) (?P<last_name>.+)$")
ID_REGEX = re.compile(r"^(?P<id_type>[^:]+):(?P<id_value>.+)$")


def extract_userinfo(claims: dict) -> dict:
    ids = [x.split(":")[0] for x in claims["dataporten-userid_sec"] if ":" in x]
    if len(ids) == 1 and "nin" in ids:
        # IDPorten login
        auth_type = "idporten"
        first_name = ""
        last_name = ""
        email = ""

    else:
        # Feide login
        auth_type = "feide"
        name = claims["name"]
        # This will brake if the user only have 1 name
        match = re.search(NAMES_REGEX, name)
        first_name = match.group("first_name")
        last_name = match.group("last_name")
        email = claims["email"]

    userinfo = {
        "auth_type": auth_type,
        "email": email,
        "first_name": first_name,
        "last_name": last_name,
    }

    secondary_user_ids = claims["dataporten-userid_sec"]
    for user_id in secondary_user_ids:
        match = re.search(ID_REGEX, user_id)
        id_type = match.group("id_type")
        id_value = match.group("id_value")
        if id_type == "feide":
            userinfo["userid_feide"] = id_value
        elif id_type == "nin":
            userinfo["userid_nin"] = id_value
    return userinfo


class ValidatingOIDCBackend(OIDCAuthenticationBackend):
    def validate_issuer(self, payload):
        issuer = self.get_settings("OIDC_OP_ISSUER")
        if not issuer == payload["iss"]:
            raise SuspiciousOperation(
                f'"iss": {repr(payload["iss"])} does not match '
                f"configured value for OIDC_OP_ISSUER: {repr(issuer)}"
            )

    def validate_audience(self, payload):
        client_id = self.get_settings("OIDC_RP_CLIENT_ID")
        trusted_audiences = self.get_settings("OIDC_TRUSTED_AUDIENCES", [])
        trusted_audiences = set(trusted_audiences)
        trusted_audiences.add(client_id)

        audience = payload["aud"]
        if not isinstance(audience, list):
            audience = [audience]
        audience = set(audience)
        if client_id not in audience:
            raise SuspiciousOperation(
                f"Client id not present in audiences: {repr(audience)}"
            )
        distrusted_audiences = audience.difference(trusted_audiences)
        if distrusted_audiences:
            raise SuspiciousOperation(
                f'"aud" contains distrusted audiences: {repr(distrusted_audiences)}'
            )

    def validate_expiry(self, payload):
        expire_time = payload["exp"]
        now = time.time()
        if now > expire_time:
            raise SuspiciousOperation(
                f"Id-token is expired {repr(now)} > {repr(expire_time)}"
            )

    def validate_id_token(self, payload):
        """Validate the content of the id token as required by OpenID Connect 1.0

        This aims to fulfill point 2. 3. and 9. under section 3.1.3.7. ID Token
        Validation
        """
        self.validate_issuer(payload)
        self.validate_audience(payload)
        self.validate_expiry(payload)
        return payload

    def verify_claims(self, claims):
        """
        Override the default claims verification.

        The default implementation requires an email address.
        This does not work with IDPorten, as we only get a NIN
        """
        return True

    def verify_token(self, token, **kwargs):
        # Overriding this method to fix issues described here:
        # https://github.com/mozilla/mozilla-django-oidc/issues/340
        payload = super().verify_token(token, **kwargs)
        return self.validate_id_token(payload)


class GregOIDCBackend(ValidatingOIDCBackend):
    """Custom mozilla OIDC backend for Greg."""

    def verify_token(self, token, **kwargs):
        payload = super().verify_token(token, **kwargs)
        # While mozilla_django_oidc does have support for automatically storing the
        # id-token, it is more useful for us to be able to store the actual payload of
        # the id-token.
        session = self.request.session
        session["oidc_id_token_payload"] = {"iat": payload["iat"]}
        return payload

    def get_username(self, claims) -> str:
        return f"{self.get_settings('OIDC_OP_ISSUER')}{claims}"

    def filter_users_by_claims(self, claims):
        #  Ideally dataporten should include 'iss' in its claims, so that it can be
        #  used along with sub to create a unique identifier for the user. However, if
        #  the id-token has been validated, it means that the 'iss' specified by the
        #  id-token is equal to what is specified in the configuration (OIDC_OP_ISSUER).
        sub = claims.get("sub")
        if not sub:
            return self.UserModel.objects.none()
        username = self.get_username(sub)

        try:
            user = self.UserModel.objects.filter(username=username)
            return user
        except self.UserModel.DoesNotExist:
            return self.UserModel.objects.none()

    def get_extended_userinfo(self, access_token):
        """Return the feide extended user details dictionary."""

        extended_user_response = requests.get(
            self.get_settings("OIDC_OP_FEIDE_EXTENDED_USER_ENDPOINT"),
            headers={"Authorization": f"Bearer {access_token}"},
            verify=self.get_settings("OIDC_VERIFY_SSL", True),
            timeout=self.get_settings("OIDC_TIMEOUT", None),
            proxies=self.get_settings("OIDC_PROXY", None),
        )
        extended_user_response.raise_for_status()
        return extended_user_response.json()

    def get_or_create_user(self, access_token, id_token, payload):
        # This method has been overridden to update the user_profile object
        # for a user at login

        user = super().get_or_create_user(access_token, id_token, payload)
        if user:
            # Update or create a user_profile
            userinfo = extract_userinfo(
                self.get_userinfo(access_token, id_token, payload)
            )

            if userinfo["auth_type"] == "feide":
                extended_userinfo = self.get_extended_userinfo(access_token)
                if "norEduPersonNIN" in extended_userinfo:
                    userinfo["userid_nin"] = extended_userinfo["norEduPersonNIN"]
            self._get_or_create_greg_user_profile(userinfo, user)
        return user

    def _get_sponsor_from_userinfo(self, userinfo: dict) -> Optional[Sponsor]:
        """Get the sponsor object from userinfo."""
        if "userid_feide" in userinfo:
            feide_id = userinfo["userid_feide"]
            try:
                sponsor = Sponsor.objects.get(feide_id=feide_id)
            except Sponsor.DoesNotExist:
                sponsor = None
            return sponsor
        return None

    def _get_person_from_userinfo(self, userinfo: dict) -> Optional[Person]:
        # Get person by nin
        nin_person: Optional[Person] = None
        if "userid_nin" in userinfo:
            try:
                identity = Identity.objects.get(
                    type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
                    value=userinfo["userid_nin"],
                )
                nin_person = identity.person
                logger.info(
                    "person_match",
                    id_type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
                    person=nin_person.id,
                )
            except Identity.DoesNotExist:
                logger.info(
                    "person_no_match",
                    id_type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
                )
                nin_person = None

        if userinfo["auth_type"] == "idporten":
            # Login via IDPorten. We only get nin. No other data or IDs to check
            return nin_person

        # Get person by feide_id
        feide_person: Optional[Person] = None

        feide_ids = Identity.objects.filter(
            type=Identity.IdentityType.FEIDE_ID,
            value=userinfo["userid_feide"],
        )
        if not feide_ids:
            logger.info(
                "person_no_match",
                id_type=Identity.IdentityType.FEIDE_ID,
            )
            return None

        if len(feide_ids) == 1:
            feide_id = feide_ids.first()
        else:
            different_persons = feide_ids.exclude(person=feide_ids.first().person)
            if different_persons:
                logger.error(
                    "person_id=%s and person_id=%s have the same Feide-ID",
                    different_persons.first().pk,
                    feide_ids.first().pk,
                )
                return None
            feide_id = feide_ids.first()

        feide_person: Person = feide_id.person
        logger.info(
            "person_match",
            id_type=Identity.IdentityType.FEIDE_ID,
            person=feide_person.id,
        )

        if not feide_person and not nin_person:
            # No person match
            logger.info("person_not_found")
            return None

        # Check if we got two different persons
        if feide_person and nin_person and feide_person != nin_person:
            logger.error(
                "person_multiple_match",
                nin_person=nin_person.id,
                feide_person=feide_person.id,
            )
            return None

        return feide_person if feide_person else nin_person

    def _update_person_nin(self, person: Person, new_nin: str, source: str) -> None:
        """Adds or update a persons nin."""
        logger.bind(identity_type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER)
        nins: list[Identity] = person.identities.filter(
            type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER
        ).all()

        if len(nins) == 1:
            existing_nin = nins[0]
            if existing_nin.value != new_nin:
                # TODO. Do we abort here?
                logger.warning("identity_mismatch")

            # Update existing nin identity
            existing_nin.source = source
            existing_nin.value = new_nin
            existing_nin.verified = Identity.Verified.AUTOMATIC
            existing_nin.verified_at = timezone.make_aware(datetime.datetime.now())
            existing_nin.save()
            logger.info("identity_update", identity_id=existing_nin.id)

        else:
            if len(nins) > 1:
                # More then 1 nin on person, deleting and add the new one
                logger.warning("multiple_identities")
                for existing_nin in nins:
                    logger.info("identity_delete", identity_id=existing_nin.id)
                    existing_nin.delete()

            nin_id = Identity(
                person=person,
                source=source,
                type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
                value=new_nin,
                verified=Identity.Verified.AUTOMATIC,
                verified_at=timezone.make_aware(datetime.datetime.now()),
            )
            nin_id.save()
            logger.info("identity_add", identity_id=nin_id.id)

    def _update_person_feide_id(
        self, person: Person, new_feide_id: str, source: str
    ) -> None:
        """
        Adds or update a persons feide_ids.

        A person can in theory have feide_ids from multiple institutions.
        Here we only look at feide_ids comming from the same institution as the current login.
        """
        logger.bind(identity_type=Identity.IdentityType.FEIDE_ID)

        # Get existing feide_ids from institution
        feide_inst: str = new_feide_id.split("@")[-1]
        feide_ids: list[Identity] = [
            x
            for x in person.identities.filter(type=Identity.IdentityType.FEIDE_ID)
            .exclude(source=settings.CEREBRUM_SOURCE)
            .all()
            if x.value.split("@")[-1] == feide_inst
        ]

        if len(feide_ids) == 1:
            # Update existing feide_id
            existing_feide_id = feide_ids[0]
            if existing_feide_id.value != new_feide_id:
                logger.warning(
                    "identity_mismatch",
                    existing_feide_id=existing_feide_id,
                    new_feide_id=new_feide_id,
                )
            logger.info(
                "identity_update",
                identity_id=existing_feide_id.id,
                feide_id_new=new_feide_id,
                feide_id_old=existing_feide_id,
            )
            existing_feide_id.source = source
            existing_feide_id.value = new_feide_id
            existing_feide_id.verified = Identity.Verified.AUTOMATIC
            existing_feide_id.verified_at = timezone.make_aware(datetime.datetime.now())
            existing_feide_id.save()

        else:
            if len(feide_ids) > 1:
                # Found multiple feide ids from the same institution.
                # Delete all and add the new one.
                logger.warning("multiple_identities")
                for existing_id in feide_ids:
                    logger.info(
                        "identity_delete",
                        feide_id=existing_id.value,
                        identity_id=existing_id.id,
                    )
                    existing_id.delete()

            feide_id = Identity(
                person=person,
                source=source,
                type=Identity.IdentityType.FEIDE_ID,
                value=new_feide_id,
                verified=Identity.Verified.AUTOMATIC,
                verified_at=timezone.make_aware(datetime.datetime.now()),
            )
            feide_id.save()
            logger.info("identity_add", identity_id=feide_id.id, feide_id=new_feide_id)

    def _update_person_email(self, person: Person, new_email: str, source: str):
        """Add or update a users email."""
        logger.bind(identity_type=Identity.IdentityType.FEIDE_EMAIL)
        # We only look at emails from the same source.
        emails: list[Identity] = [
            x
            for x in person.identities.filter(
                type=Identity.IdentityType.FEIDE_EMAIL
            ).all()
            if x.source == source
        ]

        if len(emails) == 1:
            # Update existing email,
            existing_email = emails[0]
            existing_email.source = source
            old_email = existing_email.value
            existing_email.value = new_email
            existing_email.verified = Identity.Verified.AUTOMATIC
            existing_email.verified_at = timezone.make_aware(datetime.datetime.now())
            existing_email.save()
            logger.info(
                "identity_update",
                identity_id=existing_email.id,
                old_value=old_email,
                new_value=new_email,
            )
        else:
            if len(emails) > 1:
                # Found multiple emails from the same source
                # Delete all and add the new one.
                logger.warning("multiple_identities")
                for email in emails:
                    logger.info(
                        "identity_delete",
                        identity_id=email.id,
                        value=email.value,
                    )
                    email.delete()

            email_id = Identity(
                person=person,
                source=source,
                type=Identity.IdentityType.FEIDE_EMAIL,
                value=new_email,
                verified=Identity.Verified.AUTOMATIC,
                verified_at=timezone.make_aware(datetime.datetime.now()),
            )
            email_id.save()
            logger.info("identity_add", identity_id=email_id.id, value=new_email)

    def _update_person_name(
        self, person: Person, first_name: str, last_name: str
    ) -> None:
        new_combined_name = f"{first_name} {last_name}"
        existing_combined_name = f"{person.first_name} {person.last_name}"

        if name_diff_too_large(existing_combined_name, new_combined_name, 4):
            logger.warning(
                "name_diff_to_large",
                existing_name=existing_combined_name,
                new_name=new_combined_name,
            )
            # TODO: Should we stop when the diff is to large

        person.first_name = first_name
        person.last_name = last_name
        person.save()
        logger.info(
            "name_update", old_name=existing_combined_name, new_name=new_combined_name
        )

    def _update_person_from_userinfo(self, userinfo: dict, person: Person) -> None:
        if not person:
            return

        logger.bind(person=person.id, auth_type=userinfo["auth_type"])

        logger.info("updating_person")

        if userinfo["auth_type"] == "idporten":
            # IdPorten login, only data we get is the NIN
            self._update_person_nin(person, userinfo["userid_nin"], "idporten")
            return

        if "userid_nin" in userinfo:
            self._update_person_nin(person, userinfo["userid_nin"], "feide")
        if "userid_feide" in userinfo:
            self._update_person_feide_id(person, userinfo["userid_feide"], "feide")
        if "email" in userinfo:
            self._update_person_email(person, userinfo["email"], "feide")
        self._update_person_name(person, userinfo["first_name"], userinfo["last_name"])

    def _update_sponsor_from_userinfo(self, userinfo: dict, sponsor: Sponsor) -> None:
        """Updates a sponsor object with data from OIDC."""

        if userinfo["auth_type"] == "feide" and sponsor:
            sponsor.first_name = userinfo["first_name"]
            sponsor.last_name = userinfo["last_name"]
            sponsor.work_email = userinfo["email"]
            sponsor.save()

    def _update_user_profile(
        self, userinfo: dict, user_profile: GregUserProfile, person: Person
    ) -> GregUserProfile:
        sponsor = self._get_sponsor_from_userinfo(userinfo)
        self._update_sponsor_from_userinfo(userinfo, sponsor)

        if not user_profile.person:
            user_profile.person = person

        user_profile.sponsor = sponsor
        user_profile.save()
        self._update_person_from_userinfo(userinfo, user_profile.person)

        logger.info(
            "user_profile_updated",
            user_profile=user_profile,
            feide_id=userinfo.get("userid_feide", ""),
        )
        return user_profile

    def _create_user_profile(
        self, userinfo: dict, user: UserModel, person: Person
    ) -> GregUserProfile:
        sponsor = self._get_sponsor_from_userinfo(userinfo)
        self._update_sponsor_from_userinfo(userinfo, sponsor)

        self._update_person_from_userinfo(userinfo, person)
        user_profile = GregUserProfile(
            user=user,
            person=person,
            sponsor=sponsor,
            userid_feide=userinfo.get("userid_feide", ""),
        )
        user_profile.save()
        logger.info(
            "user_profile_created",
            person=person,
            sponsor=sponsor,
            feide_id=user_profile.userid_feide,
        )
        return GregUserProfile.objects.get(id=user_profile.id)

    def _get_or_create_greg_user_profile(self, userinfo: dict, user: UserModel):
        """Get or create a GregUserProfile."""

        def _get_invitation_link(session) -> Optional[InvitationLink]:
            try:
                return InvitationLink.objects.get(uuid=session["invite_id"])
            except InvitationLink.DoesNotExist:
                logger.error("invite_not_found", invite_id=session["invite_id"])
                logger.bind(authenticating_invite=False)
                return None

        def _get_invite_person(invitation_link) -> Person:
            logger.bind(authenticating_invite=True)
            role = invitation_link.invitation.role
            invite_person = role.person
            logger.info("invite_found", invite_person=invite_person.id)
            return invite_person

        logger.bind(user=user)
        try:
            # Try to find existing UserProfile
            user_profile = GregUserProfile.objects.get(user=user)
            logger.info("user_profile_found", user_profile=user_profile.id)

        except GregUserProfile.DoesNotExist:
            user_profile = None

        # Check for sponsor status.
        sponsor = self._get_sponsor_from_userinfo(userinfo)
        self._update_sponsor_from_userinfo(userinfo, sponsor)

        session = self.request.session
        if "invite_id" in session:
            old_person = None
            if user_profile and user_profile.person:
                old_person = self._get_person_from_userinfo(userinfo)
            person = old_person
            invitation_link = _get_invitation_link(session)
            if invitation_link:
                inv_person = _get_invite_person(invitation_link)
                if old_person and old_person != inv_person:
                    # Logged in person has a person object in greg but has followed an
                    # invite.
                    inv_name = f"{inv_person.first_name} {inv_person.last_name}"
                    old_name = f"{old_person.first_name} {old_person.last_name}"
                    if (
                        not inv_person.registration_completed_date
                        and not name_diff_too_large(old_name, inv_name, 4)
                    ):
                        logger.info(
                            "Invitation (%s) opened by existing person (%s)."
                            " Giving role to them and deleting invited person (%s)",
                            invitation_link.invitation.id,
                            old_person.id,
                            inv_person.id,
                        )
                        # The name is close and the invited person has not completed
                        # registration. Give the role to the existing person, and
                        # delete the invited one.
                        role = invitation_link.invitation.role
                        role.person = old_person
                        role.save()
                        inv_person.delete()
                    else:
                        # The logged in user has gotten someone else's invitation, and
                        # the invitation should be disabled.
                        logger.warning(
                            "Illegal person accessed invitation (%s). Invitation belongs to"
                            " person id %s, but was access by person id %s. Expiring"
                            " InvitationLink %s.",
                            invitation_link.invitation.id,
                            inv_person.id,
                            old_person.id,
                            invitation_link.id,
                        )
                        invitation_link.expire = timezone.now()
                        invitation_link.save()
                else:
                    person = inv_person
        else:
            person = self._get_person_from_userinfo(userinfo)
            logger.bind(authenticating_invite=False)

        if user_profile:
            return self._update_user_profile(userinfo, user_profile, person)
        return self._create_user_profile(userinfo, user, person)

    def create_user(self, claims: dict) -> UserModel:
        userinfo = extract_userinfo(claims)
        username = self.get_username(claims["sub"])
        user = self.UserModel(
            username=username,
            first_name=userinfo["first_name"],
            last_name=userinfo["last_name"],
            email=userinfo["email"],
        )
        user.save()
        self._get_or_create_greg_user_profile(userinfo, user)

        return user

    def update_user(self, user: UserModel, claims: dict) -> UserModel:
        username = self.get_username(claims["sub"])
        user = self.UserModel.objects.get(username=username)
        userinfo = extract_userinfo(claims)
        for key, new_value in userinfo.items():
            current_value = getattr(user, key, None)
            if not new_value == current_value:
                setattr(user, key, new_value)
        user.save()
        self._get_or_create_greg_user_profile(userinfo, user)

        return user


def provider_logout(request):
    id_token = request.session.get("oidc_id_token")
    query_params = urllib.parse.urlencode(
        {
            "id_token_hint": id_token,
            "post_logout_redirect_uri": settings.LOGOUT_REDIRECT_URL,
        }
    )
    logout_request_url = f"{settings.OIDC_END_SESSION_ENDPOINT}?{query_params}"
    return logout_request_url
