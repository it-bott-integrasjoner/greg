import time
import pytest

from django.core.exceptions import SuspiciousOperation

from gregui.authentication.auth_backends import GregOIDCBackend


pytestmark = pytest.mark.django_db


def test_validate_issuer(id_token_payload):
    backend = GregOIDCBackend()
    backend.validate_issuer(id_token_payload)
    id_token_payload["iss"] = "http://suspicious.no"
    with pytest.raises(SuspiciousOperation):
        backend.validate_issuer(id_token_payload)


def test_validate_audiences(id_token_payload):
    backend = GregOIDCBackend()
    backend.validate_audience(id_token_payload)

    id_token_payload["aud"] = [id_token_payload["aud"], "other_aud"]
    with pytest.raises(SuspiciousOperation):
        backend.validate_audience(id_token_payload)


def test_validate_expiry(id_token_payload):
    backend = GregOIDCBackend()
    with pytest.raises(SuspiciousOperation):
        backend.validate_expiry(id_token_payload)

    id_token_payload["exp"] = int(time.time()) + 3600
    backend.validate_expiry(id_token_payload)


def test_filter_users(claims, create_user):
    username = "https://auth.dataporten.nosubsub"

    create_user(username=username)

    backend = GregOIDCBackend()
    user = backend.filter_users_by_claims(claims).get()
    assert user.username == "https://auth.dataporten.nosubsub"

    claims["sub"] = "non-existant-sub"
    users = backend.filter_users_by_claims(claims)
    assert len(users) == 0
