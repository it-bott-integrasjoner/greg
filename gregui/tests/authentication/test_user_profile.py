import pytest

from django.test.client import RequestFactory

from greg.models import Identity
from gregui.authentication.auth_backends import GregOIDCBackend
from gregui.models import GregUserProfile, Person


@pytest.mark.django_db
def test_sponsor_first_login(sponsor_foo):
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}

    backend = GregOIDCBackend()
    backend.request = auth_request

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(sponsor=sponsor_foo)

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{sponsor_foo.feide_id}"],
        "dataporten-userid_sec": [f"feide:{sponsor_foo.feide_id}", "nin:12345612345"],
        "name": f"{sponsor_foo.first_name} {sponsor_foo.last_name}",
        "email": "foo@example.org",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person is None
    sponsor = user_profile.sponsor
    assert sponsor == sponsor_foo
    assert sponsor.feide_id == sponsor_foo.feide_id


@pytest.mark.django_db
def test_sponsor_update(sponsor_foo):
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}

    backend = GregOIDCBackend()
    backend.request = auth_request

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(sponsor=sponsor_foo)

    assert sponsor_foo.first_name == "Sponsor"
    assert sponsor_foo.work_email == "foo@example.org"

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{sponsor_foo.feide_id}"],
        "dataporten-userid_sec": [f"feide:{sponsor_foo.feide_id}", "nin:12345612345"],
        "name": f"Baz {sponsor_foo.last_name}",
        "email": "baz@example.org",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person is None
    assert user_profile.sponsor == sponsor_foo

    assert user_profile.sponsor.first_name == "Baz"
    assert user_profile.sponsor.work_email == "baz@example.org"


@pytest.mark.django_db
def test_no_existing_person():
    """
    Normal login, user without existing person or sponsor object.

    Expect a UserProfile with no person or sponsor linked.
    """

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}
    backend = GregOIDCBackend()
    backend.request = auth_request

    claims = {
        "sub": "subsub",
        "connect-userid_sec": ["feide:foo@bar.org"],
        "dataporten-userid_sec": ["feide:foo@bar.org", "nin:12345612345"],
        "name": "Foo Ba",
        "email": "Bar",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person is None
    assert user_profile.sponsor is None


@pytest.mark.django_db
def test_existing_person(person_foo):
    """
    Normal login, user with existing person and no sponsor object.

    Expect a UserProfile with person but not sponsor linked.
    """

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}

    backend = GregOIDCBackend()
    backend.request = auth_request

    feide_id = (
        person_foo.identities.filter(type=Identity.IdentityType.FEIDE_ID).first().value
    )

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person_foo)

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{person_foo.fnr.value}"],
        "name": f"{person_foo.first_name} {person_foo.last_name}",
        "email": f"{person_foo.private_email.value}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person_foo
    assert user_profile.sponsor is None


@pytest.mark.django_db
def test_multiple_feide_id(person_foo, caplog):
    """
    Should be able to log in when person has multiple feide-ids
    """
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}

    backend = GregOIDCBackend()
    backend.request = auth_request

    feide_id = (
        person_foo.identities.filter(type=Identity.IdentityType.FEIDE_ID).first().value
    )

    Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.FEIDE_ID,
        source="cerebrum",
        value="bar@baz.org",
    )

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person_foo)

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{person_foo.fnr.value}"],
        "name": f"{person_foo.first_name} {person_foo.last_name}",
        "email": f"{person_foo.private_email.value}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person_foo
    assert user_profile.sponsor is None
    assert "multiple_identities" not in caplog.text


@pytest.mark.django_db
def test_multiple_persons_same_feide_id(person_foo, caplog):
    """
    Checks that not able to log in if there are multiple people with same
    feide-id
    """
    other_person = Person.objects.create(first_name="Not", last_name="Foo")
    Identity.objects.create(
        person=other_person,
        type=Identity.IdentityType.FEIDE_ID,
        source="feide",
        value="bar@baz.org",
    )
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {}

    backend = GregOIDCBackend()
    backend.request = auth_request

    feide_id = (
        person_foo.identities.filter(type=Identity.IdentityType.FEIDE_ID).first().value
    )

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person_foo)

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{person_foo.fnr.value}"],
        "name": f"{person_foo.first_name} {person_foo.last_name}",
        "email": f"{person_foo.private_email.value}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person is None
    assert user_profile.sponsor is None
    assert "person_id=4 and person_id=2 have the same Feide-ID" in caplog.text


@pytest.mark.django_db
def test_invited_user(invited_person):
    """
    Invitation login, user with existing person and no sponsor object.

    Expect a UserProfile with person but not sponsor linked.
    """

    person, invitation_link = invited_person

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}

    backend = GregOIDCBackend()
    backend.request = auth_request

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person)

    feide_id = "foo@bar.com"
    nin = "12345678910"

    claims = {
        "sub": "subsub",
        "connect-userid_sec": [f"feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{nin}"],
        "name": f"{person.first_name} {person.last_name}",
        "email": f"{feide_id}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)

    person.refresh_from_db()
    assert user_profile.person == person
    assert user_profile.sponsor is None

    person_nins = person.identities.filter(
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
    ).all()
    assert len(person_nins) == 1
    assert person_nins[0].verified == Identity.Verified.AUTOMATIC
    assert person_nins[0].value == nin

    person_feide_ids = person.identities.filter(
        type=Identity.IdentityType.FEIDE_ID,
    ).all()
    assert len(person_feide_ids) == 1
    assert person_feide_ids[0].verified == Identity.Verified.AUTOMATIC
    assert person_feide_ids[0].value == feide_id

    person_feide_email = person.identities.filter(
        type=Identity.IdentityType.FEIDE_EMAIL
    ).all()
    assert len(person_feide_email) == 1
    assert person_feide_email[0].value == feide_id
    assert person_feide_email[0].verified == Identity.Verified.AUTOMATIC


@pytest.mark.django_db
def test_invited_user_no_id(invited_person_no_ids):
    """
    Invitation login, user with existing person and no sponsor object.

    The existing Person has no IDs, tests that we use the person
    from the invitation. Verify that the person object is updated.
    Expect a UserProfile with an updated person but not sponsor linked.
    """

    person, invitation_link = invited_person_no_ids

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}

    assert (
        len(
            person.identities.filter(
                type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER
            ).all()
        )
        == 0
    )
    assert len(person.identities.filter(type=Identity.IdentityType.FEIDE_ID).all()) == 0

    backend = GregOIDCBackend()
    backend.request = auth_request

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person)

    feide_id = "foo@bar.org"
    nin = "12345612345"

    claims = {
        "sub": "subsub",
        "connect-userid_sec": ["feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{nin}"],
        "name": f"{person.first_name} {person.last_name}",
        "email": f"{feide_id}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    user = backend.create_user(claims)
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person
    assert user_profile.sponsor is None

    person_nins = person.identities.filter(
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
    ).all()
    assert len(person_nins) == 1
    assert person_nins[0].value == nin
    assert person_nins[0].verified == Identity.Verified.AUTOMATIC

    person_feide_ids = person.identities.filter(
        type=Identity.IdentityType.FEIDE_ID
    ).all()
    assert len(person_feide_ids) == 1
    assert person_feide_ids[0].value == feide_id
    assert person_feide_ids[0].verified == Identity.Verified.AUTOMATIC

    person_feide_email = person.identities.filter(
        type=Identity.IdentityType.FEIDE_EMAIL
    ).all()
    assert len(person_feide_email) == 1
    assert person_feide_email[0].value == feide_id
    assert person_feide_email[0].verified == Identity.Verified.AUTOMATIC


@pytest.mark.django_db
def test_invited_existing_user(invited_person, user_person):
    """
    Invitation login, user with existing person and GregUserProfile

    Expect the person connected to the invite to be deleted and replaced by
    the existing person
    """
    old_user = user_person
    old_person = GregUserProfile.objects.get(user=old_user).person

    feide_id = (
        old_person.identities.filter(type=Identity.IdentityType.FEIDE_ID).first().value
    )

    person, invitation_link = invited_person

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}

    backend = GregOIDCBackend()
    backend.request = auth_request

    with pytest.raises(GregUserProfile.DoesNotExist):
        GregUserProfile.objects.get(person=person)

    claims = {
        "sub": f"{old_user.username}",
        "connect-userid_sec": [f"feide:{feide_id}"],
        "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{old_person.fnr.value}"],
        "name": f"{old_person.first_name} {old_person.last_name}",
        "email": f"{feide_id}",
        "email_verified": True,
        "picture": "https://foo.org/p:2192dff7-6989-4244-83cc-ae5e78875bdd",
    }

    # set username like this user was created by logging in
    old_user.username = backend.get_username(old_user.username)
    old_user.save()

    assert invitation_link.invitation.role.person == person
    user = backend.update_user(old_user, claims)
    invitation_link.refresh_from_db()
    assert invitation_link.invitation.role.person == old_person

    old_person.refresh_from_db()
    with pytest.raises(Person.DoesNotExist):
        person.refresh_from_db()

    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == old_person


@pytest.mark.django_db
def test_invited_feide_nin_from_extended(requests_mock, invited_person):
    """
    Invitation login, user with existing person and no sponsor object,
    nin only present in extended userinfo.

    A GregUserProfile should be created, and the person should get feide-id and nin set
    """
    person, invitation_link = invited_person

    email = "foo@example.com"
    feide_id = "bar@example.com"
    nin = "12345678901"
    access_token = "foo"
    id_token = "foo"
    payload = "foo"
    requests_mock.get(
        "https://auth.dataporten.no/openid/userinfo",
        json={
            "sub": "subsub",
            "dataporten-userid_sec": [f"feide:{feide_id}"],
            "name": f"{person.first_name} {person.last_name}",
            "email": f"{email}",
        },
    )
    requests_mock.get(
        "https://api.dataporten.no/userinfo/v1/userinfo", json={"norEduPersonNIN": nin}
    )
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}

    backend = GregOIDCBackend()
    backend.request = auth_request

    # No profile beforehand
    assert len(GregUserProfile.objects.filter(person_id=person.id)) == 0
    user = backend.get_or_create_user(access_token, id_token, payload)

    # User profile exists and all things set correctly after
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person
    assert user_profile.person.fnr and user_profile.person.fnr.value == nin
    assert (
        user_profile.person.feide_id and user_profile.person.feide_id.value == feide_id
    )
    assert user_profile.sponsor is None


@pytest.mark.django_db
def test_invited_feide_nin_from_regular(requests_mock, invited_person):
    """
    Invitation login, user with existing person and no sponsor object,
    nin present in regular userinfo.

    A GregUserProfile should be created, and the person should get feide-id and nin set
    """
    person, invitation_link = invited_person

    email = "foo@example.com"
    feide_id = "bar@example.com"
    nin = "12345678901"
    access_token = "foo"
    id_token = "foo"
    payload = "foo"
    requests_mock.get(
        "https://auth.dataporten.no/openid/userinfo",
        json={
            "sub": "subsub",
            "dataporten-userid_sec": [f"feide:{feide_id}", f"nin:{nin}"],
            "name": f"{person.first_name} {person.last_name}",
            "email": f"{email}",
        },
    )
    requests_mock.get("https://api.dataporten.no/userinfo/v1/userinfo", json={})
    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}

    backend = GregOIDCBackend()
    backend.request = auth_request

    # No profile beforehand
    assert len(GregUserProfile.objects.filter(person_id=person.id)) == 0
    user = backend.get_or_create_user(access_token, id_token, payload)

    # User profile exists and all things set correctly after
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person
    assert user_profile.person.fnr and user_profile.person.fnr.value == nin
    assert (
        user_profile.person.feide_id and user_profile.person.feide_id.value == feide_id
    )
    assert user_profile.sponsor is None


@pytest.mark.django_db
def test_invited_feide_no_nin(requests_mock, invited_person):
    """
    Invitation login, user with existing person and no sponsor object,
    nin not present

    A GregUserProfile should be created, and the person should get feide-id set
    """
    person, invitation_link = invited_person
    email = "foo@example.com"
    feide_id = "bar@example.com"
    access_token = "foo"
    id_token = "foo"
    payload = "foo"
    requests_mock.get(
        "https://auth.dataporten.no/openid/userinfo",
        json={
            "sub": "subsub",
            "dataporten-userid_sec": [f"feide:{feide_id}"],
            "name": f"{person.first_name} {person.last_name}",
            "email": f"{email}",
        },
    )
    requests_mock.get("https://api.dataporten.no/userinfo/v1/userinfo", json={})

    auth_request = RequestFactory().get("/foo", {"code": "foo", "state": "bar"})
    auth_request.session = {"invite_id": invitation_link.uuid}
    backend = GregOIDCBackend()
    backend.request = auth_request

    # No profile beforehand
    assert len(GregUserProfile.objects.filter(person_id=person.id)) == 0
    user = backend.get_or_create_user(access_token, id_token, payload)

    # User profile exists and all things set correctly after
    user_profile = GregUserProfile.objects.get(user=user)
    assert user_profile.person == person
    assert user_profile.person.fnr is None
    assert (
        user_profile.person.feide_id and user_profile.person.feide_id.value == feide_id
    )
    assert user_profile.sponsor is None
