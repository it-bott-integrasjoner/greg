from gregui.utils import name_diff, name_diff_too_large, restricted_damarau_levenshtein


def test_rdm_replace():
    assert restricted_damarau_levenshtein("abc", "abd") == 1


def test_rdm_swap():
    assert restricted_damarau_levenshtein("abc", "bac") == 1


def test_name_diff():
    assert name_diff("Foo Bar", "Foo Baz") == 1


def test_name_longer():
    """Verify stops early with default threshold"""
    assert name_diff("Abcdefgh Ijklmnop", "Qrstuvw Xyz") == 5


def test_name_threshold():
    """Verify continues with raised threshold"""
    assert name_diff("Abcdefgh Ijklmnop", "Qrstuvw Xyz", 100) == 10


def test_name_too_large():
    """Verify boolean version responds with expected boolean"""
    assert not name_diff_too_large("Test Name", "Testing Name", 3)
    assert name_diff_too_large("Test Name", "Testing Name", 2)
