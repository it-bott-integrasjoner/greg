import pytest

from django.conf import settings
from rest_framework.reverse import reverse

from greg.models import Identity
from gregui.models import GregUserProfile


@pytest.mark.django_db
def test_identity_get(client, log_in, user_sponsor, person_foo):
    log_in(user_sponsor)
    response = client.get(
        reverse("gregui-v1:identity-detail", kwargs={"pk": person_foo.fnr.id})
    )
    assert response.status_code == 200
    content = response.json()
    assert content["person"] == person_foo.id


@pytest.mark.django_db
def test_identity_patch(client, log_in, user_sponsor, person_foo):
    log_in(user_sponsor)
    ident = Identity.objects.get(pk=person_foo.fnr.id)
    assert not ident.verified

    client.patch(
        reverse("gregui-v1:identity-detail", kwargs={"pk": person_foo.fnr.id}),
        data={},
    )
    ident.refresh_from_db()
    assert ident.verified == Identity.Verified.MANUAL
    assert ident.verified_by == GregUserProfile.objects.get(user=user_sponsor).sponsor


@pytest.mark.django_db
def test_identity_check_nonexisting_fnr(
    requests_mock, client, log_in, user_sponsor, person_foo
):
    """Verify that identitycheck endpoint checks iga when queried"""
    settings.IGA_CHECK = True
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "id_type=norwegianNationalId&external_id=12345612345",
        json={"external_ids": []},
    )
    log_in(user_sponsor)
    url = reverse("gregui-v1:identitycheck", kwargs={"id": person_foo.fnr.id})
    response = client.get(url)
    assert response.json() == {"match": None}


@pytest.mark.django_db
def test_identity_check_existing_fnr(
    requests_mock, client, log_in, user_sponsor, person_foo
):
    """Verify that identitycheck endpoint checks iga when queried"""
    settings.IGA_CHECK = True
    requests_mock.get(
        "http://example.com/cerebrum/v1/search/persons/external-ids?"
        "id_type=norwegianNationalId&external_id=12345612345",
        json={
            "external_ids": [
                {
                    "person_id": 1,
                    "source_system": "dfo_sap",
                    "external_id": "12345612345",
                    "id_type": "NO_BIRTHNO",
                }
            ]
        },
    )
    requests_mock.get(
        "http://example.com/cerebrum/v1/persons/1",
        json={
            "contexts": ["string"],
            "created_at": "2022-02-17T10:17:31.305Z",
            "href": "http://example.com/cerebrum/v1/search/persons/1",
            "names": [
                {"source_system": "dfo_sap", "variant": "FIRST", "name": "Ola"},
                {"source_system": "dfo_sap", "variant": "LAST", "name": "Nordmann"},
            ],
            "birth_date": "2022-02-17T10:17:31.305Z",
            "id": 1,
        },
    )

    log_in(user_sponsor)
    url = reverse("gregui-v1:identitycheck", kwargs={"id": person_foo.fnr.id})
    response = client.get(url)
    assert response.json() == {"match": {"first": "Ola", "last": "Nordmann"}}


@pytest.mark.django_db
def test_existing_email(client, log_in, user_sponsor, person_foo):
    log_in(user_sponsor)
    response = client.get(
        reverse("gregui-v1:email", kwargs={"value": person_foo.private_email.value})
    )
    assert response.status_code == 200
    response = client.get(
        reverse("gregui-v1:email", kwargs={"value": "nosuch@email.com"})
    )
    assert response.status_code == 404
