import pytest
from rest_framework.reverse import reverse
from rest_framework import status


@pytest.mark.django_db
def test_userinfo_anon_get(client):
    """Anonymous people should be forbidden."""
    response = client.get(reverse("gregui-v1:userinfo"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_userinfo_invited_get(client, invitation_link):
    """Invited guests should get info about themself and the role."""
    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()
    response = client.get(reverse("gregui-v1:userinfo"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "auth_type": "invite",
        "feide_id": None,
        "sponsor_id": None,
        "person_id": 1,
        "first_name": "Foo",
        "last_name": "Baمr",
        "email": "foo@example.org",
        "mobile_phone": None,
        "fnr": None,
        "national_id_card_number": None,
        "passport": None,
        "registration_completed_date": None,
        "roles": [
            {
                "id": 1,
                "ou_nb": "Foo NB",
                "ou_en": "Foo EN",
                "name_nb": "Role Foo NB",
                "name_en": "Role Foo EN",
                "start_date": None,
                "end_date": "2050-10-15",
                "sponsor": {"first_name": "Sponsor", "last_name": "Bar"},
            },
        ],
        "consents": [],
    }


@pytest.mark.django_db
def test_userinfo_user_no_profile(client, log_in, user_no_profile):
    """Pure django users should be forbidden"""
    log_in(user_no_profile)
    response = client.get(reverse("gregui-v1:userinfo"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_userinfo_sponsor_get(client, log_in, user_sponsor):
    """Sponsors should get info about themselves"""
    log_in(user_sponsor)

    response = client.get(reverse("gregui-v1:userinfo"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "auth_type": "oidc",
        "first_name": "Sponsor",
        "last_name": "Bar",
        "feide_id": "foo@example.org",
        "person_id": None,
        "roles": [],
        "consents": [],
        "sponsor_id": 1,
        "registration_completed_date": None,
    }


@pytest.mark.django_db
def test_userinfo_guest_get(client, log_in, user_person):
    """Logged in guests should get info about themself"""
    log_in(user_person)
    response = client.get(reverse("gregui-v1:userinfo"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "auth_type": "oidc",
        "feide_id": "",
        "sponsor_id": None,
        "person_id": 1,
        "roles": [],
        "consents": [],
        "first_name": "Fooم",
        "last_name": "Bar",
        "email": "foo@bar.com",
        "mobile_phone": None,
        "fnr": "123456*****",
        "national_id_card_number": None,
        "passport": None,
        "registration_completed_date": None,
    }
