import datetime

import pytest
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from greg.models import Identity, Person, Role, Invitation, InvitationLink
from gregui.api.views.invitation import InvitationView
from gregui.mailutils.invite_guest import InviteGuest


@pytest.mark.django_db
def test_invite_guest(client, user_sponsor, unit_foo, role_type_foo, mocker):
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")

    test_comment = "This is a test comment"
    contact_person_unit = "This is a test contact person"
    role_start_date = datetime.datetime.today() + datetime.timedelta(days=1)
    role_end_date = datetime.datetime.today() + datetime.timedelta(days=10)

    data = {
        "first_name": "ſfooþ",
        "last_name": "barŮ",
        "email": "test@example.com",
        "role": {
            "start_date": (role_start_date).strftime("%Y-%m-%d"),
            "end_date": (role_end_date).strftime("%Y-%m-%d"),
            "orgunit": unit_foo.id,
            "type": role_type_foo.id,
            "comments": test_comment,
            "contact_person_unit": contact_person_unit,
        },
    }

    assert len(Person.objects.all()) == 0

    request = APIRequestFactory().post(
        path=reverse("gregui-v1:invitation"), data=data, format="json"
    )
    force_authenticate(request, user=user_sponsor)

    response = InvitationView.as_view()(request)

    assert response.status_code == status.HTTP_201_CREATED

    assert Person.objects.count() == 1
    person = Person.objects.first()
    assert person.first_name == "ſfooþ"
    assert person.last_name == "barŮ"

    assert Identity.objects.filter(
        person=person,
        type=Identity.IdentityType.PRIVATE_EMAIL,
        value="test@example.com",
    ).exists()

    role = Role.objects.filter(person__id=person.id).get()
    assert role.orgunit == unit_foo
    assert role.type == role_type_foo
    assert role.start_date == role_start_date.date()
    assert role.end_date == role_end_date.date()
    assert role.contact_person_unit == contact_person_unit
    assert role.comments == test_comment
    send_invite_mock_function.assert_called()


@pytest.mark.django_db
def test_invite_guest__illegal_name(client, user_sponsor, unit_foo, role_type_foo):
    test_comment = "This is a test comment"
    contact_person_unit = "This is a test contact person"
    role_start_date = datetime.datetime.today() + datetime.timedelta(days=1)
    role_end_date = datetime.datetime.today() + datetime.timedelta(days=10)

    data = {
        "first_name": "foo木👍أ",
        "last_name": "غbar水",
        "email": "test@example.com",
        "role": {
            "start_date": (role_start_date).strftime("%Y-%m-%d"),
            "end_date": (role_end_date).strftime("%Y-%m-%d"),
            "orgunit": unit_foo.id,
            "type": role_type_foo.id,
            "comments": test_comment,
            "contact_person_unit": contact_person_unit,
        },
    }

    assert len(Person.objects.all()) == 0

    request = APIRequestFactory().post(
        path=reverse("gregui-v1:invitation"), data=data, format="json"
    )
    force_authenticate(request, user=user_sponsor)

    response = InvitationView.as_view()(request)

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_invite_cancel(client, invitation_link, invitation, role, log_in, user_sponsor):
    # TODO: Should all sponsors be allowed to delete arbitrary invitations?
    log_in(user_sponsor)
    url = reverse("gregui-v1:invitation")

    # Check that the role is there
    role = Role.objects.get(id=role.id)
    response = client.delete(f"{url}?person_id={str(role.person_id)}")

    assert response.status_code == status.HTTP_200_OK

    # The role, invitation and connected links should now have been removed
    assert Role.objects.filter(id=role.id).count() == 0
    assert Invitation.objects.filter(id=invitation.id).count() == 0
    assert InvitationLink.objects.filter(invitation__id=invitation.id).count() == 0


@pytest.mark.django_db
def test_fail_delete_confirmed_invitation(
    client, role, log_in, user_sponsor, sponsor_foo, invitation, invitation_link
):
    log_in(user_sponsor)

    role = Role.objects.get(pk=role.id)
    pe = Person.objects.get(pk=role.person_id)
    pe.registration_completed_date = datetime.date.today()
    pe.save()
    assert pe.registration_completed_date
    assert pe.is_registered

    url = reverse("gregui-v1:invitation")
    response = client.delete(f"{url}?person_id={str(pe.id)}")

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    # The role, invitation and connected links should still exist
    assert Role.objects.filter(id=role.id).count() == 1
    assert Invitation.objects.filter(id=invitation.id).count() == 1
    assert InvitationLink.objects.filter(invitation__id=invitation.id).count() == 1


@pytest.mark.django_db
def test_invite_resend_existing_invite_active(
    client,
    log_in,
    user_sponsor,
    person_invited,
    invitation,
    invitation_link,
    invitation_link_expired,
    mocker,
):
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")
    log_in(user_sponsor)

    invitation_links_for_person = InvitationLink.objects.filter(
        invitation__role__person_id=person_invited.id
    )
    assert invitation_links_for_person.count() == 2

    original_expire_date = invitation_link_expired.expire
    original_date = invitation_link.expire
    url = reverse("gregui-v1:invite-resend", kwargs={"person_id": person_invited.id})
    response = client.patch(url)
    assert response.status_code == status.HTTP_200_OK

    # One e-mail should have been sent with an invite
    assert send_invite_mock_function.call_count == 1

    # Just check that the expire dates have not changed
    invitation_link_expired.refresh_from_db()
    assert original_expire_date == invitation_link_expired.expire

    invitation_link.refresh_from_db()
    assert original_date == invitation_link.expire

    # The number of invitations should not have changed
    invitation_links_for_person = InvitationLink.objects.filter(
        invitation__role__person_id=person_invited.id
    )
    assert invitation_links_for_person.count() == 2


@pytest.mark.django_db
def test_invite_resend_existing_invite_not_active(
    client, log_in, user_sponsor, person_invited, invitation_link_expired, mocker
):
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")

    log_in(user_sponsor)

    invitation_links_for_person = InvitationLink.objects.filter(
        invitation__role__person_id=person_invited.id
    )
    assert invitation_links_for_person.count() == 1

    original_expire_date = invitation_link_expired.expire
    url = reverse("gregui-v1:invite-resend", kwargs={"person_id": person_invited.id})
    response = client.patch(url)
    assert response.status_code == status.HTTP_200_OK

    # One e-mail should have been sent with an invite
    assert send_invite_mock_function.call_count == 1

    # Just check that the expire dates have not changed
    invitation_link_expired.refresh_from_db()
    assert original_expire_date == invitation_link_expired.expire

    # There should now be two invitations
    invitation_links_for_person = InvitationLink.objects.filter(
        invitation__role__person_id=person_invited.id
    )
    assert invitation_links_for_person.count() == 2

    # If there is no active link the following line will raise an exception
    InvitationLink.objects.get(
        invitation__role__person_id=person_invited.id, expire__gt=timezone.now()
    )


@pytest.mark.django_db
def test_invite_resend_person_no_invites_fail(
    client, log_in, user_sponsor, person_invited
):
    """Resending an invite for a person without invites should return bad request."""
    log_in(user_sponsor)

    url = reverse("gregui-v1:invite-resend", kwargs={"person_id": person_invited.id})
    response = client.patch(url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_invite_resend_person_multiple_links_send_all(
    client, log_in, user_sponsor, invited_person, registration_template, mocker
):
    """Resending an invite for a person with multiple working links should resend all of them."""
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")
    log_in(user_sponsor)
    person, invitation = invited_person
    invitation = Invitation.objects.filter(role__person_id=person.id).first()
    InvitationLink.objects.create(
        invitation=invitation, expire=timezone.now() + datetime.timedelta(days=1)
    )
    InvitationLink.objects.create(
        invitation=invitation, expire=timezone.now() + datetime.timedelta(days=1)
    )

    invitation_links_for_person = InvitationLink.objects.filter(
        invitation__role__person_id=person.id
    )
    assert invitation_links_for_person.count() == 3
    url = reverse("gregui-v1:invite-resend", kwargs={"person_id": person.id})
    response = client.patch(url)
    assert response.status_code == status.HTTP_200_OK
    assert send_invite_mock_function.call_count == 3


@pytest.mark.django_db
def test_invite_resend_person_multiple_expired_links_send_all(
    client, log_in, user_sponsor, invited_person, registration_template, mocker
):
    """
    Resending invites for a person with multiple invitations where all links are
    expired should resend all of them.
    """
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")

    log_in(user_sponsor)
    # Expire invitation link
    person, invitation = invited_person
    invitation = Invitation.objects.filter(role__person_id=person.id).first()
    link = InvitationLink.objects.get(invitation=invitation)
    link.expire = timezone.now() - datetime.timedelta(days=1)
    link.save()
    # Make another invitation with an expired link
    inv2 = Invitation.objects.create(role=invitation.role)
    InvitationLink.objects.create(
        invitation=inv2, expire=timezone.now() - datetime.timedelta(days=1)
    )
    # Resend request should trigger creation of new links for both invitations
    url = reverse("gregui-v1:invite-resend", kwargs={"person_id": person.id})
    response = client.patch(url)
    assert response.status_code == status.HTTP_200_OK
    assert send_invite_mock_function.call_count == 2


@pytest.mark.django_db
def test_invite_recreate__registered_guest(
    client,
    log_in,
    user_sponsor,
    person_registered: Person,
    mocker,
):
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")
    log_in(user_sponsor)

    assert person_registered.is_registered is True
    assert person_registered.is_verified is False
    assert person_registered.private_mobile is not None
    invitation_links = InvitationLink.objects.filter(
        invitation__role__person_id=person_registered.id
    )
    assert invitation_links.count() == 0

    identity_id = person_registered.feide_id.id
    url = reverse("gregui-v1:invite-recreate", kwargs={"identity_id": identity_id})
    response = client.patch(url)
    person_registered = Person.objects.get(id=person_registered.id)

    assert person_registered.is_registered is False
    assert person_registered.is_verified is False
    assert person_registered.private_mobile is None
    invitation_links = InvitationLink.objects.filter(
        invitation__role__person_id=person_registered.id
    )
    # person_registered had two invites from this sponsor, so two links should have been created
    assert invitation_links.count() == 2
    # person_registered had two invites from this sponsor, so two emails should have been sent
    assert send_invite_mock_function.call_count == 2
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_invite_recreate__verified_guest(
    client,
    log_in,
    user_sponsor,
    person_verified: Person,
    mocker,
):
    send_invite_mock_function = mocker.patch.object(InviteGuest, "queue_mail")
    log_in(user_sponsor)

    assert person_verified.is_registered is True
    assert person_verified.is_verified is True
    assert person_verified.private_mobile is not None
    invitation_links = InvitationLink.objects.filter(
        invitation__role__person_id=person_verified.id
    )
    assert invitation_links.count() == 0

    identity_id = person_verified.feide_id.id
    url = reverse("gregui-v1:invite-recreate", kwargs={"identity_id": identity_id})
    response = client.patch(url)
    person_verified = Person.objects.get(id=person_verified.id)

    assert person_verified.is_registered is False
    assert person_verified.is_verified is False
    assert person_verified.private_mobile is None
    invitation_links = InvitationLink.objects.filter(
        invitation__role__person_id=person_verified.id
    )
    # person_verified had two invites from this sponsor, so two links should have been created
    assert invitation_links.count() == 2
    # person_verified had two invites from this sponsor, so two emails should have been sent
    assert send_invite_mock_function.call_count == 2
    assert response.status_code == status.HTTP_200_OK
