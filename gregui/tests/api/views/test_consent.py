import pytest

from rest_framework import status
from rest_framework.reverse import reverse


@pytest.mark.django_db
def test_get_consents(client, consent_type_foo, consent_type_bar):
    response = client.get(reverse("gregui-v1:consent-types"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == [
        {
            "choices": [
                {"text_en": "Yes", "text_nb": "Ja", "text_nn": "Ja", "value": "yes"},
                {"text_en": "No", "text_nb": "Nei", "text_nn": "Nei", "value": "no"},
            ],
            "description_en": "Description",
            "description_nb": "Beskrivelse",
            "description_nn": "Beskriving",
            "id": 1,
            "identifier": "foo",
            "mandatory": False,
            "name_en": "Foo",
            "name_nb": "Fu",
            "name_nn": "F",
            "user_allowed_to_change": True,
            "valid_from": "2018-01-20",
            "created": consent_type_foo.created.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "updated": consent_type_foo.updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        },
        {
            "choices": [
                {"text_en": "Yes", "text_nb": "Ja", "text_nn": "Ja", "value": "yes"}
            ],
            "description_en": "Description",
            "description_nb": "Beskrivelse",
            "description_nn": "Beskriving",
            "id": 2,
            "identifier": "bar",
            "mandatory": True,
            "name_en": "Bar",
            "name_nb": "Ba",
            "name_nn": "B",
            "user_allowed_to_change": True,
            "valid_from": "2018-01-20",
            "created": consent_type_bar.created.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "updated": consent_type_bar.updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        },
    ]
