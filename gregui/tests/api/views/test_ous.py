import pytest
from rest_framework.reverse import reverse

from greg.models import OuIdentifier
from gregsite.settings.base import ORGREG_NAME


@pytest.mark.django_db
def test_orgreg_field_populated_if_identifier_present(
    unit_foo, client, log_in, user_sponsor, sponsor_foo
):
    orgreg_id = "200"
    OuIdentifier.objects.create(
        source="orgreg", name=ORGREG_NAME, value=orgreg_id, orgunit=unit_foo
    )
    log_in(user_sponsor)

    resp = client.get(reverse("gregui-v1:ou-list"))
    data = resp.json()

    assert len(data) == 1
    assert data[0]["orgreg_id"] == orgreg_id


@pytest.mark.django_db
def test_orgreg_field_empty_if_identifier_not_present(
    unit_foo, client, log_in, user_sponsor, sponsor_foo
):
    log_in(user_sponsor)

    resp = client.get(reverse("gregui-v1:ou-list"))
    data = resp.json()

    assert len(data) == 1
    assert data[0]["orgreg_id"] is None


@pytest.mark.django_db
def test_acronym_nob_populated_if_identifier_present(
    unit_foo, client, log_in, user_sponsor, sponsor_foo
):
    acronym_nob = "test-unit-01"
    OuIdentifier.objects.create(
        source="orgreg", name="acronym_nob", value=acronym_nob, orgunit=unit_foo
    )
    log_in(user_sponsor)

    resp = client.get(reverse("gregui-v1:ou-list"))
    data = resp.json()

    assert len(data) == 1
    assert data[0]["acronym_nob"] == acronym_nob


@pytest.mark.django_db
def test_acronym_nob_field_empty_if_identifier_not_present(
    unit_foo, client, log_in, user_sponsor, sponsor_foo
):
    log_in(user_sponsor)

    resp = client.get(reverse("gregui-v1:ou-list"))
    data = resp.json()
    assert len(data) == 1
    assert data[0]["acronym_nob"] is None


@pytest.mark.django_db
def test_org_str3(unit_foo3):
    assert str(unit_foo3) == " (foo_unit3) (FOO) (123456)"


@pytest.mark.django_db
def test_unit_fields_populated_if_identifier_present(
    unit_foo3, client, log_in, user_sponsor_foobar
):
    log_in(user_sponsor_foobar)

    resp = client.get(reverse("gregui-v1:ou-list"))
    data = resp.json()
    orgunit = data[0]
    assert orgunit["identifier_1"] == "FOO"  # shortname
    assert orgunit["identifier_2"] == "123456"
    assert orgunit["orgreg_id"] == "1"
