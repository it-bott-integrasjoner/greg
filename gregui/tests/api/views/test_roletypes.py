import pytest

from rest_framework.reverse import reverse
from rest_framework import status

from greg.models import RoleType


@pytest.mark.django_db
def test_get_roletypes(
    client,
    role_type_1,
    role_type_2,
    role_type_3,
    role_type_4,
    role_type_5,
):
    response = client.get(reverse("gregui-v1:role-types"), data={})
    response_body = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert len(response_body) == 5

    RoleType.objects.filter(id=role_type_1.id).update(visible=False)
    RoleType.objects.filter(id=role_type_3.id).update(visible=False)
    response = client.get(reverse("gregui-v1:role-types"), data={})
    response_body = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert len(response_body) == 3
