import datetime
import pytest
from django.utils import timezone
from rest_framework.reverse import reverse
from rest_framework import status

from greg.models import OrganizationalUnit


@pytest.mark.django_db
def test_role_anon_post(client):
    """Should get 403 forbidden since we are not a sponsor"""
    resp = client.post(reverse("gregui-v1:role-list"), data={})
    assert resp.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_role_anon_patch(client):
    """Should get 403 forbidden since we are not a sponsor"""
    resp = client.patch(reverse("gregui-v1:role-detail", kwargs={"pk": 1}), data={})
    assert resp.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_role_sponsor_post_ok(client, log_in, user_sponsor, role):
    """Should work since we are a sponsor at this unit"""
    log_in(user_sponsor)
    resp = client.post(
        reverse("gregui-v1:role-list"),
        data={
            "person": role.person.id,
            "type": role.type.id,
            "orgunit": role.orgunit.id,
            "start_date": "",
            "end_date": (timezone.now() + datetime.timedelta(days=10)).strftime(
                "%Y-%m-%d"
            ),
        },
    )
    assert resp.content == b""
    assert resp.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_role_sponsor_post_no_data_fail(client, log_in, user_sponsor):
    """Should fail since we did not provide any role data"""
    log_in(user_sponsor)
    resp = client.post(reverse("gregui-v1:role-list"), data={})
    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_role_sponsor_post_fail(client, log_in, user_sponsor, role):
    """Should fail since we are not a sponsor at this unit."""
    # Unit the sponsor is not connected to so that we fail
    ou = OrganizationalUnit.objects.create(name_nb="foo", name_en="foo_en")

    log_in(user_sponsor)
    resp = client.post(
        reverse("gregui-v1:role-list"),
        data={
            "person": role.person.id,
            "type": role.type.id,
            "orgunit": ou.id,
            "start_date": "",
            "end_date": (timezone.now() + datetime.timedelta(days=10)).strftime(
                "%Y-%m-%d"
            ),
        },
    )
    assert (
        resp.content
        == b'{"orgunit":["You can only edit roles connected to your units."]}'
    )
    assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_role_sponsor_patch_ok(
    client,
    log_in,
    user_sponsor,
    role,
):
    """Should work since we are the sponsor owning the role."""
    log_in(user_sponsor)
    new_date = (timezone.now() + datetime.timedelta(days=10)).date()

    resp = client.patch(
        reverse("gregui-v1:role-detail", kwargs={"pk": role.id}),
        data={
            "end_date": new_date.strftime("%Y-%m-%d"),
        },
    )
    assert resp.status_code == status.HTTP_200_OK
    # Verify the new date was set
    role.refresh_from_db()
    assert role.end_date == new_date


@pytest.mark.django_db
def test_role_sponsor_patch_fail(client, log_in, user_sponsor_bar, role):
    """Should fail since we are not a sponsor at this unit."""

    # Unit the sponsor is not connected to so that we fail
    OrganizationalUnit.objects.create(name_nb="foo", name_en="foo_en")

    log_in(user_sponsor_bar)
    new_date = (timezone.now() + datetime.timedelta(days=10)).date()
    resp = client.patch(
        reverse("gregui-v1:role-detail", kwargs={"pk": role.id}),
        data={
            "end_date": new_date.strftime("%Y-%m-%d"),
        },
    )
    assert resp.status_code == status.HTTP_400_BAD_REQUEST
    assert (
        resp.content
        == b'{"non_field_errors":["You can only edit roles connected to your units."]}'
    )


@pytest.mark.django_db
def test_role_sponsor_patch_fail_unknown_role(
    client,
    log_in,
    user_sponsor,
):
    """Should fail since the role does not exist"""
    log_in(user_sponsor)
    new_date = (timezone.now() + datetime.timedelta(days=10)).date()

    resp = client.patch(
        reverse("gregui-v1:role-detail", kwargs={"pk": 1}),
        data={
            "end_date": new_date.strftime("%Y-%m-%d"),
        },
    )
    assert resp.content == b""
    assert resp.status_code == status.HTTP_400_BAD_REQUEST
