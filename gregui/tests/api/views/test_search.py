import pytest
from rest_framework import status
from rest_framework.reverse import reverse


@pytest.mark.django_db
def test_no_search_parameter_fails(client, log_in, user_sponsor, create_person):
    create_person(
        first_name="foo",
        last_name="bar",
        email="foo@bar.com",
    )

    url = reverse("gregui-v1:person-search")

    log_in(user_sponsor)
    response = client.get(url)

    # No q-parameter was specified, so the request should return a 400 response
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_full_name_search(client, log_in, user_sponsor, create_person):
    person1 = create_person(
        first_name="Fjong",
        last_name="Larsen",
        email="fjong@post.no",
        phone_number="+4712345678",
    )
    person2 = create_person(
        first_name="Stiv",
        last_name="Hansen",
        email="hansen@post.no",
        phone_number="+4787654321",
    )
    person3 = create_person(
        first_name="Larsen",
        last_name="Fjong",
        email="pjofsen@post.no",
        phone_number="+4711111111",
    )

    url = reverse("gregui-v1:person-search") + "?q=fjong%20larsen"

    log_in(user_sponsor)
    response = client.get(url)

    person_ids = list(map(lambda x: x["pid"], response.data))

    assert person1.id in person_ids
    assert person2.id not in person_ids
    assert person3.id in person_ids


@pytest.mark.django_db
def test_date_of_birth_search(client, log_in, user_sponsor, create_person):
    person = create_person(
        first_name="foo",
        last_name="bar",
        email="foo@bar.com",
        date_of_birth="2005-06-20",
    )
    person2 = create_person(
        first_name="test",
        last_name="tester",
        email="test@example.org",
        date_of_birth="2005-10-20",
    )
    # A person with no date of birth set
    person3 = create_person(
        first_name="test2",
        last_name="tester2",
        email="test2@example.org",
    )

    url = reverse("gregui-v1:person-search") + "?q=2005-06-20"

    log_in(user_sponsor)
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1

    person_ids = list(map(lambda x: x["pid"], response.data))

    assert person.id in person_ids
    assert person2.id not in person_ids
    assert person3.id not in person_ids


@pytest.mark.django_db
def test_phone_number_search(client, log_in, user_sponsor, create_person):
    person1 = create_person(
        first_name="Fjong",
        last_name="Larsen",
        email="fjong@post.no",
        phone_number="+4712345678",
    )
    person2 = create_person(
        first_name="Stiv",
        last_name="Hansen",
        email="hansen@post.no",
        phone_number="+4787654321",
    )
    person3 = create_person(
        first_name="Astrid",
        last_name="Pjofsen",
        email="pjofsen@post.no",
        phone_number="+4711111111",
    )

    url = reverse("gregui-v1:person-search") + "?q=12345678"

    log_in(user_sponsor)
    response = client.get(url)

    assert len(response.data) == 1

    result_id = map(lambda x: x["pid"], response.data)

    assert person1.id in result_id
    assert person2.id not in result_id
    assert person3.id not in result_id


@pytest.mark.django_db
def test_multiple_words_search(client, log_in, user_sponsor, create_person):
    create_person(
        first_name="foo",
        last_name="bar",
        email="example@company.com",
    )
    create_person(
        first_name="test",
        last_name="test2",
        email="foo@bar.com",
        date_of_birth="2006-06-20",
    )
    create_person(
        first_name="Bob",
        last_name="Smith",
        email="bob@smith.com",
        date_of_birth="2005-06-20",
    )
    create_person(
        first_name="Frank",
        last_name="Paulsen",
        email="frank@paulsen.com",
    )

    url = reverse("gregui-v1:person-search") + "?q=foo%20bar%202005-06-20"

    log_in(user_sponsor)
    response = client.get(url)

    assert len(response.data) == 0

    person_ids = list(map(lambda x: x["pid"], response.data))

    assert not person_ids


@pytest.mark.django_db
def test_no_match(client, log_in, user_sponsor, create_person):
    create_person(
        first_name="foo",
        last_name="bar",
        email="example@company.com",
    )
    create_person(
        first_name="test",
        last_name="test2",
        email="foo@bar.com",
        date_of_birth="2006-06-20",
    )
    create_person(
        first_name="Bob",
        last_name="Smith",
        email="bob@smith.com",
        date_of_birth="2005-06-20",
    )
    create_person(
        first_name="Frank",
        last_name="Paulsen",
        email="frank@paulsen.com",
    )

    url = reverse("gregui-v1:person-search") + "?q=æøå"

    log_in(user_sponsor)
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 0
