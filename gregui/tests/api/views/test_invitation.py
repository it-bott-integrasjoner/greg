import datetime
from django.test import override_settings
from django.utils import timezone
import pytest

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from greg.models import Consent, InvitationLink, Person, Identity


@pytest.mark.django_db
def test_post_invite(client):
    """Forbid access with bad invitation link uuid"""
    response = client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": "baduuid"}
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json() == {
        "code": "invalid_invite_token",
        "message": "Invite token is invalid",
    }


@pytest.mark.django_db
def test_post_invite_ok(client, invitation_link):
    """Access okay with valid invitation link"""
    response = client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_post_invite_expired(client, invitation_link_expired):
    """Forbid access with expired invite link"""
    response = client.post(
        reverse("gregui-v1:invite-verify"),
        data={"invite_token": invitation_link_expired.uuid},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json() == {
        "code": "expired_invite_token",
        "message": "Invite token has expired",
    }


@pytest.mark.django_db
def test_post_missing_invite_id(client):
    """Forbid access if no id provided."""
    response = client.post(reverse("gregui-v1:invite-verify"))
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "code": "missing_invite_token",
        "message": "An invite token is required",
    }


@pytest.mark.django_db
def test_get_invited_info_no_session(client, invitation_link):
    """Forbid access if invite expired after session was created."""
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    # expire the invitation link
    invitation_link.expire = timezone.now() - datetime.timedelta(days=1)
    invitation_link.save()
    # fail to get info because session has expired
    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_get_invited_info_session_okay(
    client, invited_person, sponsor_foo_data, role_type_foo, unit_foo
):
    person, invitation_link = invited_person
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    # Get info
    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert data.get("person") == {
        "first_name": person.first_name,
        "last_name": person.last_name,
        "private_email": person.private_email.value,
        "private_mobile": None,
        "fnr": None,
        "national_id_card_number": None,
        "passport": None,
        "feide_id": None,
        "date_of_birth": None,
        "gender": None,
    }
    assert data.get("sponsor") == {
        "first_name": sponsor_foo_data["first_name"],
        "last_name": sponsor_foo_data["last_name"],
    }
    assert data.get("role") == {
        "start": None,
        "end": "2050-10-15",
        "contact_person_unit": "",
        "ou_name_en": unit_foo.name_en,
        "ou_name_nb": unit_foo.name_nb,
        "role_name_en": role_type_foo.name_en,
        "role_name_nb": role_type_foo.name_nb,
    }


@pytest.mark.django_db
def test_get_invited_info_okay_user_connected_before(
    client,
    user_invited_person,
    invited_person,
    log_in,
    unit_foo,
    role_type_foo,
    sponsor_foo_data,
):
    """
    If a logged in user has a profile connected to a person, their
    invite is found and they are given access to continue registering.
    """
    log_in(user_invited_person)
    pe, _ = invited_person
    response = client.get(reverse("gregui-v1:invited-info"))

    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert data.get("person") == {
        "first_name": pe.first_name,
        "last_name": pe.last_name,
        "private_email": pe.private_email.value,
        "private_mobile": None,
        "fnr": None,
        "national_id_card_number": None,
        "passport": None,
        "feide_id": None,
        "date_of_birth": None,
        "gender": None,
    }
    assert data.get("sponsor") == {
        "first_name": sponsor_foo_data["first_name"],
        "last_name": sponsor_foo_data["last_name"],
    }
    assert data.get("role") == {
        "start": None,
        "end": "2050-10-15",
        "contact_person_unit": "",
        "ou_name_en": unit_foo.name_en,
        "ou_name_nb": unit_foo.name_nb,
        "role_name_en": role_type_foo.name_en,
        "role_name_nb": role_type_foo.name_nb,
    }


@pytest.mark.django_db
def test_get_invited_info_expired_link(
    client, invitation_link, invitation_expired_date
):
    # Get a session while link is valid
    client.get(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    # Set expire link to expire long ago
    invlink = InvitationLink.objects.get(uuid=invitation_link.uuid)
    invlink.expire = invitation_expired_date
    invlink.save()
    # Make a get request that should fail because invite expired after login, but
    # before get to userinfo
    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_get_invited_info_logged_in_user(client, log_in, user_no_profile):
    """
    Logged in users with no invite_id in session and no profile
    connected to user are forbidden.
    """
    log_in(user_no_profile)
    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.content == b'{"code":"invalid_invite","message":"Invalid invite"}'


@pytest.mark.django_db
def test_invited_guest_can_post_information(
    client: APIClient, invited_person, confirmation_template
):
    person, invitation_link = invited_person
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )

    person = invitation_link.invitation.role.person
    assert person.private_mobile is None

    # post updated info to confirm from guest
    new_email = "private@example.org"
    new_phone = "+4790000000"
    data = {"person": {"private_mobile": new_phone, "private_email": new_email}}

    response = client.post(
        reverse("gregui-v1:invited-info"),
        data,
        format="json",
    )
    assert response.status_code == status.HTTP_200_OK

    # Check that the object was updated in the database
    assert Person.objects.count() == 1
    assert person.private_mobile.value == new_phone
    assert person.private_email.value == new_email


@pytest.mark.django_db
def test_post_invited_info_expired_session(
    client, invitation_link, invitation_expired_date
):
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    # Set expire link to expire long ago
    invlink = InvitationLink.objects.get(uuid=invitation_link.uuid)
    invlink.expire = invitation_expired_date
    invlink.save()
    # post updated info to confirm from guest, should fail because of expired
    # invitation link
    response = client.post(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_post_invited_info_deleted_inv_link(client, invitation_link):
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    # Delete link
    invlink = InvitationLink.objects.get(uuid=invitation_link.uuid)
    invlink.delete()
    # post updated info to confirm from guest, should fail because of removed
    # invitation link
    response = client.post(reverse("gregui-v1:invited-info"))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_post_invited_info_invalid_national_id_number(client, invited_person):
    person, invitation_link = invited_person
    data = {
        "person": {
            "private_mobile": "+4707543001",
            "private_email": "test@example.com",
            "fnr": "23478912378",
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.fnr is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    # The request should fail and the fnr-property should stay unchanged
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    person.refresh_from_db()
    assert person.fnr is None


@pytest.mark.django_db
def test_post_invited_info_valid_national_id_number(
    client, invited_person, confirmation_template
):
    person, invitation_link = invited_person
    fnr = "11120618212"
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "fnr": fnr,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.fnr is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()
    assert person.fnr.value == fnr


@pytest.mark.django_db
def test_email_update(client, invited_person, confirmation_template):
    person, invitation_link = invited_person
    email_test = person.private_email.value
    url = reverse("gregui-v1:invited-info")

    person.private_email.refresh_from_db()
    assert person.private_email.value == email_test

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test2@example.com",
        }
    }
    response = client.post(url, data, format="json")
    assert response.status_code == status.HTTP_200_OK, response.data

    person.private_email.refresh_from_db()
    assert person.private_email.value == "test2@example.com"


@pytest.mark.django_db
def test_register_passport(client, invited_person, confirmation_template):
    person, invitation_link = invited_person
    passport_information = "EN-123456789"
    data = {
        "person": {"private_mobile": "+4797543992", "passport": passport_information}
    }
    url = reverse("gregui-v1:invited-info")

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    assert (
        Identity.objects.filter(
            person__id=person.id, type=Identity.IdentityType.PASSPORT_NUMBER
        ).count()
        == 0
    )

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()

    registered_passport = Identity.objects.filter(
        person__id=person.id, type=Identity.IdentityType.PASSPORT_NUMBER
    ).get()

    assert registered_passport.value == passport_information


@pytest.mark.django_db
def test_name_update_not_allowed_if_feide_identity_is_present(
    client: APIClient,
    person_foo,
    sponsor_foo,
    unit_foo,
    role_type_foo,
    create_role,
    create_invitation,
    create_invitation_link,
    invitation_valid_date,
):
    # This person has a Feide ID, so the name is assumed to come from there as well
    # and the guest should not be allowed to update it
    role = create_role(
        person=person_foo, sponsor=sponsor_foo, unit=unit_foo, role_type=role_type_foo
    )
    invitation = create_invitation(role)
    invitation_link = create_invitation_link(invitation, invitation_valid_date)
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )

    person = invitation_link.invitation.role.person
    data = {
        "person": {
            "first_name": "Someone",
            "last_name": "Test",
            "private_mobile": "+4797543992",
        }
    }

    # The update request should fail because it contains a name update
    response = client.post(
        reverse("gregui-v1:invited-info"),
        data,
        format="json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    # Check that the name did not change in the database
    assert Person.objects.count() == 1
    assert person.first_name == person_foo.first_name
    assert person.last_name == person_foo.last_name


@pytest.mark.django_db
def test_name_update_allowed_if_feide_identity_is_not_present(
    client: APIClient,
    create_person,
    sponsor_foo,
    unit_foo,
    role_type_foo,
    create_role,
    create_invitation,
    create_invitation_link,
    invitation_valid_date,
    confirmation_template,
):
    # This person does not have a Feide ID, so he will be viewed as someone whose name
    # was entered manually by the sponsor, and in it that case the guest can update it
    person = create_person(first_name="Foo", last_name="Bar", email="foo@bar.com")
    role = create_role(
        person=person, sponsor=sponsor_foo, unit=unit_foo, role_type=role_type_foo
    )
    invitation_link = create_invitation_link(
        create_invitation(role), invitation_valid_date
    )
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )

    person = invitation_link.invitation.role.person
    data = {
        "person": {
            "first_name": "Someone",
            "last_name": "Test",
            "private_mobile": "+4797543992",
        }
    }

    response = client.post(
        reverse("gregui-v1:invited-info"),
        data,
        format="json",
    )
    assert response.status_code == status.HTTP_200_OK, response.data

    # Check that the name has been updated in the database
    assert Person.objects.count() == 1
    person.refresh_from_db()
    assert person.first_name == "Someone"
    assert person.last_name == "Test"


@pytest.mark.django_db
def test_post_info_fail_fnr_already_verified(client, invited_person_verified_nin):
    """Ensure that an automatically verified fnr cannot be changed"""
    person, invitation_link = invited_person_verified_nin
    fnr = "10093720895"

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    url = reverse("gregui-v1:invited-info")
    data = {"person": {"private_mobile": "+4797543992", "fnr": fnr}}
    response = client.post(url, data, format="json")

    # Verify rejection
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    # Verify fnr was not changed
    person.refresh_from_db()
    assert person.fnr.value != fnr


@pytest.mark.django_db
def test_post_info_fail_unknown_field(client, invited_person_verified_nin):
    """Ensure that including an unknown field gives bad request"""
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    url = reverse("gregui-v1:invited-info")
    data = {"person": {"private_mobile": "+4797543992", "badfield": "foo"}}
    response = client.post(url, data, format="json")

    # Verify rejection
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_date_of_birth_stored(
    client, invited_person_verified_nin, confirmation_template
):
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    date_of_birth = "1995-10-28"
    url = reverse("gregui-v1:invited-info")
    data = {"person": {"private_mobile": "+4797543992", "date_of_birth": date_of_birth}}
    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK

    person = Person.objects.get()
    assert (
        person.date_of_birth
        == datetime.datetime.strptime(date_of_birth, "%Y-%m-%d").date()
    )


@pytest.mark.django_db
def test_invalid_date_of_birth_rejected(client, invited_person_verified_nin):
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    date_of_birth = "1900-03-16"
    url = reverse("gregui-v1:invited-info")
    data = {"person": {"private_mobile": "+4797543992", "date_of_birth": date_of_birth}}
    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    person = Person.objects.get()
    assert person.date_of_birth is None


@pytest.mark.django_db
def test_saves_consents(
    client, invited_person, consent_type_foo, consent_type_bar, confirmation_template
):
    person, invitation_link = invited_person
    fnr = "11120618212"

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    consents = [
        {"type": consent_type_foo.identifier, "choice": "no"},
        {"type": consent_type_bar.identifier, "choice": "yes"},
    ]

    data = {
        "person": {"private_mobile": "+4797543992", "fnr": fnr, "consents": consents}
    }
    url = reverse("gregui-v1:invited-info")
    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()

    assert Consent.objects.filter(
        person=person,
        type=consent_type_foo,
        choice=consent_type_foo.choices.filter(value="no").first(),
    ).exists()

    assert Consent.objects.filter(
        person=person,
        type=consent_type_bar,
        choice=consent_type_bar.choices.filter(value="yes").first(),
    ).exists()


@pytest.mark.django_db
def test_post_invited_info_valid_dnumber(client, invited_person, confirmation_template):
    person, invitation_link = invited_person
    d_number = "53097248016"
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "fnr": d_number,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.fnr is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()
    assert person.fnr.value == d_number


@pytest.mark.django_db
def test_gender_stored(client, invited_person_verified_nin, confirmation_template):
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    date_of_birth = "1995-10-28"
    url = reverse("gregui-v1:invited-info")
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "date_of_birth": date_of_birth,
            "gender": "male",
        }
    }
    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK

    person = Person.objects.get()
    assert person.gender == "male"


@pytest.mark.django_db
def test_gender_blank_allowed(
    client, invited_person_verified_nin, confirmation_template
):
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    date_of_birth = "1995-10-28"
    url = reverse("gregui-v1:invited-info")
    data = {"person": {"private_mobile": "+4797543992", "date_of_birth": date_of_birth}}
    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK

    person = Person.objects.get()
    assert person.gender is None


@pytest.mark.django_db
def test_invalid_gender_rejected(client, invited_person_verified_nin):
    _, invitation_link = invited_person_verified_nin

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    date_of_birth = "1995-10-28"
    url = reverse("gregui-v1:invited-info")
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "date_of_birth": date_of_birth,
            "gender": "abcdefg",
        }
    }
    response = client.post(url, data, format="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    person = Person.objects.get()
    assert person.gender is None


@pytest.mark.django_db
def test_session_type_feide_id(
    client, invited_person_feide_id_set, log_in, user_no_profile
):
    _, invitation_link = invited_person_feide_id_set
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    log_in(user_no_profile)

    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.json().get("meta").get("session_type") == "feide"


@pytest.mark.django_db
def test_session_type_feide_id_name_update_fails(
    client, invited_person_feide_id_set, log_in, user_no_profile
):
    person, invitation_link = invited_person_feide_id_set
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    log_in(user_no_profile)

    # Try to update the name, this should fail. Also need to specify a mobile phone in the input,
    # since this is a required field the user has to fill in
    data = {
        "person": {
            "first_name": "Updated",
            "last_name": "Updated2",
            "private_mobile": "+4797543992",
        }
    }
    url = reverse("gregui-v1:invited-info")

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    person.refresh_from_db()

    # The name should not have changed
    assert person.first_name == "Victor"
    assert person.last_name == "Verified"


@pytest.mark.django_db
def test_session_type_id_porten(
    client,
    invited_person_id_porten_nin_set,
    log_in,
    user_no_profile,
    confirmation_template,
):
    person, invitation_link = invited_person_id_porten_nin_set
    # get a session
    client.post(
        reverse("gregui-v1:invite-verify"), data={"invite_token": invitation_link.uuid}
    )
    log_in(user_no_profile)

    response = client.get(reverse("gregui-v1:invited-info"))
    assert response.json().get("meta").get("session_type") == "idporten"

    # Try to update the name, this is allowed since the name is not coming from ID-porten and
    # can be updated by the user
    data = {
        "person": {
            "first_name": "Updated",
            "last_name": "UpdatedTwo",
            "private_mobile": "+4797543992",
        }
    }
    url = reverse("gregui-v1:invited-info")

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()

    assert person.first_name == "Updated"
    assert person.last_name == "UpdatedTwo"


@pytest.mark.django_db
def test_duplicate_national_id(
    client, invited_person, confirmation_template, person_foo
):
    person, invitation_link = invited_person
    fnr = "11120618212"

    Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_NUMBER,
        value=fnr,
    )

    # Try to respond to an invite with a Norwegian national ID number that already exists
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "fnr": fnr,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.fnr is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.data
    response_data = response.json()
    assert response_data["code"] == "duplicate_identity_info"

    person.refresh_from_db()
    # The Norwegian national ID number should still be not set since the post request failed
    assert person.fnr is None


@override_settings(ENABLE_NATIONAL_ID_CARD_NUMBER=True)
@pytest.mark.django_db
def test_post_invited_info_national_id_card_number(
    client, invited_person, confirmation_template
):
    person, invitation_link = invited_person
    national_id_card_number = "123"
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "national_id_card_number": national_id_card_number,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.national_id_card_number is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()
    assert person.national_id_card_number.value == national_id_card_number


@override_settings(ENABLE_NATIONAL_ID_CARD_NUMBER=False)
@pytest.mark.django_db
def test_post_invited_info_national_id_card_number_is_not_created_when_turned_off(
    client, invited_person, confirmation_template
):
    person, invitation_link = invited_person
    national_id_card_number = "123"
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "national_id_card_number": national_id_card_number,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.national_id_card_number is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_200_OK, response.data
    person.refresh_from_db()
    # The national ID card number should not be set since the feature is turned off
    assert person.national_id_card_number is None


@override_settings(ENABLE_NATIONAL_ID_CARD_NUMBER=True)
@pytest.mark.django_db
def test_duplicate_national_id_card_number(
    client, invited_person, confirmation_template, person_foo
):
    person, invitation_link = invited_person
    national_id_card_number = "123"

    Identity.objects.create(
        person=person_foo,
        type=Identity.IdentityType.NORWEGIAN_NATIONAL_ID_CARD_NUMBER,
        value=national_id_card_number,
    )

    # Try to respond to an invite with a Norwegian national ID card number that already exists
    data = {
        "person": {
            "private_mobile": "+4797543992",
            "private_email": "test@example.com",
            "national_id_card_number": national_id_card_number,
        }
    }
    url = reverse("gregui-v1:invited-info")

    assert person.national_id_card_number is None

    session = client.session
    session["invite_id"] = str(invitation_link.uuid)
    session.save()

    response = client.post(url, data, format="json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.data
    response_data = response.json()
    assert response_data["code"] == "duplicate_private_national_id_card_number"

    person.refresh_from_db()
    # The Norwegian national ID card number should still be not set since the post request failed
    assert person.national_id_card_number is None
