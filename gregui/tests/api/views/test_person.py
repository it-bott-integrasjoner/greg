import pytest
from rest_framework import status
from rest_framework.reverse import reverse


@pytest.mark.django_db
def test_get_person_fail(client):
    """Anonymous user cannot get person info"""
    url = reverse("gregui-v1:person-detail", kwargs={"pk": 1})
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_get_person(client, log_in, user_sponsor, invited_person):
    """Logged in sponsor can get person info"""
    person, _ = invited_person
    url = reverse("gregui-v1:person-detail", kwargs={"pk": person.id})
    log_in(user_sponsor)
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_patch_person_no_data_fail(client, log_in, user_sponsor, invited_person):
    """No data in patch should fail"""
    person, _ = invited_person
    url = reverse("gregui-v1:person-detail", kwargs={"pk": person.id})
    log_in(user_sponsor)
    response = client.patch(url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        "code": "no_data_received",
        "message": "Received no data",
    }


@pytest.mark.django_db
def test_patch_person_new_email_ok(client, log_in, user_sponsor, invited_person):
    """Logged in sponsor can update email address of person"""
    person, _ = invited_person
    url = reverse("gregui-v1:person-detail", kwargs={"pk": person.id})
    log_in(user_sponsor)
    assert person.private_email.value == "foo@example.org"
    response = client.patch(url, data={"email": "new@example.com"})
    assert response.status_code == status.HTTP_200_OK
    person.refresh_from_db()
    assert person.private_email.value == "new@example.com"


@pytest.mark.django_db
def test_patch_person_new_mobile_ok(client, log_in, user_sponsor, invited_person2):
    """Logged in sponsor can update phone number of person"""
    person, _ = invited_person2
    url = reverse("gregui-v1:person-detail", kwargs={"pk": person.id})
    log_in(user_sponsor)
    assert person.private_mobile.value == "+4741000000"
    response = client.patch(url, data={"mobile": "+4741111111"})
    assert response.status_code == status.HTTP_200_OK
    person.refresh_from_db()
    assert person.private_mobile.value == "+4741111111"


@pytest.mark.django_db
def test_patch_person_new_email_and_new_mobile_ok(
    client, log_in, user_sponsor, invited_person2
):
    """Logged in sponsor can update phone number of person"""
    person, _ = invited_person2
    url = reverse("gregui-v1:person-detail", kwargs={"pk": person.id})
    log_in(user_sponsor)
    assert person.private_email.value == "foohoo@example.org"
    assert person.private_mobile.value == "+4741000000"
    response = client.patch(
        url, data={"email": "new@example.com", "mobile": "+4741111111"}
    )
    assert response.status_code == status.HTTP_200_OK
    person.refresh_from_db()
    assert person.private_email.value == "new@example.com"
    assert person.private_mobile.value == "+4741111111"


@pytest.mark.django_db
def test_person_update_ok(client, log_in, invited_person2, user_invited_person2):
    """Logged in guest can update their phone number"""
    log_in(user_invited_person2)
    person, _ = invited_person2
    url = reverse("gregui-v1:profile-update")
    assert person.private_mobile.value == "+4741000000"
    response = client.patch(url, data={"mobile": "+4741111111"})
    assert response.status_code == status.HTTP_200_OK
    person.refresh_from_db()
    assert person.private_mobile.value == "+4741111111"
