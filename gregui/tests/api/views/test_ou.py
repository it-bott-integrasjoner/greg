import pytest
from django.urls import reverse
from rest_framework import status

from greg.models import OrganizationalUnit, SponsorOrganizationalUnit


@pytest.mark.django_db
def test_hierarchy_access_sponsor_ous(client, user_sponsor, unit_bar, log_in):
    """
    Verify that setting hierarchical access makes sub units visible for
    sponsors.
    """
    url = reverse("gregui-v1:ou-list")
    log_in(user_sponsor)

    # add extra unit not visible without hierarchical access
    unit_bar.parent = OrganizationalUnit.objects.get(pk=1)
    unit_bar.save()

    # Only direct connection visible
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1

    # Tree visible with hierarchy access
    sou = SponsorOrganizationalUnit.objects.get(pk=1)
    sou.hierarchical_access = True
    sou.save()
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 2
