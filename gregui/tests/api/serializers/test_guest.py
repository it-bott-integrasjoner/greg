from datetime import timedelta

import pytest
from django.conf import settings
from django.utils import timezone

from gregui.api.serializers.guest import GuestSerializer


@pytest.mark.django_db
def test_serialize_guest(invited_person):
    person, _ = invited_person
    assert GuestSerializer().to_representation(person) == {
        "active": False,
        "email": "foo@example.org",
        "feide_id": None,
        "first": "Foo",
        "fnr": None,
        "national_id_card_number": None,
        "invitation_status": "active",
        "last": "Baمr",
        "mobile": None,
        "passport": None,
        "pid": "1",
        "registered": False,
        "roles": [
            {
                "id": 1,
                "name_nb": "Role Foo NB",
                "name_en": "Role Foo EN",
                "ou_nb": "Foo NB",
                "ou_en": "Foo EN",
                "start_date": None,
                "end_date": "2050-10-15",
                "max_days": 365,
                "contact_person_unit": "",
                "comments": "",
                "sponsor_name": "Sponsor Bar",
                "sponsor_feide_id": "foo@example.org",
                "ou_id": 1,
            },
        ],
        "verified": False,
    }


@pytest.mark.django_db
def test_invitation_status(invited_person):
    s = GuestSerializer()
    person, invitation = invited_person

    # there's an active invitation link
    assert s.to_representation(person).get("invitation_status") == "active"

    # the invitation link has expired
    invitation.expire = timezone.now() - timedelta(days=settings.INVITATION_DURATION)
    invitation.save()
    assert s.to_representation(person).get("invitation_status") == "expired"

    # there are no invitation links
    invitation.delete()
    assert s.to_representation(person).get("invitation_status") == "none"
