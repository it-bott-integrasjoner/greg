Dette er en automatisk generert melding fra gjesteregistreringstjenesten.
Din registrering ved {{ institution }} har blitt avslått av {{ sponsor }} grunnet ugyldig identifisering.
For å rette opp i dette, vennligst følg denne lenken: {{ registration_link }}

This message has been automatically generated by the guest registration system.
Your registration at {{ institution }} has been rejected by {{ sponsor }} due to invalid identification.
To amend this, please follow this link: {{ registration_link }}
