from typing import List

from django.urls import include
from django.urls import path
from django.urls.resolvers import URLResolver

from gregui.api import urls as api_urls

urlpatterns: List[URLResolver] = [
    path(
        "api/ui/v1/", include((api_urls.urlpatterns, "gregui"), namespace="gregui-v1")
    ),
]
